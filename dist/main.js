"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const trading_app_module_1 = require("./trading-app.module");
const config_1 = require("./config");
const types_1 = require("./config/types");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const exceptions_1 = require("./utils/exceptions");
const path_1 = require("path");
const background_app_module_1 = require("./background-app.module");
const event_1 = require("./backgroud-jobs/event");
async function bootstrap() {
    const tradingAppModule = await core_1.NestFactory.create(trading_app_module_1.TradingAppModule);
    tradingAppModule.enableCors();
    tradingAppModule.useStaticAssets((0, path_1.join)(__dirname, '..', 'public'));
    tradingAppModule.useGlobalPipes(new common_1.ValidationPipe({ whitelist: false, forbidNonWhitelisted: true }));
    tradingAppModule.useGlobalFilters(new exceptions_1.ExceptionsUtil());
    if (config_1.APP_CONFIG.ENVIRONMENT !== types_1.ENVIRONMENT.PRODUCTION) {
        const merchantSwaggerConfig = new swagger_1.DocumentBuilder()
            .setTitle('8X Trading Document')
            .setDescription('This is user roles API document')
            .setVersion('1.0')
            .addBearerAuth()
            .addServer(config_1.APP_CONFIG.BASE_URL)
            .build();
        const userRolesAppDocument = swagger_1.SwaggerModule.createDocument(tradingAppModule, merchantSwaggerConfig);
        swagger_1.SwaggerModule.setup('api/v1/swagger', tradingAppModule, userRolesAppDocument);
    }
    await tradingAppModule.listen(config_1.APP_CONFIG.PORT || 3000);
    const backgroudApp = await core_1.NestFactory.createApplicationContext(background_app_module_1.BackgroudAppModule);
    const eventJob = backgroudApp.get(event_1.EventJob);
    eventJob.run();
}
bootstrap();
//# sourceMappingURL=main.js.map