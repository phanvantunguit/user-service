import { RoleRepository } from 'src/infrastructure/data/database/role.repository';
import { MerchantListRoleOuput } from './validate';
export declare class MerchantListRoleHandler {
    private roleRepository;
    constructor(roleRepository: RoleRepository);
    execute(): Promise<MerchantListRoleOuput>;
}
