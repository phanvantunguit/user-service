"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantListRoleHandler = void 0;
const common_1 = require("@nestjs/common");
const role_repository_1 = require("../../../../infrastructure/data/database/role.repository");
let MerchantListRoleHandler = class MerchantListRoleHandler {
    constructor(roleRepository) {
        this.roleRepository = roleRepository;
    }
    async execute() {
        const roles = await this.roleRepository.list();
        return {
            payload: roles,
        };
    }
};
MerchantListRoleHandler = __decorate([
    __param(0, (0, common_1.Inject)(role_repository_1.RoleRepository)),
    __metadata("design:paramtypes", [role_repository_1.RoleRepository])
], MerchantListRoleHandler);
exports.MerchantListRoleHandler = MerchantListRoleHandler;
//# sourceMappingURL=index.js.map