export declare class AdminDeleteEventInput {
    id: string;
}
export declare class AdminDeleteEventOutput {
    payload: boolean;
}
