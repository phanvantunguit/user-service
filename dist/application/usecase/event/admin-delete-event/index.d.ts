import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminDeleteEventInput, AdminDeleteEventOutput } from './validate';
export declare class AdminDeleteEventHandler {
    private eventRepository;
    constructor(eventRepository: EventRepository);
    execute(param: AdminDeleteEventInput, userId?: string): Promise<AdminDeleteEventOutput>;
}
