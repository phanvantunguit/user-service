"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminInviteEventHandler = void 0;
const common_1 = require("@nestjs/common");
const user_event_domain_1 = require("../../../../domain/event/user-event.domain");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const mail_1 = require("../../../../infrastructure/services/mail");
const storage_1 = require("../../../../infrastructure/services/storage");
const qrcode_util_1 = require("../../../../utils/qrcode.util");
const date_fns_tz_1 = require("date-fns-tz");
const config_1 = require("../../../../config");
const types_1 = require("../../../../domain/event/types");
let AdminInviteEventHandler = class AdminInviteEventHandler {
    constructor(userEventRepository, eventRepository, mailService, storageService) {
        this.userEventRepository = userEventRepository;
        this.eventRepository = eventRepository;
        this.mailService = mailService;
        this.storageService = storageService;
    }
    async execute(param, admin_id) {
        const event = await this.eventRepository.findById(param.event_id);
        if (!event) {
            (0, throw_exception_1.notFoundError)('Event');
        }
        if (param.email) {
            const registered = await this.userEventRepository.findOne({
                event_id: event.id,
                email: param.email,
            });
            if (registered) {
                (0, throw_exception_1.badRequestError)('Email', const_1.ERROR_CODE.EXISTED);
            }
        }
        const count = await this.userEventRepository.count({ event_id: event.id });
        const userEvent = new user_event_domain_1.UserEventDomain();
        userEvent.fullname = param.fullname;
        userEvent.phone = param.phone;
        userEvent.email = param.email;
        userEvent.event_id = event.id;
        userEvent.confirm_status = param.confirm_status;
        userEvent.created_by = admin_id || null;
        userEvent.updated_by = admin_id || null;
        const lengthCount = event.attendees_number
            ? event.attendees_number.toString().length
            : 3;
        userEvent.setInviteCode(param.type, count + 1, lengthCount);
        userEvent.telegram = param.telegram;
        userEvent.payment_status = param.payment_status;
        const userEventSaved = await this.userEventRepository.save(userEvent);
        const attendUrl = param.callback_attend_url.includes('?')
            ? param.callback_attend_url + `&token=${userEventSaved.id}`
            : param.callback_attend_url + `?token=${userEventSaved.id}`;
        if (param.email) {
            let qrcode_url = '';
            let template_id = config_1.APP_CONFIG.INVITE_EVENT_SIMPLE_TEMPLATE_ID;
            if (event.create_qrcode) {
                template_id = config_1.APP_CONFIG.INVITE_EVENT_QRCODE_TEMPLATE_ID;
                try {
                    const qrcode = await (0, qrcode_util_1.generateQRCode)(attendUrl);
                    qrcode_url = await this.storageService.uploadBuffer(qrcode, `${userEventSaved.id}.png`);
                }
                catch (error) {
                    await this.userEventRepository.deleteById(userEventSaved.id);
                    (0, throw_exception_1.serverError)('QRCode', const_1.ERROR_CODE.UNPROCESSABLE);
                }
            }
            if (event.email_confirm) {
                template_id = event.email_confirm_template_id || template_id;
                const sendEmail = await this.mailService.sendEmailDynamicTemplate(userEvent.email, {
                    email: userEvent.email,
                    name: userEvent.fullname,
                    phone: userEvent.phone,
                    event_name: event.name,
                    event_code: event.code,
                    invite_code: userEvent.invite_code,
                    attendees_number: event.attendees_number,
                    start_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.start_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                    finish_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.finish_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                    qrcode_url: qrcode_url,
                    callback_confirm_url: param.callback_confirm_url,
                }, template_id);
                if (!sendEmail) {
                    await this.userEventRepository.deleteById(userEventSaved.id);
                    (0, throw_exception_1.serverError)('Email', const_1.ERROR_CODE.UNPROCESSABLE);
                }
            }
            if (param.payment_status === types_1.PAYMENT_STATUS.COMPLETED) {
                const template_id = config_1.APP_CONFIG.PAYMENT_EVENT_TEMPLATE_ID;
                await this.mailService.sendEmailDynamicTemplate(userEvent.email, {
                    email: userEvent.email,
                    name: userEvent.fullname,
                    phone: userEvent.phone,
                    event_name: event.name,
                    event_code: event.code,
                    invite_code: userEvent.invite_code,
                    attendees_number: event.attendees_number,
                    start_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.start_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                    finish_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.finish_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                }, template_id);
            }
        }
        return {
            payload: attendUrl,
        };
    }
};
AdminInviteEventHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __param(2, (0, common_1.Inject)(mail_1.MailService)),
    __param(3, (0, common_1.Inject)(storage_1.StorageService)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        event_repository_1.EventRepository,
        mail_1.MailService,
        storage_1.StorageService])
], AdminInviteEventHandler);
exports.AdminInviteEventHandler = AdminInviteEventHandler;
//# sourceMappingURL=index.js.map