import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { AdminInviteEventInput, AdminInviteEventOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { StorageService } from 'src/infrastructure/services/storage';
export declare class AdminInviteEventHandler {
    private userEventRepository;
    private eventRepository;
    private mailService;
    private storageService;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository, mailService: MailService, storageService: StorageService);
    execute(param: AdminInviteEventInput, admin_id: string): Promise<AdminInviteEventOutput>;
}
