"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminInviteEventOutput = exports.AdminInviteEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
class AdminInviteEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Fullname of user register',
        example: 'Nguyen Van A',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Telegram',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "telegram", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Url webstite result confirm',
        example: 'http://localhost:3000',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "callback_confirm_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Url webstite result scan qrcode',
        example: 'http://localhost:3000',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "callback_attend_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Type of ticket',
        example: types_1.INVITE_CODE_TYPE.VIP,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.INVITE_CODE_TYPE)),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Event id uuid',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "event_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status confirm of ticket',
        example: types_1.EVENT_CONFIRM_STATUS.APPROVED,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_CONFIRM_STATUS)),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "confirm_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status payment of event',
        example: types_1.PAYMENT_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.PAYMENT_STATUS)),
    __metadata("design:type", String)
], AdminInviteEventInput.prototype, "payment_status", void 0);
exports.AdminInviteEventInput = AdminInviteEventInput;
class AdminInviteEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Data to generate qrcode',
        example: 'https://event-dev.coinmap.tech?token=abc',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventOutput.prototype, "payload", void 0);
exports.AdminInviteEventOutput = AdminInviteEventOutput;
//# sourceMappingURL=validate.js.map