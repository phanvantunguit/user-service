import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE, PAYMENT_STATUS } from 'src/domain/event/types';
export declare class AdminInviteEventInput {
    fullname: string;
    email?: string;
    phone?: string;
    telegram?: string;
    callback_confirm_url: string;
    callback_attend_url: string;
    type: INVITE_CODE_TYPE;
    event_id: string;
    confirm_status: EVENT_CONFIRM_STATUS;
    payment_status: PAYMENT_STATUS;
}
export declare class AdminInviteEventOutput {
    payload: string;
}
