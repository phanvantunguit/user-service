"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserConfirmEventHandler = void 0;
const common_1 = require("@nestjs/common");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let UserConfirmEventHandler = class UserConfirmEventHandler {
    constructor(userEventRepository) {
        this.userEventRepository = userEventRepository;
    }
    async execute(param) {
        const userRegistered = await this.userEventRepository.findById(param.token);
        if (!userRegistered) {
            (0, throw_exception_1.badRequestError)('Registration', const_1.ERROR_CODE.NOT_FOUND);
        }
        await this.userEventRepository.save({
            id: param.token,
            confirm_status: param.status.toUpperCase(),
        });
        return {
            payload: true,
        };
    }
};
UserConfirmEventHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository])
], UserConfirmEventHandler);
exports.UserConfirmEventHandler = UserConfirmEventHandler;
//# sourceMappingURL=index.js.map