"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserConfirmEventOutput = exports.UserConfirmEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
class UserConfirmEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Id of registration',
        example: '2d676c14-f781-4668-aec0-692ffe4a0ae1',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserConfirmEventInput.prototype, "token", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: `Confirm status ${types_1.EVENT_CONFIRM_STATUS.APPROVED} or ${types_1.EVENT_CONFIRM_STATUS.REJECTED}`,
        example: types_1.EVENT_CONFIRM_STATUS.APPROVED,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserConfirmEventInput.prototype, "status", void 0);
exports.UserConfirmEventInput = UserConfirmEventInput;
class UserConfirmEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result of confirm',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserConfirmEventOutput.prototype, "payload", void 0);
exports.UserConfirmEventOutput = UserConfirmEventOutput;
//# sourceMappingURL=validate.js.map