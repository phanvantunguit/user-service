import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { UserConfirmEventInput, UserConfirmEventOutput } from './validate';
export declare class UserConfirmEventHandler {
    private userEventRepository;
    constructor(userEventRepository: UserEventRepository);
    execute(param: UserConfirmEventInput): Promise<UserConfirmEventOutput>;
}
