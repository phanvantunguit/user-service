import { EVENT_CONFIRM_STATUS } from 'src/domain/event/types';
export declare class UserConfirmEventInput {
    token: string;
    status: EVENT_CONFIRM_STATUS;
}
export declare class UserConfirmEventOutput {
    payload: boolean;
}
