import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserGetEventInput, UserGetEventOutput } from './validate';
export declare class UserGetEventHandler {
    private eventRepository;
    constructor(eventRepository: EventRepository);
    execute(param: UserGetEventInput): Promise<UserGetEventOutput>;
}
