import { EventDataValidate } from '../validate';
export declare class UserGetEventInput {
    code: string;
}
export declare class UserGetEventOutput {
    payload: EventDataValidate;
}
