import { EVENT_CONFIRM_STATUS, EVENT_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';
export declare class EventDataValidate {
    id: string;
    name: string;
    code?: string;
    status?: EVENT_STATUS;
    attendees_number?: number;
    email_remind_at?: number;
    email_remind_template_id?: string;
    email_confirm?: boolean;
    email_confirm_template_id?: string;
    start_at?: number;
    finish_at?: number;
    created_at: number;
    updated_at: number;
    deleted_at?: number;
    create_qrcode?: boolean;
}
export declare class UserDataValidate {
    id: string;
    confirm_status: EVENT_CONFIRM_STATUS;
    attend: boolean;
    remind: boolean;
    fullname: string;
    email: string;
    phone?: string;
    invite_code: string;
    payment_status: PAYMENT_STATUS;
    telegram?: string;
    updated_by?: string;
    created_by?: string;
    created_at: number;
    updated_at: number;
}
