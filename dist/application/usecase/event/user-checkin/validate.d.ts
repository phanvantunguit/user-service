export declare class UserCheckinEventInput {
    token: string;
}
export declare class UserCheckinEventDataOutput {
    fullname: string;
    email: string;
    phone: string;
    invite_code: string;
    checkin: boolean;
}
export declare class UserCheckinEventOutput {
    payload: UserCheckinEventDataOutput;
}
