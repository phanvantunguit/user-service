"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCheckinEventHandler = void 0;
const common_1 = require("@nestjs/common");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const time_util_1 = require("../../../../utils/time.util");
let UserCheckinEventHandler = class UserCheckinEventHandler {
    constructor(userEventRepository, eventRepository) {
        this.userEventRepository = userEventRepository;
        this.eventRepository = eventRepository;
    }
    async execute(param) {
        const userRegistered = await this.userEventRepository.findById(param.token);
        if (!userRegistered) {
            (0, throw_exception_1.badRequestError)('Registration', const_1.ERROR_CODE.NOT_FOUND);
        }
        const event = await this.eventRepository.findById(userRegistered.event_id);
        if (!event) {
            (0, throw_exception_1.badRequestError)('Event', const_1.ERROR_CODE.NOT_FOUND);
        }
        if (event.start_at) {
            const timeChange = event.start_at - Date.now();
            const allowCheckin = 3 * time_util_1.HOUR;
            if (timeChange > allowCheckin) {
                return {
                    payload: {
                        fullname: userRegistered.fullname,
                        email: userRegistered.email,
                        phone: userRegistered.phone,
                        invite_code: userRegistered.invite_code,
                        checkin: false,
                    },
                };
            }
        }
        await this.userEventRepository.save({
            id: param.token,
            attend: true,
        });
        return {
            payload: {
                fullname: userRegistered.fullname,
                email: userRegistered.email,
                phone: userRegistered.phone,
                invite_code: userRegistered.invite_code,
                checkin: true,
            },
        };
    }
};
UserCheckinEventHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        event_repository_1.EventRepository])
], UserCheckinEventHandler);
exports.UserCheckinEventHandler = UserCheckinEventHandler;
//# sourceMappingURL=index.js.map