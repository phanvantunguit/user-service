import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { UserCheckinEventInput, UserCheckinEventOutput } from './validate';
export declare class UserCheckinEventHandler {
    private userEventRepository;
    private eventRepository;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository);
    execute(param: UserCheckinEventInput): Promise<UserCheckinEventOutput>;
}
