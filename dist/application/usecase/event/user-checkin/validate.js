"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCheckinEventOutput = exports.UserCheckinEventDataOutput = exports.UserCheckinEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class UserCheckinEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Id of registration',
        example: '2d676c14-f781-4668-aec0-692ffe4a0ae1',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckinEventInput.prototype, "token", void 0);
exports.UserCheckinEventInput = UserCheckinEventInput;
class UserCheckinEventDataOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Fullname of user register',
        example: 'Nguyen Van A',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckinEventDataOutput.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], UserCheckinEventDataOutput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckinEventDataOutput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Code to attend to event',
        example: 'Guest0001',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckinEventDataOutput.prototype, "invite_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Checkin success or not',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserCheckinEventDataOutput.prototype, "checkin", void 0);
exports.UserCheckinEventDataOutput = UserCheckinEventDataOutput;
class UserCheckinEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'User information',
        example: { fullname: 'fullname', email: 'email', phone: 'phone' },
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", UserCheckinEventDataOutput)
], UserCheckinEventOutput.prototype, "payload", void 0);
exports.UserCheckinEventOutput = UserCheckinEventOutput;
//# sourceMappingURL=validate.js.map