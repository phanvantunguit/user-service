import { EVENT_STATUS } from 'src/domain/event/types';
import { EventDataValidate } from '../validate';
export declare class AdminListEventInput {
    page: number;
    size: number;
    keyword: string;
    status: EVENT_STATUS;
    deleted: number;
}
export declare class PagingEventDataOutput {
    rows: EventDataValidate[];
    page: number;
    size: number;
    count: number;
    total: number;
}
export declare class AdminListEventOutput {
    payload: PagingEventDataOutput;
}
