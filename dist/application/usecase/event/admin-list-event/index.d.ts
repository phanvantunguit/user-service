import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminListEventInput, AdminListEventOutput } from './validate';
export declare class AdminListEventHandler {
    private eventRepository;
    constructor(eventRepository: EventRepository);
    execute(param: AdminListEventInput): Promise<AdminListEventOutput>;
}
