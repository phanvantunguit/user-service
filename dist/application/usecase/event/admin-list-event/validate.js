"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminListEventOutput = exports.PagingEventDataOutput = exports.AdminListEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
const class_transformer_1 = require("class-transformer");
const validate_1 = require("../validate");
class AdminListEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], AdminListEventInput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], AdminListEventInput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'The keyword to find user ex: phone number, name, email',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminListEventInput.prototype, "keyword", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `Status of event ${Object.values(types_1.EVENT_STATUS).join(' or ')}`,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_STATUS)),
    __metadata("design:type", String)
], AdminListEventInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Includes event was deleted',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    (0, class_validator_1.IsIn)(['1', '0']),
    __metadata("design:type", Number)
], AdminListEventInput.prototype, "deleted", void 0);
exports.AdminListEventInput = AdminListEventInput;
class PagingEventDataOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'List of event',
        isArray: true,
        type: validate_1.EventDataValidate,
    }),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => validate_1.EventDataValidate),
    __metadata("design:type", Array)
], PagingEventDataOutput.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Current page',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingEventDataOutput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Maximun number of list',
        example: 50,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingEventDataOutput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Number of current list',
        example: 10,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingEventDataOutput.prototype, "count", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Number of query',
        example: 100,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingEventDataOutput.prototype, "total", void 0);
exports.PagingEventDataOutput = PagingEventDataOutput;
class AdminListEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result event paging',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", PagingEventDataOutput)
], AdminListEventOutput.prototype, "payload", void 0);
exports.AdminListEventOutput = AdminListEventOutput;
//# sourceMappingURL=validate.js.map