import { EventDataValidate, UserDataValidate } from '../validate';
export declare class RemindAttendEventInput {
    user: UserDataValidate;
    event: EventDataValidate;
}
export declare class RemindAttendEventOutput {
    payload: boolean;
}
