import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { RemindAttendEventInput, RemindAttendEventOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
export declare class RemindAttendEventHandler {
    private userEventRepository;
    private mailService;
    constructor(userEventRepository: UserEventRepository, mailService: MailService);
    execute(param: RemindAttendEventInput): Promise<RemindAttendEventOutput>;
}
