"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemindAttendEventHandler = void 0;
const common_1 = require("@nestjs/common");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const mail_1 = require("../../../../infrastructure/services/mail");
const config_1 = require("../../../../config");
const date_fns_tz_1 = require("date-fns-tz");
let RemindAttendEventHandler = class RemindAttendEventHandler {
    constructor(userEventRepository, mailService) {
        this.userEventRepository = userEventRepository;
        this.mailService = mailService;
    }
    async execute(param) {
        const { user, event } = param;
        if (!user.remind) {
            await this.userEventRepository.save({
                id: user.id,
                remind: true,
            });
            const sendEmail = await this.mailService.sendEmailDynamicTemplate(user.email, {
                email: user.email,
                name: user.fullname,
                phone: user.phone,
                event_name: event.name,
                event_code: event.code,
                invite_code: user.invite_code,
                attendees_number: event.attendees_number,
                start_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.start_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                finish_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.finish_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                countdown: '7 ngày',
            }, event.email_remind_template_id || config_1.APP_CONFIG.REMIND_EVENT_TEMPLATE_ID);
            if (!sendEmail) {
                await this.userEventRepository.save({
                    id: user.id,
                    remind: false,
                });
                return {
                    payload: false,
                };
            }
        }
        return {
            payload: true,
        };
    }
};
RemindAttendEventHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(mail_1.MailService)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        mail_1.MailService])
], RemindAttendEventHandler);
exports.RemindAttendEventHandler = RemindAttendEventHandler;
//# sourceMappingURL=index.js.map