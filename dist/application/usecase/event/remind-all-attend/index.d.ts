import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { RemindAllAttendEventInput, RemindAllAttendEventOutput } from './validate';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { RemindAttendEventHandler } from '../remind-attend';
import { EventDataValidate } from '../validate';
export declare class RemindAllAttendEventHandler {
    private userEventRepository;
    private eventRepository;
    private remindAttendEventHandler;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository, remindAttendEventHandler: RemindAttendEventHandler);
    execute(param: RemindAllAttendEventInput): Promise<RemindAllAttendEventOutput>;
    asyncExecute(event: EventDataValidate): Promise<RemindAllAttendEventOutput>;
}
