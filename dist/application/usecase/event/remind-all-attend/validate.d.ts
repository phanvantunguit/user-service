export declare class RemindAllAttendEventInput {
    invite_id?: string;
    event_id?: string;
}
export declare class RemindAllAttendEventOutput {
    payload: boolean;
}
