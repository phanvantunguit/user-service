"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemindAllDynamicHandler = void 0;
const common_1 = require("@nestjs/common");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const remind_dynamic_1 = require("../remind-dynamic");
let RemindAllDynamicHandler = class RemindAllDynamicHandler {
    constructor(userEventRepository, eventRepository, remindDynamicHandler) {
        this.userEventRepository = userEventRepository;
        this.eventRepository = eventRepository;
        this.remindDynamicHandler = remindDynamicHandler;
    }
    async execute(param) {
        if (param.event_id) {
            const event = (await this.eventRepository.findById(param.event_id));
            if (!event) {
                (0, throw_exception_1.notFoundError)('Event');
            }
            this.asyncExecute(event, param.template_id);
        }
        else if (param.invite_id) {
            const user = (await this.userEventRepository.findById(param.invite_id));
            if (!user) {
                (0, throw_exception_1.notFoundError)('Event');
            }
            const event = (await this.eventRepository.findById(param.event_id));
            await this.remindDynamicHandler.execute({
                user,
                event,
                template_id: param.template_id,
            });
        }
        return {
            payload: true,
        };
    }
    async asyncExecute(event, template_id) {
        const users = (await this.userEventRepository.find({
            event_id: event.id,
        }));
        for (const user of users) {
            try {
                await this.remindDynamicHandler.execute({
                    event,
                    user,
                    template_id,
                });
            }
            catch (error) { }
        }
        return {
            payload: true,
        };
    }
};
RemindAllDynamicHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __param(2, (0, common_1.Inject)(remind_dynamic_1.RemindDynamicHandler)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        event_repository_1.EventRepository,
        remind_dynamic_1.RemindDynamicHandler])
], RemindAllDynamicHandler);
exports.RemindAllDynamicHandler = RemindAllDynamicHandler;
//# sourceMappingURL=index.js.map