export declare class RemindAllDynamicInput {
    invite_id?: string;
    event_id?: string;
    template_id: string;
}
export declare class RemindAllDynamicOutput {
    payload: boolean;
}
