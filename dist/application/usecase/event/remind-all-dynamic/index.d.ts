import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { RemindAllDynamicInput, RemindAllDynamicOutput } from './validate';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { EventDataValidate } from '../validate';
import { RemindDynamicHandler } from '../remind-dynamic';
export declare class RemindAllDynamicHandler {
    private userEventRepository;
    private eventRepository;
    private remindDynamicHandler;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository, remindDynamicHandler: RemindDynamicHandler);
    execute(param: RemindAllDynamicInput): Promise<RemindAllDynamicOutput>;
    asyncExecute(event: EventDataValidate, template_id: string): Promise<RemindAllDynamicOutput>;
}
