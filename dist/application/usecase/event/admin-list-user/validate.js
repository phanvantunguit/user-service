"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminListUserOutput = exports.PagingUserDataOutput = exports.AdminListUserInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
const class_transformer_1 = require("class-transformer");
const validate_1 = require("../validate");
class AdminListUserInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], AdminListUserInput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], AdminListUserInput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'The keyword to find user ex: phone number, name, email',
        example: 'john',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminListUserInput.prototype, "keyword", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Event id uuid',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminListUserInput.prototype, "event_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Type of ticket',
        example: types_1.INVITE_CODE_TYPE.GUEST,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.INVITE_CODE_TYPE)),
    __metadata("design:type", String)
], AdminListUserInput.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status confirm of ticket',
        example: types_1.EVENT_CONFIRM_STATUS.APPROVED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_CONFIRM_STATUS)),
    __metadata("design:type", String)
], AdminListUserInput.prototype, "confirm_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status payment of event',
        example: types_1.PAYMENT_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.PAYMENT_STATUS)),
    __metadata("design:type", String)
], AdminListUserInput.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status checkin of user',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    (0, class_validator_1.IsIn)(['1', '0']),
    __metadata("design:type", Number)
], AdminListUserInput.prototype, "attend", void 0);
exports.AdminListUserInput = AdminListUserInput;
class PagingUserDataOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'List of user',
        isArray: true,
        type: validate_1.UserDataValidate,
    }),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => validate_1.UserDataValidate),
    __metadata("design:type", Array)
], PagingUserDataOutput.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Current page',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserDataOutput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Maximun number of list',
        example: 50,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserDataOutput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Number of current list',
        example: 10,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserDataOutput.prototype, "count", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Number of query',
        example: 100,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserDataOutput.prototype, "total", void 0);
exports.PagingUserDataOutput = PagingUserDataOutput;
class AdminListUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result user paging',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", PagingUserDataOutput)
], AdminListUserOutput.prototype, "payload", void 0);
exports.AdminListUserOutput = AdminListUserOutput;
//# sourceMappingURL=validate.js.map