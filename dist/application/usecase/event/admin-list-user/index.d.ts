import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { AdminListUserInput, AdminListUserOutput } from './validate';
export declare class AdminListUserHandler {
    private userEventRepository;
    constructor(userEventRepository: UserEventRepository);
    execute(param: AdminListUserInput): Promise<AdminListUserOutput>;
}
