import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE, PAYMENT_STATUS } from 'src/domain/event/types';
import { UserDataValidate } from '../validate';
export declare class AdminListUserInput {
    page: number;
    size: number;
    keyword: string;
    event_id: string;
    type: INVITE_CODE_TYPE;
    confirm_status: EVENT_CONFIRM_STATUS;
    payment_status: PAYMENT_STATUS;
    attend: number;
}
export declare class PagingUserDataOutput {
    rows: UserDataValidate[];
    page: number;
    size: number;
    count: number;
    total: number;
}
export declare class AdminListUserOutput {
    payload: PagingUserDataOutput;
}
