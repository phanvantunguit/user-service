import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { AdminGetUserInput, AdminGetUserOutput } from './validate';
export declare class AdminGetUserHandler {
    private userEventRepository;
    constructor(userEventRepository: UserEventRepository);
    execute(param: AdminGetUserInput): Promise<AdminGetUserOutput>;
}
