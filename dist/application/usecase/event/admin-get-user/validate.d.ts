import { UserDataValidate } from '../validate';
export declare class AdminGetUserInput {
    id: string;
}
export declare class AdminGetUserOutput {
    payload: UserDataValidate;
}
