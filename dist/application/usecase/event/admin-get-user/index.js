"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminGetUserHandler = void 0;
const common_1 = require("@nestjs/common");
const user_event_domain_1 = require("../../../../domain/event/user-event.domain");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
let AdminGetUserHandler = class AdminGetUserHandler {
    constructor(userEventRepository) {
        this.userEventRepository = userEventRepository;
    }
    async execute(param) {
        const user = await this.userEventRepository.findById(param.id);
        const result = new user_event_domain_1.UserEventDomain();
        result.id = user.id;
        result.fullname = user.fullname;
        result.email = user.email;
        result.phone = user.phone;
        result.event_id = user.event_id;
        result.attend = user.attend;
        result.invite_code = user.invite_code;
        result.confirm_status = user.confirm_status;
        result.payment_status = user.payment_status;
        result.telegram = user.telegram;
        result.created_by = user.created_by;
        result.updated_by = user.updated_by;
        result.created_at = user.created_at;
        result.updated_at = user.updated_at;
        return {
            payload: result,
        };
    }
};
AdminGetUserHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository])
], AdminGetUserHandler);
exports.AdminGetUserHandler = AdminGetUserHandler;
//# sourceMappingURL=index.js.map