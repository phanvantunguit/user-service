"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRegisterEventOutput = exports.UserRegisterEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class UserRegisterEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Fullname of user register',
        example: 'Nguyen Van A',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Telegram',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "telegram", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Url webstite result confirm',
        example: 'http://localhost:3000',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "callback_confirm_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Url webstite result scan qrcode',
        example: 'http://localhost:3000',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "callback_attend_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Unique code of event',
        example: 'coinmapaxi',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventInput.prototype, "event_code", void 0);
exports.UserRegisterEventInput = UserRegisterEventInput;
class UserRegisterEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Code to attend to event',
        example: 'Guest0001',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserRegisterEventOutput.prototype, "payload", void 0);
exports.UserRegisterEventOutput = UserRegisterEventOutput;
//# sourceMappingURL=validate.js.map