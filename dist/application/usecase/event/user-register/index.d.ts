import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { UserRegisterEventInput, UserRegisterEventOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { StorageService } from 'src/infrastructure/services/storage';
import { DBContext } from 'src/infrastructure/data/database/db-context';
export declare class UserRegisterEventHandler {
    private userEventRepository;
    private eventRepository;
    private mailService;
    private dBContext;
    private storageService;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository, mailService: MailService, dBContext: DBContext, storageService: StorageService);
    execute(param: UserRegisterEventInput): Promise<UserRegisterEventOutput>;
}
