export declare class UserRegisterEventInput {
    fullname: string;
    email: string;
    phone: string;
    telegram: string;
    callback_confirm_url: string;
    callback_attend_url: string;
    event_code: string;
}
export declare class UserRegisterEventOutput {
    payload: string;
}
