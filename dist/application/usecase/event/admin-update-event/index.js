"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUpdateEventHandler = void 0;
const common_1 = require("@nestjs/common");
const event_domain_1 = require("../../../../domain/event/event.domain");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let AdminUpdateEventHandler = class AdminUpdateEventHandler {
    constructor(eventRepository) {
        this.eventRepository = eventRepository;
    }
    async execute(id, param, userId) {
        const data = new event_domain_1.EventDomain();
        data.id = id;
        data.name = param.name;
        data.code = param.code;
        data.attendees_number = param.attendees_number;
        data.status = param.status;
        data.email_remind_at = param.email_remind_at;
        data.email_remind_template_id = param.email_remind_template_id;
        data.email_confirm = param.email_confirm;
        data.email_confirm_template_id = param.email_confirm_template_id;
        data.start_at = param.start_at;
        data.finish_at = param.finish_at;
        data.updated_by = userId;
        data.create_qrcode = param.create_qrcode;
        if (data.code) {
            const event = await this.eventRepository.findOne({ code: data.code });
            if (event && event.id !== data.id) {
                (0, throw_exception_1.badRequestError)('Event Code', const_1.ERROR_CODE.EXISTED);
            }
        }
        await this.eventRepository.save(data);
        return {
            payload: true,
        };
    }
};
AdminUpdateEventHandler = __decorate([
    __param(0, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __metadata("design:paramtypes", [event_repository_1.EventRepository])
], AdminUpdateEventHandler);
exports.AdminUpdateEventHandler = AdminUpdateEventHandler;
//# sourceMappingURL=index.js.map