import { EVENT_STATUS } from 'src/domain/event/types';
export declare class AdminUpdateEventInput {
    name?: string;
    code?: string;
    attendees_number?: number;
    email_remind_at?: number;
    email_remind_template_id?: string;
    email_confirm?: boolean;
    email_confirm_template_id?: string;
    status?: EVENT_STATUS;
    start_at?: number;
    finish_at?: number;
    create_qrcode?: boolean;
}
export declare class AdminUpdateEventOutput {
    payload: boolean;
}
