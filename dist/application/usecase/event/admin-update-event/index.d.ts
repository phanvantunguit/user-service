import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminUpdateEventInput, AdminUpdateEventOutput } from './validate';
export declare class AdminUpdateEventHandler {
    private eventRepository;
    constructor(eventRepository: EventRepository);
    execute(id: string, param: AdminUpdateEventInput, userId: string): Promise<AdminUpdateEventOutput>;
}
