import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminCreateEventInput, AdminCreateEventOutput } from './validate';
export declare class AdminCreateEventHandler {
    private eventRepository;
    constructor(eventRepository: EventRepository);
    execute(param: AdminCreateEventInput, userId: string): Promise<AdminCreateEventOutput>;
}
