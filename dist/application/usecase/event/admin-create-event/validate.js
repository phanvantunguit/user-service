"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminCreateEventOutput = exports.AdminCreateEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
class AdminCreateEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Name of event',
        example: 'Coinmap vs Axi',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], AdminCreateEventInput.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Code of event',
        example: 'coinmapaxi',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], AdminCreateEventInput.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Attendees number',
        example: 1000,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminCreateEventInput.prototype, "attendees_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email remind at',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminCreateEventInput.prototype, "email_remind_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email remind template id',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], AdminCreateEventInput.prototype, "email_remind_template_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email confirm',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminCreateEventInput.prototype, "email_confirm", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email confirm template id',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], AdminCreateEventInput.prototype, "email_confirm_template_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `Status of event ${Object.values(types_1.EVENT_STATUS).join(' or ')}`,
        example: types_1.EVENT_STATUS.OPEN_REGISTRATION,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_STATUS)),
    __metadata("design:type", String)
], AdminCreateEventInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Time start event',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminCreateEventInput.prototype, "start_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Time finish event',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminCreateEventInput.prototype, "finish_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Determined create qrcode',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminCreateEventInput.prototype, "create_qrcode", void 0);
exports.AdminCreateEventInput = AdminCreateEventInput;
class AdminCreateEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of event',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminCreateEventOutput.prototype, "payload", void 0);
exports.AdminCreateEventOutput = AdminCreateEventOutput;
//# sourceMappingURL=validate.js.map