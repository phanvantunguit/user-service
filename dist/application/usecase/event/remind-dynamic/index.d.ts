import { RemindDynamicInput, RemindDynamicOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { UserRemindRepository } from 'src/infrastructure/data/database/user-remind.repository';
export declare class RemindDynamicHandler {
    private mailService;
    private userRemindRepository;
    constructor(mailService: MailService, userRemindRepository: UserRemindRepository);
    execute(param: RemindDynamicInput): Promise<RemindDynamicOutput>;
}
