"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemindDynamicHandler = void 0;
const common_1 = require("@nestjs/common");
const mail_1 = require("../../../../infrastructure/services/mail");
const date_fns_tz_1 = require("date-fns-tz");
const user_remind_repository_1 = require("../../../../infrastructure/data/database/user-remind.repository");
const time_util_1 = require("../../../../utils/time.util");
let RemindDynamicHandler = class RemindDynamicHandler {
    constructor(mailService, userRemindRepository) {
        this.mailService = mailService;
        this.userRemindRepository = userRemindRepository;
    }
    async execute(param) {
        const { user, event, template_id, remind_at } = param;
        const remindAt = remind_at || Number(user.created_at) + time_util_1.DAY;
        const userRemind = await this.userRemindRepository.findOne({
            event_id: event.id,
            template_id,
            remind_at: remindAt,
            user_id: user.id,
        });
        if (userRemind && userRemind.remind) {
            return {
                payload: true,
            };
        }
        let userRemindSaved;
        if (userRemind) {
            userRemindSaved = await this.userRemindRepository.save({
                id: userRemind.id,
                event_id: event.id,
                user_id: user.id,
                template_id,
                remind: true,
                remind_at: remindAt,
            });
        }
        else {
            userRemindSaved = await this.userRemindRepository.save({
                event_id: event.id,
                user_id: user.id,
                template_id,
                remind: true,
                remind_at: remindAt,
            });
        }
        const count_user = Number(user.invite_code.replace(/\D/g, ''));
        const sendEmail = await this.mailService.sendEmailDynamicTemplate(user.email, {
            email: user.email,
            name: user.fullname,
            phone: user.phone,
            event_name: event.name,
            event_code: event.code,
            invite_code: user.invite_code,
            attendees_number: event.attendees_number,
            start_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.start_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
            finish_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.finish_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
            count_user,
            countdown: '2 ngày',
        }, template_id);
        if (!sendEmail) {
            await this.userRemindRepository.save({
                id: userRemindSaved.id,
                event_id: event.id,
                user_id: user.id,
                template_id,
                remind: false,
                remind_at,
            });
            return {
                payload: false,
            };
        }
        return {
            payload: true,
        };
    }
};
RemindDynamicHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(mail_1.MailService)),
    __param(1, (0, common_1.Inject)(user_remind_repository_1.UserRemindRepository)),
    __metadata("design:paramtypes", [mail_1.MailService,
        user_remind_repository_1.UserRemindRepository])
], RemindDynamicHandler);
exports.RemindDynamicHandler = RemindDynamicHandler;
//# sourceMappingURL=index.js.map