"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemindDynamicOutput = exports.RemindDynamicInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const validate_1 = require("../validate");
class RemindDynamicInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'user',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", validate_1.UserDataValidate)
], RemindDynamicInput.prototype, "user", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'user',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", validate_1.EventDataValidate)
], RemindDynamicInput.prototype, "event", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'template id',
        example: 'abc',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], RemindDynamicInput.prototype, "template_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'remind at',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], RemindDynamicInput.prototype, "remind_at", void 0);
exports.RemindDynamicInput = RemindDynamicInput;
class RemindDynamicOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status of update',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], RemindDynamicOutput.prototype, "payload", void 0);
exports.RemindDynamicOutput = RemindDynamicOutput;
//# sourceMappingURL=validate.js.map