import { EventDataValidate, UserDataValidate } from '../validate';
export declare class RemindDynamicInput {
    user: UserDataValidate;
    event: EventDataValidate;
    template_id: string;
    remind_at?: number;
}
export declare class RemindDynamicOutput {
    payload: boolean;
}
