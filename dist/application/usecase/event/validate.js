"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDataValidate = exports.EventDataValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../domain/event/types");
class EventDataValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of event',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventDataValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Name of event',
        example: 'Coinmap vs Axi',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventDataValidate.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Code of event',
        example: 'coinmapaxi',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventDataValidate.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `Status of event ${Object.values(types_1.EVENT_STATUS).join(' or ')}`,
        example: types_1.EVENT_STATUS.OPEN_REGISTRATION,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_STATUS)),
    __metadata("design:type", String)
], EventDataValidate.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Attendees number',
        example: 1000,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "attendees_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email remind at',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "email_remind_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email remind template id',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventDataValidate.prototype, "email_remind_template_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email confirm',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], EventDataValidate.prototype, "email_confirm", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email confirm template id',
        example: Date.now(),
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventDataValidate.prototype, "email_confirm_template_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Time start event',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "start_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Time finish event',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "finish_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Time created',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Last updated',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Deleted',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], EventDataValidate.prototype, "deleted_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Determined create qrcode',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], EventDataValidate.prototype, "create_qrcode", void 0);
exports.EventDataValidate = EventDataValidate;
class UserDataValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of user',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status of user confirm attend to event',
        example: types_1.EVENT_CONFIRM_STATUS.APPROVED,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "confirm_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status attend of user',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserDataValidate.prototype, "attend", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Remind user attend',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserDataValidate.prototype, "remind", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Fullname of user register',
        example: 'Nguyen Van A',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Code to attend to event',
        example: 'Guest0001',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "invite_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status payment of event',
        example: types_1.PAYMENT_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.PAYMENT_STATUS)),
    __metadata("design:type", String)
], UserDataValidate.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Telegram',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "telegram", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Updated By',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "updated_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Created By',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserDataValidate.prototype, "created_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Time user register',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], UserDataValidate.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Last time user update',
        example: 1664519091465,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], UserDataValidate.prototype, "updated_at", void 0);
exports.UserDataValidate = UserDataValidate;
//# sourceMappingURL=validate.js.map