"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCheckParamOutput = exports.UserCheckParamDataOutput = exports.UserCheckParamInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class UserCheckParamInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], UserCheckParamInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckParamInput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Telegram',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckParamInput.prototype, "telegram", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Unique code of event',
        example: 'coinmapaxi',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserCheckParamInput.prototype, "event_code", void 0);
exports.UserCheckParamInput = UserCheckParamInput;
class UserCheckParamDataOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email to confirm register',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserCheckParamDataOutput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone number',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserCheckParamDataOutput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Telegram',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserCheckParamDataOutput.prototype, "telegram", void 0);
exports.UserCheckParamDataOutput = UserCheckParamDataOutput;
class UserCheckParamOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status of information',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", UserCheckParamDataOutput)
], UserCheckParamOutput.prototype, "payload", void 0);
exports.UserCheckParamOutput = UserCheckParamOutput;
//# sourceMappingURL=validate.js.map