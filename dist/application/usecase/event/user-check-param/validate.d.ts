export declare class UserCheckParamInput {
    email: string;
    phone: string;
    telegram: string;
    event_code: string;
}
export declare class UserCheckParamDataOutput {
    email?: boolean;
    phone?: boolean;
    telegram?: boolean;
}
export declare class UserCheckParamOutput {
    payload: UserCheckParamDataOutput;
}
