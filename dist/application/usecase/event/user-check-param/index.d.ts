import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { UserCheckParamInput, UserCheckParamOutput } from './validate';
export declare class UserCheckParamHandler {
    private userEventRepository;
    private eventRepository;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository);
    execute(param: UserCheckParamInput): Promise<UserCheckParamOutput>;
}
