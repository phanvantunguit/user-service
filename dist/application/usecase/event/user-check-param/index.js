"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCheckParamHandler = void 0;
const common_1 = require("@nestjs/common");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let UserCheckParamHandler = class UserCheckParamHandler {
    constructor(userEventRepository, eventRepository) {
        this.userEventRepository = userEventRepository;
        this.eventRepository = eventRepository;
    }
    async execute(param) {
        const event = param.event_code
            ? await this.eventRepository.findOne({ code: param.event_code })
            : await this.eventRepository.findOne({});
        if (!event) {
            (0, throw_exception_1.notFoundError)('Event');
        }
        const result = {};
        if (param.email) {
            const email = await this.userEventRepository.findOne({
                event_id: event.id,
                email: param.email,
            });
            result.email = email ? false : true;
        }
        if (param.telegram) {
            const telegram = await this.userEventRepository.findOne({
                event_id: event.id,
                telegram: param.telegram,
            });
            result.telegram = telegram ? false : true;
        }
        if (param.phone) {
            const phone = await this.userEventRepository.findOne({
                event_id: event.id,
                phone: param.phone,
            });
            result.phone = phone ? false : true;
        }
        return {
            payload: result,
        };
    }
};
UserCheckParamHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        event_repository_1.EventRepository])
], UserCheckParamHandler);
exports.UserCheckParamHandler = UserCheckParamHandler;
//# sourceMappingURL=index.js.map