import { EventDataValidate } from '../validate';
export declare class AdminGetEventInput {
    id: string;
}
export declare class AdminGetEventOutput {
    payload: EventDataValidate;
}
