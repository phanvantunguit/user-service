"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminGetEventHandler = void 0;
const common_1 = require("@nestjs/common");
const event_domain_1 = require("../../../../domain/event/event.domain");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let AdminGetEventHandler = class AdminGetEventHandler {
    constructor(eventRepository) {
        this.eventRepository = eventRepository;
    }
    async execute(param) {
        const event = await this.eventRepository.findById(param.id);
        if (!event) {
            (0, throw_exception_1.notFoundError)('Event');
        }
        const result = new event_domain_1.EventDomain();
        result.name = event.name;
        result.code = event.code;
        result.attendees_number = event.attendees_number;
        result.status = event.status;
        result.email_remind_at = event.email_remind_at;
        result.email_remind_template_id = event.email_remind_template_id;
        result.email_confirm = event.email_confirm;
        result.email_confirm_template_id = event.email_confirm_template_id;
        result.start_at = event.start_at;
        result.remind = event.remind;
        result.finish_at = event.finish_at;
        result.updated_by = event.updated_by;
        result.created_by = event.created_by;
        result.updated_at = event.updated_at;
        result.created_at = event.created_at;
        result.deleted_at = event.deleted_at;
        return {
            payload: result,
        };
    }
};
AdminGetEventHandler = __decorate([
    __param(0, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __metadata("design:paramtypes", [event_repository_1.EventRepository])
], AdminGetEventHandler);
exports.AdminGetEventHandler = AdminGetEventHandler;
//# sourceMappingURL=index.js.map