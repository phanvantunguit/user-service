import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminGetEventInput, AdminGetEventOutput } from './validate';
export declare class AdminGetEventHandler {
    private eventRepository;
    constructor(eventRepository: EventRepository);
    execute(param: AdminGetEventInput): Promise<AdminGetEventOutput>;
}
