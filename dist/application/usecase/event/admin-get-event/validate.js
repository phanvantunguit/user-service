"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminGetEventOutput = exports.AdminGetEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const validate_1 = require("../validate");
class AdminGetEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of event',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminGetEventInput.prototype, "id", void 0);
exports.AdminGetEventInput = AdminGetEventInput;
class AdminGetEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result event',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", validate_1.EventDataValidate)
], AdminGetEventOutput.prototype, "payload", void 0);
exports.AdminGetEventOutput = AdminGetEventOutput;
//# sourceMappingURL=validate.js.map