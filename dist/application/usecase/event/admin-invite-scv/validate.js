"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminInviteEventCSVOutput = exports.AdminInviteEventCSVDataOutput = exports.AdminInviteEventCSVFileDataInput = exports.AdminInviteEventCSVInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
const class_transformer_1 = require("class-transformer");
class AdminInviteEventCSVInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Url webstite result confirm',
        example: 'http://localhost:3000',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVInput.prototype, "callback_confirm_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Url webstite result scan qrcode',
        example: 'http://localhost:3000',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVInput.prototype, "callback_attend_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Event id uuid',
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], AdminInviteEventCSVInput.prototype, "event_id", void 0);
exports.AdminInviteEventCSVInput = AdminInviteEventCSVInput;
class AdminInviteEventCSVFileDataInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Numerical order',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminInviteEventCSVFileDataInput.prototype, "index", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Fullname',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Telegram',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "telegram", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Phone number',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'type of ticket',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Confirm status of user',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Payment status of user',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVFileDataInput.prototype, "payment_status", void 0);
exports.AdminInviteEventCSVFileDataInput = AdminInviteEventCSVFileDataInput;
class AdminInviteEventCSVDataOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Numerical order',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminInviteEventCSVDataOutput.prototype, "index", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], AdminInviteEventCSVDataOutput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Error message',
        example: 'Email duplicate',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminInviteEventCSVDataOutput.prototype, "message", void 0);
exports.AdminInviteEventCSVDataOutput = AdminInviteEventCSVDataOutput;
class AdminInviteEventCSVOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Numerical order failed',
        isArray: true,
        type: AdminInviteEventCSVDataOutput,
    }),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => AdminInviteEventCSVDataOutput),
    __metadata("design:type", Array)
], AdminInviteEventCSVOutput.prototype, "payload", void 0);
exports.AdminInviteEventCSVOutput = AdminInviteEventCSVOutput;
//# sourceMappingURL=validate.js.map