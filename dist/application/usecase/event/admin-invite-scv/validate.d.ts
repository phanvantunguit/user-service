import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE, PAYMENT_STATUS } from 'src/domain/event/types';
export declare class AdminInviteEventCSVInput {
    callback_confirm_url: string;
    callback_attend_url: string;
    event_id: string;
}
export declare class AdminInviteEventCSVFileDataInput {
    index: number;
    email: string;
    fullname: string;
    telegram: string;
    phone: string;
    type: INVITE_CODE_TYPE;
    status: EVENT_CONFIRM_STATUS;
    payment_status: PAYMENT_STATUS;
}
export declare class AdminInviteEventCSVDataOutput {
    index: number;
    email: string;
    message: string;
}
export declare class AdminInviteEventCSVOutput {
    payload: AdminInviteEventCSVDataOutput[];
}
