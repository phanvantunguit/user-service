"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminInviteEventCSVHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/event/types");
const XLSX = require("xlsx");
const admin_invite_1 = require("../admin-invite");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let AdminInviteEventCSVHandler = class AdminInviteEventCSVHandler {
    constructor(adminInviteEventHandler, userEventRepository, eventRepository) {
        this.adminInviteEventHandler = adminInviteEventHandler;
        this.userEventRepository = userEventRepository;
        this.eventRepository = eventRepository;
    }
    async execute(file, param, admin_id) {
        const workbook = XLSX.read(file.buffer, { type: 'buffer' });
        const rowObjects = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
        const event = await this.eventRepository.findById(param.event_id);
        if (!event) {
            (0, throw_exception_1.notFoundError)('Event');
        }
        const result = (await new Promise((resolve) => {
            const emails = [];
            let count = 0;
            const dataOutputs = [];
            rowObjects.map(async (row) => {
                if (row.email) {
                    const registered = await this.userEventRepository.findOne({
                        event_id: event.id,
                        email: row.email,
                    });
                    if (registered) {
                        dataOutputs.push({
                            index: row.index,
                            email: row.email,
                            message: 'Email Existed',
                        });
                    }
                    else if (emails.includes(row.email)) {
                        dataOutputs.push({
                            index: row.index,
                            email: row.email,
                            message: 'Email duplicate',
                        });
                    }
                    else {
                        emails.push(row.email);
                    }
                }
                count += 1;
                if (count === rowObjects.length) {
                    resolve(dataOutputs);
                }
            });
        }));
        this.asyncExecute(rowObjects, param.callback_confirm_url, param.callback_attend_url, param.event_id, admin_id);
        return {
            payload: result,
        };
    }
    async asyncExecute(rowObjects, callback_confirm_url, callback_attend_url, event_id, admin_id) {
        let count = 0;
        while (count < rowObjects.length) {
            try {
                if (rowObjects[count]) {
                    await this.adminInviteEventHandler.execute({
                        fullname: rowObjects[count].fullname,
                        email: rowObjects[count].email,
                        phone: rowObjects[count].phone,
                        callback_confirm_url: callback_confirm_url,
                        callback_attend_url: callback_attend_url,
                        type: rowObjects[count].type || types_1.INVITE_CODE_TYPE.VIP,
                        event_id: event_id,
                        confirm_status: rowObjects[count].email
                            ? types_1.EVENT_CONFIRM_STATUS.WAITING
                            : rowObjects[count].status &&
                                Object.values(types_1.EVENT_CONFIRM_STATUS).includes(rowObjects[count].status)
                                ? rowObjects[count].status
                                : types_1.EVENT_CONFIRM_STATUS.APPROVED,
                        telegram: rowObjects[count].telegram,
                        payment_status: rowObjects[count].payment_status,
                    }, admin_id);
                }
            }
            catch (error) { }
            count += 1;
        }
    }
};
AdminInviteEventCSVHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(admin_invite_1.AdminInviteEventHandler)),
    __param(1, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(2, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __metadata("design:paramtypes", [admin_invite_1.AdminInviteEventHandler,
        user_event_repository_1.UserEventRepository,
        event_repository_1.EventRepository])
], AdminInviteEventCSVHandler);
exports.AdminInviteEventCSVHandler = AdminInviteEventCSVHandler;
//# sourceMappingURL=index.js.map