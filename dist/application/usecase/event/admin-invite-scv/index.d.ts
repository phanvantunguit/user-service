import { AdminInviteEventCSVInput, AdminInviteEventCSVOutput } from './validate';
import { AdminInviteEventHandler } from '../admin-invite';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
export declare class AdminInviteEventCSVHandler {
    private adminInviteEventHandler;
    private userEventRepository;
    private eventRepository;
    constructor(adminInviteEventHandler: AdminInviteEventHandler, userEventRepository: UserEventRepository, eventRepository: EventRepository);
    execute(file: any, param: AdminInviteEventCSVInput, admin_id: string): Promise<AdminInviteEventCSVOutput>;
    private asyncExecute;
}
