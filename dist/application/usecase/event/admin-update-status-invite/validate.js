"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUpdateStatusInviteEventOutput = exports.AdminUpdateStatusInviteEventInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
class AdminUpdateStatusInviteEventInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Ticket id uuid',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminUpdateStatusInviteEventInput.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status confirm of ticket',
        example: types_1.EVENT_CONFIRM_STATUS.APPROVED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_CONFIRM_STATUS)),
    __metadata("design:type", String)
], AdminUpdateStatusInviteEventInput.prototype, "confirm_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status payment of event',
        example: types_1.PAYMENT_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.PAYMENT_STATUS)),
    __metadata("design:type", String)
], AdminUpdateStatusInviteEventInput.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Checkin or not',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminUpdateStatusInviteEventInput.prototype, "attend", void 0);
exports.AdminUpdateStatusInviteEventInput = AdminUpdateStatusInviteEventInput;
class AdminUpdateStatusInviteEventOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status of update',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminUpdateStatusInviteEventOutput.prototype, "payload", void 0);
exports.AdminUpdateStatusInviteEventOutput = AdminUpdateStatusInviteEventOutput;
//# sourceMappingURL=validate.js.map