import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { MailService } from 'src/infrastructure/services';
import { AdminUpdateStatusInviteEventInput, AdminUpdateStatusInviteEventOutput } from './validate';
export declare class AdminUpdateStatusInviteEventHandler {
    private userEventRepository;
    private eventRepository;
    private mailService;
    constructor(userEventRepository: UserEventRepository, eventRepository: EventRepository, mailService: MailService);
    execute(param: AdminUpdateStatusInviteEventInput, admin_id: string): Promise<AdminUpdateStatusInviteEventOutput>;
}
