"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUpdateStatusInviteEventHandler = void 0;
const common_1 = require("@nestjs/common");
const date_fns_tz_1 = require("date-fns-tz");
const config_1 = require("../../../../config");
const types_1 = require("../../../../domain/event/types");
const event_repository_1 = require("../../../../infrastructure/data/database/event.repository");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const services_1 = require("../../../../infrastructure/services");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let AdminUpdateStatusInviteEventHandler = class AdminUpdateStatusInviteEventHandler {
    constructor(userEventRepository, eventRepository, mailService) {
        this.userEventRepository = userEventRepository;
        this.eventRepository = eventRepository;
        this.mailService = mailService;
    }
    async execute(param, admin_id) {
        const registered = await this.userEventRepository.findById(param.id);
        if (!registered) {
            (0, throw_exception_1.notFoundError)('Ticket');
        }
        await this.userEventRepository.save(Object.assign(Object.assign({}, param), { updated_by: admin_id }));
        if (param.payment_status !== registered.payment_status &&
            param.payment_status === types_1.PAYMENT_STATUS.COMPLETED) {
            const event = await this.eventRepository.findById(registered.event_id);
            const template_id = config_1.APP_CONFIG.PAYMENT_EVENT_TEMPLATE_ID;
            const count_user = Number(registered.invite_code.replace(/\D/g, ''));
            await this.mailService.sendEmailDynamicTemplate(registered.email, {
                email: registered.email,
                name: registered.fullname,
                phone: registered.phone,
                event_name: event.name,
                event_code: event.code,
                invite_code: registered.invite_code,
                attendees_number: event.attendees_number,
                start_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.start_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                finish_at: `${(0, date_fns_tz_1.formatInTimeZone)(Number(event.finish_at), 'Asia/Ho_Chi_Minh', 'HH:mm dd/MM/yyyy')} (GMT+7)`,
                count_user,
            }, template_id);
        }
        return {
            payload: true,
        };
    }
};
AdminUpdateStatusInviteEventHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __param(2, (0, common_1.Inject)(services_1.MailService)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        event_repository_1.EventRepository,
        services_1.MailService])
], AdminUpdateStatusInviteEventHandler);
exports.AdminUpdateStatusInviteEventHandler = AdminUpdateStatusInviteEventHandler;
//# sourceMappingURL=index.js.map