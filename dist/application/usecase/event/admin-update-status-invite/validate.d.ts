import { EVENT_CONFIRM_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';
export declare class AdminUpdateStatusInviteEventInput {
    id: string;
    confirm_status: EVENT_CONFIRM_STATUS;
    payment_status: PAYMENT_STATUS;
    attend: boolean;
}
export declare class AdminUpdateStatusInviteEventOutput {
    payload: boolean;
}
