"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminSendPromotionHandler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../../config");
const user_event_repository_1 = require("../../../../infrastructure/data/database/user-event.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const send_promotion_dynamic_1 = require("../send-promotion-dynamic");
let AdminSendPromotionHandler = class AdminSendPromotionHandler {
    constructor(userEventRepository, sendPromotionDynamicHandler) {
        this.userEventRepository = userEventRepository;
        this.sendPromotionDynamicHandler = sendPromotionDynamicHandler;
    }
    async execute(param) {
        if (this.proccessing) {
            (0, throw_exception_1.badRequestError)('Promotion Email', const_1.ERROR_CODE.PROCESSING);
        }
        const template_id = param.template_id || config_1.APP_CONFIG.PROMOTION_TEMPLATE_ID;
        this.proccessing = true;
        let users;
        if (param.users) {
            users = param.users;
        }
        else {
            users = await this.userEventRepository.getEmails(param);
        }
        this.asyncExecute(users, template_id, param.from_email, param.from_name);
        return {
            payload: users.length,
        };
    }
    async asyncExecute(users, template_id, from_email, from_name) {
        for (const user of users) {
            try {
                await this.sendPromotionDynamicHandler.execute({
                    email: user.email,
                    fullname: user.fullname,
                    template_id,
                    from_email,
                    from_name,
                });
            }
            catch (error) { }
        }
        this.proccessing = false;
        return {
            payload: true,
        };
    }
};
AdminSendPromotionHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(1, (0, common_1.Inject)(send_promotion_dynamic_1.SendPromotionDynamicHandler)),
    __metadata("design:paramtypes", [user_event_repository_1.UserEventRepository,
        send_promotion_dynamic_1.SendPromotionDynamicHandler])
], AdminSendPromotionHandler);
exports.AdminSendPromotionHandler = AdminSendPromotionHandler;
//# sourceMappingURL=index.js.map