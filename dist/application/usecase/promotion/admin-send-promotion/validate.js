"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminSendPromotionOutput = exports.AdminSendPromotionInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/event/types");
const validate_1 = require("../validate");
const class_transformer_1 = require("class-transformer");
class AdminSendPromotionInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email sender',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], AdminSendPromotionInput.prototype, "from_email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Sender name',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminSendPromotionInput.prototype, "from_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Template id',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminSendPromotionInput.prototype, "template_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Event id uuid',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], AdminSendPromotionInput.prototype, "event_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status confirm of ticket',
        example: types_1.EVENT_CONFIRM_STATUS.APPROVED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.EVENT_CONFIRM_STATUS)),
    __metadata("design:type", String)
], AdminSendPromotionInput.prototype, "confirm_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Status payment of event',
        example: types_1.PAYMENT_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.PAYMENT_STATUS)),
    __metadata("design:type", String)
], AdminSendPromotionInput.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'attend',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminSendPromotionInput.prototype, "attend", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: validate_1.UserDataPromotionValidate,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => validate_1.UserDataPromotionValidate),
    __metadata("design:type", Array)
], AdminSendPromotionInput.prototype, "users", void 0);
exports.AdminSendPromotionInput = AdminSendPromotionInput;
class AdminSendPromotionOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'number of email',
        example: 1000,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminSendPromotionOutput.prototype, "payload", void 0);
exports.AdminSendPromotionOutput = AdminSendPromotionOutput;
//# sourceMappingURL=validate.js.map