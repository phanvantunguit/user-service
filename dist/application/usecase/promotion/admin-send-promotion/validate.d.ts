import { EVENT_CONFIRM_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';
import { UserDataPromotionValidate } from '../validate';
export declare class AdminSendPromotionInput {
    from_email?: string;
    from_name?: string;
    template_id: string;
    event_id: string;
    confirm_status: EVENT_CONFIRM_STATUS;
    payment_status: PAYMENT_STATUS;
    attend: boolean;
    users: UserDataPromotionValidate[];
}
export declare class AdminSendPromotionOutput {
    payload: number;
}
