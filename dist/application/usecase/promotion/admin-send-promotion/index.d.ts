import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { SendPromotionDynamicHandler } from '../send-promotion-dynamic';
import { UserDataPromotionValidate } from '../validate';
import { AdminSendPromotionInput, AdminSendPromotionOutput } from './validate';
export declare class AdminSendPromotionHandler {
    private userEventRepository;
    private sendPromotionDynamicHandler;
    proccessing: boolean;
    constructor(userEventRepository: UserEventRepository, sendPromotionDynamicHandler: SendPromotionDynamicHandler);
    execute(param: AdminSendPromotionInput): Promise<AdminSendPromotionOutput>;
    asyncExecute(users: UserDataPromotionValidate[], template_id: string, from_email?: string, from_name?: string): Promise<{
        payload: boolean;
    }>;
}
