"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromotionDynamicOutput = exports.PromotionDynamicInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class PromotionDynamicInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email sender',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], PromotionDynamicInput.prototype, "from_email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Sender name',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PromotionDynamicInput.prototype, "from_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'fullname',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PromotionDynamicInput.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'email',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], PromotionDynamicInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'template id',
        example: 'abc',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PromotionDynamicInput.prototype, "template_id", void 0);
exports.PromotionDynamicInput = PromotionDynamicInput;
class PromotionDynamicOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status of update',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], PromotionDynamicOutput.prototype, "payload", void 0);
exports.PromotionDynamicOutput = PromotionDynamicOutput;
//# sourceMappingURL=validate.js.map