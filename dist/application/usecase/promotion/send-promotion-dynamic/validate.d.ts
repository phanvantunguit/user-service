export declare class PromotionDynamicInput {
    from_email?: string;
    from_name?: string;
    fullname: string;
    email: string;
    template_id: string;
}
export declare class PromotionDynamicOutput {
    payload: boolean;
}
