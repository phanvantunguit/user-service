import { PromotionDynamicInput, PromotionDynamicOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { UserPromotionRepository } from 'src/infrastructure/data/database/user-promotion.repository';
export declare class SendPromotionDynamicHandler {
    private mailService;
    private userPromotionRepository;
    constructor(mailService: MailService, userPromotionRepository: UserPromotionRepository);
    execute(param: PromotionDynamicInput): Promise<PromotionDynamicOutput>;
}
