"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendPromotionDynamicHandler = void 0;
const common_1 = require("@nestjs/common");
const mail_1 = require("../../../../infrastructure/services/mail");
const user_promotion_repository_1 = require("../../../../infrastructure/data/database/user-promotion.repository");
const user_promotion_domain_1 = require("../../../../domain/promotion/user-promotion.domain");
let SendPromotionDynamicHandler = class SendPromotionDynamicHandler {
    constructor(mailService, userPromotionRepository) {
        this.mailService = mailService;
        this.userPromotionRepository = userPromotionRepository;
    }
    async execute(param) {
        const { fullname, email, template_id, from_email, from_name } = param;
        const userPromotion = await this.userPromotionRepository.findOne({
            template_id,
            email,
        });
        const dataUser = new user_promotion_domain_1.UserPromotionDomain({
            template_id,
            email,
            send: true,
        });
        const sendEmail = await this.mailService.sendEmailDynamicTemplate(email, {
            email: email,
            name: fullname,
        }, template_id, null, from_email, from_name);
        if (!sendEmail) {
        }
        return {
            payload: true,
        };
    }
};
SendPromotionDynamicHandler = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(mail_1.MailService)),
    __param(1, (0, common_1.Inject)(user_promotion_repository_1.UserPromotionRepository)),
    __metadata("design:paramtypes", [mail_1.MailService,
        user_promotion_repository_1.UserPromotionRepository])
], SendPromotionDynamicHandler);
exports.SendPromotionDynamicHandler = SendPromotionDynamicHandler;
//# sourceMappingURL=index.js.map