import { EventStoreRepository } from 'src/infrastructure/data/database/event-store.repository';
import { ListEventInactiveBySystemInput, ListEventInactiveBySystemOutput } from './validate';
export declare class ListInactiveBySystemHandler {
    private eventStoreRepository;
    constructor(eventStoreRepository: EventStoreRepository);
    execute(param: ListEventInactiveBySystemInput): Promise<ListEventInactiveBySystemOutput>;
}
