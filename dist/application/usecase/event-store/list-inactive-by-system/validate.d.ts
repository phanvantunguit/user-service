export declare class ListEventInactiveBySystemInput {
    user_id?: string;
    state?: string;
    bot_id: string;
}
export declare class EventInactiveBySystemValidate {
    bot_id: string;
    strategy_id: string;
    subscriber_id: string;
    trade_id: string;
    symbol: string;
    time: number;
}
export declare class ListEventInactiveBySystemOutput {
    payload: EventInactiveBySystemValidate[];
}
