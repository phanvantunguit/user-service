"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListEventInactiveBySystemOutput = exports.EventInactiveBySystemValidate = exports.ListEventInactiveBySystemInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class ListEventInactiveBySystemInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'user_id',
    }),
    (0, class_validator_1.IsUUID)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListEventInactiveBySystemInput.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'state',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListEventInactiveBySystemInput.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'bot_id',
    }),
    (0, class_validator_1.IsUUID)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListEventInactiveBySystemInput.prototype, "bot_id", void 0);
exports.ListEventInactiveBySystemInput = ListEventInactiveBySystemInput;
class EventInactiveBySystemValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'bot_id',
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], EventInactiveBySystemValidate.prototype, "bot_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'strategy_id',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventInactiveBySystemValidate.prototype, "strategy_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'subscriber_id',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventInactiveBySystemValidate.prototype, "subscriber_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'trade_id',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventInactiveBySystemValidate.prototype, "trade_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'symbol',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EventInactiveBySystemValidate.prototype, "symbol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'time',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], EventInactiveBySystemValidate.prototype, "time", void 0);
exports.EventInactiveBySystemValidate = EventInactiveBySystemValidate;
class ListEventInactiveBySystemOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Numerical order failed',
        isArray: true,
        type: EventInactiveBySystemValidate,
    }),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => EventInactiveBySystemValidate),
    __metadata("design:type", Array)
], ListEventInactiveBySystemOutput.prototype, "payload", void 0);
exports.ListEventInactiveBySystemOutput = ListEventInactiveBySystemOutput;
//# sourceMappingURL=validate.js.map