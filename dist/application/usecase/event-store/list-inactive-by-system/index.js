"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListInactiveBySystemHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/event-store/types");
const event_store_repository_1 = require("../../../../infrastructure/data/database/event-store.repository");
let ListInactiveBySystemHandler = class ListInactiveBySystemHandler {
    constructor(eventStoreRepository) {
        this.eventStoreRepository = eventStoreRepository;
    }
    async execute(param) {
        const { user_id, bot_id } = param;
        const events = await this.eventStoreRepository.find({
            user_id,
            event_name: types_1.EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
            state: types_1.EVENT_STORE_STATE.OPEN,
        });
        let dataResult = [];
        if (bot_id) {
            events.forEach(e => {
                var _a;
                if (((_a = e.metadata) === null || _a === void 0 ? void 0 : _a.bot_id) === bot_id) {
                    dataResult.push(e.metadata);
                }
            });
        }
        else {
            dataResult = events.map(e => e.metadata);
        }
        return {
            payload: dataResult
        };
    }
};
ListInactiveBySystemHandler = __decorate([
    __param(0, (0, common_1.Inject)(event_store_repository_1.EventStoreRepository)),
    __metadata("design:paramtypes", [event_store_repository_1.EventStoreRepository])
], ListInactiveBySystemHandler);
exports.ListInactiveBySystemHandler = ListInactiveBySystemHandler;
//# sourceMappingURL=index.js.map