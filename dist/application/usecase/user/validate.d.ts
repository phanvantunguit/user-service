export declare class UserValidate {
    id: string;
    email: string;
    username: string;
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    affiliate_code: string;
    link_affiliate: string;
    referral_code: string;
    profile_pic: string;
    active: boolean;
    email_confirmed: boolean;
    note_updated: string;
    country: string;
    year_of_birth: string;
    gender: string;
    phone_code: string;
    merchant_code: string;
    created_at?: number;
    updated_at?: number;
}
export declare class ListUserValidate extends UserValidate {
    tbots: string[];
}
