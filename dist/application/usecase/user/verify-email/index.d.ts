import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
export declare class VerifyEmailUserHandler {
    private userRepo;
    private merchantRepository;
    constructor(userRepo: UserRepository, merchantRepository: MerchantRepository);
    execute(token: string): Promise<any>;
}
