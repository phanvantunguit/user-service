"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyEmailUserHandler = void 0;
const common_1 = require("@nestjs/common");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let VerifyEmailUserHandler = class VerifyEmailUserHandler {
    constructor(userRepo, merchantRepository) {
        this.userRepo = userRepo;
        this.merchantRepository = merchantRepository;
    }
    async execute(token) {
        const verifyToken = await this.userRepo.findOneToken({
            token,
        });
        if (verifyToken) {
            const findUser = await this.userRepo.findOneUser({
                id: verifyToken.user_id,
            });
            if (!findUser) {
                (0, throw_exception_1.badRequestError)('User', const_1.ERROR_CODE.NOT_FOUND);
            }
            if (findUser.email_confirmed) {
                return findUser;
            }
            await this.userRepo.saveUser({
                id: findUser.id,
                active: true,
                email_confirmed: true,
            });
            return findUser;
        }
        (0, throw_exception_1.badRequestError)('', const_1.ERROR_CODE.SESSION_INVALID);
    }
};
VerifyEmailUserHandler = __decorate([
    __param(1, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        merchant_repository_1.MerchantRepository])
], VerifyEmailUserHandler);
exports.VerifyEmailUserHandler = VerifyEmailUserHandler;
//# sourceMappingURL=index.js.map