"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListUserOutput = exports.PagingUserOutput = exports.ListUserInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/transaction/types");
const validate_1 = require("../validate");
class ListUserInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListUserInput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListUserInput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'The keyword to find user ex: order id, name, email',
        example: 'john',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ListUserInput.prototype, "keyword", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'From',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListUserInput.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'To',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListUserInput.prototype, "to", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `List bot filter: ${Object.values(types_1.ITEM_STATUS)}`,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ListUserInput.prototype, "tbot_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'List bot id',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ListUserInput.prototype, "tbots", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'type',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ListUserInput.prototype, "asset_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'merchant_code',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ListUserInput.prototype, "merchant_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Email confirmed: true false',
        example: 'true',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBooleanString)(),
    __metadata("design:type", Boolean)
], ListUserInput.prototype, "email_confirmed", void 0);
exports.ListUserInput = ListUserInput;
class PagingUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserOutput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserOutput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserOutput.prototype, "count", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingUserOutput.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: validate_1.ListUserValidate,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => validate_1.ListUserValidate),
    __metadata("design:type", Array)
], PagingUserOutput.prototype, "rows", void 0);
exports.PagingUserOutput = PagingUserOutput;
class ListUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result transaction paging',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", PagingUserOutput)
], ListUserOutput.prototype, "payload", void 0);
exports.ListUserOutput = ListUserOutput;
//# sourceMappingURL=validate.js.map