import { ListUserValidate } from '../validate';
export declare class ListUserInput {
    page: number;
    size: number;
    keyword: string;
    from: number;
    to: number;
    tbot_status?: string;
    tbots?: string;
    asset_type?: string;
    merchant_code?: string;
    email_confirmed?: boolean;
}
export declare class PagingUserOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: ListUserValidate[];
}
export declare class ListUserOutput {
    payload: PagingUserOutput;
}
