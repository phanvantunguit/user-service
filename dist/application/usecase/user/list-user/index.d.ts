import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { ListUserInput, ListUserOutput } from './validate';
export declare class ListUserHandler {
    private userRepository;
    constructor(userRepository: UserRepository);
    execute(param: ListUserInput, merchant_code: string): Promise<ListUserOutput>;
}
