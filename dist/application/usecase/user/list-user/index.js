"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListUserHandler = void 0;
const common_1 = require("@nestjs/common");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
let ListUserHandler = class ListUserHandler {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    async execute(param, merchant_code) {
        const { from, to, tbots, page, size, keyword, email_confirmed, tbot_status } = param;
        const dataRes = await this.userRepository.getPaging({
            page,
            size,
            keyword,
            from,
            to,
            merchant_code,
            tbots: tbots ? tbots.split(',') : [],
            email_confirmed,
            tbot_status: tbot_status ? tbot_status.split(',') : [],
        });
        return {
            payload: dataRes,
        };
    }
};
ListUserHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], ListUserHandler);
exports.ListUserHandler = ListUserHandler;
//# sourceMappingURL=index.js.map