import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { GetUserOutput } from './validate';
export declare class GetUserHandler {
    private userRepository;
    constructor(userRepository: UserRepository);
    execute(id: string, merchant_code?: string): Promise<GetUserOutput>;
}
