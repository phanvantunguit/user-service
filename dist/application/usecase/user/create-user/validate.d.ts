export declare class PutUserProfileDto {
    email: string;
    first_name: string;
    last_name: string;
    password: string;
    gender: string;
}
export declare class MerchantCreateUserOutput {
    payload: boolean;
}
