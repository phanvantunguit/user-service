"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUserCreateServiceHandler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../../config");
const permission_1 = require("../../../../const/permission");
const user_1 = require("../../../../const/user");
const types_1 = require("../../../../domain/merchant/types");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const time_util_1 = require("../../../../utils/time.util");
const verify_token_1 = require("../../verify-token");
let MerchantUserCreateServiceHandler = class MerchantUserCreateServiceHandler {
    constructor(mailService, verifyTokenService, userRepo, merchantRepository) {
        this.mailService = mailService;
        this.verifyTokenService = verifyTokenService;
        this.userRepo = userRepo;
        this.merchantRepository = merchantRepository;
    }
    async execute(params, owner_created) {
        const email = params.email.toLocaleLowerCase();
        const merchant = await this.merchantRepository.findOne({
            id: owner_created,
        });
        if (merchant.config['domain_type'] === types_1.MERCHANT_TYPE.OTHERS) {
            const findUser = await this.userRepo.findOneUser({
                email,
                merchant_code: merchant.code,
            });
            if (findUser) {
                (0, throw_exception_1.badRequestError)('Email address was', const_1.ERROR_CODE.EXISTED);
            }
        }
        else {
            const merchants = await this.merchantRepository.findMerchant({
                domain_type: types_1.MERCHANT_TYPE.COINMAP,
            });
            if (merchants.length > 0) {
                const findUser = await this.userRepo.findUser({
                    email,
                    merchant_code: merchants.map((m) => m.code),
                });
                if (findUser.length > 0) {
                    (0, throw_exception_1.badRequestError)('Email address was', const_1.ERROR_CODE.EXISTED);
                }
            }
        }
        const userCreated = await this.userRepo.saveUser(Object.assign(Object.assign({ created_at: Date.now() }, params), { email, merchant_code: merchant.code }));
        const verifyToken = await this.verifyTokenService.createTokenUUID({
            user_id: userCreated.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * time_util_1.SECOND,
            type: user_1.VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
        });
        if (verifyToken) {
            await this.userRepo.saveUserRole([
                {
                    user_id: userCreated.id,
                    role_id: permission_1.DEFAULT_ROLE.id,
                    description: 'Role default',
                    owner_created: 'system',
                },
            ]);
            let from_email;
            let from_name;
            let logo_url;
            let header_url;
            let main_content;
            let footer_content;
            let twitter_url;
            let youtube_url;
            let facebook_url;
            let telegram_url;
            let company_name;
            if (merchant && merchant.config) {
                if (merchant.config['verified_sender'] &&
                    merchant.config['email_sender']) {
                    from_email = merchant.config['email_sender']['from_email'];
                    from_name = merchant.config['email_sender']['from_name'];
                }
                if (merchant.config['social_media']) {
                    twitter_url = merchant.config['social_media']['twitter_url'];
                    youtube_url = merchant.config['social_media']['youtube_url'];
                    facebook_url = merchant.config['social_media']['facebook_url'];
                    telegram_url = merchant.config['social_media']['telegram_url'];
                }
                logo_url = merchant.config['email_logo_url'];
                header_url = merchant.config['email_banner_url'];
                main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`;
                footer_content = 'Contact us:';
                if (merchant.config.domain_type === types_1.MERCHANT_TYPE.OTHERS) {
                    company_name = merchant.name;
                    from_email = from_email || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL;
                    from_name = from_name || config_1.APP_CONFIG.SENDGRID_SENDER_ALGO_NAME;
                    logo_url = logo_url || config_1.APP_CONFIG.ALGO_LOGO_EMAIL;
                    header_url = header_url || config_1.APP_CONFIG.ALGO_BANNER_EMAIL;
                }
            }
            const resultSendMail = await this.mailService.sendEmailConfirmAccount(params.email, verifyToken.token, params.password, from_email, from_name, logo_url, header_url, main_content, twitter_url, youtube_url, facebook_url, telegram_url, company_name, `${userCreated.first_name || ''} ${userCreated.last_name || ''}`);
            if (resultSendMail) {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify success',
                });
            }
            else {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify failed',
                });
            }
            delete userCreated.password;
            return userCreated;
        }
        (0, throw_exception_1.badRequestError)('', const_1.ERROR_CODE.SEND_EMAIL_FAILED);
    }
};
MerchantUserCreateServiceHandler = __decorate([
    __param(0, (0, common_1.Inject)(services_1.MailService)),
    __param(1, (0, common_1.Inject)(verify_token_1.VerifyTokenService)),
    __param(2, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __param(3, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [services_1.MailService,
        verify_token_1.VerifyTokenService,
        user_repository_1.UserRepository,
        merchant_repository_1.MerchantRepository])
], MerchantUserCreateServiceHandler);
exports.MerchantUserCreateServiceHandler = MerchantUserCreateServiceHandler;
//# sourceMappingURL=index.js.map