import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ReportChartVisitorOutput } from './validate';
export declare class ReportChartVisitorHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(merchant_code: string): Promise<ReportChartVisitorOutput>;
}
