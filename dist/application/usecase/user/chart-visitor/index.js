"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportChartVisitorHandler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../../config");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const { google } = require('googleapis');
const scopes = 'https://www.googleapis.com/auth/analytics.readonly';
const jwt = new google.auth.JWT(config_1.APP_CONFIG.CLIENT_EMAIL, null, config_1.APP_CONFIG.PRIVATE_KEY.replace(/\\n/g, '\n'), scopes);
async function getChart30Days(view_id) {
    var _a;
    try {
        await jwt.authorize();
        const response = await google.analytics('v3').data.ga.get({
            auth: jwt,
            ids: 'ga:' + view_id,
            'start-date': '30daysAgo',
            'end-date': 'today',
            dimensions: 'ga:date',
            metrics: 'ga:pageviews',
        });
        return await ((_a = response.data) === null || _a === void 0 ? void 0 : _a.rows);
    }
    catch (err) {
        console.log(err);
    }
}
let ReportChartVisitorHandler = class ReportChartVisitorHandler {
    constructor(merchantRepository) {
        this.merchantRepository = merchantRepository;
    }
    async execute(merchant_code) {
        var _a, _b;
        const merchantConfig = await this.merchantRepository.queryMerchant(merchant_code);
        const view_id = (_b = (_a = merchantConfig[0]) === null || _a === void 0 ? void 0 : _a.config) === null || _b === void 0 ? void 0 : _b.view_id;
        const getViewReport = await getChart30Days(view_id);
        const retunrViewReport = getViewReport === null || getViewReport === void 0 ? void 0 : getViewReport.map((item) => {
            const [timeRaw, visitor] = item;
            const year = timeRaw.substring(0, 4);
            const month = timeRaw.substring(4, 6);
            const day = timeRaw.substring(6, 8);
            const time = year + '-' + month + '-' + day;
            const yourObject = { time, visitor };
            return yourObject;
        });
        return {
            payload: retunrViewReport,
        };
    }
};
ReportChartVisitorHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [merchant_repository_1.MerchantRepository])
], ReportChartVisitorHandler);
exports.ReportChartVisitorHandler = ReportChartVisitorHandler;
//# sourceMappingURL=index.js.map