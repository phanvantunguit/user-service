export declare class PutUserProfileDto {
    email: string;
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    affiliate_code: string;
    profile_pic: string;
    link_affiliate: string;
    referral_code: string;
    note_updated: string;
    password: string;
    old_password: string;
    username: string;
    country: string;
    gender: string;
    year_of_birth: string;
    phone_code: string;
}
export declare class MerchantCreateUserOutput {
    payload: boolean;
}
