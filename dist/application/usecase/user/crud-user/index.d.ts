import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MailService } from 'src/infrastructure/services';
import { VerifyTokenService } from '../../verify-token';
import { PutUserProfileDto } from './validate';
export declare class MerchantUserCRUDServiceHandler {
    private mailService;
    private verifyTokenService;
    private userRepo;
    private merchantRepository;
    constructor(mailService: MailService, verifyTokenService: VerifyTokenService, userRepo: UserRepository, merchantRepository: MerchantRepository);
    execute(params: PutUserProfileDto, owner_created: string): Promise<any>;
}
