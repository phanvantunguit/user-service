"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantCreateUserOutput = exports.PutUserProfileDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const user_1 = require("../../../../const/user");
class PutUserProfileDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to register account Coinmap',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'First name',
        example: 'John',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Last name',
        example: 'Nguyen',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Full address',
        example: '123 To Hien Thanh, phuong 10, quan 10, tp HCM',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Affiliate code',
        example: 'affiliate_code',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "affiliate_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Link image',
        example: 'https://static-dev.cextrading.io/1660898281635Untitled.png',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "profile_pic", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Link affiliate',
        example: '',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "link_affiliate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Referral_code',
        example: '',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "referral_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Note updated',
        example: '',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "note_updated", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.Matches)(new RegExp(user_1.PasswordRegex), {
        message: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    (0, class_validator_1.ValidateIf)((o) => o.password),
    (0, class_validator_1.Matches)(new RegExp(user_1.PasswordRegex), {
        message: 'Old password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "old_password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Username is unique',
        example: 'John',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Country',
        example: 'Viet Nam',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "country", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'gender: male, female or other',
        example: 'male',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(user_1.GENDER)),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "gender", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Year of birth',
        example: '2000',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "year_of_birth", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone code',
        example: '+84',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PutUserProfileDto.prototype, "phone_code", void 0);
exports.PutUserProfileDto = PutUserProfileDto;
class MerchantCreateUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantCreateUserOutput.prototype, "payload", void 0);
exports.MerchantCreateUserOutput = MerchantCreateUserOutput;
//# sourceMappingURL=validate.js.map