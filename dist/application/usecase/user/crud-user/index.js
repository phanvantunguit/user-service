"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUserCRUDServiceHandler = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../../../config");
const permission_1 = require("../../../../const/permission");
const user_1 = require("../../../../const/user");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const hash_util_1 = require("../../../../utils/hash.util");
const time_util_1 = require("../../../../utils/time.util");
const verify_token_1 = require("../../verify-token");
let MerchantUserCRUDServiceHandler = class MerchantUserCRUDServiceHandler {
    constructor(mailService, verifyTokenService, userRepo, merchantRepository) {
        this.mailService = mailService;
        this.verifyTokenService = verifyTokenService;
        this.userRepo = userRepo;
        this.merchantRepository = merchantRepository;
    }
    async execute(params, owner_created) {
        const email = params.email.toLocaleLowerCase();
        const findUser = await this.userRepo.findOneUser({ email });
        if (findUser) {
            (0, throw_exception_1.badRequestError)('Email address was', const_1.ERROR_CODE.EXISTED);
        }
        const password = (0, hash_util_1.generatePassword)();
        const userCreated = await this.userRepo.saveUser(Object.assign(Object.assign({ created_at: Date.now() }, params), { password,
            email }));
        const verifyToken = await this.verifyTokenService.createTokenUUID({
            user_id: userCreated.id,
            expires_at: Date.now() + Number(config_1.APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * time_util_1.SECOND,
            type: user_1.VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
        });
        if (verifyToken) {
            await this.userRepo.saveUserRole([
                {
                    user_id: userCreated.id,
                    role_id: permission_1.DEFAULT_ROLE.id,
                    description: 'Role default',
                    owner_created: 'system',
                },
            ]);
            const merchant = await this.merchantRepository.findOne({
                id: owner_created,
            });
            let from_email;
            let from_name;
            if (merchant &&
                merchant.config &&
                merchant.config['verified_sender'] &&
                merchant.config['email_sender']) {
                from_email = merchant.config['email_sender']['from_email'];
                from_name = merchant.config['email_sender']['from_name'];
            }
            const resultSendMail = await this.mailService.sendEmailCreateAccount(params.email, verifyToken.token, password, from_email, from_name);
            if (resultSendMail) {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify success',
                });
            }
            else {
                await this.userRepo.saveUser({
                    id: userCreated.id,
                    note_updated: 'Send mail verify failed',
                });
            }
            delete userCreated.password;
            return userCreated;
        }
        (0, throw_exception_1.badRequestError)('', const_1.ERROR_CODE.SEND_EMAIL_FAILED);
    }
};
MerchantUserCRUDServiceHandler = __decorate([
    __param(0, (0, common_1.Inject)(services_1.MailService)),
    __param(3, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [services_1.MailService,
        verify_token_1.VerifyTokenService,
        user_repository_1.UserRepository,
        merchant_repository_1.MerchantRepository])
], MerchantUserCRUDServiceHandler);
exports.MerchantUserCRUDServiceHandler = MerchantUserCRUDServiceHandler;
//# sourceMappingURL=index.js.map