"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListUserValidate = exports.UserValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class UserValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], UserValidate.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'username',
        example: 'nguyenvana',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'first name',
        example: 'John',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'last name',
        example: 'Dang',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'address',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'affiliate code',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "affiliate_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'link affiliate',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "link_affiliate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'affiliate code of another user',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "referral_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'profile picture link',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "profile_pic", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'active status',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserValidate.prototype, "active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'email confirmed status',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], UserValidate.prototype, "email_confirmed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'note updated',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "note_updated", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'country',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "country", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'year of birth',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "year_of_birth", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'gender',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "gender", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'phone code',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "phone_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'merchant code',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UserValidate.prototype, "merchant_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: Date.now(),
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], UserValidate.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: Date.now(),
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], UserValidate.prototype, "updated_at", void 0);
exports.UserValidate = UserValidate;
class ListUserValidate extends UserValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: String,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => String),
    __metadata("design:type", Array)
], ListUserValidate.prototype, "tbots", void 0);
exports.ListUserValidate = ListUserValidate;
//# sourceMappingURL=validate.js.map