export declare class UpdateUserProfileDto {
    first_name: string;
    last_name: string;
    password: string;
    gender: string;
}
export declare class MerchantUpdateUserOutput {
    payload: boolean;
}
