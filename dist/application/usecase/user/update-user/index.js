"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUserUpdateServiceHandler = void 0;
const common_1 = require("@nestjs/common");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let MerchantUserUpdateServiceHandler = class MerchantUserUpdateServiceHandler {
    constructor(userRepo) {
        this.userRepo = userRepo;
    }
    async execute(params, user_id) {
        const findUser = await this.userRepo.findById(user_id);
        if (!findUser) {
            (0, throw_exception_1.badRequestError)('User', const_1.ERROR_CODE.NOT_FOUND);
        }
        if (params.password) {
        }
        if (findUser === null || findUser === void 0 ? void 0 : findUser.username) {
            const check = await this.checkUsername(findUser.username, findUser.id);
            if (!check) {
                (0, throw_exception_1.badRequestError)('Username was', const_1.ERROR_CODE.EXISTED);
            }
        }
        params['id'] = user_id;
        const userUpdated = await this.userRepo.saveUser(params);
        delete userUpdated.password;
        return userUpdated;
    }
    async checkUsername(username, user_id) {
        const user = await this.userRepo.findOneUser({ username });
        if (!user || user.id === user_id) {
            return true;
        }
        return false;
    }
};
MerchantUserUpdateServiceHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], MerchantUserUpdateServiceHandler);
exports.MerchantUserUpdateServiceHandler = MerchantUserUpdateServiceHandler;
//# sourceMappingURL=index.js.map