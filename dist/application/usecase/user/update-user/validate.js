"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateUserOutput = exports.UpdateUserProfileDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const user_1 = require("../../../../const/user");
class UpdateUserProfileDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'First name',
        example: 'John',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateUserProfileDto.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Last name',
        example: 'Nguyen',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdateUserProfileDto.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
        example: '123456Aa',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.Matches)(new RegExp(user_1.PasswordRegex), {
        message: 'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    }),
    __metadata("design:type", String)
], UpdateUserProfileDto.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'gender: male, female or other',
        example: 'male',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(user_1.GENDER)),
    __metadata("design:type", String)
], UpdateUserProfileDto.prototype, "gender", void 0);
exports.UpdateUserProfileDto = UpdateUserProfileDto;
class MerchantUpdateUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantUpdateUserOutput.prototype, "payload", void 0);
exports.MerchantUpdateUserOutput = MerchantUpdateUserOutput;
//# sourceMappingURL=validate.js.map