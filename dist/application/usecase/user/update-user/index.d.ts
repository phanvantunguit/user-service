import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { UpdateUserProfileDto } from './validate';
export declare class MerchantUserUpdateServiceHandler {
    private userRepo;
    constructor(userRepo: UserRepository);
    execute(params: UpdateUserProfileDto, user_id: string): Promise<any>;
    checkUsername(username: string, user_id?: string): Promise<boolean>;
}
