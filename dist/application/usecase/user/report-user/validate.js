"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportUserOutput = exports.ReportUserValidate = exports.ReportUserInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ReportUserInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'From',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ReportUserInput.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'To',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ReportUserInput.prototype, "to", void 0);
exports.ReportUserInput = ReportUserInput;
class ReportUserValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'User confirmed email',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportUserValidate.prototype, "count_confirmed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Waiting user confirm email',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportUserValidate.prototype, "count_not_confirmed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Number View',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportUserValidate.prototype, "pageView", void 0);
exports.ReportUserValidate = ReportUserValidate;
class ReportUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Report user',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", ReportUserValidate)
], ReportUserOutput.prototype, "payload", void 0);
exports.ReportUserOutput = ReportUserOutput;
//# sourceMappingURL=validate.js.map