import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { ReportUserOutput, ReportUserInput } from './validate';
export declare class ReportUserHandler {
    private userRepository;
    private merchantRepository;
    constructor(userRepository: UserRepository, merchantRepository: MerchantRepository);
    execute(param: ReportUserInput, merchant_code: string): Promise<ReportUserOutput>;
}
