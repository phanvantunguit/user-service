export declare class ReportUserInput {
    from: number;
    to: number;
}
export declare class ReportUserValidate {
    count_confirmed: number;
    count_not_confirmed: number;
    pageView: number;
}
export declare class ReportUserOutput {
    payload: ReportUserValidate;
}
