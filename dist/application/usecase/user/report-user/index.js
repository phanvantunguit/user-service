"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportUserHandler = void 0;
const common_1 = require("@nestjs/common");
const moment = require("moment-timezone");
const config_1 = require("../../../../config");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const { google } = require('googleapis');
const scopes = 'https://www.googleapis.com/auth/analytics.readonly';
const jwt = new google.auth.JWT(config_1.APP_CONFIG.CLIENT_EMAIL, null, config_1.APP_CONFIG.PRIVATE_KEY.replace(/\\n/g, '\n'), scopes);
async function getViews(view_id, from, to) {
    var _a, _b, _c, _d;
    try {
        const timeFrom = moment(Number(from))
            .tz('Asia/Ho_Chi_Minh')
            .format('YYYY-MM-DD');
        const timeTo = moment(Number(to))
            .tz('Asia/Ho_Chi_Minh')
            .format('YYYY-MM-DD');
        const today = 'today';
        await jwt.authorize();
        if (from != undefined && to != undefined) {
            const response = await google.analytics('v3').data.ga.get({
                auth: jwt,
                ids: 'ga:' + view_id,
                'start-date': timeFrom,
                'end-date': timeTo,
                metrics: 'ga:pageviews',
            });
            return Object.values((_a = response.data) === null || _a === void 0 ? void 0 : _a.totalsForAllResults)[0];
        }
        else if (from != undefined && to == undefined) {
            const response = await google.analytics('v3').data.ga.get({
                auth: jwt,
                ids: 'ga:' + view_id,
                'start-date': timeFrom,
                'end-date': today,
                metrics: 'ga:pageviews',
            });
            return Object.values((_b = response.data) === null || _b === void 0 ? void 0 : _b.totalsForAllResults)[0];
        }
        else if (from == undefined && to != undefined) {
            const response = await google.analytics('v3').data.ga.get({
                auth: jwt,
                ids: 'ga:' + view_id,
                'start-date': new Date(1672531200000).toISOString().substring(0, 10),
                'end-date': timeTo,
                metrics: 'ga:pageviews',
            });
            return Object.values((_c = response.data) === null || _c === void 0 ? void 0 : _c.totalsForAllResults)[0];
        }
        else {
            const response = await google.analytics('v3').data.ga.get({
                auth: jwt,
                ids: 'ga:' + view_id,
                'start-date': new Date(1672531200000).toISOString().substring(0, 10),
                'end-date': today,
                metrics: 'ga:pageviews',
            });
            return Object.values((_d = response.data) === null || _d === void 0 ? void 0 : _d.totalsForAllResults)[0];
        }
    }
    catch (err) {
        console.log(err);
    }
}
let ReportUserHandler = class ReportUserHandler {
    constructor(userRepository, merchantRepository) {
        this.userRepository = userRepository;
        this.merchantRepository = merchantRepository;
    }
    async execute(param, merchant_code) {
        var _a, _b;
        const { from, to } = param;
        const merchantConfig = await this.merchantRepository.queryMerchant(merchant_code);
        const view_id = (_b = (_a = merchantConfig[0]) === null || _a === void 0 ? void 0 : _a.config) === null || _b === void 0 ? void 0 : _b.view_id;
        const getViewReport = await getViews(view_id, from, to);
        const reports = await this.userRepository.report({
            from,
            to,
            merchant_code,
        });
        const dataRes = reports[0];
        return {
            payload: {
                count_confirmed: Number(dataRes.count_confirmed),
                count_not_confirmed: Number(dataRes.count_not_confirmed),
                pageView: Number(getViewReport),
            },
        };
    }
};
ReportUserHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __param(1, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        merchant_repository_1.MerchantRepository])
], ReportUserHandler);
exports.ReportUserHandler = ReportUserHandler;
//# sourceMappingURL=index.js.map