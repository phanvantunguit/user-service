import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { BaseCreateOutput } from '../../validate';
import { CreateAppSettingInput } from './validate';
export declare class CreateAppSettingHandler {
    private settingRepository;
    constructor(settingRepository: SettingRepository);
    execute(param: CreateAppSettingInput): Promise<BaseCreateOutput>;
}
