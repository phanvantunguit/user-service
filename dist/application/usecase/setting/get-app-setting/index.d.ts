import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { GetAppSettingOutput } from './validate';
export declare class GetAppSettingHandler {
    private settingRepository;
    constructor(settingRepository: SettingRepository);
    execute(id: string): Promise<GetAppSettingOutput>;
}
