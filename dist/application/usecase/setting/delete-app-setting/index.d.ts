import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { BaseUpdateOutput } from '../../validate';
export declare class DeleteAppSettingHandler {
    private settingRepository;
    constructor(settingRepository: SettingRepository);
    execute(id: string): Promise<BaseUpdateOutput>;
}
