import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { BaseUpdateOutput } from '../../validate';
import { UpdateAppSettingInput } from './validate';
export declare class UpdateAppSettingHandler {
    private settingRepository;
    constructor(settingRepository: SettingRepository);
    execute(param: UpdateAppSettingInput): Promise<BaseUpdateOutput>;
}
