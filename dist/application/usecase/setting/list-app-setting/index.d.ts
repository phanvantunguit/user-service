import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { ListAppSettingInput, ListAppSettingOutput } from './validate';
export declare class ListAppSettingHandler {
    private settingRepository;
    constructor(settingRepository: SettingRepository);
    execute(param: ListAppSettingInput): Promise<ListAppSettingOutput>;
}
