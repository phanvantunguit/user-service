import { AppSettingValidate } from '../validate';
export declare class ListAppSettingInput {
    name?: string;
    user_id?: string;
}
export declare class ListAppSettingOutput {
    payload: AppSettingValidate[];
}
