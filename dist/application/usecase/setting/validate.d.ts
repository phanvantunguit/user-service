export declare class AppSettingValidate {
    id?: string;
    name?: string;
    value?: string;
    user_id?: string;
    description?: string;
    owner_created?: string;
    created_at?: number;
    updated_at?: number;
}
