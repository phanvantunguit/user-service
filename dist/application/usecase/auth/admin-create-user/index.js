"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminCreateUserHandler = void 0;
const common_1 = require("@nestjs/common");
const admin_repository_1 = require("../../../../infrastructure/data/database/admin.repository");
const db_context_1 = require("../../../../infrastructure/data/database/db-context");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const hash_util_1 = require("../../../../utils/hash.util");
let AdminCreateUserHandler = class AdminCreateUserHandler {
    constructor(adminRepository, dBContext, mailService) {
        this.adminRepository = adminRepository;
        this.dBContext = dBContext;
        this.mailService = mailService;
    }
    async execute(param, admin_id) {
        await this.dBContext.runInTransaction(async (queryRunner) => {
            const email = param.email.toLocaleLowerCase();
            const findAdmin = await this.adminRepository.findOne({ email });
            if (findAdmin) {
                (0, throw_exception_1.badRequestError)('Email', const_1.ERROR_CODE.EXISTED);
            }
            const password = (0, hash_util_1.generatePassword)();
            const admin = await this.adminRepository.save(Object.assign(Object.assign({}, param), { email,
                password, created_by: admin_id || null, updated_by: admin_id || null }), queryRunner);
            await this.mailService.sendEmailCreateAccount(email, admin.id, password);
            return admin;
        });
        return {
            payload: true,
        };
    }
};
AdminCreateUserHandler = __decorate([
    __param(0, (0, common_1.Inject)(admin_repository_1.AdminRepository)),
    __param(1, (0, common_1.Inject)(db_context_1.DBContext)),
    __param(2, (0, common_1.Inject)(services_1.MailService)),
    __metadata("design:paramtypes", [admin_repository_1.AdminRepository,
        db_context_1.DBContext,
        services_1.MailService])
], AdminCreateUserHandler);
exports.AdminCreateUserHandler = AdminCreateUserHandler;
//# sourceMappingURL=index.js.map