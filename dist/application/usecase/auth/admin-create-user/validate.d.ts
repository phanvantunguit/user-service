import { ADMIN_STATUS } from 'src/domain/admin/types';
export declare class AdminCreateUserInput {
    fullname: string;
    email: string;
    phone: string;
    super_admin: boolean;
    status: ADMIN_STATUS;
}
export declare class AdminCreateUserOutput {
    payload: boolean;
}
