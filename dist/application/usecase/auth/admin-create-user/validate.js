"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminCreateUserOutput = exports.AdminCreateUserInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/admin/types");
class AdminCreateUserInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Fullname of user register',
        example: 'Nguyen Van A',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminCreateUserInput.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Email to confirm register',
        example: 'john@gmail.com',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], AdminCreateUserInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Phone number',
        example: '0987654321',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminCreateUserInput.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Set super admin',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminCreateUserInput.prototype, "super_admin", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `Status of account ${Object.values(types_1.ADMIN_STATUS).join(' or ')}`,
        example: types_1.ADMIN_STATUS.ACTIVATE,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.ADMIN_STATUS)),
    __metadata("design:type", String)
], AdminCreateUserInput.prototype, "status", void 0);
exports.AdminCreateUserInput = AdminCreateUserInput;
class AdminCreateUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminCreateUserOutput.prototype, "payload", void 0);
exports.AdminCreateUserOutput = AdminCreateUserOutput;
//# sourceMappingURL=validate.js.map