import { AdminRepository } from 'src/infrastructure/data/database/admin.repository';
import { DBContext } from 'src/infrastructure/data/database/db-context';
import { MailService } from 'src/infrastructure/services';
import { AdminCreateUserInput, AdminCreateUserOutput } from './validate';
export declare class AdminCreateUserHandler {
    private adminRepository;
    private dBContext;
    private mailService;
    constructor(adminRepository: AdminRepository, dBContext: DBContext, mailService: MailService);
    execute(param: AdminCreateUserInput, admin_id: string): Promise<AdminCreateUserOutput>;
}
