import { AdminRepository } from 'src/infrastructure/data/database/admin.repository';
import { AdminVerifyEmailInput, AdminVerifyEmailOutput } from './validate';
export declare class AdminVerifyEmailHandler {
    private adminRepository;
    constructor(adminRepository: AdminRepository);
    execute(param: AdminVerifyEmailInput): Promise<AdminVerifyEmailOutput>;
}
