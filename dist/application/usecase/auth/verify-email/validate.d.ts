export declare class AdminVerifyEmailInput {
    token: string;
}
export declare class AdminVerifyEmailOutput {
    payload: boolean;
}
