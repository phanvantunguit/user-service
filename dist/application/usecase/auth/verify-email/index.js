"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminVerifyEmailHandler = void 0;
const common_1 = require("@nestjs/common");
const admin_repository_1 = require("../../../../infrastructure/data/database/admin.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let AdminVerifyEmailHandler = class AdminVerifyEmailHandler {
    constructor(adminRepository) {
        this.adminRepository = adminRepository;
    }
    async execute(param) {
        const admin = await this.adminRepository.findById(param.token);
        if (!admin) {
            (0, throw_exception_1.badRequestError)('Account', const_1.ERROR_CODE.NOT_FOUND);
        }
        await this.adminRepository.save({ id: param.token, email_confirmed: true });
        return {
            payload: true,
        };
    }
};
AdminVerifyEmailHandler = __decorate([
    __param(0, (0, common_1.Inject)(admin_repository_1.AdminRepository)),
    __metadata("design:paramtypes", [admin_repository_1.AdminRepository])
], AdminVerifyEmailHandler);
exports.AdminVerifyEmailHandler = AdminVerifyEmailHandler;
//# sourceMappingURL=index.js.map