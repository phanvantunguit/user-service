import { AdminRepository } from 'src/infrastructure/data/database/admin.repository';
import { AdminLoginInput, AdminLoginOutput } from './validate';
export declare class AdminLoginHandler {
    private adminRepository;
    constructor(adminRepository: AdminRepository);
    execute(param: AdminLoginInput): Promise<AdminLoginOutput>;
}
