export declare class AdminLoginInput {
    email: string;
    password: string;
}
export declare class AdminLoginOutput {
    payload: string;
}
