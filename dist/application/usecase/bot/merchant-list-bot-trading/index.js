"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantListBotTradingHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/transaction/types");
const bot_trading_repository_1 = require("../../../../infrastructure/data/database/bot-trading.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
let MerchantListBotTradingHandler = class MerchantListBotTradingHandler {
    constructor(botTradingRepository, userRepository) {
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
    }
    async execute(user_id, merchant_id) {
        let bots = [];
        if (user_id) {
            const userBots = await this.userRepository.findUserBotTrading({
                user_id,
            });
            const userAssetLog = await this.userRepository.findAssetLogLatest({
                user_id,
                category: types_1.ORDER_CATEGORY.TBOT,
                status: [types_1.ITEM_STATUS.NOT_CONNECT],
            });
            const botIds = userBots.map((e) => e.bot_id);
            if (botIds.length > 0) {
                const tbots = await this.botTradingRepository.findByIds(botIds);
                bots = tbots.map((e) => {
                    const userBotExit = userBots.find((ub) => ub.bot_id === e.id);
                    const userAssetLogExit = userAssetLog.find((ual) => ual.asset_id === e.id);
                    return Object.assign(Object.assign({}, e), { user_bot_id: userBotExit === null || userBotExit === void 0 ? void 0 : userBotExit.id, expires_at: userBotExit === null || userBotExit === void 0 ? void 0 : userBotExit.expires_at, status: userBotExit === null || userBotExit === void 0 ? void 0 : userBotExit.status, quantity: userAssetLogExit === null || userAssetLogExit === void 0 ? void 0 : userAssetLogExit.quantity, package_type: userAssetLogExit === null || userAssetLogExit === void 0 ? void 0 : userAssetLogExit.package_type, tbot_type: userBotExit.type, balance: userBotExit.balance, price: userAssetLogExit.price, created_at: userAssetLogExit.created_at });
                });
            }
        }
        else {
            bots = await this.botTradingRepository.list({ merchant_id });
        }
        return {
            payload: bots,
        };
    }
};
MerchantListBotTradingHandler = __decorate([
    __param(0, (0, common_1.Inject)(bot_trading_repository_1.BotTradingRepository)),
    __param(1, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository])
], MerchantListBotTradingHandler);
exports.MerchantListBotTradingHandler = MerchantListBotTradingHandler;
//# sourceMappingURL=index.js.map