import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MerchantListBotTradingOuput } from './validate';
export declare class MerchantListBotTradingHandler {
    private botTradingRepository;
    private userRepository;
    constructor(botTradingRepository: BotTradingRepository, userRepository: UserRepository);
    execute(user_id?: string, merchant_id?: string): Promise<MerchantListBotTradingOuput>;
}
