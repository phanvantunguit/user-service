import { BotRepository } from 'src/infrastructure/data/database/bot.repository';
import { MerchantListBotOuput } from './validate';
export declare class MerchantListBotHandler {
    private botRepository;
    constructor(botRepository: BotRepository);
    execute(): Promise<MerchantListBotOuput>;
}
