"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteMerchantAdditionalDataHandler = void 0;
const common_1 = require("@nestjs/common");
const merchant_additional_data_repository_1 = require("../../../../infrastructure/data/database/merchant-additional-data.repository");
let DeleteMerchantAdditionalDataHandler = class DeleteMerchantAdditionalDataHandler {
    constructor(merchantAdditionalDataRepository) {
        this.merchantAdditionalDataRepository = merchantAdditionalDataRepository;
    }
    async execute(id) {
        await this.merchantAdditionalDataRepository.deleteById(id);
        return {
            payload: true
        };
    }
};
DeleteMerchantAdditionalDataHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_additional_data_repository_1.MerchantAdditionalDataRepository)),
    __metadata("design:paramtypes", [merchant_additional_data_repository_1.MerchantAdditionalDataRepository])
], DeleteMerchantAdditionalDataHandler);
exports.DeleteMerchantAdditionalDataHandler = DeleteMerchantAdditionalDataHandler;
//# sourceMappingURL=index.js.map