import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { BaseUpdateOutput } from '../../validate';
export declare class DeleteMerchantAdditionalDataHandler {
    private merchantAdditionalDataRepository;
    constructor(merchantAdditionalDataRepository: MerchantAdditionalDataRepository);
    execute(id: string): Promise<BaseUpdateOutput>;
}
