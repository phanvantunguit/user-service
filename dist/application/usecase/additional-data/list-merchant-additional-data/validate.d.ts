import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';
import { MerchantAdditionalDataValidate } from '../validate';
export declare class ListMerchantAdditionalDataInput {
    type?: ADDITIONAL_DATA_TYPE;
    keyword?: string;
    status?: MERCHANT_ADDITIONAL_DATA_STATUS;
}
export declare class ListMerchantAdditionalDataOutput {
    payload: MerchantAdditionalDataValidate[];
}
export declare class GetMerchantAdditionalDataOutput {
    payload: MerchantAdditionalDataValidate;
}
