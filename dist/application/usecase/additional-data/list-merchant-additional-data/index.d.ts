import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { ListMerchantAdditionalDataInput, ListMerchantAdditionalDataOutput } from './validate';
export declare class ListMerchantAdditionalDataHandler {
    private merchantAdditionalDataRepository;
    constructor(merchantAdditionalDataRepository: MerchantAdditionalDataRepository);
    execute(param: ListMerchantAdditionalDataInput, merchantId?: string, id?: string): Promise<ListMerchantAdditionalDataOutput>;
}
