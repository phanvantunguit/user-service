import { ADDITIONAL_DATA_TYPE } from 'src/domain/additional-data/types';
import { AdditionalDataValidate } from '../validate';
export declare class ListAdditionalDataInput {
    type?: ADDITIONAL_DATA_TYPE;
    keyword?: string;
}
export declare class ListAdditionalDataOutput {
    payload: AdditionalDataValidate[];
}
