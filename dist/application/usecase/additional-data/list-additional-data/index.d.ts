import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { ListAdditionalDataInput, ListAdditionalDataOutput } from './validate';
export declare class ListAdditionalDataHandler {
    private additionalDataRepository;
    constructor(additionalDataRepository: AdditionalDataRepository);
    execute(param: ListAdditionalDataInput): Promise<ListAdditionalDataOutput>;
}
