import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { BaseUpdateOutput } from '../../validate';
export declare class DeleteAdditionalDataHandler {
    private additionalDataRepository;
    private merchantAdditionalDataRepository;
    constructor(additionalDataRepository: AdditionalDataRepository, merchantAdditionalDataRepository: MerchantAdditionalDataRepository);
    execute(id: string): Promise<BaseUpdateOutput>;
}
