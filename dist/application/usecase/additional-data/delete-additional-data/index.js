"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteAdditionalDataHandler = void 0;
const common_1 = require("@nestjs/common");
const additional_data_repository_1 = require("../../../../infrastructure/data/database/additional-data.repository");
const merchant_additional_data_repository_1 = require("../../../../infrastructure/data/database/merchant-additional-data.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let DeleteAdditionalDataHandler = class DeleteAdditionalDataHandler {
    constructor(additionalDataRepository, merchantAdditionalDataRepository) {
        this.additionalDataRepository = additionalDataRepository;
        this.merchantAdditionalDataRepository = merchantAdditionalDataRepository;
    }
    async execute(id) {
        const merchantAdditional = await this.merchantAdditionalDataRepository.findOne({ additional_data_id: id });
        if (merchantAdditional) {
            (0, throw_exception_1.badRequestError)('Data', const_1.ERROR_CODE.UNPROCESSABLE);
        }
        await this.additionalDataRepository.deleteById(id);
        return {
            payload: true
        };
    }
};
DeleteAdditionalDataHandler = __decorate([
    __param(0, (0, common_1.Inject)(additional_data_repository_1.AdditionalDataRepository)),
    __param(1, (0, common_1.Inject)(merchant_additional_data_repository_1.MerchantAdditionalDataRepository)),
    __metadata("design:paramtypes", [additional_data_repository_1.AdditionalDataRepository,
        merchant_additional_data_repository_1.MerchantAdditionalDataRepository])
], DeleteAdditionalDataHandler);
exports.DeleteAdditionalDataHandler = DeleteAdditionalDataHandler;
//# sourceMappingURL=index.js.map