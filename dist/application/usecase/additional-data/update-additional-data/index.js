"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAdditionalDataHandler = void 0;
const common_1 = require("@nestjs/common");
const additional_data_repository_1 = require("../../../../infrastructure/data/database/additional-data.repository");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const types_1 = require("../../../../domain/additional-data/types");
const const_1 = require("../../../../utils/exceptions/const");
let UpdateAdditionalDataHandler = class UpdateAdditionalDataHandler {
    constructor(additionalDataRepository) {
        this.additionalDataRepository = additionalDataRepository;
    }
    async execute(id, param) {
        var _a;
        param['id'] = id;
        const NameBotTBOTFEE = await this.additionalDataRepository.findOne({
            name: 'DEFAULT',
        });
        const BOTFEEID = await this.additionalDataRepository.findById(id);
        if ((NameBotTBOTFEE === null || NameBotTBOTFEE === void 0 ? void 0 : NameBotTBOTFEE.name) == param.name && BOTFEEID.name != 'DEFAULT') {
            (0, throw_exception_1.badRequestError)('BOT DEFAULT ', const_1.ERROR_CODE.EXISTED);
        }
        if (param.type === types_1.ADDITIONAL_DATA_TYPE.TBOT_FEE) {
            (_a = param.data) === null || _a === void 0 ? void 0 : _a.ranges.map((item, i, array) => {
                if (item.to != null && Number(item.from) > Number(item.to)) {
                    (0, throw_exception_1.badRequestError)('Ranges', const_1.ERROR_CODE.RANGES_INVALID);
                }
                if (Number(i) > 0) {
                    Number(array[i - 1].to) != Number(array[i].from) &&
                        (0, throw_exception_1.badRequestError)('Ranges', const_1.ERROR_CODE.RANGES_FROM_TO);
                }
            });
        }
        await this.additionalDataRepository.save(param);
        return {
            payload: true,
        };
    }
};
UpdateAdditionalDataHandler = __decorate([
    __param(0, (0, common_1.Inject)(additional_data_repository_1.AdditionalDataRepository)),
    __metadata("design:paramtypes", [additional_data_repository_1.AdditionalDataRepository])
], UpdateAdditionalDataHandler);
exports.UpdateAdditionalDataHandler = UpdateAdditionalDataHandler;
//# sourceMappingURL=index.js.map