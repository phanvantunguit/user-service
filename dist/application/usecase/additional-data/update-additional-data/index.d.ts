import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { BaseUpdateOutput } from '../../validate';
import { UpdateAdditionalDataInput } from './validate';
export declare class UpdateAdditionalDataHandler {
    private additionalDataRepository;
    constructor(additionalDataRepository: AdditionalDataRepository);
    execute(id: string, param: UpdateAdditionalDataInput): Promise<BaseUpdateOutput>;
}
