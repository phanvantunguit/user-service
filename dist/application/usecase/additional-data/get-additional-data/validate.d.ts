import { AdditionalDataValidate } from '../validate';
export declare class GetAdditionalDataOutput {
    payload: AdditionalDataValidate;
}
