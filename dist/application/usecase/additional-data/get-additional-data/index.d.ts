import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { GetAdditionalDataOutput } from './validate';
export declare class GetAdditionalDataHandler {
    private additionalDataRepository;
    constructor(additionalDataRepository: AdditionalDataRepository);
    execute(id: string): Promise<GetAdditionalDataOutput>;
}
