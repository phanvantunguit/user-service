import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { BaseCreateOutput } from '../../validate';
import { CreateAdditionalDataInput } from './validate';
export declare class CreateAdditionalDataHandler {
    private additionalDataRepository;
    constructor(additionalDataRepository: AdditionalDataRepository);
    execute(param: CreateAdditionalDataInput): Promise<BaseCreateOutput>;
}
