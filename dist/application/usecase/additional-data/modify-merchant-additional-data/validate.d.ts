import { MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';
export declare class ModifyMerchantAdditionalData {
    id?: string;
    additional_data_id?: string;
    status: MERCHANT_ADDITIONAL_DATA_STATUS;
}
export declare class ModifyMerchantAdditionalDataInput {
    data: ModifyMerchantAdditionalData[];
}
