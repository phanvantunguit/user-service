import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { BaseUpdateOutput } from '../../validate';
import { ModifyMerchantAdditionalDataInput } from './validate';
export declare class ModifyMerchantAdditionalDataHandler {
    private merchantAdditionalDataRepository;
    constructor(merchantAdditionalDataRepository: MerchantAdditionalDataRepository);
    execute(param: ModifyMerchantAdditionalDataInput, merchantId: string, adminId?: string): Promise<BaseUpdateOutput>;
}
