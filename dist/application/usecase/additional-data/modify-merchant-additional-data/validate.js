"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifyMerchantAdditionalDataInput = exports.ModifyMerchantAdditionalData = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/additional-data/types");
class ModifyMerchantAdditionalData {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'additional_data_id',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ModifyMerchantAdditionalData.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'additional_data_id',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ModifyMerchantAdditionalData.prototype, "additional_data_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `status: ${Object.values(types_1.MERCHANT_ADDITIONAL_DATA_STATUS)}`,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_ADDITIONAL_DATA_STATUS)),
    __metadata("design:type", String)
], ModifyMerchantAdditionalData.prototype, "status", void 0);
exports.ModifyMerchantAdditionalData = ModifyMerchantAdditionalData;
class ModifyMerchantAdditionalDataInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: ModifyMerchantAdditionalData,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => ModifyMerchantAdditionalData),
    __metadata("design:type", Array)
], ModifyMerchantAdditionalDataInput.prototype, "data", void 0);
exports.ModifyMerchantAdditionalDataInput = ModifyMerchantAdditionalDataInput;
//# sourceMappingURL=validate.js.map