import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';
export declare class AdditionalDataValidate {
    type?: ADDITIONAL_DATA_TYPE;
    name?: string;
    data?: any;
}
export declare class MerchantAdditionalDataValidate extends AdditionalDataValidate {
    status?: MERCHANT_ADDITIONAL_DATA_STATUS;
    merchant_id?: string;
    additional_data_id?: string;
}
