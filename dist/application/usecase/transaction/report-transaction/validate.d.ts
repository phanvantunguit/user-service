export declare class ReportTransactionInput {
    from: number;
    to: number;
}
export declare class ReportTransactionValidate {
    count_pending: number;
    count_complete: number;
    count_failed: number;
    amount_pending: number;
    amount_complete: number;
    amount_failed: number;
    commission_cash: number;
    payout_complete: number;
    payout_pending: number;
}
export declare class ReportTransactionOutput {
    payload: ReportTransactionValidate;
}
