"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportTransactionOutput = exports.ReportTransactionValidate = exports.ReportTransactionInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ReportTransactionInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'From',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ReportTransactionInput.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'To',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ReportTransactionInput.prototype, "to", void 0);
exports.ReportTransactionInput = ReportTransactionInput;
class ReportTransactionValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Count transaction pending',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "count_pending", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Count transaction complete',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "count_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Count transaction failed',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "count_failed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount transaction pending',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "amount_pending", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount transaction complete',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "amount_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount transaction failed',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "amount_failed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Total commission cash',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "commission_cash", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Payout complete',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "payout_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Payout pending',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionValidate.prototype, "payout_pending", void 0);
exports.ReportTransactionValidate = ReportTransactionValidate;
class ReportTransactionOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Report user',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", ReportTransactionValidate)
], ReportTransactionOutput.prototype, "payload", void 0);
exports.ReportTransactionOutput = ReportTransactionOutput;
//# sourceMappingURL=validate.js.map