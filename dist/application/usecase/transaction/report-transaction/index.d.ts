import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ReportTransactionInput, ReportTransactionOutput } from './validate';
export declare class ReportTransactionHandler {
    private transactionRepository;
    private merchantInvoiceRepository;
    private merchantRepository;
    constructor(transactionRepository: TransactionRepository, merchantInvoiceRepository: MerchantInvoiceRepository, merchantRepository: MerchantRepository);
    execute(param: ReportTransactionInput, merchant_id: string, merchant_code?: string): Promise<ReportTransactionOutput>;
}
