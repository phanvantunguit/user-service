"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportTransactionHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/merchant/types");
const merchant_invoice_repository_1 = require("../../../../infrastructure/data/database/merchant-invoice.repository");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const transaction_repository_1 = require("../../../../infrastructure/data/database/transaction.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let ReportTransactionHandler = class ReportTransactionHandler {
    constructor(transactionRepository, merchantInvoiceRepository, merchantRepository) {
        this.transactionRepository = transactionRepository;
        this.merchantInvoiceRepository = merchantInvoiceRepository;
        this.merchantRepository = merchantRepository;
    }
    async execute(param, merchant_id, merchant_code) {
        const { from, to } = param;
        if (!merchant_code) {
            const merchant = await this.merchantRepository.findById(merchant_id);
            if (!merchant) {
                (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
            }
            merchant_code = merchant.code;
        }
        const [amountInvoiceComplete, amountInvoicePending] = await Promise.all([
            this.merchantInvoiceRepository.totalCommission({
                merchant_id,
                status: [types_1.MERCHANT_INVOICE_STATUS.COMPLETED],
            }),
            this.merchantInvoiceRepository.totalCommission({
                merchant_id,
                status: [
                    types_1.MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
                    types_1.MERCHANT_INVOICE_STATUS.WALLET_INVALID,
                ],
            }),
        ]);
        const reports = await this.transactionRepository.report({
            from,
            to,
            merchant_code,
        });
        const dataRes = reports[0];
        return {
            payload: {
                count_pending: Number(dataRes.count_total) -
                    Number(dataRes.count_complete) -
                    Number(dataRes.count_failed),
                count_complete: Number(dataRes.count_complete),
                count_failed: Number(dataRes.count_failed),
                amount_pending: Number(dataRes.amount_total) -
                    Number(dataRes.amount_complete) -
                    Number(dataRes.amount_failed),
                amount_complete: Number(dataRes.amount_complete),
                amount_failed: Number(dataRes.amount_failed),
                commission_cash: Number(dataRes.commission_cash),
                payout_complete: Number(amountInvoiceComplete[0].total),
                payout_pending: Number(amountInvoicePending[0].total),
            },
        };
    }
};
ReportTransactionHandler = __decorate([
    __param(0, (0, common_1.Inject)(transaction_repository_1.TransactionRepository)),
    __param(1, (0, common_1.Inject)(merchant_invoice_repository_1.MerchantInvoiceRepository)),
    __param(2, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [transaction_repository_1.TransactionRepository,
        merchant_invoice_repository_1.MerchantInvoiceRepository,
        merchant_repository_1.MerchantRepository])
], ReportTransactionHandler);
exports.ReportTransactionHandler = ReportTransactionHandler;
//# sourceMappingURL=index.js.map