import { TRANSACTION_STATUS } from 'src/domain/transaction/types';
import { ListTransactionValidate } from '../validate';
export declare class ListTransactionInput {
    page: number;
    size: number;
    keyword: string;
    category?: string;
    name?: string;
    status: TRANSACTION_STATUS;
    from: number;
    to: number;
}
export declare class PagingTransactionOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: ListTransactionValidate[];
}
export declare class ListTransactionOutput {
    payload: PagingTransactionOutput;
}
