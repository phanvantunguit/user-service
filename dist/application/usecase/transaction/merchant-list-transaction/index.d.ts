import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ListTransactionInput, ListTransactionOutput } from './validate';
export declare class MerchantListTransactionHandler {
    private transactionRepository;
    constructor(transactionRepository: TransactionRepository);
    execute(param: ListTransactionInput, merchant_code: string): Promise<ListTransactionOutput>;
}
