"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionDetailValidate = exports.TransactionLogValidate = exports.TransactionMetadataValidate = exports.ListTransactionValidate = exports.ItemValidate = exports.TransactionValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../domain/transaction/types");
class TransactionValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e1',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        example: 'Nguyen Van A',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "fullname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        example: 'nguyenvana@gmail.com',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'BOT1802220001',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 0,
        description: 'total commission cash',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], TransactionValidate.prototype, "commission_cash", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Currency for user payment',
        example: 'LTCT',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "currency", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Payment method',
        example: types_1.PAYMENT_METHOD.COIN_PAYMENT,
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.PAYMENT_METHOD)),
    __metadata("design:type", String)
], TransactionValidate.prototype, "payment_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: Object.values(types_1.TRANSACTION_STATUS).join(','),
        example: types_1.TRANSACTION_STATUS.CREATED,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Payment id of coinpayment',
        example: 'OAINONOFASF1203412',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionValidate.prototype, "payment_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: Date.now(),
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], TransactionValidate.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: Date.now(),
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], TransactionValidate.prototype, "updated_at", void 0);
exports.TransactionValidate = TransactionValidate;
class ItemValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ItemValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'Bot signal',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ItemValidate.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'MONTH',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ItemValidate.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '0.001',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ItemValidate.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'BOT',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ItemValidate.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ItemValidate.prototype, "quantity", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 0,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ItemValidate.prototype, "discount_rate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 0,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ItemValidate.prototype, "discount_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 0,
        description: 'commission rate',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ItemValidate.prototype, "commission_rate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 0,
        description: 'commission cash',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ItemValidate.prototype, "commission_cash", void 0);
exports.ItemValidate = ItemValidate;
class ListTransactionValidate extends TransactionValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: ItemValidate,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => ItemValidate),
    __metadata("design:type", Array)
], ListTransactionValidate.prototype, "items", void 0);
exports.ListTransactionValidate = ListTransactionValidate;
class TransactionMetadataValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionMetadataValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionMetadataValidate.prototype, "transaction_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'wallet_address',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionMetadataValidate.prototype, "attribute", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'moBpkWoiMmJJKhBjDXeXPWPaTJFRCdmM1g',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionMetadataValidate.prototype, "value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: Date.now(),
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], TransactionMetadataValidate.prototype, "created_at", void 0);
exports.TransactionMetadataValidate = TransactionMetadataValidate;
class TransactionLogValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionLogValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionLogValidate.prototype, "transaction_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'PAYMENT_COMPLETE',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionLogValidate.prototype, "transaction_event", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'PROCESSING',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TransactionLogValidate.prototype, "transaction_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: {},
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", Object)
], TransactionLogValidate.prototype, "metadata", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: Date.now(),
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], TransactionLogValidate.prototype, "created_at", void 0);
exports.TransactionLogValidate = TransactionLogValidate;
class TransactionDetailValidate {
}
exports.TransactionDetailValidate = TransactionDetailValidate;
//# sourceMappingURL=validate.js.map