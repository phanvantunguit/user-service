import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ReportTransactionChartInput, ReportTransactionChartOutput } from './validate';
export declare class ReportTransactionChartHandler {
    private transactionRepository;
    constructor(transactionRepository: TransactionRepository);
    execute(param: ReportTransactionChartInput, merchant_code: string): Promise<ReportTransactionChartOutput>;
}
