export declare class ReportTransactionChartInput {
    from: number;
    to: number;
    timezone: string;
    time_type: 'DAY' | 'MONTH';
}
export declare class ReportTransactionChartValidate {
    time: string;
    count_complete: number;
    amount_complete: number;
    commission_cash: number;
}
export declare class ReportTransactionChartOutput {
    payload: ReportTransactionChartValidate[];
}
