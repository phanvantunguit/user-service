"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportTransactionChartOutput = exports.ReportTransactionChartValidate = exports.ReportTransactionChartInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class ReportTransactionChartInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'From',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ReportTransactionChartInput.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'To',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ReportTransactionChartInput.prototype, "to", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'timezone: +7, -7',
        example: '+7',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ReportTransactionChartInput.prototype, "timezone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'time_type: DAY or MONTH',
        example: 'DAY',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(['DAY', 'MONTH']),
    __metadata("design:type", String)
], ReportTransactionChartInput.prototype, "time_type", void 0);
exports.ReportTransactionChartInput = ReportTransactionChartInput;
class ReportTransactionChartValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Time group',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ReportTransactionChartValidate.prototype, "time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Count transaction complete',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionChartValidate.prototype, "count_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Amount transaction complete',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionChartValidate.prototype, "amount_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Total commission cash',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], ReportTransactionChartValidate.prototype, "commission_cash", void 0);
exports.ReportTransactionChartValidate = ReportTransactionChartValidate;
class ReportTransactionChartOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: ReportTransactionChartValidate,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => ReportTransactionChartValidate),
    __metadata("design:type", Array)
], ReportTransactionChartOutput.prototype, "payload", void 0);
exports.ReportTransactionChartOutput = ReportTransactionChartOutput;
//# sourceMappingURL=validate.js.map