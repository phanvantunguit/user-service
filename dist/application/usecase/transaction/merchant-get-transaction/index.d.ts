import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { GetTransactionOutput } from './validate';
export declare class MerchantGetTransactionHandler {
    private transactionRepository;
    constructor(transactionRepository: TransactionRepository);
    execute(params: {
        id: string;
        merchant_code: string;
    }): Promise<GetTransactionOutput>;
}
