import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/domain/transaction/types';
export declare class TransactionValidate {
    id?: string;
    user_id?: string;
    fullname?: string;
    email?: string;
    order_id?: string;
    amount?: string;
    commission_cash?: number;
    currency?: string;
    payment_method?: PAYMENT_METHOD;
    status?: TRANSACTION_STATUS;
    payment_id?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class ItemValidate {
    id: string;
    name: string;
    type: string;
    price: string;
    category: string;
    quantity: number;
    discount_rate: number;
    discount_amount: number;
    commission_rate: number;
    commission_cash: number;
}
export declare class ListTransactionValidate extends TransactionValidate {
    items?: ItemValidate[];
}
export declare class TransactionMetadataValidate {
    id?: string;
    transaction_id?: string;
    attribute?: string;
    value?: string;
    created_at?: number;
}
export declare class TransactionLogValidate {
    id?: string;
    transaction_id?: string;
    transaction_event?: string;
    transaction_status?: string;
    metadata?: any;
    created_at?: number;
}
export declare class TransactionDetailValidate {
    transaction: TransactionValidate;
    metadatas: TransactionMetadataValidate[];
    logs: TransactionLogValidate[];
}
