export declare class BaseUpdateOutput {
    payload: boolean;
}
export declare class BaseCreateOutput {
    payload: string;
}
