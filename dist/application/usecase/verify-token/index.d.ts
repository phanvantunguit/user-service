import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { RawVerifyToken } from './verify-token.types';
export declare class VerifyTokenService {
    private userRepo;
    constructor(userRepo: UserRepository);
    createTokenUUID(params: RawVerifyToken, metadata?: any): Promise<RawVerifyToken>;
}
