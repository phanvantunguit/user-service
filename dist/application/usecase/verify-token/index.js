"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyTokenService = void 0;
const uuid_1 = require("uuid");
const common_1 = require("@nestjs/common");
const user_repository_1 = require("../../../infrastructure/data/database/user.repository");
let VerifyTokenService = class VerifyTokenService {
    constructor(userRepo) {
        this.userRepo = userRepo;
    }
    createTokenUUID(params, metadata) {
        const { user_id, expires_at, type } = params;
        const verifyToken = {
            user_id,
            expires_at,
            type,
            token: (0, uuid_1.v4)(),
            metadata: metadata,
        };
        return this.userRepo.saveVerifyToken(verifyToken);
    }
};
VerifyTokenService = __decorate([
    __param(0, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository])
], VerifyTokenService);
exports.VerifyTokenService = VerifyTokenService;
//# sourceMappingURL=index.js.map