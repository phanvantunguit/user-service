import { VERIFY_TOKEN_TYPE } from 'src/const/user';
export interface VerifyTokenDomain {
    createTokenUUID(params: CreateTokenUUID, metdata: any): Promise<RawVerifyToken>;
}
export declare type CreateTokenUUID = {
    user_id: string;
    type: VERIFY_TOKEN_TYPE;
    expires_at: number;
};
export declare type QueryToken = {
    user_id?: string;
    token?: string;
    type?: VERIFY_TOKEN_TYPE;
};
export declare type DeleteToken = {
    user_id?: string;
    token?: string;
    type?: VERIFY_TOKEN_TYPE;
};
export declare type RawVerifyToken = {
    id?: string;
    user_id?: string;
    token?: string;
    type?: VERIFY_TOKEN_TYPE;
    expires_at?: number;
    metadata?: any;
};
