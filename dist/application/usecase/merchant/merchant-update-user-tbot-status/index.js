"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateUserBotTradingStatusHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/transaction/types");
const bot_trading_repository_1 = require("../../../../infrastructure/data/database/bot-trading.repository");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const coinmap_1 = require("../../../../infrastructure/services/coinmap");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let MerchantUpdateUserBotTradingStatusHandler = class MerchantUpdateUserBotTradingStatusHandler {
    constructor(userRepository, botTradingRepository, coinmapService, merchantRepository) {
        this.userRepository = userRepository;
        this.botTradingRepository = botTradingRepository;
        this.coinmapService = coinmapService;
        this.merchantRepository = merchantRepository;
    }
    async execute(params, id, merchantId) {
        const userBot = await this.userRepository.findByAssetIds([id]);
        if (!userBot[0]) {
            (0, throw_exception_1.badRequestError)('Bot', const_1.ERROR_CODE.NOT_FOUND);
        }
        const bot = await this.botTradingRepository.findById(userBot[0].bot_id);
        const merchant = await this.merchantRepository.findById(merchantId);
        if (!merchant) {
            (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
        }
        if (params.status === types_1.ITEM_STATUS.INACTIVE) {
            const updateStatus = await this.coinmapService.updateUserBotTradingStatus({
                id: userBot[0].id,
                status: params.status,
                merchant_code: merchant.code,
                password: merchant.password
            });
            if (!updateStatus.status) {
                (0, throw_exception_1.badRequestError)('Bot', const_1.ERROR_CODE.UNPROCESSABLE);
            }
        }
        else {
            await this.userRepository.saveStatusBotTrading({
                id,
                status: params.status,
            });
        }
        const dataLog = {
            asset_id: userBot[0].bot_id,
            user_id: userBot[0].user_id,
            category: types_1.ORDER_CATEGORY.TBOT,
            status: params.status,
            owner_created: merchantId,
            name: bot.name
        };
        await this.userRepository.saveUserAssetLog([dataLog]);
        return {
            payload: true,
        };
    }
};
MerchantUpdateUserBotTradingStatusHandler = __decorate([
    __param(0, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __param(1, (0, common_1.Inject)(bot_trading_repository_1.BotTradingRepository)),
    __param(2, (0, common_1.Inject)(coinmap_1.CoinmapService)),
    __param(3, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        bot_trading_repository_1.BotTradingRepository,
        coinmap_1.CoinmapService,
        merchant_repository_1.MerchantRepository])
], MerchantUpdateUserBotTradingStatusHandler);
exports.MerchantUpdateUserBotTradingStatusHandler = MerchantUpdateUserBotTradingStatusHandler;
//# sourceMappingURL=index.js.map