import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { CoinmapService } from 'src/infrastructure/services/coinmap';
import { MerchantUpdateStatusInput, MerchantUpdateStatusInputOutput } from './validate';
export declare class MerchantUpdateUserBotTradingStatusHandler {
    private userRepository;
    private botTradingRepository;
    private coinmapService;
    private merchantRepository;
    constructor(userRepository: UserRepository, botTradingRepository: BotTradingRepository, coinmapService: CoinmapService, merchantRepository: MerchantRepository);
    execute(params: MerchantUpdateStatusInput, id: string, merchantId: string): Promise<MerchantUpdateStatusInputOutput>;
}
