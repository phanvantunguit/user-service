import { ITEM_STATUS } from 'src/domain/transaction/types';
export declare class MerchantUpdateStatusInput {
    status: ITEM_STATUS;
}
export declare class MerchantUpdateStatusInputOutput {
    payload: boolean;
}
