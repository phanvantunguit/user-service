import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import { ORDER_CATEGORY } from 'src/domain/transaction/types';
import { AdminUpdateCommissionValidate } from '../validate';
export declare class ListMerchantCommissionValidate extends AdminUpdateCommissionValidate {
    category: ORDER_CATEGORY;
}
export declare class ListMerchantCommissionInput {
    page: number;
    size: number;
    from: number;
    to: number;
    sort_by: string;
    order_by: string;
    status?: MERCHANT_COMMISION_STATUS;
    category: ORDER_CATEGORY;
}
export declare class PagingListMerchantCommissionOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: ListMerchantCommissionValidate[];
}
export declare class ListMerchantCommissioOutput {
    payload: PagingListMerchantCommissionOutput;
}
