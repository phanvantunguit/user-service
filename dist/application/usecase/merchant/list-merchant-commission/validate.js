"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListMerchantCommissioOutput = exports.PagingListMerchantCommissionOutput = exports.ListMerchantCommissionInput = exports.ListMerchantCommissionValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/commission/types");
const types_2 = require("../../../../domain/transaction/types");
const validate_1 = require("../validate");
class ListMerchantCommissionValidate extends validate_1.AdminUpdateCommissionValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: `${Object.keys(types_2.ORDER_CATEGORY).join(',')}`,
        example: types_2.ORDER_CATEGORY.TBOT,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_2.ORDER_CATEGORY)),
    __metadata("design:type", String)
], ListMerchantCommissionValidate.prototype, "category", void 0);
exports.ListMerchantCommissionValidate = ListMerchantCommissionValidate;
class ListMerchantCommissionInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantCommissionInput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantCommissionInput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'From',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantCommissionInput.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'To',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantCommissionInput.prototype, "to", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'filed name: created_at, commission, price',
        example: 'commission',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(['created_at', 'commission', 'price']),
    __metadata("design:type", String)
], ListMerchantCommissionInput.prototype, "sort_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'desc or asc',
        example: 'desc',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(['desc', 'asc']),
    __metadata("design:type", String)
], ListMerchantCommissionInput.prototype, "order_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${types_1.MERCHANT_COMMISION_STATUS.ACTIVE} or ${types_1.MERCHANT_COMMISION_STATUS.INACTIVE}`,
        example: types_1.MERCHANT_COMMISION_STATUS.ACTIVE,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_COMMISION_STATUS)),
    __metadata("design:type", String)
], ListMerchantCommissionInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${Object.keys(types_2.ORDER_CATEGORY).join(',')}`,
        example: types_2.ORDER_CATEGORY.TBOT,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_2.ORDER_CATEGORY)),
    __metadata("design:type", String)
], ListMerchantCommissionInput.prototype, "category", void 0);
exports.ListMerchantCommissionInput = ListMerchantCommissionInput;
class PagingListMerchantCommissionOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantCommissionOutput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantCommissionOutput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantCommissionOutput.prototype, "count", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantCommissionOutput.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: ListMerchantCommissionValidate,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => ListMerchantCommissionValidate),
    __metadata("design:type", Array)
], PagingListMerchantCommissionOutput.prototype, "rows", void 0);
exports.PagingListMerchantCommissionOutput = PagingListMerchantCommissionOutput;
class ListMerchantCommissioOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result transaction paging',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", PagingListMerchantCommissionOutput)
], ListMerchantCommissioOutput.prototype, "payload", void 0);
exports.ListMerchantCommissioOutput = ListMerchantCommissioOutput;
//# sourceMappingURL=validate.js.map