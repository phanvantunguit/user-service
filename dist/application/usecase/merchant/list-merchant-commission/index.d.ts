import { MerchantCommissionRepository } from 'src/infrastructure/data/database/merchant-commission.repository';
import { ListMerchantCommissionInput, ListMerchantCommissioOutput } from './validate';
export declare class ListMerchantCommissionHandler {
    private merchantCommissionRepository;
    constructor(merchantCommissionRepository: MerchantCommissionRepository);
    execute(param: ListMerchantCommissionInput, id: string): Promise<ListMerchantCommissioOutput>;
}
