import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { AdminListMerchantOutput } from './validate';
export declare class AdminListMerchantHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(): Promise<AdminListMerchantOutput>;
}
