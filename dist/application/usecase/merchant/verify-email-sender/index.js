"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyEmailSenderHandler = void 0;
const common_1 = require("@nestjs/common");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let VerifyEmailSenderHandler = class VerifyEmailSenderHandler {
    constructor(mailService, merchantRepository) {
        this.mailService = mailService;
        this.merchantRepository = merchantRepository;
    }
    async execute(param, id, userId) {
        const { url_verify } = param;
        const merchant = await this.merchantRepository.findById(id);
        if (!merchant) {
            (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
        }
        const data = decodeURIComponent(url_verify);
        const query = data.split('?').find((e) => e.includes('token'));
        if (query) {
            console.log('query', query);
            const urlParams = new URLSearchParams(query);
            const token = urlParams.get('token');
            console.log('token', token);
            const verified = await this.mailService.sendVerifySender({ token });
            if (verified) {
                const config = merchant.config;
                config['verified_sender'] = true;
                const saveConfig = {
                    id,
                    config,
                };
                if (userId) {
                    saveConfig['updated_by'] = userId;
                }
                await this.merchantRepository.save(saveConfig);
                return {
                    payload: true,
                };
            }
        }
        (0, throw_exception_1.badRequestError)('URL', const_1.ERROR_CODE.INVALID);
    }
};
VerifyEmailSenderHandler = __decorate([
    __param(0, (0, common_1.Inject)(services_1.MailService)),
    __param(1, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [services_1.MailService,
        merchant_repository_1.MerchantRepository])
], VerifyEmailSenderHandler);
exports.VerifyEmailSenderHandler = VerifyEmailSenderHandler;
//# sourceMappingURL=index.js.map