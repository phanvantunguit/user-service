import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { VerifyEmailSenderInput } from './validate';
export declare class VerifyEmailSenderHandler {
    private mailService;
    private merchantRepository;
    constructor(mailService: MailService, merchantRepository: MerchantRepository);
    execute(param: VerifyEmailSenderInput, id: string, userId?: string): Promise<any>;
}
