"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListMerchantInvoiceOutput = exports.PagingListMerchantInvoiceOutput = exports.ListMerchantInvoiceValidate = exports.ListMerchantInvoiceInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/invoices/types");
class ListMerchantInvoiceInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'keyword',
        example: 'Merchant name or Merchant Email',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceInput.prototype, "keyword", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantInvoiceInput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantInvoiceInput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'From',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantInvoiceInput.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'To',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumberString)(),
    __metadata("design:type", Number)
], ListMerchantInvoiceInput.prototype, "to", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'filed name: created_at, start_at, finish_at',
        example: 'start_at',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(['created_at', 'start_at', 'finish_at']),
    __metadata("design:type", String)
], ListMerchantInvoiceInput.prototype, "sort_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'desc or asc',
        example: 'desc',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(['desc', 'asc']),
    __metadata("design:type", String)
], ListMerchantInvoiceInput.prototype, "order_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${types_1.MERCHANT_INVOICES_STATUS.COMPLETED} or ${types_1.MERCHANT_INVOICES_STATUS.WAITING_PAYMENT} or ${types_1.MERCHANT_INVOICES_STATUS.WALLET_INVALID}`,
        example: types_1.MERCHANT_INVOICES_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_INVOICES_STATUS)),
    __metadata("design:type", String)
], ListMerchantInvoiceInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'get by merchant',
    }),
    (0, class_validator_1.IsUUID)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceInput.prototype, "merchant_id", void 0);
exports.ListMerchantInvoiceInput = ListMerchantInvoiceInput;
class ListMerchantInvoiceValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'id',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'merchant_id',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "merchant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'created_at',
        example: '1678098869024',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "start_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Finish at',
        example: '1678098869024',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "finish_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Count pending',
        example: 'Count pending',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "count_pending", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Count complete',
        example: 'Complete',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "count_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount pending',
        example: 'Amount pending',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "amount_pending", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount complete',
        example: 'Amount complete',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "amount_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount commission pending',
        example: 'Amount commission pending',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "amount_commission_pending", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Amount commission complete',
        example: 'Amount commission complete',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "amount_commission_complete", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${types_1.MERCHANT_INVOICES_STATUS.COMPLETED} or ${types_1.MERCHANT_INVOICES_STATUS.WAITING_PAYMENT} or ${types_1.MERCHANT_INVOICES_STATUS.WALLET_INVALID}`,
        example: types_1.MERCHANT_INVOICES_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_INVOICES_STATUS)),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Transaction id',
        example: 'TRC20',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "transaction_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Wallet address',
        example: 'TRC20',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "wallet_address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Description',
        example: 'Description',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Created at',
        example: '1678098869024',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Updated at',
        example: '1678098869024',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Updated by',
        example: 'Updated by',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ListMerchantInvoiceValidate.prototype, "updated_by", void 0);
exports.ListMerchantInvoiceValidate = ListMerchantInvoiceValidate;
class PagingListMerchantInvoiceOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Page',
        example: 1,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantInvoiceOutput.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Size',
        example: 50,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantInvoiceOutput.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantInvoiceOutput.prototype, "count", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'count item of page',
        example: 30,
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PagingListMerchantInvoiceOutput.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: ListMerchantInvoiceValidate,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => ListMerchantInvoiceValidate),
    __metadata("design:type", Array)
], PagingListMerchantInvoiceOutput.prototype, "rows", void 0);
exports.PagingListMerchantInvoiceOutput = PagingListMerchantInvoiceOutput;
class ListMerchantInvoiceOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Result transaction paging',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", PagingListMerchantInvoiceOutput)
], ListMerchantInvoiceOutput.prototype, "payload", void 0);
exports.ListMerchantInvoiceOutput = ListMerchantInvoiceOutput;
//# sourceMappingURL=validate.js.map