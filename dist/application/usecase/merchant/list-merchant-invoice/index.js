"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListMerchantInvoiceHandler = void 0;
const common_1 = require("@nestjs/common");
const merchant_invoice_repository_1 = require("../../../../infrastructure/data/database/merchant-invoice.repository");
let ListMerchantInvoiceHandler = class ListMerchantInvoiceHandler {
    constructor(merchantInvoiceRepository) {
        this.merchantInvoiceRepository = merchantInvoiceRepository;
    }
    async execute(param) {
        const dataRes = await this.merchantInvoiceRepository.getPaging(Object.assign({}, param));
        return {
            payload: dataRes,
        };
    }
};
ListMerchantInvoiceHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_invoice_repository_1.MerchantInvoiceRepository)),
    __metadata("design:paramtypes", [merchant_invoice_repository_1.MerchantInvoiceRepository])
], ListMerchantInvoiceHandler);
exports.ListMerchantInvoiceHandler = ListMerchantInvoiceHandler;
//# sourceMappingURL=index.js.map