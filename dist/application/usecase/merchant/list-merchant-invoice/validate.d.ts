import { MERCHANT_INVOICES_STATUS } from 'src/domain/invoices/types';
export declare class ListMerchantInvoiceInput {
    keyword: string;
    page: number;
    size: number;
    from: number;
    to: number;
    sort_by: string;
    order_by: string;
    status?: MERCHANT_INVOICES_STATUS;
    merchant_id: string;
}
export declare class ListMerchantInvoiceValidate {
    id: string;
    merchant_id: string;
    start_at: string;
    finish_at: string;
    count_pending: string;
    count_complete: string;
    amount_pending: string;
    amount_complete: string;
    amount_commission_pending: string;
    amount_commission_complete: string;
    status?: MERCHANT_INVOICES_STATUS;
    transaction_id: string;
    wallet_address: string;
    description: string;
    created_at: string;
    updated_at: string;
    updated_by: string;
}
export declare class PagingListMerchantInvoiceOutput {
    page: number;
    size: number;
    count: number;
    total: number;
    rows: ListMerchantInvoiceValidate[];
}
export declare class ListMerchantInvoiceOutput {
    payload: PagingListMerchantInvoiceOutput;
}
