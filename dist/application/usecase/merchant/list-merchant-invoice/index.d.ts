import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { ListMerchantInvoiceInput, ListMerchantInvoiceOutput } from './validate';
export declare class ListMerchantInvoiceHandler {
    private merchantInvoiceRepository;
    constructor(merchantInvoiceRepository: MerchantInvoiceRepository);
    execute(param: ListMerchantInvoiceInput): Promise<ListMerchantInvoiceOutput>;
}
