"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantAddNumberBotOutput = exports.MerchantAddNumberBotDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class MerchantAddNumberBotDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Name of app setting',
        example: 'NUMBER_OF_TBOT',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantAddNumberBotDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Number',
        example: '1',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantAddNumberBotDto.prototype, "value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Description of app setting',
        example: '',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantAddNumberBotDto.prototype, "description", void 0);
exports.MerchantAddNumberBotDto = MerchantAddNumberBotDto;
class MerchantAddNumberBotOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantAddNumberBotOutput.prototype, "payload", void 0);
exports.MerchantAddNumberBotOutput = MerchantAddNumberBotOutput;
//# sourceMappingURL=validate.js.map