export declare class MerchantAddNumberBotDto {
    name: string;
    value: string;
    description: string;
}
export declare class MerchantAddNumberBotOutput {
    payload: boolean;
}
