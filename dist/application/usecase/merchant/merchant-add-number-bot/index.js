"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantAddBotForUserHandle = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/bot/types");
const setting_repository_1 = require("../../../../infrastructure/data/database/setting.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let MerchantAddBotForUserHandle = class MerchantAddBotForUserHandle {
    constructor(settingRepository, userRepository) {
        this.settingRepository = settingRepository;
        this.userRepository = userRepository;
    }
    async execute(params, user_id, ownerId) {
        const settingNumberOfBot = await this.settingRepository.findOneAppSetting({
            name: params.name,
            user_id: user_id,
        });
        const userBotConnected = await this.userRepository.findUserBotTradingSQL({ user_id, type: types_1.TBOT_TYPE.SELECT });
        const settingNumberOfBotUsed = Number(userBotConnected.length) || 0;
        if (settingNumberOfBot) {
            const valueAddBot = Number(params.value);
            if (settingNumberOfBotUsed > valueAddBot) {
                (0, throw_exception_1.badRequestError)('Value Bot no bigger than value Bot used', const_1.ERROR_CODE.BAD_REQUEST);
            }
            await this.settingRepository.saveAppSetting(Object.assign(Object.assign({}, params), { id: settingNumberOfBot.id, user_id: user_id, owner_created: ownerId }));
        }
        else {
            await this.settingRepository.saveAppSetting(Object.assign(Object.assign({}, params), { user_id: user_id, owner_created: ownerId }));
        }
        return {
            payload: true,
        };
    }
};
MerchantAddBotForUserHandle = __decorate([
    __param(0, (0, common_1.Inject)(setting_repository_1.SettingRepository)),
    __param(1, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [setting_repository_1.SettingRepository,
        user_repository_1.UserRepository])
], MerchantAddBotForUserHandle);
exports.MerchantAddBotForUserHandle = MerchantAddBotForUserHandle;
//# sourceMappingURL=index.js.map