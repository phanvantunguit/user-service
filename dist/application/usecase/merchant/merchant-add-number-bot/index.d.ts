import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MerchantAddNumberBotDto, MerchantAddNumberBotOutput } from './validate';
export declare class MerchantAddBotForUserHandle {
    private settingRepository;
    private userRepository;
    constructor(settingRepository: SettingRepository, userRepository: UserRepository);
    execute(params: MerchantAddNumberBotDto, user_id: string, ownerId: string): Promise<MerchantAddNumberBotOutput>;
}
