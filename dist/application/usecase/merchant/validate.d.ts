import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import { MERCHANT_STATUS } from 'src/domain/merchant/types';
export declare class MerchantModifyConfigValidate {
    logo_url: string;
}
export declare class AdminModifyMerchantConfigValidate extends MerchantModifyConfigValidate {
    permission?: string[];
    commission: number;
}
export declare class MerchantValidate {
    name: string;
    code: string;
    email: string;
    status: MERCHANT_STATUS;
    description: string;
    domain: string;
    config: AdminModifyMerchantConfigValidate;
}
export declare class AdminUpdateCommissionValidate {
    id?: string;
    asset_id: string;
    status?: MERCHANT_COMMISION_STATUS;
    commission: number;
}
export declare class MerchantUpdateCommissionValidate {
    id?: string;
    asset_id: string;
    status?: MERCHANT_COMMISION_STATUS;
}
