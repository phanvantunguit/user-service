"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateProfileHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/merchant/types");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let MerchantUpdateProfileHandler = class MerchantUpdateProfileHandler {
    constructor(merchantRepository) {
        this.merchantRepository = merchantRepository;
    }
    async execute(param, id) {
        const merchant = await this.merchantRepository.findById(id);
        if (!merchant) {
            (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
        }
        if (param['password']) {
            delete param['password'];
        }
        const data = {
            id,
        };
        if (param.config) {
            for (let key of types_1.MERCHANT_CONFIG_ONLY_ADMIN) {
                delete param.config[key];
            }
            for (let key of types_1.MERCHANT_CONFIG_ONLY_SYSTEM) {
                delete param.config[key];
            }
            data['config'] = Object.assign(Object.assign({}, merchant.config), param.config);
        }
        await this.merchantRepository.save(data);
        return {
            payload: true,
        };
    }
};
MerchantUpdateProfileHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [merchant_repository_1.MerchantRepository])
], MerchantUpdateProfileHandler);
exports.MerchantUpdateProfileHandler = MerchantUpdateProfileHandler;
//# sourceMappingURL=index.js.map