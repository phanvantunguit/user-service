import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MerchantUpdateProfileInput, MerchantUpdateProfileOutput } from './validate';
export declare class MerchantUpdateProfileHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(param: MerchantUpdateProfileInput, id: string): Promise<MerchantUpdateProfileOutput>;
}
