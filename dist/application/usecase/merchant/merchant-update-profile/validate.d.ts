import { MerchantModifyConfigValidate } from '../validate';
export declare class MerchantUpdateProfileInput {
    config: MerchantModifyConfigValidate;
}
export declare class MerchantUpdateProfileOutput {
    payload: boolean;
}
