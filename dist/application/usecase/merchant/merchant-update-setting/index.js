"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateStatusHandle = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/transaction/types");
const bot_trading_repository_1 = require("../../../../infrastructure/data/database/bot-trading.repository");
const user_repository_1 = require("../../../../infrastructure/data/database/user.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let MerchantUpdateStatusHandle = class MerchantUpdateStatusHandle {
    constructor(userRepository, botTradingRepository) {
        this.userRepository = userRepository;
        this.botTradingRepository = botTradingRepository;
    }
    async execute(params, id, owner_created) {
        const userBot = await this.userRepository.findByAssetIds([id]);
        if (!userBot[0]) {
            (0, throw_exception_1.badRequestError)('Bot', const_1.ERROR_CODE.NOT_FOUND);
        }
        const bot = await this.botTradingRepository.findById(userBot[0].bot_id);
        const dataLog = {
            asset_id: userBot[0].bot_id,
            user_id: userBot[0].user_id,
            category: types_1.ORDER_CATEGORY.TBOT,
            status: params.status,
            owner_created,
            name: bot.name
        };
        await this.userRepository.saveUserAssetLog([dataLog]);
        await this.userRepository.saveStatusBotTrading({
            id,
            status: params.status,
        });
        return {
            payload: true,
        };
    }
};
MerchantUpdateStatusHandle = __decorate([
    __param(0, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __param(1, (0, common_1.Inject)(bot_trading_repository_1.BotTradingRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository,
        bot_trading_repository_1.BotTradingRepository])
], MerchantUpdateStatusHandle);
exports.MerchantUpdateStatusHandle = MerchantUpdateStatusHandle;
//# sourceMappingURL=index.js.map