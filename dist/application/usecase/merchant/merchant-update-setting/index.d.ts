import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MerchantUpdateStatusInput, MerchantUpdateStatusInputOutput } from './validate';
export declare class MerchantUpdateStatusHandle {
    private userRepository;
    private botTradingRepository;
    constructor(userRepository: UserRepository, botTradingRepository: BotTradingRepository);
    execute(params: MerchantUpdateStatusInput, id: string, owner_created: string): Promise<MerchantUpdateStatusInputOutput>;
}
