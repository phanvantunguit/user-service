import { MerchantCommissionRepository } from 'src/infrastructure/data/database/merchant-commission.repository';
import { AdminUpdateMerchantOutput } from '../admin-update-merchant/validate';
import { MerchantUpdateCommissionInput, AdminUpdateCommissionInput } from './validate';
export declare class UpdateMerchantCommissionHandler {
    private merchantCommissionRepository;
    constructor(merchantCommissionRepository: MerchantCommissionRepository);
    execute(param: AdminUpdateCommissionInput | MerchantUpdateCommissionInput, id: string, userId?: string): Promise<AdminUpdateMerchantOutput>;
}
