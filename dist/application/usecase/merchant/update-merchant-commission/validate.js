"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateCommissionInput = exports.AdminUpdateCommissionInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/transaction/types");
const validate_1 = require("../validate");
class AdminUpdateCommissionInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: `${Object.keys(types_1.ORDER_CATEGORY).join(',')}`,
        example: types_1.ORDER_CATEGORY.TBOT,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.ORDER_CATEGORY)),
    __metadata("design:type", String)
], AdminUpdateCommissionInput.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        isArray: true,
        type: validate_1.AdminUpdateCommissionValidate,
    }),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => validate_1.AdminUpdateCommissionValidate),
    __metadata("design:type", Array)
], AdminUpdateCommissionInput.prototype, "data", void 0);
exports.AdminUpdateCommissionInput = AdminUpdateCommissionInput;
class MerchantUpdateCommissionInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: `${Object.keys(types_1.ORDER_CATEGORY).join(',')}`,
        example: types_1.ORDER_CATEGORY.TBOT,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.ORDER_CATEGORY)),
    __metadata("design:type", String)
], MerchantUpdateCommissionInput.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        isArray: true,
        type: validate_1.MerchantUpdateCommissionValidate,
    }),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => validate_1.MerchantUpdateCommissionValidate),
    __metadata("design:type", Array)
], MerchantUpdateCommissionInput.prototype, "data", void 0);
exports.MerchantUpdateCommissionInput = MerchantUpdateCommissionInput;
//# sourceMappingURL=validate.js.map