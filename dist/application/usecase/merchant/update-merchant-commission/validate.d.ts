import { ORDER_CATEGORY } from 'src/domain/transaction/types';
import { AdminUpdateCommissionValidate, MerchantUpdateCommissionValidate } from '../validate';
export declare class AdminUpdateCommissionInput {
    category: ORDER_CATEGORY;
    data: AdminUpdateCommissionValidate[];
}
export declare class MerchantUpdateCommissionInput {
    category: ORDER_CATEGORY;
    data: MerchantUpdateCommissionValidate[];
}
