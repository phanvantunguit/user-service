import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MerchantUpdatePasswordInput, MerchantUpdatePasswordOutput } from './validate';
export declare class MerchantUpdatePasswordHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(param: MerchantUpdatePasswordInput, userId: string): Promise<MerchantUpdatePasswordOutput>;
}
