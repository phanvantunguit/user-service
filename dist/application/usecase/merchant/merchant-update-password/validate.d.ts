export declare class MerchantUpdatePasswordInput {
    current_password: string;
    new_password: string;
}
export declare class MerchantUpdatePasswordOutput {
    payload: boolean;
}
