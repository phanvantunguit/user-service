import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { AdminUpdateMerchantOutput } from '../admin-update-merchant/validate';
export declare class ResetMerchantPasswordHandler {
    private merchantRepository;
    private mailService;
    constructor(merchantRepository: MerchantRepository, mailService: MailService);
    execute(id: string, user_id: string): Promise<AdminUpdateMerchantOutput>;
}
