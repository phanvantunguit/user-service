import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { GetMerchantOutput } from './validate';
export declare class GetMerchantHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(id: string): Promise<GetMerchantOutput>;
}
