import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { GetNumberOfBotForUserOutput } from './validate';
export declare class GetNumberOfBotUserHandler {
    private settingRepository;
    constructor(settingRepository: SettingRepository);
    execute(user_id: string, owner_created: string): Promise<GetNumberOfBotForUserOutput>;
}
