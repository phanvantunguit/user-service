"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetNumberOfBotForUserOutput = exports.GetNumberOfBotForUser = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class GetNumberOfBotForUser {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'id',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], GetNumberOfBotForUser.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Userid',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], GetNumberOfBotForUser.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Owner created',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], GetNumberOfBotForUser.prototype, "owner_created", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'name',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], GetNumberOfBotForUser.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'value number of bot',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", String)
], GetNumberOfBotForUser.prototype, "value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'description',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], GetNumberOfBotForUser.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'created at',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], GetNumberOfBotForUser.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'created at',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Number)
], GetNumberOfBotForUser.prototype, "updated_at", void 0);
exports.GetNumberOfBotForUser = GetNumberOfBotForUser;
class GetNumberOfBotForUserOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Get number of bot',
    }),
    (0, class_validator_1.IsObject)(),
    __metadata("design:type", GetNumberOfBotForUser)
], GetNumberOfBotForUserOutput.prototype, "payload", void 0);
exports.GetNumberOfBotForUserOutput = GetNumberOfBotForUserOutput;
//# sourceMappingURL=validate.js.map