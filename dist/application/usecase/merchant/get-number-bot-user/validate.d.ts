export declare class GetNumberOfBotForUser {
    id?: string;
    user_id?: string;
    owner_created?: string;
    name?: string;
    value?: string;
    description?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class GetNumberOfBotForUserOutput {
    payload: GetNumberOfBotForUser;
}
