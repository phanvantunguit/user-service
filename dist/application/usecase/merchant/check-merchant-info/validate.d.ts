export declare class CheckMerchantInfoInput {
    id?: string;
    name?: string;
    code?: string;
    email?: string;
}
export declare class CheckMerchantInfoOutput {
    payload: boolean;
}
