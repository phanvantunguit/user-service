import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { CheckMerchantInfoInput, CheckMerchantInfoOutput } from './validate';
export declare class CheckMerchantInfoHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(param: CheckMerchantInfoInput): Promise<CheckMerchantInfoOutput>;
}
