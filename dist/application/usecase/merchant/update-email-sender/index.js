"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateEmailSenderHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/merchant/types");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let UpdateEmailSenderHandler = class UpdateEmailSenderHandler {
    constructor(mailService, merchantRepository) {
        this.mailService = mailService;
        this.merchantRepository = merchantRepository;
    }
    async execute(param, id, userId) {
        const { email, name, country, city, address } = param;
        const merchant = await this.merchantRepository.findById(id);
        if (!merchant) {
            (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
        }
        if (merchant.config['domain_type'] === types_1.MERCHANT_TYPE.OTHERS) {
            const config = merchant.config;
            const dataSender = {
                nickname: name,
                from_email: email,
                from_name: name,
                reply_to: email,
                address,
                city,
                country,
            };
            let resSerder;
            if (config['email_sender'] &&
                config['email_sender']['from_email'] &&
                config['email_sender']['id']) {
                if (config['email_sender']['from_email'] === email) {
                    resSerder = await this.mailService.updateSender(Object.assign(Object.assign({}, dataSender), { id: config['email_sender']['id'] }));
                }
                else {
                    await this.mailService.deleteSender({
                        id: config['email_sender']['id'],
                    });
                    resSerder = await this.mailService.sendRequestVerifySender(dataSender);
                    config['verified_sender'] = false;
                }
            }
            else {
                resSerder = await this.mailService.sendRequestVerifySender(dataSender);
                config['verified_sender'] = false;
            }
            if (resSerder) {
                if (resSerder.id) {
                    dataSender['id'] = resSerder.id;
                }
                config['email_sender'] = dataSender;
                const saveConfig = {
                    id,
                    config,
                };
                if (userId) {
                    saveConfig['updated_by'] = userId;
                }
                await this.merchantRepository.save(saveConfig);
                return true;
            }
            (0, throw_exception_1.badRequestError)('Serder', const_1.ERROR_CODE.INVALID);
        }
        (0, throw_exception_1.badRequestError)('KOL', const_1.ERROR_CODE.BAD_REQUEST);
    }
};
UpdateEmailSenderHandler = __decorate([
    __param(0, (0, common_1.Inject)(services_1.MailService)),
    __param(1, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [services_1.MailService,
        merchant_repository_1.MerchantRepository])
], UpdateEmailSenderHandler);
exports.UpdateEmailSenderHandler = UpdateEmailSenderHandler;
//# sourceMappingURL=index.js.map