import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { UpdateEmailSenderInput } from './validate';
export declare class UpdateEmailSenderHandler {
    private mailService;
    private merchantRepository;
    constructor(mailService: MailService, merchantRepository: MerchantRepository);
    execute(param: UpdateEmailSenderInput, id: string, userId?: string): Promise<any>;
}
