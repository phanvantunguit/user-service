export declare class UpdateEmailSenderInput {
    email: string;
    name: string;
    country: string;
    city: string;
    address: string;
}
