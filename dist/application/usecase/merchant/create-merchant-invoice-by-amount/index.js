"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMerchantInvoiceByAmountHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/merchant/types");
const merchant_invoice_repository_1 = require("../../../../infrastructure/data/database/merchant-invoice.repository");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const transaction_repository_1 = require("../../../../infrastructure/data/database/transaction.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const config_1 = require("../../../../config");
let CreateMerchantInvoiceByAmountHandler = class CreateMerchantInvoiceByAmountHandler {
    constructor(merchantInvoiceRepository, transactionRepository, merchantRepository) {
        this.merchantInvoiceRepository = merchantInvoiceRepository;
        this.transactionRepository = transactionRepository;
        this.merchantRepository = merchantRepository;
    }
    async execute(param, merchant_id, merchant_code, user_id) {
        var _a, _b, _c, _d;
        const { amount } = param;
        if (amount < config_1.APP_CONFIG.MIN_INVOICE_AMOUNT) {
            const error = {
                error_code: const_1.ERROR_CODE.INVALID.error_code,
                message: `minimum is ${config_1.APP_CONFIG.MIN_INVOICE_AMOUNT} USD`,
            };
            (0, throw_exception_1.badRequestError)('Amount', error);
        }
        const merchant = await this.merchantRepository.findById(merchant_id);
        if (!merchant_code) {
            if (!merchant) {
                (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
            }
            merchant_code = merchant.code;
        }
        const invoiceRepository = await this.merchantInvoiceRepository.findOne([
            {
                merchant_id,
                status: types_1.MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
            },
            {
                merchant_id,
                status: types_1.MERCHANT_INVOICE_STATUS.WALLET_INVALID,
            },
        ]);
        if (invoiceRepository) {
            (0, throw_exception_1.badRequestError)('Invoice', const_1.ERROR_CODE.EXISTED);
        }
        const [totalCommission, totalAmountInvoice] = await Promise.all([
            this.transactionRepository.totalCommission({ merchant_code }),
            this.merchantInvoiceRepository.totalCommission({ merchant_id }),
        ]);
        const amountAvailable = Number(totalCommission[0].total) + Number(totalAmountInvoice[0].total);
        if (Number(amountAvailable.toFixed(2)) < +Number(amount)) {
            (0, throw_exception_1.badRequestError)('Amount', const_1.ERROR_CODE.INVALID);
        }
        const invoice = {
            merchant_id: merchant_id,
            amount_commission_complete: Number(amount),
            status: types_1.MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
            updated_by: user_id,
            wallet_address: (_b = (_a = merchant.config) === null || _a === void 0 ? void 0 : _a.wallet) === null || _b === void 0 ? void 0 : _b.wallet_address,
            wallet_network: (_d = (_c = merchant.config) === null || _c === void 0 ? void 0 : _c.wallet) === null || _d === void 0 ? void 0 : _d.network,
            created_at: Date.now()
        };
        const invoiceCreate = Object.assign(Object.assign({}, invoice), { metadata: {
                histories: [invoice],
            } });
        const saved = await this.merchantInvoiceRepository.save(invoiceCreate);
        return {
            payload: saved.id,
        };
    }
};
CreateMerchantInvoiceByAmountHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_invoice_repository_1.MerchantInvoiceRepository)),
    __param(1, (0, common_1.Inject)(transaction_repository_1.TransactionRepository)),
    __param(2, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [merchant_invoice_repository_1.MerchantInvoiceRepository,
        transaction_repository_1.TransactionRepository,
        merchant_repository_1.MerchantRepository])
], CreateMerchantInvoiceByAmountHandler);
exports.CreateMerchantInvoiceByAmountHandler = CreateMerchantInvoiceByAmountHandler;
//# sourceMappingURL=index.js.map