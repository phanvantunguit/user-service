import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { CreateMerchantInvoiceByAmountInput } from './validate';
import { BaseCreateOutput } from '../../validate';
export declare class CreateMerchantInvoiceByAmountHandler {
    private merchantInvoiceRepository;
    private transactionRepository;
    private merchantRepository;
    constructor(merchantInvoiceRepository: MerchantInvoiceRepository, transactionRepository: TransactionRepository, merchantRepository: MerchantRepository);
    execute(param: CreateMerchantInvoiceByAmountInput, merchant_id: string, merchant_code?: string, user_id?: string): Promise<BaseCreateOutput>;
}
