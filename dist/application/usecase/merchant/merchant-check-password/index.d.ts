import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MerchantVerifyPasswordInput, MerchantVerifyPasswordOutput } from './validate';
export declare class MerchantVerifyPasswordHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(param: MerchantVerifyPasswordInput, id: string): Promise<MerchantVerifyPasswordOutput>;
}
