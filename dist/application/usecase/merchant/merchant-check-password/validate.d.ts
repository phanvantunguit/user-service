export declare class MerchantVerifyPasswordInput {
    password: string;
}
export declare class MerchantVerifyPasswordOutput {
    payload: boolean;
}
