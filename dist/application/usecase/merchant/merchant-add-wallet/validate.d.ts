import { MerchantAddConfigWallet } from '../validate';
export declare class MerchantAddWallet {
    config: MerchantAddConfigWallet;
}
export declare class MerchantAddWalletOutput {
    payload: boolean;
}
