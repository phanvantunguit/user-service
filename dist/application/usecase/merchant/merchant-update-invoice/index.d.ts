import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MerchantUpdateInvoiceInput, MerchantUpdateInvoiceOutput } from './validate';
export declare class UpdateMerchantInvoiceHandler {
    private merchantInvoiceRepository;
    private merchantRepository;
    constructor(merchantInvoiceRepository: MerchantInvoiceRepository, merchantRepository: MerchantRepository);
    execute(invoice_id: string, param: MerchantUpdateInvoiceInput): Promise<MerchantUpdateInvoiceOutput>;
}
