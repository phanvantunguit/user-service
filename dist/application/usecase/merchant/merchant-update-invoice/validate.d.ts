import { MERCHANT_INVOICE_STATUS, MERCHANT_WALLET_TYPE } from 'src/domain/merchant/types';
export declare class MerchantUpdateInvoiceInput {
    status: MERCHANT_INVOICE_STATUS;
    transaction_id: string;
    description: string;
    type: MERCHANT_WALLET_TYPE;
}
export declare class MerchantUpdateInvoiceOutput {
    payload: boolean;
}
