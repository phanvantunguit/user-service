"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateMerchantInvoiceHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/invoices/types");
const types_2 = require("../../../../domain/merchant/types");
const merchant_invoice_repository_1 = require("../../../../infrastructure/data/database/merchant-invoice.repository");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const axios_1 = require("axios");
const config_1 = require("../../../../config");
const TronWeb = require('tronweb');
var crypto = require('crypto');
var privateKey = crypto.randomBytes(32).toString('hex');
const HttpProvider = TronWeb.providers.HttpProvider;
const fullNode = new HttpProvider('https://api.trongrid.io');
const solidityNode = new HttpProvider('https://api.trongrid.io');
const eventServer = new HttpProvider('https://api.trongrid.io');
const tronWeb = new TronWeb(fullNode, solidityNode, eventServer, privateKey);
let UpdateMerchantInvoiceHandler = class UpdateMerchantInvoiceHandler {
    constructor(merchantInvoiceRepository, merchantRepository) {
        this.merchantInvoiceRepository = merchantInvoiceRepository;
        this.merchantRepository = merchantRepository;
    }
    async execute(invoice_id, param) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r;
        const invoice = await this.merchantInvoiceRepository.findById(invoice_id);
        if (!invoice) {
            (0, throw_exception_1.badRequestError)('Invoice', const_1.ERROR_CODE.NOT_FOUND);
        }
        const histories = ((_a = invoice.metadata) === null || _a === void 0 ? void 0 : _a.histories) || [];
        delete invoice.metadata;
        if ((invoice === null || invoice === void 0 ? void 0 : invoice.status) == types_1.MERCHANT_INVOICES_STATUS.COMPLETED) {
            delete param.transaction_id;
            delete param.status;
            param['id'] = invoice_id;
            const dataUpdate = Object.assign(Object.assign({}, invoice), { description: param.description, created_at: Date.now(), updated_at: Date.now() });
            histories.push(dataUpdate);
            param['metadata'] = {
                histories,
            };
            await this.merchantInvoiceRepository.save(param);
        }
        else {
            if (param.status != 'COMPLETED') {
                delete param.transaction_id;
                param['id'] = invoice_id;
                const dataUpdate = Object.assign(Object.assign({}, invoice), { description: param.description, status: param.status, created_at: Date.now(), updated_at: Date.now() });
                histories.push(dataUpdate);
                param['metadata'] = {
                    histories,
                };
                await this.merchantInvoiceRepository.save(param);
            }
            else {
                const TransactionExist = await this.merchantInvoiceRepository.getTransaction(param.transaction_id);
                if (TransactionExist[0].count != '0') {
                    (0, throw_exception_1.badRequestError)('Transaction was', const_1.ERROR_CODE.EXISTED);
                }
                if (!invoice) {
                    (0, throw_exception_1.badRequestError)('Invoice', const_1.ERROR_CODE.NOT_FOUND);
                }
                const merchant = await this.merchantRepository.findById(invoice === null || invoice === void 0 ? void 0 : invoice.merchant_id);
                if (((_b = merchant.config) === null || _b === void 0 ? void 0 : _b.status) == types_2.MERCHANT_STATUS.INACTIVE) {
                    (0, throw_exception_1.badRequestError)('Wallet', const_1.ERROR_CODE.INVALID);
                }
                const hexToDecimal = (hex) => parseInt(hex, 16);
                if (param.type == types_2.MERCHANT_WALLET_TYPE.TRC20) {
                    await tronWeb.trx
                        .getConfirmedTransaction(param.transaction_id)
                        .then((result) => {
                        var _a, _b, _c, _d, _e, _f, _g;
                        const walletAddress = tronWeb.address.fromHex('41' +
                            ((_b = (_a = result.raw_data.contract[0].parameter) === null || _a === void 0 ? void 0 : _a.value) === null || _b === void 0 ? void 0 : _b.data.slice(32, 72)));
                        if (walletAddress != (invoice === null || invoice === void 0 ? void 0 : invoice.wallet_address)) {
                            (0, throw_exception_1.badRequestError)('Wallet', const_1.ERROR_CODE.INVALID);
                        }
                        if (result.raw_data.timestamp < invoice.created_at) {
                            (0, throw_exception_1.badRequestError)('Transaction created before invoice', const_1.ERROR_CODE.BAD_REQUEST);
                        }
                        if (!((_d = (_c = result.raw_data.contract[0].parameter) === null || _c === void 0 ? void 0 : _c.value) === null || _d === void 0 ? void 0 : _d.data)) {
                            (0, throw_exception_1.badRequestError)('Transaction transfer value', const_1.ERROR_CODE.NOT_FOUND);
                        }
                        const decimalAmount = hexToDecimal((_f = (_e = result.raw_data.contract[0].parameter) === null || _e === void 0 ? void 0 : _e.value) === null || _f === void 0 ? void 0 : _f.data.slice(-64));
                        const amountCompele = Number(invoice.amount_commission_complete) * 1000000;
                        if (decimalAmount < amountCompele) {
                            (0, throw_exception_1.badRequestError)('The deposited amount is less than the commission amount', const_1.ERROR_CODE.BAD_REQUEST);
                        }
                        if (((_g = result === null || result === void 0 ? void 0 : result.ret[0]) === null || _g === void 0 ? void 0 : _g.contractRet) != 'SUCCESS') {
                            (0, throw_exception_1.badRequestError)('Confirmed Transaction', const_1.ERROR_CODE.INVALID);
                        }
                    })
                        .catch((error) => {
                        if (error.status)
                            (0, throw_exception_1.badRequestError)('', error);
                        (0, throw_exception_1.badRequestError)('Transaction', const_1.ERROR_CODE.NOT_FOUND);
                    });
                }
                else if (param.type == types_2.MERCHANT_WALLET_TYPE.BSC20) {
                    let walletRp = await axios_1.default.get(`https://api.bscscan.com/api?module=proxy&action=eth_getTransactionByHash&txhash=${param.transaction_id}&apikey=${config_1.APP_CONFIG.APIKEY_BSC20}`);
                    let walletFrom = (_d = (_c = walletRp.data) === null || _c === void 0 ? void 0 : _c.result) === null || _d === void 0 ? void 0 : _d.from;
                    let contractTo = (_f = (_e = walletRp.data) === null || _e === void 0 ? void 0 : _e.result) === null || _f === void 0 ? void 0 : _f.to;
                    let blockBumber = hexToDecimal((_h = (_g = walletRp.data) === null || _g === void 0 ? void 0 : _g.result) === null || _h === void 0 ? void 0 : _h.blockNumber);
                    let checkInvoid = await axios_1.default.get(`https://api.bscscan.com/api?module=account&action=tokentx&contractaddress=${contractTo}&address=${walletFrom}&page=0&offset=5&startblock=${blockBumber}&endblock=${blockBumber}&sort=asc&apikey=${config_1.APP_CONFIG.APIKEY_BSC20}`);
                    const walletAddress = (_j = checkInvoid.data) === null || _j === void 0 ? void 0 : _j.result[0].to;
                    if (walletAddress != (invoice === null || invoice === void 0 ? void 0 : invoice.wallet_address)) {
                        (0, throw_exception_1.badRequestError)('Wallet', const_1.ERROR_CODE.INVALID);
                    }
                    if (((_k = checkInvoid.data) === null || _k === void 0 ? void 0 : _k.result[0].timeStamp) * 1000 <
                        invoice.created_at) {
                        (0, throw_exception_1.badRequestError)('Transaction created before invoice', const_1.ERROR_CODE.BAD_REQUEST);
                    }
                    if (!((_m = (_l = checkInvoid.data) === null || _l === void 0 ? void 0 : _l.result[0]) === null || _m === void 0 ? void 0 : _m.value)) {
                        (0, throw_exception_1.badRequestError)('Transaction transfer value', const_1.ERROR_CODE.NOT_FOUND);
                    }
                    const amount = (_p = (_o = checkInvoid.data) === null || _o === void 0 ? void 0 : _o.result[0]) === null || _p === void 0 ? void 0 : _p.value;
                    const amountCompele = Number(invoice.amount_commission_complete) * 1000000000000000000;
                    if (amount < amountCompele) {
                        (0, throw_exception_1.badRequestError)('The deposited amount is less than the commission amount', const_1.ERROR_CODE.BAD_REQUEST);
                    }
                }
                invoice.status = param.status;
                invoice.transaction_id = param.transaction_id;
                invoice.description = param.description;
                invoice.wallet_address = (_r = (_q = merchant.config) === null || _q === void 0 ? void 0 : _q.wallet) === null || _r === void 0 ? void 0 : _r.wallet_address;
                const dataUpdate = Object.assign(Object.assign({}, invoice), { created_at: Date.now(), updated_at: Date.now() });
                histories.push(dataUpdate);
                param['metadata'] = {
                    histories,
                };
                await this.merchantInvoiceRepository.save(invoice);
            }
        }
        return {
            payload: true,
        };
    }
};
UpdateMerchantInvoiceHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_invoice_repository_1.MerchantInvoiceRepository)),
    __param(1, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [merchant_invoice_repository_1.MerchantInvoiceRepository,
        merchant_repository_1.MerchantRepository])
], UpdateMerchantInvoiceHandler);
exports.UpdateMerchantInvoiceHandler = UpdateMerchantInvoiceHandler;
//# sourceMappingURL=index.js.map