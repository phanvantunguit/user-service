"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateInvoiceOutput = exports.MerchantUpdateInvoiceInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/merchant/types");
class MerchantUpdateInvoiceInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: `${Object.keys(types_1.MERCHANT_INVOICE_STATUS).join(',')}`,
        example: types_1.MERCHANT_INVOICE_STATUS.COMPLETED,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_INVOICE_STATUS)),
    __metadata("design:type", String)
], MerchantUpdateInvoiceInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Transaction Id',
        example: '0daa9f2507c4e79e39391ea165bb76ed018c4cd69d7da129edf9e95f0dae99e2',
    }),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], MerchantUpdateInvoiceInput.prototype, "transaction_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Description',
        example: 'description',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantUpdateInvoiceInput.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: types_1.MERCHANT_WALLET_TYPE.TRC20,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_WALLET_TYPE)),
    __metadata("design:type", String)
], MerchantUpdateInvoiceInput.prototype, "type", void 0);
exports.MerchantUpdateInvoiceInput = MerchantUpdateInvoiceInput;
class MerchantUpdateInvoiceOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'true or false',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantUpdateInvoiceOutput.prototype, "payload", void 0);
exports.MerchantUpdateInvoiceOutput = MerchantUpdateInvoiceOutput;
//# sourceMappingURL=validate.js.map