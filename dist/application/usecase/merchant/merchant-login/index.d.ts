import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MerchantLoginInput, MerchantLoginOutput } from './validate';
export declare class MerchantLoginHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(param: MerchantLoginInput, merchantCode?: string): Promise<MerchantLoginOutput>;
}
