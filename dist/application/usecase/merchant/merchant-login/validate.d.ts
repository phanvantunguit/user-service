export declare class MerchantLoginInput {
    email: string;
    password: string;
}
export declare class MerchantLoginOutput {
    payload: string;
}
