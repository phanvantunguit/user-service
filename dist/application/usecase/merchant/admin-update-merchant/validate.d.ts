import { MERCHANT_STATUS } from 'src/domain/merchant/types';
import { AdminModifyMerchantConfigValidate } from '../validate';
export declare class AdminUpdateMerchantInput {
    name?: string;
    code?: string;
    email: string;
    status: MERCHANT_STATUS;
    description: string;
    config: AdminModifyMerchantConfigValidate;
    domain: string;
}
export declare class AdminUpdateMerchantOutput {
    payload: boolean;
}
