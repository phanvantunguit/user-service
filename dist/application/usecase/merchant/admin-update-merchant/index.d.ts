import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { CheckMerchantInfoHandler } from '../check-merchant-info';
import { AdminUpdateMerchantInput, AdminUpdateMerchantOutput } from './validate';
export declare class AdminUpdateMerchantHandler {
    private merchantRepository;
    private checkMerchantInfoHandler;
    constructor(merchantRepository: MerchantRepository, checkMerchantInfoHandler: CheckMerchantInfoHandler);
    execute(param: AdminUpdateMerchantInput, id: string, userId: string): Promise<AdminUpdateMerchantOutput>;
}
