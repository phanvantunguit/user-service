"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminUpdateMerchantOutput = exports.AdminUpdateMerchantInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../domain/merchant/types");
const validate_1 = require("../validate");
class AdminUpdateMerchantInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'name',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], AdminUpdateMerchantInput.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'code',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminUpdateMerchantInput.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'email',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], AdminUpdateMerchantInput.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${Object.keys(types_1.MERCHANT_STATUS).join(',')}`,
        example: types_1.MERCHANT_STATUS.ACTIVE,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminUpdateMerchantInput.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'code',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], AdminUpdateMerchantInput.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'config merchant',
    }),
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", validate_1.AdminModifyMerchantConfigValidate)
], AdminUpdateMerchantInput.prototype, "config", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'domain',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], AdminUpdateMerchantInput.prototype, "domain", void 0);
exports.AdminUpdateMerchantInput = AdminUpdateMerchantInput;
class AdminUpdateMerchantOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'true or false',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], AdminUpdateMerchantOutput.prototype, "payload", void 0);
exports.AdminUpdateMerchantOutput = AdminUpdateMerchantOutput;
//# sourceMappingURL=validate.js.map