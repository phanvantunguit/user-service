"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMerchantInvoiceHandler = void 0;
const common_1 = require("@nestjs/common");
const date_fns_tz_1 = require("date-fns-tz");
const date_fns_1 = require("date-fns");
const types_1 = require("../../../../domain/merchant/types");
const merchant_invoice_repository_1 = require("../../../../infrastructure/data/database/merchant-invoice.repository");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const transaction_repository_1 = require("../../../../infrastructure/data/database/transaction.repository");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
let CreateMerchantInvoiceHandler = class CreateMerchantInvoiceHandler {
    constructor(merchantInvoiceRepository, transactionRepository, merchantRepository) {
        this.merchantInvoiceRepository = merchantInvoiceRepository;
        this.transactionRepository = transactionRepository;
        this.merchantRepository = merchantRepository;
    }
    async execute(merchant_id, merchant_code, user_id) {
        console.log('merchant_id', merchant_id);
        return;
        const lastInvoice = await this.merchantInvoiceRepository.findOne({
            merchant_id,
        });
        if (!merchant_code) {
            const merchant = await this.merchantRepository.findById(merchant_id);
            if (!merchant) {
                (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.NOT_FOUND);
            }
            merchant_code = merchant.code;
        }
        console.log('lastInvoice', lastInvoice);
        let lastDayInvoice = lastInvoice
            ? (0, date_fns_1.startOfMonth)(Number(lastInvoice.created_at))
            : null;
        let from;
        if (lastDayInvoice) {
            from = (0, date_fns_tz_1.toDate)(lastDayInvoice, {
                timeZone: 'Asia/Ho_Chi_Minh',
            }).valueOf();
        }
        var currentDay = new Date();
        var lastDayOfMonth = new Date(currentDay.getFullYear(), currentDay.getMonth(), 0);
        const to = (0, date_fns_tz_1.toDate)((0, date_fns_1.endOfDay)(lastDayOfMonth), {
            timeZone: 'Asia/Ho_Chi_Minh',
        }).valueOf();
        console.log('CreateMerchantInvoiceHandler from:', from);
        console.log('CreateMerchantInvoiceHandler to:', to);
        const chart = await this.transactionRepository.chart({
            from,
            to,
            merchant_code,
            timezone: 'UTC-7',
            time_type: 'MONTH',
        });
        const dataMerchantInvoices = [];
        for (let report of chart) {
            if (Number(report.commission_cash) > 0) {
                dataMerchantInvoices.push({
                    merchant_id: merchant_id,
                    start_at: report.start_at,
                    finish_at: report.finish_at,
                    count_pending: Number(report.count_total) -
                        Number(report.count_complete) -
                        Number(report.count_failed),
                    count_complete: Number(report.count_complete),
                    amount_pending: Number(report.amount_total) -
                        Number(report.amount_complete) -
                        Number(report.amount_failed),
                    amount_complete: Number(report.amount_complete),
                    amount_commission_pending: Number(report.commission_cash_total) -
                        Number(report.commission_cash_failed) -
                        Number(report.commission_cash),
                    amount_commission_complete: Number(report.commission_cash),
                    status: types_1.MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
                    updated_by: user_id,
                });
            }
        }
        console.log('dataMerchantInvoices', dataMerchantInvoices);
        if (dataMerchantInvoices.length === 0) {
            return false;
        }
        const saved = await this.merchantInvoiceRepository.save(dataMerchantInvoices);
        console.log('CreateMerchantInvoiceHandler saved:', saved);
        return saved;
    }
};
CreateMerchantInvoiceHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_invoice_repository_1.MerchantInvoiceRepository)),
    __param(1, (0, common_1.Inject)(transaction_repository_1.TransactionRepository)),
    __param(2, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [merchant_invoice_repository_1.MerchantInvoiceRepository,
        transaction_repository_1.TransactionRepository,
        merchant_repository_1.MerchantRepository])
], CreateMerchantInvoiceHandler);
exports.CreateMerchantInvoiceHandler = CreateMerchantInvoiceHandler;
//# sourceMappingURL=index.js.map