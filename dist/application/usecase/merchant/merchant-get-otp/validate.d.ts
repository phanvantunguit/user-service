export declare class MerchantSendOtpInput {
    password: string;
}
export declare class MerchantSendOtpOutput {
    payload: boolean;
}
