import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MerchantSendOtpInput, MerchantSendOtpOutput } from './validate';
export declare class SendOtpWalletHandler {
    private merchantRepository;
    constructor(merchantRepository: MerchantRepository);
    execute(param: MerchantSendOtpInput, id: string): Promise<MerchantSendOtpOutput>;
}
