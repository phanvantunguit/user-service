"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantRemoveUserAssetHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../../domain/transaction/types");
const bot_trading_repository_1 = require("../../../../../infrastructure/data/database/bot-trading.repository");
const user_repository_1 = require("../../../../../infrastructure/data/database/user.repository");
const const_1 = require("../../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../../utils/exceptions/throw.exception");
const merchant_repository_1 = require("../../../../../infrastructure/data/database/merchant.repository");
const hash_util_1 = require("../../../../../utils/hash.util");
const services_1 = require("../../../../../infrastructure/services");
let MerchantRemoveUserAssetHandler = class MerchantRemoveUserAssetHandler {
    constructor(botTradingRepository, userRepository, merchantRepository, coinmapService) {
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
        this.merchantRepository = merchantRepository;
        this.coinmapService = coinmapService;
    }
    async execute(params, ownerCreated) {
        const { rows, password } = params;
        const merchant = await this.merchantRepository.findById(ownerCreated);
        if (!merchant) {
            (0, throw_exception_1.badRequestError)('id', const_1.ERROR_CODE.NOT_FOUND);
        }
        const checkPass = await (0, hash_util_1.comparePassword)(password, merchant.password);
        if (!checkPass) {
            (0, throw_exception_1.badRequestError)('Password', const_1.ERROR_CODE.INCORRECT);
        }
        const assetIds = [];
        rows.map((item) => assetIds.push(item.user_asset_id));
        const assetIdList = await this.userRepository.findByAssetIds(assetIds);
        if (assetIdList.length !== rows.length) {
            (0, throw_exception_1.badRequestError)('Asset', const_1.ERROR_CODE.BAD_REQUEST);
        }
        const botIds = [];
        assetIdList.map((item) => botIds.push(item.bot_id));
        const botIdList = await this.botTradingRepository.findByIds(botIds);
        const updateUserAssetLogs = [];
        for (let item of assetIdList) {
            try {
                const deleted = await this.coinmapService.deleteUserBotTrading({
                    id: item.id,
                    merchant_code: merchant.code,
                    password: merchant.password,
                });
                if (deleted.status) {
                    const updateUserAssetLog = {
                        user_id: item.user_id,
                        category: types_1.ORDER_CATEGORY.TBOT,
                        status: types_1.ITEM_STATUS.DELETED,
                        owner_created: ownerCreated,
                    };
                    let asset;
                    asset = botIdList.find((botId) => botId.id == item.bot_id);
                    updateUserAssetLog['name'] = asset.name;
                    updateUserAssetLog['asset_id'] = asset.id;
                    updateUserAssetLogs.push(updateUserAssetLog);
                }
            }
            catch (error) {
                console.log('check error', error);
            }
        }
        await this.userRepository.saveUserAssetLog(updateUserAssetLogs);
        return {
            payload: true,
        };
    }
};
MerchantRemoveUserAssetHandler = __decorate([
    __param(0, (0, common_1.Inject)(bot_trading_repository_1.BotTradingRepository)),
    __param(1, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __param(2, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __param(3, (0, common_1.Inject)(services_1.CoinmapService)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository,
        merchant_repository_1.MerchantRepository,
        services_1.CoinmapService])
], MerchantRemoveUserAssetHandler);
exports.MerchantRemoveUserAssetHandler = MerchantRemoveUserAssetHandler;
//# sourceMappingURL=index.js.map