"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantRemoveAssetOutput = exports.MerchantRemoveAssetInput = exports.MerchantRemoveUserAssetInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
class MerchantRemoveUserAssetInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantRemoveUserAssetInput.prototype, "user_asset_id", void 0);
exports.MerchantRemoveUserAssetInput = MerchantRemoveUserAssetInput;
class MerchantRemoveAssetInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: MerchantRemoveUserAssetInput,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => MerchantRemoveUserAssetInput),
    __metadata("design:type", Array)
], MerchantRemoveAssetInput.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: 'Merchant password',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantRemoveAssetInput.prototype, "password", void 0);
exports.MerchantRemoveAssetInput = MerchantRemoveAssetInput;
class MerchantRemoveAssetOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantRemoveAssetOutput.prototype, "payload", void 0);
exports.MerchantRemoveAssetOutput = MerchantRemoveAssetOutput;
//# sourceMappingURL=validate.js.map