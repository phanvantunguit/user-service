export declare class MerchantRemoveUserAssetInput {
    user_asset_id: string;
}
export declare class MerchantRemoveAssetInput {
    rows: MerchantRemoveUserAssetInput[];
    password: string;
}
export declare class MerchantRemoveAssetOutput {
    payload: boolean;
}
