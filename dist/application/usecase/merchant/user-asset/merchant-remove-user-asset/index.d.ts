import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MerchantRemoveAssetInput, MerchantRemoveAssetOutput } from './validate';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { CoinmapService } from 'src/infrastructure/services';
export declare class MerchantRemoveUserAssetHandler {
    private botTradingRepository;
    private userRepository;
    private merchantRepository;
    private coinmapService;
    constructor(botTradingRepository: BotTradingRepository, userRepository: UserRepository, merchantRepository: MerchantRepository, coinmapService: CoinmapService);
    execute(params: MerchantRemoveAssetInput, ownerCreated: string): Promise<MerchantRemoveAssetOutput>;
}
