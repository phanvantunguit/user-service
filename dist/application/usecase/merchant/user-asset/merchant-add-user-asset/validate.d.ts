import { PACKAGE_TYPE } from 'src/domain/commission/types';
export declare class AddUserAssetInput {
    asset_id: string;
    quantity: number;
    type: PACKAGE_TYPE;
}
export declare class MerchantCreateAssetInput {
    rows: AddUserAssetInput[];
}
export declare class MerchantCreateAssetOutput {
    payload: boolean;
}
