"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantCreateAssetOutput = exports.MerchantCreateAssetInput = exports.AddUserAssetInput = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../../../domain/commission/types");
class AddUserAssetInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Asset id',
        example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e5',
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], AddUserAssetInput.prototype, "asset_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'quantity of item',
    }),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AddUserAssetInput.prototype, "quantity", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'type of item',
        example: types_1.PACKAGE_TYPE.MONTH,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.PACKAGE_TYPE)),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AddUserAssetInput.prototype, "type", void 0);
exports.AddUserAssetInput = AddUserAssetInput;
class MerchantCreateAssetInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        isArray: true,
        type: AddUserAssetInput,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => AddUserAssetInput),
    __metadata("design:type", Array)
], MerchantCreateAssetInput.prototype, "rows", void 0);
exports.MerchantCreateAssetInput = MerchantCreateAssetInput;
class MerchantCreateAssetOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantCreateAssetOutput.prototype, "payload", void 0);
exports.MerchantCreateAssetOutput = MerchantCreateAssetOutput;
//# sourceMappingURL=validate.js.map