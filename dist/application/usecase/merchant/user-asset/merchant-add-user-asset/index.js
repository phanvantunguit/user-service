"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantAddUserAssetHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../../domain/bot/types");
const types_2 = require("../../../../../domain/commission/types");
const types_3 = require("../../../../../domain/transaction/types");
const bot_trading_repository_1 = require("../../../../../infrastructure/data/database/bot-trading.repository");
const user_repository_1 = require("../../../../../infrastructure/data/database/user.repository");
const const_1 = require("../../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../../utils/exceptions/throw.exception");
let MerchantAddUserAssetHandler = class MerchantAddUserAssetHandler {
    constructor(botTradingRepository, userRepository) {
        this.botTradingRepository = botTradingRepository;
        this.userRepository = userRepository;
    }
    async execute(params, ownerCreated, user_id) {
        const { rows } = params;
        const botIds = [...new Set(rows.map((e) => e.asset_id))];
        const botIdList = await this.botTradingRepository.findByIds(botIds);
        if (botIdList.length !== botIds.length) {
            (0, throw_exception_1.badRequestError)('Asset', const_1.ERROR_CODE.BAD_REQUEST);
        }
        const dataAdds = [];
        const updateUserAssetLogs = [];
        for (let item of rows) {
            try {
                let expiresAt = null;
                if (item.quantity) {
                    const expiredTime = new Date();
                    if (item.type === types_2.PACKAGE_TYPE.DAY) {
                        expiredTime.setDate(expiredTime.getDate() + Number(item.quantity));
                    }
                    else {
                        expiredTime.setMonth(expiredTime.getMonth() + Number(item.quantity));
                    }
                    expiresAt = expiredTime.valueOf();
                }
                const dataAdd = {
                    user_id: user_id,
                    expires_at: expiresAt,
                    owner_created: ownerCreated,
                    type: types_1.TBOT_TYPE.ASSIGN
                };
                const updateUserAssetLog = {
                    user_id: user_id,
                    category: types_3.ORDER_CATEGORY.TBOT,
                    status: types_3.ITEM_STATUS.NOT_CONNECT,
                    owner_created: ownerCreated,
                    expires_at: expiresAt,
                    quantity: item.quantity,
                    package_type: item.type,
                    type: types_1.TBOT_TYPE.ASSIGN
                };
                let asset;
                let user_asset;
                asset = botIdList.find((botId) => botId.id == item.asset_id);
                user_asset = await this.userRepository.findUserBotTrading({
                    user_id: user_id,
                    bot_id: item.asset_id,
                });
                dataAdd['bot_id'] = item.asset_id;
                if (asset) {
                    if (user_asset[0]) {
                        dataAdd['id'] = user_asset[0].id;
                    }
                    dataAdd['status'] = types_3.ITEM_STATUS.NOT_CONNECT;
                    updateUserAssetLog['name'] = asset.name;
                    dataAdds.push(dataAdd);
                }
                updateUserAssetLog['asset_id'] = asset.id;
                updateUserAssetLogs.push(updateUserAssetLog);
            }
            catch (error) { }
        }
        await this.userRepository.saveUserBotTrading(dataAdds);
        await this.userRepository.saveUserAssetLog(updateUserAssetLogs);
        return {
            payload: true,
        };
    }
};
MerchantAddUserAssetHandler = __decorate([
    __param(0, (0, common_1.Inject)(bot_trading_repository_1.BotTradingRepository)),
    __param(1, (0, common_1.Inject)(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [bot_trading_repository_1.BotTradingRepository,
        user_repository_1.UserRepository])
], MerchantAddUserAssetHandler);
exports.MerchantAddUserAssetHandler = MerchantAddUserAssetHandler;
//# sourceMappingURL=index.js.map