import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MerchantCreateAssetInput, MerchantCreateAssetOutput } from './validate';
export declare class MerchantAddUserAssetHandler {
    private botTradingRepository;
    private userRepository;
    constructor(botTradingRepository: BotTradingRepository, userRepository: UserRepository);
    execute(params: MerchantCreateAssetInput, ownerCreated: string, user_id: string): Promise<MerchantCreateAssetOutput>;
}
