import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { MerchantCreateInput, MerchantCreateOutput } from './validate';
export declare class CreateWalletHandler {
    private merchantRepository;
    private mailService;
    constructor(merchantRepository: MerchantRepository, mailService: MailService);
    execute(param: MerchantCreateInput, id: string): Promise<MerchantCreateOutput>;
}
