"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateWalletHandler = void 0;
const common_1 = require("@nestjs/common");
const types_1 = require("../../../../domain/merchant/types");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const hash_util_1 = require("../../../../utils/hash.util");
const axios_1 = require("axios");
const config_1 = require("../../../../config");
const tronWeb = require('tronweb');
let CreateWalletHandler = class CreateWalletHandler {
    constructor(merchantRepository, mailService) {
        this.merchantRepository = merchantRepository;
        this.mailService = mailService;
    }
    async execute(param, id) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j;
        const merchant = await this.merchantRepository.findById(id);
        if (!merchant) {
            (0, throw_exception_1.badRequestError)('id', const_1.ERROR_CODE.NOT_FOUND);
        }
        const checkPass = await (0, hash_util_1.comparePassword)(param.password, merchant.password);
        if (!checkPass) {
            (0, throw_exception_1.badRequestError)('Password', const_1.ERROR_CODE.INCORRECT);
        }
        if (param.otp != ((_b = (_a = merchant.config) === null || _a === void 0 ? void 0 : _a.wallet) === null || _b === void 0 ? void 0 : _b.otp)) {
            (0, throw_exception_1.badRequestError)('OTP', const_1.ERROR_CODE.INCORRECT);
        }
        const expiresTime = new Date().getTime();
        if (expiresTime > ((_d = (_c = merchant.config) === null || _c === void 0 ? void 0 : _c.wallet) === null || _d === void 0 ? void 0 : _d.expires_at)) {
            (0, throw_exception_1.badRequestError)('OTP', const_1.ERROR_CODE.EXPIRES);
        }
        if (param.type == 'BSC20') {
            const walletRp = await axios_1.default.get(`https://api.bscscan.com/api?module=account&action=balance&address=${param.wallet_address}&apikey=${config_1.APP_CONFIG.APIKEY_BSC20}`);
            if (((_e = walletRp === null || walletRp === void 0 ? void 0 : walletRp.data) === null || _e === void 0 ? void 0 : _e.message) != 'OK') {
                (0, throw_exception_1.badRequestError)('Type of wallet address must be', const_1.ERROR_CODE.NOT_TYPE_BSC20);
            }
        }
        else {
            const checkWallet = await tronWeb.isAddress(param.wallet_address);
            if (!checkWallet) {
                (0, throw_exception_1.badRequestError)('Type of wallet address must be', const_1.ERROR_CODE.NOT_TYPE_TRC20);
            }
        }
        if (param.wallet_address != ((_g = (_f = merchant.config) === null || _f === void 0 ? void 0 : _f.wallet) === null || _g === void 0 ? void 0 : _g.send_mail_wallet_address)) {
            (0, throw_exception_1.badRequestError)('Wallet address does not match the wallet address in the email.', const_1.ERROR_CODE.BAD_REQUEST);
        }
        merchant.config = Object.assign(Object.assign({}, merchant.config), { wallet: {
                wallet_address: param.wallet_address,
                status: types_1.MERCHANT_STATUS.ACTIVE,
                network: param.type,
                otp: null,
                expires_at: null,
            }, wallet_active_history: (_j = (_h = merchant.config) === null || _h === void 0 ? void 0 : _h.wallet_active_history) !== null && _j !== void 0 ? _j : [] });
        merchant.config.wallet_active_history.push({
            wallet_address: merchant.config.wallet.wallet_address,
            create_at: expiresTime,
        });
        delete merchant.password;
        await this.merchantRepository.save(merchant);
        return {
            payload: true,
        };
    }
};
CreateWalletHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __param(1, (0, common_1.Inject)(services_1.MailService)),
    __metadata("design:paramtypes", [merchant_repository_1.MerchantRepository,
        services_1.MailService])
], CreateWalletHandler);
exports.CreateWalletHandler = CreateWalletHandler;
//# sourceMappingURL=index.js.map