import { MERCHANT_WALLET_TYPE } from 'src/domain/merchant/types';
export declare class MerchantCreateInput {
    password: string;
    wallet_address: string;
    otp: string;
    type: MERCHANT_WALLET_TYPE;
}
export declare class MerchantCreateOutput {
    payload: boolean;
}
