"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantCreateOutput = exports.MerchantCreateInput = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const types_1 = require("../../../../domain/merchant/types");
class MerchantCreateInput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Input password',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantCreateInput.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Input wallet address',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantCreateInput.prototype, "wallet_address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Input OTP',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantCreateInput.prototype, "otp", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        example: types_1.MERCHANT_WALLET_TYPE.TRC20,
    }),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_WALLET_TYPE)),
    __metadata("design:type", String)
], MerchantCreateInput.prototype, "type", void 0);
exports.MerchantCreateInput = MerchantCreateInput;
class MerchantCreateOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Status',
        example: true,
    }),
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], MerchantCreateOutput.prototype, "payload", void 0);
exports.MerchantCreateOutput = MerchantCreateOutput;
//# sourceMappingURL=validate.js.map