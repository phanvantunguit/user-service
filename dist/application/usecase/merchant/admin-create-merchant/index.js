"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminCreateMerchantHandler = void 0;
const common_1 = require("@nestjs/common");
const merchant_domain_1 = require("../../../../domain/merchant/merchant.domain");
const types_1 = require("../../../../domain/merchant/types");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const services_1 = require("../../../../infrastructure/services");
const const_1 = require("../../../../utils/exceptions/const");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const hash_util_1 = require("../../../../utils/hash.util");
const check_merchant_info_1 = require("../check-merchant-info");
let AdminCreateMerchantHandler = class AdminCreateMerchantHandler {
    constructor(merchantRepository, checkMerchantInfoHandler, mailService) {
        this.merchantRepository = merchantRepository;
        this.checkMerchantInfoHandler = checkMerchantInfoHandler;
        this.mailService = mailService;
    }
    async execute(param, userId) {
        const checkInfo = await this.checkMerchantInfoHandler.execute(param);
        if (!checkInfo.payload) {
            (0, throw_exception_1.badRequestError)('Merchant', const_1.ERROR_CODE.INVALID);
        }
        if (param.domain) {
            const merchantExisted = await this.merchantRepository.findOne({
                domain: param.domain,
            });
            if (merchantExisted &&
                merchantExisted.config &&
                merchantExisted.config.domain_type === types_1.MERCHANT_TYPE.OTHERS) {
                (0, throw_exception_1.badRequestError)('Domain', const_1.ERROR_CODE.EXISTED);
            }
        }
        if (param.config) {
            for (let key of types_1.MERCHANT_CONFIG_ONLY_SYSTEM) {
                delete param.config[key];
            }
        }
        const password = (0, hash_util_1.generatePassword)();
        const data = new merchant_domain_1.MerchantDomain(Object.assign(Object.assign({}, param), { created_by: userId, updated_by: userId, password }));
        const result = await this.merchantRepository.save(data);
        await this.mailService.sendEmailCreateMerchant(param.email, password);
        return {
            payload: result.id,
        };
    }
};
AdminCreateMerchantHandler = __decorate([
    __param(0, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __param(1, (0, common_1.Inject)(check_merchant_info_1.CheckMerchantInfoHandler)),
    __param(2, (0, common_1.Inject)(services_1.MailService)),
    __metadata("design:paramtypes", [merchant_repository_1.MerchantRepository,
        check_merchant_info_1.CheckMerchantInfoHandler,
        services_1.MailService])
], AdminCreateMerchantHandler);
exports.AdminCreateMerchantHandler = AdminCreateMerchantHandler;
//# sourceMappingURL=index.js.map