import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { CheckMerchantInfoHandler } from '../check-merchant-info';
import { AdminCreateMerchantInput, AdminCreateMerchantOutput } from './validate';
export declare class AdminCreateMerchantHandler {
    private merchantRepository;
    private checkMerchantInfoHandler;
    private mailService;
    constructor(merchantRepository: MerchantRepository, checkMerchantInfoHandler: CheckMerchantInfoHandler, mailService: MailService);
    execute(param: AdminCreateMerchantInput, userId: string): Promise<AdminCreateMerchantOutput>;
}
