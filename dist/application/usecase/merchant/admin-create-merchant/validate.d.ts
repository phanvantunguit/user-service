import { MerchantValidate } from '../validate';
export declare class AdminCreateMerchantInput extends MerchantValidate {
}
export declare class AdminCreateMerchantOutput {
    payload: string;
}
