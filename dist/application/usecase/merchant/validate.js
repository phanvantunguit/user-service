"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantUpdateCommissionValidate = exports.AdminUpdateCommissionValidate = exports.MerchantValidate = exports.AdminModifyMerchantConfigValidate = exports.MerchantModifyConfigValidate = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const types_1 = require("../../../domain/commission/types");
const types_2 = require("../../../domain/merchant/types");
class MerchantModifyConfigValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'logo link',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantModifyConfigValidate.prototype, "logo_url", void 0);
exports.MerchantModifyConfigValidate = MerchantModifyConfigValidate;
class AdminModifyMerchantConfigValidate extends MerchantModifyConfigValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'permission',
        isArray: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_transformer_1.Type)(() => String),
    __metadata("design:type", Array)
], AdminModifyMerchantConfigValidate.prototype, "permission", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'commission for each transaction',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminModifyMerchantConfigValidate.prototype, "commission", void 0);
exports.AdminModifyMerchantConfigValidate = AdminModifyMerchantConfigValidate;
class MerchantValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'name',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'code',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'email',
    }),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: `${Object.keys(types_2.MERCHANT_STATUS).join(',')}`,
        example: types_2.MERCHANT_STATUS.ACTIVE,
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'code',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'domain',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], MerchantValidate.prototype, "domain", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'config merchant',
    }),
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", AdminModifyMerchantConfigValidate)
], MerchantValidate.prototype, "config", void 0);
exports.MerchantValidate = MerchantValidate;
class AdminUpdateCommissionValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'UUID of merchant commission',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], AdminUpdateCommissionValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of asset',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], AdminUpdateCommissionValidate.prototype, "asset_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${types_1.MERCHANT_COMMISION_STATUS.ACTIVE} or ${types_1.MERCHANT_COMMISION_STATUS.INACTIVE}`,
        example: types_1.MERCHANT_COMMISION_STATUS.ACTIVE,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_COMMISION_STATUS)),
    __metadata("design:type", String)
], AdminUpdateCommissionValidate.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: 'Percent commission: 0 to 1, example 0.05 is 5%',
        example: 0.1,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], AdminUpdateCommissionValidate.prototype, "commission", void 0);
exports.AdminUpdateCommissionValidate = AdminUpdateCommissionValidate;
class MerchantUpdateCommissionValidate {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of merchant commission',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], MerchantUpdateCommissionValidate.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'UUID of asset',
        example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
    }),
    (0, class_validator_1.IsUUID)(),
    __metadata("design:type", String)
], MerchantUpdateCommissionValidate.prototype, "asset_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: false,
        description: `${types_1.MERCHANT_COMMISION_STATUS.ACTIVE} or ${types_1.MERCHANT_COMMISION_STATUS.INACTIVE}`,
        example: types_1.MERCHANT_COMMISION_STATUS.ACTIVE,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(types_1.MERCHANT_COMMISION_STATUS)),
    __metadata("design:type", String)
], MerchantUpdateCommissionValidate.prototype, "status", void 0);
exports.MerchantUpdateCommissionValidate = MerchantUpdateCommissionValidate;
//# sourceMappingURL=validate.js.map