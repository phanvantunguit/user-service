import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
export declare class GetMerchantInvoiceHandler {
    private merchantInvoiceRepository;
    constructor(merchantInvoiceRepository: MerchantInvoiceRepository);
    execute(id: string): Promise<any>;
}
