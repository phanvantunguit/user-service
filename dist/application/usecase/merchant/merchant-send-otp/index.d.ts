import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { MerchantSendOtpInput, MerchantSendOtpOutput } from './validate';
export declare class SendOtpWalletHandler {
    private merchantRepository;
    private mailService;
    constructor(merchantRepository: MerchantRepository, mailService: MailService);
    execute(param: MerchantSendOtpInput, id: string): Promise<MerchantSendOtpOutput>;
}
