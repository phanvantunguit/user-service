import { MERCHANT_WALLET_TYPE } from 'src/domain/merchant/types';
export declare class MerchantSendOtpInput {
    password: string;
    wallet_address: string;
    type: MERCHANT_WALLET_TYPE;
}
export declare class MerchantSendOtpOutput {
    payload: boolean;
}
