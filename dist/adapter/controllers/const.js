"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROOT_ROUTE = void 0;
var ROOT_ROUTE;
(function (ROOT_ROUTE) {
    ROOT_ROUTE["api_v1_admin"] = "api/v1/admin";
    ROOT_ROUTE["api_v1"] = "api/v1";
    ROOT_ROUTE["api_v1_merchant"] = "api/v1/merchant";
})(ROOT_ROUTE = exports.ROOT_ROUTE || (exports.ROOT_ROUTE = {}));
//# sourceMappingURL=const.js.map