import { MerchantListBotTradingHandler } from 'src/application';
export declare class AdminV1BotTradingController {
    private merchantListBotTradingHandler;
    constructor(merchantListBotTradingHandler: MerchantListBotTradingHandler);
    listBot(): Promise<import("../../../../application/usecase/bot/validate").MerchantBotValidate[]>;
}
