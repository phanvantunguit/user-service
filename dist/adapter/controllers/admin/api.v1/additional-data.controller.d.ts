import { CreateAdditionalDataHandler } from 'src/application/usecase/additional-data/create-additional-data';
import { CreateAdditionalDataInput } from 'src/application/usecase/additional-data/create-additional-data/validate';
import { DeleteAdditionalDataHandler } from 'src/application/usecase/additional-data/delete-additional-data';
import { GetAdditionalDataHandler } from 'src/application/usecase/additional-data/get-additional-data';
import { ListAdditionalDataHandler } from 'src/application/usecase/additional-data/list-additional-data';
import { ListAdditionalDataInput } from 'src/application/usecase/additional-data/list-additional-data/validate';
import { UpdateAdditionalDataHandler } from 'src/application/usecase/additional-data/update-additional-data';
import { UpdateAdditionalDataInput } from 'src/application/usecase/additional-data/update-additional-data/validate';
export declare class AdminAdditionalDataController {
    private createAdditionalDataHandler;
    private updateAdditionalDataHandler;
    private deleteAdditionalDataHandler;
    private listAdditionalDataHandler;
    private getAdditionalDataHandler;
    constructor(createAdditionalDataHandler: CreateAdditionalDataHandler, updateAdditionalDataHandler: UpdateAdditionalDataHandler, deleteAdditionalDataHandler: DeleteAdditionalDataHandler, listAdditionalDataHandler: ListAdditionalDataHandler, getAdditionalDataHandler: GetAdditionalDataHandler);
    createAdditionalData(body: CreateAdditionalDataInput): Promise<string>;
    updateAdditionalData(id: string, body: UpdateAdditionalDataInput): Promise<boolean>;
    deleteAdditionalData(id: string): Promise<boolean>;
    listAdditionalData(query: ListAdditionalDataInput): Promise<import("../../../../application/usecase/additional-data/validate").AdditionalDataValidate[]>;
    getAdditionalData(id: string): Promise<import("../../../../application/usecase/additional-data/validate").AdditionalDataValidate>;
}
