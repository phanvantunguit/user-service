"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1EventController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const admin_list_event_1 = require("../../../../application/usecase/event/admin-list-event");
const validate_1 = require("../../../../application/usecase/event/admin-list-event/validate");
const admin_invite_1 = require("../../../../application/usecase/event/admin-invite");
const validate_2 = require("../../../../application/usecase/event/admin-invite/validate");
const validate_3 = require("../../../../application/usecase/event/admin-get-event/validate");
const admin_get_event_1 = require("../../../../application/usecase/event/admin-get-event");
const application_1 = require("../../../../application");
const validate_4 = require("../../../../application/usecase/event/admin-create-event/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
const platform_express_1 = require("@nestjs/platform-express");
const validate_5 = require("../../../../application/usecase/event/admin-invite-scv/validate");
const admin_update_status_invite_1 = require("../../../../application/usecase/event/admin-update-status-invite");
const validate_6 = require("../../../../application/usecase/event/admin-update-status-invite/validate");
const admin_invite_scv_1 = require("../../../../application/usecase/event/admin-invite-scv");
const validate_7 = require("../../../../application/usecase/event/admin-update-event/validate");
const validate_8 = require("../../../../application/usecase/event/remind-all-attend/validate");
const remind_all_attend_1 = require("../../../../application/usecase/event/remind-all-attend");
const validate_9 = require("../../../../application/usecase/event/admin-delete-event/validate");
const validate_10 = require("../../../../application/usecase/event/remind-all-dynamic/validate");
let AdminV1EventController = class AdminV1EventController {
    constructor(adminInviteEventHandler, adminListEventHandler, adminGetEventHandler, adminCreateEventHandler, adminUpdateStatusInviteEventHandler, adminInviteEventCSVHandler, adminUpdateEventHandler, remindAllAttendEventHandler, adminDeleteEventHandler, remindAllDynamicHandler) {
        this.adminInviteEventHandler = adminInviteEventHandler;
        this.adminListEventHandler = adminListEventHandler;
        this.adminGetEventHandler = adminGetEventHandler;
        this.adminCreateEventHandler = adminCreateEventHandler;
        this.adminUpdateStatusInviteEventHandler = adminUpdateStatusInviteEventHandler;
        this.adminInviteEventCSVHandler = adminInviteEventCSVHandler;
        this.adminUpdateEventHandler = adminUpdateEventHandler;
        this.remindAllAttendEventHandler = remindAllAttendEventHandler;
        this.adminDeleteEventHandler = adminDeleteEventHandler;
        this.remindAllDynamicHandler = remindAllDynamicHandler;
    }
    async inviteCSV(file, body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminInviteEventCSVHandler.execute(file, body, user_id);
        return result.payload;
    }
    async invite(body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminInviteEventHandler.execute(body, user_id);
        return result.payload;
    }
    async updateInvite(body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminUpdateStatusInviteEventHandler.execute(body, user_id);
        return result.payload;
    }
    async remind(body) {
        const result = await this.remindAllAttendEventHandler.execute(body);
        return result.payload;
    }
    async remindDynamic(body) {
        const result = await this.remindAllDynamicHandler.execute(body);
        return result.payload;
    }
    async create(body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminCreateEventHandler.execute(body, user_id);
        return result.payload;
    }
    async update(id, body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminUpdateEventHandler.execute(id, body, user_id);
        return result.payload;
    }
    async deleteEvent(param) {
        const result = await this.adminDeleteEventHandler.execute(param);
        return result.payload;
    }
    async listEvent(query) {
        const result = await this.adminListEventHandler.execute(query);
        return result.payload;
    }
    async getEvent(param) {
        const result = await this.adminGetEventHandler.execute(param);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Post)('invite-csv'),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        schema: {
            type: 'object',
            properties: {
                callback_confirm_url: { type: 'string' },
                callback_attend_url: { type: 'string' },
                event_id: { type: 'string' },
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminInviteEventCSVOutputMap extends (0, swagger_1.IntersectionType)(validate_5.AdminInviteEventCSVOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Save user and send email invite',
    }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file')),
    __param(0, (0, common_1.UploadedFile)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_5.AdminInviteEventCSVInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "inviteCSV", null);
__decorate([
    (0, common_1.Post)('invite'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminInviteEventOutputMap extends (0, swagger_1.IntersectionType)(validate_2.AdminInviteEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Save user and send email invite',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.AdminInviteEventInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "invite", null);
__decorate([
    (0, common_1.Put)('invite'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateStatusInviteEventOutputMap extends (0, swagger_1.IntersectionType)(validate_6.AdminUpdateStatusInviteEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Save user and send email invite',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_6.AdminUpdateStatusInviteEventInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "updateInvite", null);
__decorate([
    (0, common_1.Put)('/remind'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class RemindAllAttendEventOutputputMap extends (0, swagger_1.IntersectionType)(validate_8.RemindAllAttendEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Remind attend event',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_8.RemindAllAttendEventInput]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "remind", null);
__decorate([
    (0, common_1.Put)('/remind-dynaminc'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class RemindAllDynamicOutputMap extends (0, swagger_1.IntersectionType)(validate_10.RemindAllDynamicOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Remind attend event',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_10.RemindAllDynamicInput]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "remindDynamic", null);
__decorate([
    (0, common_1.Post)(''),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminCreateEventOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminCreateEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create new event',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_4.AdminCreateEventInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "create", null);
__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateEventOutputMap extends (0, swagger_1.IntersectionType)(validate_7.AdminUpdateEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update event',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_7.AdminUpdateEventInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminDeleteEventOutputMap extends (0, swagger_1.IntersectionType)(validate_9.AdminDeleteEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List event',
    }),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_9.AdminDeleteEventInput]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "deleteEvent", null);
__decorate([
    (0, common_1.Get)('list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminListEventOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminListEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List event',
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.AdminListEventInput]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "listEvent", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminGetEventOutputMap extends (0, swagger_1.IntersectionType)(validate_3.AdminGetEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Detail event',
    }),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.AdminGetEventInput]),
    __metadata("design:returntype", Promise)
], AdminV1EventController.prototype, "getEvent", null);
AdminV1EventController = __decorate([
    (0, swagger_1.ApiTags)('admin/event'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/event`),
    __param(0, (0, common_1.Inject)(admin_invite_1.AdminInviteEventHandler)),
    __param(1, (0, common_1.Inject)(admin_list_event_1.AdminListEventHandler)),
    __param(2, (0, common_1.Inject)(admin_get_event_1.AdminGetEventHandler)),
    __param(3, (0, common_1.Inject)(application_1.AdminCreateEventHandler)),
    __param(4, (0, common_1.Inject)(admin_update_status_invite_1.AdminUpdateStatusInviteEventHandler)),
    __param(5, (0, common_1.Inject)(admin_invite_scv_1.AdminInviteEventCSVHandler)),
    __param(6, (0, common_1.Inject)(application_1.AdminUpdateEventHandler)),
    __param(7, (0, common_1.Inject)(remind_all_attend_1.RemindAllAttendEventHandler)),
    __param(8, (0, common_1.Inject)(application_1.AdminDeleteEventHandler)),
    __param(9, (0, common_1.Inject)(application_1.RemindAllDynamicHandler)),
    __metadata("design:paramtypes", [admin_invite_1.AdminInviteEventHandler,
        admin_list_event_1.AdminListEventHandler,
        admin_get_event_1.AdminGetEventHandler,
        application_1.AdminCreateEventHandler,
        admin_update_status_invite_1.AdminUpdateStatusInviteEventHandler,
        admin_invite_scv_1.AdminInviteEventCSVHandler,
        application_1.AdminUpdateEventHandler,
        remind_all_attend_1.RemindAllAttendEventHandler,
        application_1.AdminDeleteEventHandler,
        application_1.RemindAllDynamicHandler])
], AdminV1EventController);
exports.AdminV1EventController = AdminV1EventController;
//# sourceMappingURL=event.controller.js.map