import { AdminGetUserHandler } from 'src/application/usecase/event/admin-get-user';
import { AdminGetUserInput } from 'src/application/usecase/event/admin-get-user/validate';
import { AdminListUserHandler } from 'src/application/usecase/event/admin-list-user';
import { AdminListUserInput } from 'src/application/usecase/event/admin-list-user/validate';
export declare class AdminV1UserController {
    private adminListUserHandler;
    private adminGetUserHandler;
    constructor(adminListUserHandler: AdminListUserHandler, adminGetUserHandler: AdminGetUserHandler);
    login(query: AdminListUserInput): Promise<import("src/application/usecase/event/admin-list-user/validate").PagingUserDataOutput>;
    getEvent(param: AdminGetUserInput): Promise<import("../../../../application/usecase/event/validate").UserDataValidate>;
}
