"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1AuthController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const admin_create_user_1 = require("../../../../application/usecase/auth/admin-create-user");
const validate_1 = require("../../../../application/usecase/auth/admin-create-user/validate");
const admin_login_1 = require("../../../../application/usecase/auth/admin-login");
const validate_2 = require("../../../../application/usecase/auth/admin-login/validate");
const verify_email_1 = require("../../../../application/usecase/auth/verify-email");
const validate_3 = require("../../../../application/usecase/auth/verify-email/validate");
const config_1 = require("../../../../config");
const types_1 = require("../../../../domain/auth/types");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminV1AuthController = class AdminV1AuthController {
    constructor(adminLoginHandler, adminCreateUserHandler, adminVerifyEmailHandler) {
        this.adminLoginHandler = adminLoginHandler;
        this.adminCreateUserHandler = adminCreateUserHandler;
        this.adminVerifyEmailHandler = adminVerifyEmailHandler;
    }
    async login(body) {
        const result = await this.adminLoginHandler.execute(body);
        return result.payload;
    }
    async create(body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminCreateUserHandler.execute(body, user_id);
        return result.payload;
    }
    async verify(param, res) {
        await this.adminVerifyEmailHandler.execute(param);
        const urlReplace = `${config_1.APP_CONFIG.DASHBOARD_ADMIN_BASE_URL}/login`;
        res.redirect(urlReplace);
    }
};
__decorate([
    (0, auth_1.AdminPermission)(types_1.WITHOUT_AUTHORIZATION),
    (0, common_1.Post)('login'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminLoginOutputMap extends (0, swagger_1.IntersectionType)(validate_2.AdminLoginOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Login',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.AdminLoginInput]),
    __metadata("design:returntype", Promise)
], AdminV1AuthController.prototype, "login", null);
__decorate([
    (0, common_1.Post)('create'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminCreateUserOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminCreateUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create new admin',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.AdminCreateUserInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1AuthController.prototype, "create", null);
__decorate([
    (0, auth_1.AdminPermission)(types_1.WITHOUT_AUTHORIZATION),
    (0, common_1.Get)('verify-email/:token'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminVerifyEmailOutputMap extends (0, swagger_1.IntersectionType)(validate_3.AdminVerifyEmailOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Admin verify email',
    }),
    __param(0, (0, common_1.Param)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.AdminVerifyEmailInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1AuthController.prototype, "verify", null);
AdminV1AuthController = __decorate([
    (0, swagger_1.ApiTags)('admin/auth'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/auth`),
    __param(0, (0, common_1.Inject)(admin_login_1.AdminLoginHandler)),
    __param(1, (0, common_1.Inject)(admin_create_user_1.AdminCreateUserHandler)),
    __param(2, (0, common_1.Inject)(verify_email_1.AdminVerifyEmailHandler)),
    __metadata("design:paramtypes", [admin_login_1.AdminLoginHandler,
        admin_create_user_1.AdminCreateUserHandler,
        verify_email_1.AdminVerifyEmailHandler])
], AdminV1AuthController);
exports.AdminV1AuthController = AdminV1AuthController;
//# sourceMappingURL=auth.controller.js.map