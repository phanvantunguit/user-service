"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1PromotionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const application_1 = require("../../../../application");
const validate_1 = require("../../../../application/usecase/promotion/admin-send-promotion/validate");
const const_1 = require("../../const");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminV1PromotionController = class AdminV1PromotionController {
    constructor(adminSendPromotionHandler) {
        this.adminSendPromotionHandler = adminSendPromotionHandler;
    }
    async send(body) {
        const result = await this.adminSendPromotionHandler.execute(body);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Post)('send'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminInviteEventOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminSendPromotionOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Send Promotion for user',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.AdminSendPromotionInput]),
    __metadata("design:returntype", Promise)
], AdminV1PromotionController.prototype, "send", null);
AdminV1PromotionController = __decorate([
    (0, swagger_1.ApiTags)('admin/promotion'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/promotion`),
    __param(0, (0, common_1.Inject)(application_1.AdminSendPromotionHandler)),
    __metadata("design:paramtypes", [application_1.AdminSendPromotionHandler])
], AdminV1PromotionController);
exports.AdminV1PromotionController = AdminV1PromotionController;
//# sourceMappingURL=promotion.controller.js.map