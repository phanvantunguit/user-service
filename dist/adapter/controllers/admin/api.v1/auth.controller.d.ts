import { Response } from 'express';
import { AdminCreateUserHandler } from 'src/application/usecase/auth/admin-create-user';
import { AdminCreateUserInput } from 'src/application/usecase/auth/admin-create-user/validate';
import { AdminLoginHandler } from 'src/application/usecase/auth/admin-login';
import { AdminLoginInput } from 'src/application/usecase/auth/admin-login/validate';
import { AdminVerifyEmailHandler } from 'src/application/usecase/auth/verify-email';
import { AdminVerifyEmailInput } from 'src/application/usecase/auth/verify-email/validate';
export declare class AdminV1AuthController {
    private adminLoginHandler;
    private adminCreateUserHandler;
    private adminVerifyEmailHandler;
    constructor(adminLoginHandler: AdminLoginHandler, adminCreateUserHandler: AdminCreateUserHandler, adminVerifyEmailHandler: AdminVerifyEmailHandler);
    login(body: AdminLoginInput): Promise<string>;
    create(body: AdminCreateUserInput, req: any): Promise<boolean>;
    verify(param: AdminVerifyEmailInput, res: Response): Promise<void>;
}
