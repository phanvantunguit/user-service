"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const admin_get_user_1 = require("../../../../application/usecase/event/admin-get-user");
const validate_1 = require("../../../../application/usecase/event/admin-get-user/validate");
const admin_list_user_1 = require("../../../../application/usecase/event/admin-list-user");
const validate_2 = require("../../../../application/usecase/event/admin-list-user/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminV1UserController = class AdminV1UserController {
    constructor(adminListUserHandler, adminGetUserHandler) {
        this.adminListUserHandler = adminListUserHandler;
        this.adminGetUserHandler = adminGetUserHandler;
    }
    async login(query) {
        const result = await this.adminListUserHandler.execute(query);
        return result.payload;
    }
    async getEvent(param) {
        const result = await this.adminGetUserHandler.execute(param);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminListUserOutputMap extends (0, swagger_1.IntersectionType)(validate_2.AdminListUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List user invite',
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.AdminListUserInput]),
    __metadata("design:returntype", Promise)
], AdminV1UserController.prototype, "login", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminGetUserOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminGetUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Detail event',
    }),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.AdminGetUserInput]),
    __metadata("design:returntype", Promise)
], AdminV1UserController.prototype, "getEvent", null);
AdminV1UserController = __decorate([
    (0, swagger_1.ApiTags)('admin/user'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/user`),
    __param(0, (0, common_1.Inject)(admin_list_user_1.AdminListUserHandler)),
    __param(1, (0, common_1.Inject)(admin_get_user_1.AdminGetUserHandler)),
    __metadata("design:paramtypes", [admin_list_user_1.AdminListUserHandler,
        admin_get_user_1.AdminGetUserHandler])
], AdminV1UserController);
exports.AdminV1UserController = AdminV1UserController;
//# sourceMappingURL=user.controller.js.map