"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1SettingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const create_app_setting_1 = require("../../../../application/usecase/setting/create-app-setting");
const validate_1 = require("../../../../application/usecase/setting/create-app-setting/validate");
const delete_app_setting_1 = require("../../../../application/usecase/setting/delete-app-setting");
const get_app_setting_1 = require("../../../../application/usecase/setting/get-app-setting");
const validate_2 = require("../../../../application/usecase/setting/get-app-setting/validate");
const list_app_setting_1 = require("../../../../application/usecase/setting/list-app-setting");
const validate_3 = require("../../../../application/usecase/setting/list-app-setting/validate");
const update_app_setting_1 = require("../../../../application/usecase/setting/update-app-setting");
const validate_4 = require("../../../../application/usecase/setting/update-app-setting/validate");
const validate_5 = require("../../../../application/usecase/validate");
const const_1 = require("../../const");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminV1SettingController = class AdminV1SettingController {
    constructor(listAppSettingHandler, getAppSettingHandler, createAppSettingHandler, updateAppSettingHandler, deleteAppSettingHandler) {
        this.listAppSettingHandler = listAppSettingHandler;
        this.getAppSettingHandler = getAppSettingHandler;
        this.createAppSettingHandler = createAppSettingHandler;
        this.updateAppSettingHandler = updateAppSettingHandler;
        this.deleteAppSettingHandler = deleteAppSettingHandler;
    }
    async listAppSetting(query) {
        const result = await this.listAppSettingHandler.execute(query);
        return result.payload;
    }
    async getAppSetting(id) {
        const result = await this.getAppSettingHandler.execute(id);
        return result.payload;
    }
    async createAppSetting(body, req) {
        const owner_created = req.user.user_id;
        const result = await this.createAppSettingHandler.execute(Object.assign(Object.assign({}, body), { owner_created }));
        return result.payload;
    }
    async updateAppSetting(id, body, req) {
        const owner_created = req.user.user_id;
        const result = await this.updateAppSettingHandler.execute(Object.assign(Object.assign({}, body), { id,
            owner_created }));
        return result.payload;
    }
    async deleteAppSetting(id) {
        const result = await this.deleteAppSettingHandler.execute(id);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListAppSettingOutputMap extends (0, swagger_1.IntersectionType)(validate_3.ListAppSettingOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List app-setting',
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.ListAppSettingInput]),
    __metadata("design:returntype", Promise)
], AdminV1SettingController.prototype, "listAppSetting", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class GetAppSettingOutputMap extends (0, swagger_1.IntersectionType)(validate_2.GetAppSettingOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get app-setting by id',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1SettingController.prototype, "getAppSetting", null);
__decorate([
    (0, common_1.Post)(''),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseCreateOutputMap extends (0, swagger_1.IntersectionType)(validate_5.BaseCreateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create new app-setting',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.CreateAppSettingInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1SettingController.prototype, "createAppSetting", null);
__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseCreateOutputMap extends (0, swagger_1.IntersectionType)(validate_5.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update app-setting',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_4.UpdateAppSettingInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1SettingController.prototype, "updateAppSetting", null);
__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseCreateOutputMap extends (0, swagger_1.IntersectionType)(validate_5.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Delete app-setting',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1SettingController.prototype, "deleteAppSetting", null);
AdminV1SettingController = __decorate([
    (0, swagger_1.ApiTags)('admin/app-setting'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/app-setting`),
    __param(0, (0, common_1.Inject)(list_app_setting_1.ListAppSettingHandler)),
    __param(1, (0, common_1.Inject)(get_app_setting_1.GetAppSettingHandler)),
    __param(2, (0, common_1.Inject)(create_app_setting_1.CreateAppSettingHandler)),
    __param(3, (0, common_1.Inject)(update_app_setting_1.UpdateAppSettingHandler)),
    __param(4, (0, common_1.Inject)(delete_app_setting_1.DeleteAppSettingHandler)),
    __metadata("design:paramtypes", [list_app_setting_1.ListAppSettingHandler,
        get_app_setting_1.GetAppSettingHandler,
        create_app_setting_1.CreateAppSettingHandler,
        update_app_setting_1.UpdateAppSettingHandler,
        delete_app_setting_1.DeleteAppSettingHandler])
], AdminV1SettingController);
exports.AdminV1SettingController = AdminV1SettingController;
//# sourceMappingURL=setting.controller.js.map