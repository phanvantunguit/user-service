"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1MerchantUserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const validate_1 = require("../../../../application/usecase/user/get-user/validate");
const validate_2 = require("../../../../application/usecase/user/list-user/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminV1MerchantUserController = class AdminV1MerchantUserController {
    constructor(listUserHandler, getUserHandler) {
        this.listUserHandler = listUserHandler;
        this.getUserHandler = getUserHandler;
    }
    async listTransaction(req, query) {
        const result = await this.listUserHandler.execute(query, query.merchant_code);
        return result.payload;
    }
    async getEvent(id) {
        const result = await this.getUserHandler.execute(id);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListUserOutputMap extends (0, swagger_1.IntersectionType)(validate_2.ListUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List user of merchant',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_2.ListUserInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantUserController.prototype, "listTransaction", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class GetUserOutputMap extends (0, swagger_1.IntersectionType)(validate_1.GetUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Detail user',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantUserController.prototype, "getEvent", null);
AdminV1MerchantUserController = __decorate([
    (0, swagger_1.ApiTags)('admin/merchant-user'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/merchant-user`),
    __param(0, (0, common_1.Inject)(application_1.ListUserHandler)),
    __param(1, (0, common_1.Inject)(application_1.GetUserHandler)),
    __metadata("design:paramtypes", [application_1.ListUserHandler,
        application_1.GetUserHandler])
], AdminV1MerchantUserController);
exports.AdminV1MerchantUserController = AdminV1MerchantUserController;
//# sourceMappingURL=merchant-user.controller.js.map