"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminAdditionalDataController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const create_additional_data_1 = require("../../../../application/usecase/additional-data/create-additional-data");
const validate_1 = require("../../../../application/usecase/additional-data/create-additional-data/validate");
const delete_additional_data_1 = require("../../../../application/usecase/additional-data/delete-additional-data");
const get_additional_data_1 = require("../../../../application/usecase/additional-data/get-additional-data");
const validate_2 = require("../../../../application/usecase/additional-data/get-additional-data/validate");
const list_additional_data_1 = require("../../../../application/usecase/additional-data/list-additional-data");
const validate_3 = require("../../../../application/usecase/additional-data/list-additional-data/validate");
const update_additional_data_1 = require("../../../../application/usecase/additional-data/update-additional-data");
const validate_4 = require("../../../../application/usecase/additional-data/update-additional-data/validate");
const validate_5 = require("../../../../application/usecase/validate");
const const_1 = require("../../const");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminAdditionalDataController = class AdminAdditionalDataController {
    constructor(createAdditionalDataHandler, updateAdditionalDataHandler, deleteAdditionalDataHandler, listAdditionalDataHandler, getAdditionalDataHandler) {
        this.createAdditionalDataHandler = createAdditionalDataHandler;
        this.updateAdditionalDataHandler = updateAdditionalDataHandler;
        this.deleteAdditionalDataHandler = deleteAdditionalDataHandler;
        this.listAdditionalDataHandler = listAdditionalDataHandler;
        this.getAdditionalDataHandler = getAdditionalDataHandler;
    }
    async createAdditionalData(body) {
        const result = await this.createAdditionalDataHandler.execute(body);
        return result.payload;
    }
    async updateAdditionalData(id, body) {
        const result = await this.updateAdditionalDataHandler.execute(id, body);
        return result.payload;
    }
    async deleteAdditionalData(id) {
        const result = await this.deleteAdditionalDataHandler.execute(id);
        return result.payload;
    }
    async listAdditionalData(query) {
        const result = await this.listAdditionalDataHandler.execute(query);
        return result.payload;
    }
    async getAdditionalData(id) {
        const result = await this.getAdditionalDataHandler.execute(id);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Post)('/'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseCreateOutputMap extends (0, swagger_1.IntersectionType)(validate_5.BaseCreateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create Additional Data',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.CreateAdditionalDataInput]),
    __metadata("design:returntype", Promise)
], AdminAdditionalDataController.prototype, "createAdditionalData", null);
__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseUpdateOutputMap extends (0, swagger_1.IntersectionType)(validate_5.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_4.UpdateAdditionalDataInput]),
    __metadata("design:returntype", Promise)
], AdminAdditionalDataController.prototype, "updateAdditionalData", null);
__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseUpdateOutputMap extends (0, swagger_1.IntersectionType)(validate_5.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Delete Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminAdditionalDataController.prototype, "deleteAdditionalData", null);
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListAdditionalDataOutputMap extends (0, swagger_1.IntersectionType)(validate_3.ListAdditionalDataOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List Additional Data',
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.ListAdditionalDataInput]),
    __metadata("design:returntype", Promise)
], AdminAdditionalDataController.prototype, "listAdditionalData", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class GetAdditionalDataOutputMap extends (0, swagger_1.IntersectionType)(validate_2.GetAdditionalDataOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminAdditionalDataController.prototype, "getAdditionalData", null);
AdminAdditionalDataController = __decorate([
    (0, swagger_1.ApiTags)('admin/additional-data'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/additional-data`),
    __param(0, (0, common_1.Inject)(create_additional_data_1.CreateAdditionalDataHandler)),
    __param(1, (0, common_1.Inject)(update_additional_data_1.UpdateAdditionalDataHandler)),
    __param(2, (0, common_1.Inject)(delete_additional_data_1.DeleteAdditionalDataHandler)),
    __param(3, (0, common_1.Inject)(list_additional_data_1.ListAdditionalDataHandler)),
    __param(4, (0, common_1.Inject)(get_additional_data_1.GetAdditionalDataHandler)),
    __metadata("design:paramtypes", [create_additional_data_1.CreateAdditionalDataHandler,
        update_additional_data_1.UpdateAdditionalDataHandler,
        delete_additional_data_1.DeleteAdditionalDataHandler,
        list_additional_data_1.ListAdditionalDataHandler,
        get_additional_data_1.GetAdditionalDataHandler])
], AdminAdditionalDataController);
exports.AdminAdditionalDataController = AdminAdditionalDataController;
//# sourceMappingURL=additional-data.controller.js.map