import { AdminSendPromotionHandler } from 'src/application';
import { AdminSendPromotionInput } from 'src/application/usecase/promotion/admin-send-promotion/validate';
export declare class AdminV1PromotionController {
    private adminSendPromotionHandler;
    constructor(adminSendPromotionHandler: AdminSendPromotionHandler);
    send(body: AdminSendPromotionInput): Promise<number>;
}
