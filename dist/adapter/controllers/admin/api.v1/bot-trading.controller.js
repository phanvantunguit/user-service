"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1BotTradingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const application_1 = require("../../../../application");
const validate_1 = require("../../../../application/usecase/bot/merchant-list-bot-trading/validate");
const const_1 = require("../../const");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let AdminV1BotTradingController = class AdminV1BotTradingController {
    constructor(merchantListBotTradingHandler) {
        this.merchantListBotTradingHandler = merchantListBotTradingHandler;
    }
    async listBot() {
        const result = await this.merchantListBotTradingHandler.execute();
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantListBotTradingOuputMap extends (0, swagger_1.IntersectionType)(validate_1.MerchantListBotTradingOuput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List bot trading',
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AdminV1BotTradingController.prototype, "listBot", null);
AdminV1BotTradingController = __decorate([
    (0, swagger_1.ApiTags)('admin/bot-trading'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/bot-trading`),
    __param(0, (0, common_1.Inject)(application_1.MerchantListBotTradingHandler)),
    __metadata("design:paramtypes", [application_1.MerchantListBotTradingHandler])
], AdminV1BotTradingController);
exports.AdminV1BotTradingController = AdminV1BotTradingController;
//# sourceMappingURL=bot-trading.controller.js.map