import { GetUserHandler, ListUserHandler } from 'src/application';
import { ListUserInput } from 'src/application/usecase/user/list-user/validate';
export declare class AdminV1MerchantUserController {
    private listUserHandler;
    private getUserHandler;
    constructor(listUserHandler: ListUserHandler, getUserHandler: GetUserHandler);
    listTransaction(req: any, query: ListUserInput): Promise<import("src/application/usecase/user/list-user/validate").PagingUserOutput>;
    getEvent(id: string): Promise<import("../../../../application/usecase/user/validate").UserValidate>;
}
