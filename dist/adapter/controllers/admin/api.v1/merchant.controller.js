"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminV1MerchantController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const application_1 = require("../../../../application");
const modify_merchant_additional_data_1 = require("../../../../application/usecase/additional-data/modify-merchant-additional-data");
const delete_merchant_additional_data_1 = require("../../../../application/usecase/additional-data/delete-merchant-additional-data");
const list_merchant_additional_data_1 = require("../../../../application/usecase/additional-data/list-merchant-additional-data");
const validate_1 = require("../../../../application/usecase/additional-data/list-merchant-additional-data/validate");
const admin_create_merchant_1 = require("../../../../application/usecase/merchant/admin-create-merchant");
const validate_2 = require("../../../../application/usecase/merchant/admin-create-merchant/validate");
const admin_list_merchant_1 = require("../../../../application/usecase/merchant/admin-list-merchant");
const validate_3 = require("../../../../application/usecase/merchant/admin-list-merchant/validate");
const admin_update_merchant_1 = require("../../../../application/usecase/merchant/admin-update-merchant");
const validate_4 = require("../../../../application/usecase/merchant/admin-update-merchant/validate");
const validate_5 = require("../../../../application/usecase/merchant/create-merchant-invoice-by-amount/validate");
const get_merchant_1 = require("../../../../application/usecase/merchant/get-merchant");
const validate_6 = require("../../../../application/usecase/merchant/get-merchant-invoice/validate");
const validate_7 = require("../../../../application/usecase/merchant/get-merchant/validate");
const list_merchant_commission_1 = require("../../../../application/usecase/merchant/list-merchant-commission");
const validate_8 = require("../../../../application/usecase/merchant/list-merchant-commission/validate");
const list_merchant_invoice_1 = require("../../../../application/usecase/merchant/list-merchant-invoice");
const validate_9 = require("../../../../application/usecase/merchant/list-merchant-invoice/validate");
const merchant_update_invoice_1 = require("../../../../application/usecase/merchant/merchant-update-invoice");
const validate_10 = require("../../../../application/usecase/merchant/merchant-update-invoice/validate");
const reset_merchant_password_1 = require("../../../../application/usecase/merchant/reset-merchant-password");
const update_email_sender_1 = require("../../../../application/usecase/merchant/update-email-sender");
const validate_11 = require("../../../../application/usecase/merchant/update-email-sender/validate");
const update_merchant_commission_1 = require("../../../../application/usecase/merchant/update-merchant-commission");
const validate_12 = require("../../../../application/usecase/merchant/update-merchant-commission/validate");
const validate_13 = require("../../../../application/usecase/merchant/verify-email-sender/validate");
const validate_14 = require("../../../../application/usecase/transaction/report-transaction/validate");
const validate_15 = require("../../../../application/usecase/validate");
const merchant_commission_repository_1 = require("../../../../infrastructure/data/database/merchant-commission.repository");
const const_1 = require("../../const");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
const validate_16 = require("../../../../application/usecase/additional-data/modify-merchant-additional-data/validate");
let AdminV1MerchantController = class AdminV1MerchantController {
    constructor(adminUpdateMerchantHandler, adminCreateMerchantHandler, getMerchantHandler, adminListMerchantHandler, resetMerchantPasswordHandler, verifyEmailSenderHandler, updateEmailSenderHandler, updateMerchantCommissionHandler, listMerchantCommissionHandler, merchantCommissionRepository, listMerchantInvoiceHandler, getMerchantInvoiceHandler, updateMerchantInvoiceHandler, createMerchantInvoiceByAmountHandler, reportTransactionHandler, modifyMerchantAdditionalDataHandler, deleteMerchantAdditionalDataHandler, listMerchantAdditionalDataHandler) {
        this.adminUpdateMerchantHandler = adminUpdateMerchantHandler;
        this.adminCreateMerchantHandler = adminCreateMerchantHandler;
        this.getMerchantHandler = getMerchantHandler;
        this.adminListMerchantHandler = adminListMerchantHandler;
        this.resetMerchantPasswordHandler = resetMerchantPasswordHandler;
        this.verifyEmailSenderHandler = verifyEmailSenderHandler;
        this.updateEmailSenderHandler = updateEmailSenderHandler;
        this.updateMerchantCommissionHandler = updateMerchantCommissionHandler;
        this.listMerchantCommissionHandler = listMerchantCommissionHandler;
        this.merchantCommissionRepository = merchantCommissionRepository;
        this.listMerchantInvoiceHandler = listMerchantInvoiceHandler;
        this.getMerchantInvoiceHandler = getMerchantInvoiceHandler;
        this.updateMerchantInvoiceHandler = updateMerchantInvoiceHandler;
        this.createMerchantInvoiceByAmountHandler = createMerchantInvoiceByAmountHandler;
        this.reportTransactionHandler = reportTransactionHandler;
        this.modifyMerchantAdditionalDataHandler = modifyMerchantAdditionalDataHandler;
        this.deleteMerchantAdditionalDataHandler = deleteMerchantAdditionalDataHandler;
        this.listMerchantAdditionalDataHandler = listMerchantAdditionalDataHandler;
    }
    async createMerchant(body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminCreateMerchantHandler.execute(body, user_id);
        return result.payload;
    }
    async resetMerchantPassword(id, req) {
        const user_id = req.user.user_id;
        const result = await this.resetMerchantPasswordHandler.execute(id, user_id);
        return result.payload;
    }
    async verifySender(id, req, body) {
        const user_id = req.user.user_id;
        const result = await this.verifyEmailSenderHandler.execute(body, id, user_id);
        return result.payload;
    }
    async deleteCommission(id, req) {
        const result = await this.merchantCommissionRepository.deleteById(id);
        return true;
    }
    async updateSender(id, req, body) {
        const user_id = req.user.user_id;
        const result = await this.updateEmailSenderHandler.execute(body, id, user_id);
        return result.payload;
    }
    async updateCommission(id, req, body) {
        const user_id = req.user.user_id;
        const result = await this.updateMerchantCommissionHandler.execute(body, id, user_id);
        return result.payload;
    }
    async createMerchantInvoice(id, body, req) {
        const adminId = req.user.user_id;
        const result = await this.createMerchantInvoiceByAmountHandler.execute(body, id, undefined, adminId);
        return result.payload;
    }
    async listBotCommission(id, query) {
        const result = await this.listMerchantCommissionHandler.execute(query, id);
        return result.payload;
    }
    async report(id, query) {
        const result = await this.reportTransactionHandler.execute(query, id);
        return result.payload;
    }
    async updateMerchant(id, body, req) {
        const user_id = req.user.user_id;
        const result = await this.adminUpdateMerchantHandler.execute(body, id, user_id);
        return result.payload;
    }
    async listMerchant() {
        const result = await this.adminListMerchantHandler.execute();
        return result.payload;
    }
    async listMerchantInvoice(query) {
        const result = await this.listMerchantInvoiceHandler.execute(query);
        return result.payload;
    }
    async getMerchant(id) {
        const result = await this.getMerchantHandler.execute(id);
        return result.payload;
    }
    async getMerchantInvoice(id) {
        const result = await this.getMerchantInvoiceHandler.execute(id);
        return result.payload;
    }
    async updateMerchantInvoice(id, body) {
        const result = await this.updateMerchantInvoiceHandler.execute(id, body);
        return result;
    }
    async modifyAdditionalData(id, body, req) {
        const user_id = req.user.user_id;
        const result = await this.modifyMerchantAdditionalDataHandler.execute(body, id, user_id);
        return result.payload;
    }
    async deleteAdditionalData(id) {
        const result = await this.deleteMerchantAdditionalDataHandler.execute(id);
        return result.payload;
    }
    async listAdditionalData(id, query) {
        const result = await this.listMerchantAdditionalDataHandler.execute(query, id);
        return result.payload;
    }
    async getAdditionalData(id) {
        const result = await this.listMerchantAdditionalDataHandler.execute({}, id);
        return result.payload[0];
    }
};
__decorate([
    (0, common_1.Post)(''),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminCreateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_2.AdminCreateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create merchant',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.AdminCreateMerchantInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "createMerchant", null);
__decorate([
    (0, common_1.Put)('/:id/reset-password'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update merchant',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "resetMerchantPassword", null);
__decorate([
    (0, common_1.Put)('/:id/verify-sender'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Verify sender',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, validate_13.VerifyEmailSenderInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "verifySender", null);
__decorate([
    (0, common_1.Delete)('/commission/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Delete commission',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "deleteCommission", null);
__decorate([
    (0, common_1.Put)('/:id/update-sender'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update sender',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, validate_11.UpdateEmailSenderInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "updateSender", null);
__decorate([
    (0, common_1.Put)('/:id/commission'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update commission',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, validate_12.AdminUpdateCommissionInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "updateCommission", null);
__decorate([
    (0, common_1.Post)('/:id/invoice'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseCreateOutputMap extends (0, swagger_1.IntersectionType)(validate_15.BaseCreateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create invoice',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_5.CreateMerchantInvoiceByAmountInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "createMerchantInvoice", null);
__decorate([
    (0, common_1.Get)('/:id/list-commission'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListMerchantCommissioOutputMap extends (0, swagger_1.IntersectionType)(validate_8.ListMerchantCommissioOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List bot trading',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_8.ListMerchantCommissionInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "listBotCommission", null);
__decorate([
    (0, common_1.Get)('/:id/transaction-report'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ReportTransactionOutputMap extends (0, swagger_1.IntersectionType)(validate_14.ReportTransactionOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Report transaction',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_14.ReportTransactionInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "report", null);
__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_4.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update merchant',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_4.AdminUpdateMerchantInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "updateMerchant", null);
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminListMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_3.AdminListMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List merchant',
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "listMerchant", null);
__decorate([
    (0, common_1.Get)('list-invoice'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListMerchantInvoiceOutputMap extends (0, swagger_1.IntersectionType)(validate_9.ListMerchantInvoiceOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List invoice',
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_9.ListMerchantInvoiceInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "listMerchantInvoice", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminGetMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_7.GetMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get one merchant',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "getMerchant", null);
__decorate([
    (0, common_1.Get)('/invoice/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminGetMerchantInvoiceOutputMap extends (0, swagger_1.IntersectionType)(validate_6.GetMerchantInvoiceIdOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get one invoice by id',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "getMerchantInvoice", null);
__decorate([
    (0, common_1.Put)('/:id/update-invoice'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantUpdateInvoiceOutputMap extends (0, swagger_1.IntersectionType)(validate_10.MerchantUpdateInvoiceOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Admin update merchant invoice',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_10.MerchantUpdateInvoiceInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "updateMerchantInvoice", null);
__decorate([
    (0, common_1.Put)('/:id/additional-data'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseUpdateOutputMap extends (0, swagger_1.IntersectionType)(validate_15.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create Merchant Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_16.ModifyMerchantAdditionalDataInput, Object]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "modifyAdditionalData", null);
__decorate([
    (0, common_1.Delete)('/additional-data/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseUpdateOutputMap extends (0, swagger_1.IntersectionType)(validate_15.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Delete Merchant Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "deleteAdditionalData", null);
__decorate([
    (0, common_1.Get)('/:id/additional-data/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListMerchantAdditionalDataOutputMap extends (0, swagger_1.IntersectionType)(validate_1.ListMerchantAdditionalDataOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, validate_1.ListMerchantAdditionalDataInput]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "listAdditionalData", null);
__decorate([
    (0, common_1.Get)('/additional-data/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class GetMerchantAdditionalDataOutputMap extends (0, swagger_1.IntersectionType)(validate_1.GetMerchantAdditionalDataOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminV1MerchantController.prototype, "getAdditionalData", null);
AdminV1MerchantController = __decorate([
    (0, swagger_1.ApiTags)('admin/merchant'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.AdminAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_admin}/merchant`),
    __param(0, (0, common_1.Inject)(admin_update_merchant_1.AdminUpdateMerchantHandler)),
    __param(1, (0, common_1.Inject)(admin_create_merchant_1.AdminCreateMerchantHandler)),
    __param(2, (0, common_1.Inject)(get_merchant_1.GetMerchantHandler)),
    __param(3, (0, common_1.Inject)(admin_list_merchant_1.AdminListMerchantHandler)),
    __param(4, (0, common_1.Inject)(reset_merchant_password_1.ResetMerchantPasswordHandler)),
    __param(5, (0, common_1.Inject)(application_1.VerifyEmailSenderHandler)),
    __param(6, (0, common_1.Inject)(update_email_sender_1.UpdateEmailSenderHandler)),
    __param(7, (0, common_1.Inject)(update_merchant_commission_1.UpdateMerchantCommissionHandler)),
    __param(8, (0, common_1.Inject)(list_merchant_commission_1.ListMerchantCommissionHandler)),
    __param(9, (0, common_1.Inject)(merchant_commission_repository_1.MerchantCommissionRepository)),
    __param(10, (0, common_1.Inject)(list_merchant_invoice_1.ListMerchantInvoiceHandler)),
    __param(11, (0, common_1.Inject)(application_1.GetMerchantInvoiceHandler)),
    __param(12, (0, common_1.Inject)(merchant_update_invoice_1.UpdateMerchantInvoiceHandler)),
    __param(13, (0, common_1.Inject)(application_1.CreateMerchantInvoiceByAmountHandler)),
    __param(14, (0, common_1.Inject)(application_1.ReportTransactionHandler)),
    __param(15, (0, common_1.Inject)(modify_merchant_additional_data_1.ModifyMerchantAdditionalDataHandler)),
    __param(16, (0, common_1.Inject)(delete_merchant_additional_data_1.DeleteMerchantAdditionalDataHandler)),
    __param(17, (0, common_1.Inject)(list_merchant_additional_data_1.ListMerchantAdditionalDataHandler)),
    __metadata("design:paramtypes", [admin_update_merchant_1.AdminUpdateMerchantHandler,
        admin_create_merchant_1.AdminCreateMerchantHandler,
        get_merchant_1.GetMerchantHandler,
        admin_list_merchant_1.AdminListMerchantHandler,
        reset_merchant_password_1.ResetMerchantPasswordHandler,
        application_1.VerifyEmailSenderHandler,
        update_email_sender_1.UpdateEmailSenderHandler,
        update_merchant_commission_1.UpdateMerchantCommissionHandler,
        list_merchant_commission_1.ListMerchantCommissionHandler,
        merchant_commission_repository_1.MerchantCommissionRepository,
        list_merchant_invoice_1.ListMerchantInvoiceHandler,
        application_1.GetMerchantInvoiceHandler,
        merchant_update_invoice_1.UpdateMerchantInvoiceHandler,
        application_1.CreateMerchantInvoiceByAmountHandler,
        application_1.ReportTransactionHandler,
        modify_merchant_additional_data_1.ModifyMerchantAdditionalDataHandler,
        delete_merchant_additional_data_1.DeleteMerchantAdditionalDataHandler,
        list_merchant_additional_data_1.ListMerchantAdditionalDataHandler])
], AdminV1MerchantController);
exports.AdminV1MerchantController = AdminV1MerchantController;
//# sourceMappingURL=merchant.controller.js.map