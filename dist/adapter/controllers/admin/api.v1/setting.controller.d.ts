import { CreateAppSettingHandler } from 'src/application/usecase/setting/create-app-setting';
import { CreateAppSettingInput } from 'src/application/usecase/setting/create-app-setting/validate';
import { DeleteAppSettingHandler } from 'src/application/usecase/setting/delete-app-setting';
import { GetAppSettingHandler } from 'src/application/usecase/setting/get-app-setting';
import { ListAppSettingHandler } from 'src/application/usecase/setting/list-app-setting';
import { ListAppSettingInput } from 'src/application/usecase/setting/list-app-setting/validate';
import { UpdateAppSettingHandler } from 'src/application/usecase/setting/update-app-setting';
import { UpdateAppSettingInput } from 'src/application/usecase/setting/update-app-setting/validate';
export declare class AdminV1SettingController {
    private listAppSettingHandler;
    private getAppSettingHandler;
    private createAppSettingHandler;
    private updateAppSettingHandler;
    private deleteAppSettingHandler;
    constructor(listAppSettingHandler: ListAppSettingHandler, getAppSettingHandler: GetAppSettingHandler, createAppSettingHandler: CreateAppSettingHandler, updateAppSettingHandler: UpdateAppSettingHandler, deleteAppSettingHandler: DeleteAppSettingHandler);
    listAppSetting(query: ListAppSettingInput): Promise<import("../../../../application/usecase/setting/validate").AppSettingValidate[]>;
    getAppSetting(id: string): Promise<import("../../../../application/usecase/setting/validate").AppSettingValidate>;
    createAppSetting(body: CreateAppSettingInput, req: any): Promise<string>;
    updateAppSetting(id: string, body: UpdateAppSettingInput, req: any): Promise<boolean>;
    deleteAppSetting(id: string): Promise<boolean>;
}
