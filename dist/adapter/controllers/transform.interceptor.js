"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseApiOutput = exports.TransformInterceptor = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const operators_1 = require("rxjs/operators");
const const_1 = require("../../utils/exceptions/const");
let TransformInterceptor = class TransformInterceptor {
    intercept(context, next) {
        return next.handle().pipe((0, operators_1.map)((data) => (Object.assign(Object.assign({}, const_1.ERROR_CODE.SUCCESS), { payload: data }))));
    }
};
TransformInterceptor = __decorate([
    (0, common_1.Injectable)()
], TransformInterceptor);
exports.TransformInterceptor = TransformInterceptor;
class BaseApiOutput {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Error code of request',
        example: 'SUCCESS',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], BaseApiOutput.prototype, "error_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        required: true,
        description: 'Message describe error code',
        example: 'Success',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], BaseApiOutput.prototype, "message", void 0);
exports.BaseApiOutput = BaseApiOutput;
//# sourceMappingURL=transform.interceptor.js.map