import { StorageService } from 'src/infrastructure/services';
export declare class UserPublicController {
    private storageService;
    constructor(storageService: StorageService);
    uploadFile(file: any): Promise<{
        file_url: string;
    }>;
}
