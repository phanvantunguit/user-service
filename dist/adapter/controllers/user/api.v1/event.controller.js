"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserV1EventController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const validate_1 = require("../../../../application/usecase/event/user-check-param/validate");
const user_checkin_1 = require("../../../../application/usecase/event/user-checkin");
const validate_2 = require("../../../../application/usecase/event/user-checkin/validate");
const user_confirm_1 = require("../../../../application/usecase/event/user-confirm");
const validate_3 = require("../../../../application/usecase/event/user-confirm/validate");
const user_get_event_1 = require("../../../../application/usecase/event/user-get-event");
const validate_4 = require("../../../../application/usecase/event/user-get-event/validate");
const validate_5 = require("../../../../application/usecase/event/user-register/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
let UserV1EventController = class UserV1EventController {
    constructor(userRegisterEventHandler, userConfirmEventHandler, userCheckinEventHandler, userGetEventHandler, userCheckParamHandler) {
        this.userRegisterEventHandler = userRegisterEventHandler;
        this.userConfirmEventHandler = userConfirmEventHandler;
        this.userCheckinEventHandler = userCheckinEventHandler;
        this.userGetEventHandler = userGetEventHandler;
        this.userCheckParamHandler = userCheckParamHandler;
    }
    async register(body) {
        const result = await this.userRegisterEventHandler.execute(body);
        return result.payload;
    }
    async confirm(query) {
        const result = await this.userConfirmEventHandler.execute(query);
        return result.payload;
    }
    async attend(query) {
        const result = await this.userCheckinEventHandler.execute(query);
        return result.payload;
    }
    async checkParam(body) {
        const result = await this.userCheckParamHandler.execute(body);
        return result.payload;
    }
    async getEvent(param) {
        const result = await this.userGetEventHandler.execute(param);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Post)('register'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class UserRegisterEventOutputMap extends (0, swagger_1.IntersectionType)(validate_5.UserRegisterEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Save user and send email invite',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_5.UserRegisterEventInput]),
    __metadata("design:returntype", Promise)
], UserV1EventController.prototype, "register", null);
__decorate([
    (0, common_1.Post)('confirm'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class UserConfirmEventOutputMap extends (0, swagger_1.IntersectionType)(validate_3.UserConfirmEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Save status confirm',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.UserConfirmEventInput]),
    __metadata("design:returntype", Promise)
], UserV1EventController.prototype, "confirm", null);
__decorate([
    (0, common_1.Post)('checkin'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class UserCheckinEventOutputMap extends (0, swagger_1.IntersectionType)(validate_2.UserCheckinEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'User checkin',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.UserCheckinEventInput]),
    __metadata("design:returntype", Promise)
], UserV1EventController.prototype, "attend", null);
__decorate([
    (0, common_1.Put)('/check-param'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class UserCheckParamOutputMap extends (0, swagger_1.IntersectionType)(validate_1.UserCheckParamOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Detail event',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.UserCheckParamInput]),
    __metadata("design:returntype", Promise)
], UserV1EventController.prototype, "checkParam", null);
__decorate([
    (0, common_1.Get)('/:code'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class UserGetEventOutputMap extends (0, swagger_1.IntersectionType)(validate_4.UserGetEventOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Detail event',
    }),
    __param(0, (0, common_1.Param)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_4.UserGetEventInput]),
    __metadata("design:returntype", Promise)
], UserV1EventController.prototype, "getEvent", null);
UserV1EventController = __decorate([
    (0, swagger_1.ApiTags)('public/event'),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1}/event`),
    __param(0, (0, common_1.Inject)(application_1.UserRegisterEventHandler)),
    __param(1, (0, common_1.Inject)(user_confirm_1.UserConfirmEventHandler)),
    __param(2, (0, common_1.Inject)(user_checkin_1.UserCheckinEventHandler)),
    __param(3, (0, common_1.Inject)(user_get_event_1.UserGetEventHandler)),
    __param(4, (0, common_1.Inject)(application_1.UserCheckParamHandler)),
    __metadata("design:paramtypes", [application_1.UserRegisterEventHandler,
        user_confirm_1.UserConfirmEventHandler,
        user_checkin_1.UserCheckinEventHandler,
        user_get_event_1.UserGetEventHandler,
        application_1.UserCheckParamHandler])
], UserV1EventController);
exports.UserV1EventController = UserV1EventController;
//# sourceMappingURL=event.controller.js.map