"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPublicController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const platform_express_1 = require("@nestjs/platform-express");
const const_1 = require("../../const");
const transform_interceptor_1 = require("../../transform.interceptor");
const services_1 = require("../../../../infrastructure/services");
const throw_exception_1 = require("../../../../utils/exceptions/throw.exception");
const const_2 = require("../../../../utils/exceptions/const");
let UserPublicController = class UserPublicController {
    constructor(storageService) {
        this.storageService = storageService;
    }
    async uploadFile(file) {
        if (file) {
            const file_url = await this.storageService.upload(file);
            return { file_url };
        }
        else {
            (0, throw_exception_1.badRequestError)('Upload', const_2.ERROR_CODE.BAD_REQUEST);
        }
    }
};
__decorate([
    (0, common_1.Post)('/upload'),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        schema: {
            type: 'object',
            properties: {
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file')),
    __param(0, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserPublicController.prototype, "uploadFile", null);
UserPublicController = __decorate([
    (0, swagger_1.ApiTags)('public'),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1}`),
    __param(0, (0, common_1.Inject)(services_1.StorageService)),
    __metadata("design:paramtypes", [services_1.StorageService])
], UserPublicController);
exports.UserPublicController = UserPublicController;
//# sourceMappingURL=public.controller.js.map