import { UserCheckParamHandler, UserRegisterEventHandler } from 'src/application';
import { UserCheckParamInput } from 'src/application/usecase/event/user-check-param/validate';
import { UserCheckinEventHandler } from 'src/application/usecase/event/user-checkin';
import { UserCheckinEventInput } from 'src/application/usecase/event/user-checkin/validate';
import { UserConfirmEventHandler } from 'src/application/usecase/event/user-confirm';
import { UserConfirmEventInput } from 'src/application/usecase/event/user-confirm/validate';
import { UserGetEventHandler } from 'src/application/usecase/event/user-get-event';
import { UserGetEventInput } from 'src/application/usecase/event/user-get-event/validate';
import { UserRegisterEventInput } from 'src/application/usecase/event/user-register/validate';
export declare class UserV1EventController {
    private userRegisterEventHandler;
    private userConfirmEventHandler;
    private userCheckinEventHandler;
    private userGetEventHandler;
    private userCheckParamHandler;
    constructor(userRegisterEventHandler: UserRegisterEventHandler, userConfirmEventHandler: UserConfirmEventHandler, userCheckinEventHandler: UserCheckinEventHandler, userGetEventHandler: UserGetEventHandler, userCheckParamHandler: UserCheckParamHandler);
    register(body: UserRegisterEventInput): Promise<string>;
    confirm(query: UserConfirmEventInput): Promise<boolean>;
    attend(query: UserCheckinEventInput): Promise<import("src/application/usecase/event/user-checkin/validate").UserCheckinEventDataOutput>;
    checkParam(body: UserCheckParamInput): Promise<import("src/application/usecase/event/user-check-param/validate").UserCheckParamDataOutput>;
    getEvent(param: UserGetEventInput): Promise<import("../../../../application/usecase/event/validate").EventDataValidate>;
}
