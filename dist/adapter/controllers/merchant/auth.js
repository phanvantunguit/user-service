"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantAuthGuard = exports.MerchantPermission = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const config_1 = require("../../../config");
const throw_exception_1 = require("../../../utils/exceptions/throw.exception");
const hash_util_1 = require("../../../utils/hash.util");
const common_2 = require("@nestjs/common");
const const_1 = require("../../../utils/exceptions/const");
const types_1 = require("../../../domain/auth/types");
const MerchantPermission = (permission) => (0, common_2.SetMetadata)('permission', permission);
exports.MerchantPermission = MerchantPermission;
let MerchantAuthGuard = class MerchantAuthGuard {
    constructor(reflector) {
        this.reflector = reflector;
    }
    async canActivate(context) {
        const permission = this.reflector.get('permission', context.getHandler());
        if (permission === types_1.WITHOUT_AUTHORIZATION) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const authHeader = request.get('Authorization');
        if (authHeader) {
            const [type, token] = authHeader.split(' ');
            if (type.toLowerCase() === 'bearer' && token) {
                let decode;
                try {
                    decode = await (0, hash_util_1.verifyTokenSecret)(token, config_1.APP_CONFIG.SECRET_KEY);
                }
                catch (error) {
                    (0, throw_exception_1.unauthorizedError)('Token', const_1.ERROR_CODE.UNAUTHORIZED);
                }
                if (decode.merchant) {
                    request.user = decode;
                    return true;
                }
                (0, throw_exception_1.accessDeniedError)();
            }
        }
        (0, throw_exception_1.unauthorizedError)('Token', const_1.ERROR_CODE.UNAUTHORIZED);
    }
};
MerchantAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector])
], MerchantAuthGuard);
exports.MerchantAuthGuard = MerchantAuthGuard;
//# sourceMappingURL=auth.js.map