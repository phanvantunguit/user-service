import { ModifyMerchantAdditionalDataHandler } from 'src/application';
import { ListMerchantAdditionalDataHandler } from 'src/application/usecase/additional-data/list-merchant-additional-data';
import { ListMerchantAdditionalDataInput } from 'src/application/usecase/additional-data/list-merchant-additional-data/validate';
import { ModifyMerchantAdditionalDataInput } from 'src/application/usecase/additional-data/modify-merchant-additional-data/validate';
export declare class MerchantV1AdditionalDataController {
    private listMerchantAdditionalDataHandler;
    private modifyMerchantAdditionalDataHandler;
    constructor(listMerchantAdditionalDataHandler: ListMerchantAdditionalDataHandler, modifyMerchantAdditionalDataHandler: ModifyMerchantAdditionalDataHandler);
    listAdditionalData(query: ListMerchantAdditionalDataInput, req: any): Promise<import("../../../../application/usecase/additional-data/validate").MerchantAdditionalDataValidate[]>;
    updateAdditionalData(body: ModifyMerchantAdditionalDataInput, req: any): Promise<boolean>;
    getAdditionalData(id: string): Promise<import("../../../../application/usecase/additional-data/validate").MerchantAdditionalDataValidate>;
}
