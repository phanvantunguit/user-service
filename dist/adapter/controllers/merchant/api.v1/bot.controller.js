"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1BotController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const merchant_list_bot_1 = require("../../../../application/usecase/bot/merchant-list-bot");
const validate_1 = require("../../../../application/usecase/bot/merchant-list-bot/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1BotController = class MerchantV1BotController {
    constructor(merchantListBotHandler) {
        this.merchantListBotHandler = merchantListBotHandler;
    }
    async listBot() {
        const result = await this.merchantListBotHandler.execute();
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantListBotOuputMap extends (0, swagger_1.IntersectionType)(validate_1.MerchantListBotOuput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List bot',
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MerchantV1BotController.prototype, "listBot", null);
MerchantV1BotController = __decorate([
    (0, swagger_1.ApiTags)('merchant/bot'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/bot`),
    __param(0, (0, common_1.Inject)(merchant_list_bot_1.MerchantListBotHandler)),
    __metadata("design:paramtypes", [merchant_list_bot_1.MerchantListBotHandler])
], MerchantV1BotController);
exports.MerchantV1BotController = MerchantV1BotController;
//# sourceMappingURL=bot.controller.js.map