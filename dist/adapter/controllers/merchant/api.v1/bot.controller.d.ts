import { MerchantListBotHandler } from 'src/application/usecase/bot/merchant-list-bot';
export declare class MerchantV1BotController {
    private merchantListBotHandler;
    constructor(merchantListBotHandler: MerchantListBotHandler);
    listBot(): Promise<import("../../../../application/usecase/bot/validate").MerchantBotValidate[]>;
}
