import { MerchantUpdateProfileHandler, UpdateEmailSenderHandler, VerifyEmailSenderHandler } from 'src/application';
import { GetMerchantHandler } from 'src/application/usecase/merchant/get-merchant';
import { MerchantVerifyPasswordHandler } from 'src/application/usecase/merchant/merchant-check-password';
import { MerchantVerifyPasswordInput, MerchantVerifyPasswordOutput } from 'src/application/usecase/merchant/merchant-check-password/validate';
import { CreateWalletHandler } from 'src/application/usecase/merchant/merchant-create-wallet';
import { MerchantCreateInput } from 'src/application/usecase/merchant/merchant-create-wallet/validate';
import { SendOtpWalletHandler } from 'src/application/usecase/merchant/merchant-send-otp';
import { MerchantSendOtpInput } from 'src/application/usecase/merchant/merchant-send-otp/validate';
import { MerchantUpdatePasswordHandler } from 'src/application/usecase/merchant/merchant-update-password';
import { MerchantUpdatePasswordInput } from 'src/application/usecase/merchant/merchant-update-password/validate';
import { MerchantUpdateProfileInput } from 'src/application/usecase/merchant/merchant-update-profile/validate';
import { UpdateEmailSenderInput } from 'src/application/usecase/merchant/update-email-sender/validate';
import { VerifyEmailSenderInput } from 'src/application/usecase/merchant/verify-email-sender/validate';
export declare class MerchantV1ProfileController {
    private getMerchantHandler;
    private merchantUpdatePasswordHandler;
    private merchantUpdateProfileHandler;
    private verifyEmailSenderHandler;
    private updateEmailSenderHandler;
    private merchantVerifyPasswordHandler;
    private sendOtpWalletHandler;
    private createWalletHandler;
    constructor(getMerchantHandler: GetMerchantHandler, merchantUpdatePasswordHandler: MerchantUpdatePasswordHandler, merchantUpdateProfileHandler: MerchantUpdateProfileHandler, verifyEmailSenderHandler: VerifyEmailSenderHandler, updateEmailSenderHandler: UpdateEmailSenderHandler, merchantVerifyPasswordHandler: MerchantVerifyPasswordHandler, sendOtpWalletHandler: SendOtpWalletHandler, createWalletHandler: CreateWalletHandler);
    getProfile(req: any): Promise<import("../../../../application/usecase/merchant/validate").MerchantValidate>;
    updateProfile(req: any, body: MerchantUpdateProfileInput): Promise<boolean>;
    changePassword(req: any, body: MerchantUpdatePasswordInput): Promise<boolean>;
    verifySender(req: any, body: VerifyEmailSenderInput): Promise<any>;
    updateSender(req: any, body: UpdateEmailSenderInput): Promise<any>;
    verifyPassWord(param: MerchantVerifyPasswordInput, req: any): Promise<MerchantVerifyPasswordOutput>;
    sendOtpWallet(param: MerchantSendOtpInput, req: any): Promise<boolean>;
    createWallet(param: MerchantCreateInput, req: any): Promise<boolean>;
}
