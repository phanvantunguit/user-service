"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1CommissionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const validate_1 = require("../../../../application/usecase/merchant/admin-update-merchant/validate");
const validate_2 = require("../../../../application/usecase/merchant/list-merchant-commission/validate");
const validate_3 = require("../../../../application/usecase/merchant/update-merchant-commission/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1CommissionController = class MerchantV1CommissionController {
    constructor(updateMerchantCommissionHandler, listMerchantCommissionHandler) {
        this.updateMerchantCommissionHandler = updateMerchantCommissionHandler;
        this.listMerchantCommissionHandler = listMerchantCommissionHandler;
    }
    async updateCommission(req, body) {
        const user_id = req.user.user_id;
        const result = await this.updateMerchantCommissionHandler.execute(body, user_id);
        return result.payload;
    }
    async listBotCommission(req, query) {
        const user_id = req.user.user_id;
        const result = await this.listMerchantCommissionHandler.execute(query, user_id);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Put)('/update'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update commission',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_3.MerchantUpdateCommissionInput]),
    __metadata("design:returntype", Promise)
], MerchantV1CommissionController.prototype, "updateCommission", null);
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListMerchantCommissioOutputMap extends (0, swagger_1.IntersectionType)(validate_2.ListMerchantCommissioOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List bot trading',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_2.ListMerchantCommissionInput]),
    __metadata("design:returntype", Promise)
], MerchantV1CommissionController.prototype, "listBotCommission", null);
MerchantV1CommissionController = __decorate([
    (0, swagger_1.ApiTags)('merchant/commission'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/commission`),
    __param(0, (0, common_1.Inject)(application_1.UpdateMerchantCommissionHandler)),
    __param(1, (0, common_1.Inject)(application_1.ListMerchantCommissionHandler)),
    __metadata("design:paramtypes", [application_1.UpdateMerchantCommissionHandler,
        application_1.ListMerchantCommissionHandler])
], MerchantV1CommissionController);
exports.MerchantV1CommissionController = MerchantV1CommissionController;
//# sourceMappingURL=commission.controller.js.map