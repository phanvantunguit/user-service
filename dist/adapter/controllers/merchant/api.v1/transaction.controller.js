"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1TransactionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const merchant_get_transaction_1 = require("../../../../application/usecase/transaction/merchant-get-transaction");
const merchant_list_transaction_1 = require("../../../../application/usecase/transaction/merchant-list-transaction");
const validate_1 = require("../../../../application/usecase/transaction/merchant-list-transaction/validate");
const report_transaction_1 = require("../../../../application/usecase/transaction/report-transaction");
const validate_2 = require("../../../../application/usecase/transaction/report-transaction-chart/validate");
const validate_3 = require("../../../../application/usecase/transaction/report-transaction/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1TransactionController = class MerchantV1TransactionController {
    constructor(merchantListTransactionHandler, merchantGetTransactionHandler, reportTransactionHandler, reportTransactionChartHandler) {
        this.merchantListTransactionHandler = merchantListTransactionHandler;
        this.merchantGetTransactionHandler = merchantGetTransactionHandler;
        this.reportTransactionHandler = reportTransactionHandler;
        this.reportTransactionChartHandler = reportTransactionChartHandler;
    }
    async listTransaction(req, query) {
        const merchant_code = req.user.merchant_code;
        const result = await this.merchantListTransactionHandler.execute(query, merchant_code);
        return result.payload;
    }
    async report(req, query) {
        const merchant_code = req.user.merchant_code;
        const merchant_id = req.user.user_id;
        const result = await this.reportTransactionHandler.execute(query, merchant_id, merchant_code);
        return result.payload;
    }
    async chart(req, query) {
        const merchant_code = req.user.merchant_code;
        const result = await this.reportTransactionChartHandler.execute(query, merchant_code);
        return result.payload;
    }
    async getTransaction(id, req) {
        const merchant_code = req.user.merchant_code;
        const result = await this.merchantGetTransactionHandler.execute({
            id,
            merchant_code,
        });
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListTransactionOutputMap extends (0, swagger_1.IntersectionType)(validate_1.ListTransactionOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List transaction',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_1.ListTransactionInput]),
    __metadata("design:returntype", Promise)
], MerchantV1TransactionController.prototype, "listTransaction", null);
__decorate([
    (0, common_1.Get)('/report'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ReportTransactionOutputMap extends (0, swagger_1.IntersectionType)(validate_3.ReportTransactionOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Report transaction',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_3.ReportTransactionInput]),
    __metadata("design:returntype", Promise)
], MerchantV1TransactionController.prototype, "report", null);
__decorate([
    (0, common_1.Get)('/chart'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ReportTransactionChartOutputMap extends (0, swagger_1.IntersectionType)(validate_2.ReportTransactionChartOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Report transaction chart',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_2.ReportTransactionChartInput]),
    __metadata("design:returntype", Promise)
], MerchantV1TransactionController.prototype, "chart", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListTransactionOutputMap extends (0, swagger_1.IntersectionType)(validate_1.ListTransactionOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get transaction',
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1TransactionController.prototype, "getTransaction", null);
MerchantV1TransactionController = __decorate([
    (0, swagger_1.ApiTags)('merchant/transaction'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/transaction`),
    __param(0, (0, common_1.Inject)(merchant_list_transaction_1.MerchantListTransactionHandler)),
    __param(1, (0, common_1.Inject)(merchant_get_transaction_1.MerchantGetTransactionHandler)),
    __param(2, (0, common_1.Inject)(report_transaction_1.ReportTransactionHandler)),
    __param(3, (0, common_1.Inject)(application_1.ReportTransactionChartHandler)),
    __metadata("design:paramtypes", [merchant_list_transaction_1.MerchantListTransactionHandler,
        merchant_get_transaction_1.MerchantGetTransactionHandler,
        report_transaction_1.ReportTransactionHandler,
        application_1.ReportTransactionChartHandler])
], MerchantV1TransactionController);
exports.MerchantV1TransactionController = MerchantV1TransactionController;
//# sourceMappingURL=transaction.controller.js.map