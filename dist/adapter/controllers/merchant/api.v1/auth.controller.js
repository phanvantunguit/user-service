"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1AuthController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const merchant_login_1 = require("../../../../application/usecase/merchant/merchant-login");
const validate_1 = require("../../../../application/usecase/merchant/merchant-login/validate");
const types_1 = require("../../../../domain/auth/types");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1AuthController = class MerchantV1AuthController {
    constructor(merchantLoginHandler) {
        this.merchantLoginHandler = merchantLoginHandler;
    }
    async login(body, query) {
        const result = await this.merchantLoginHandler.execute(body, query.merchant_code);
        return result.payload;
    }
};
__decorate([
    (0, auth_1.MerchantPermission)(types_1.WITHOUT_AUTHORIZATION),
    (0, common_1.Post)('login'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminLoginOutputMap extends (0, swagger_1.IntersectionType)(validate_1.MerchantLoginOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Login',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.MerchantLoginInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1AuthController.prototype, "login", null);
MerchantV1AuthController = __decorate([
    (0, swagger_1.ApiTags)('merchant/auth'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/auth`),
    __param(0, (0, common_1.Inject)(merchant_login_1.MerchantLoginHandler)),
    __metadata("design:paramtypes", [merchant_login_1.MerchantLoginHandler])
], MerchantV1AuthController);
exports.MerchantV1AuthController = MerchantV1AuthController;
//# sourceMappingURL=auth.controller.js.map