import { MerchantListRoleHandler } from 'src/application';
export declare class MerchantV1RoleController {
    private merchantListRoleHandler;
    constructor(merchantListRoleHandler: MerchantListRoleHandler);
    listRole(): Promise<import("../../../../application/usecase/role/validate").MerchantRoleValidate[]>;
}
