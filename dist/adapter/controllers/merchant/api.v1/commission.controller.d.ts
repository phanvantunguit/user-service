import { UpdateMerchantCommissionHandler, ListMerchantCommissionHandler } from 'src/application';
import { ListMerchantCommissionInput } from 'src/application/usecase/merchant/list-merchant-commission/validate';
import { MerchantUpdateCommissionInput } from 'src/application/usecase/merchant/update-merchant-commission/validate';
export declare class MerchantV1CommissionController {
    private updateMerchantCommissionHandler;
    private listMerchantCommissionHandler;
    constructor(updateMerchantCommissionHandler: UpdateMerchantCommissionHandler, listMerchantCommissionHandler: ListMerchantCommissionHandler);
    updateCommission(req: any, body: MerchantUpdateCommissionInput): Promise<boolean>;
    listBotCommission(req: any, query: ListMerchantCommissionInput): Promise<import("src/application/usecase/merchant/list-merchant-commission/validate").PagingListMerchantCommissionOutput>;
}
