import { MerchantListBotTradingHandler } from 'src/application/usecase/bot/merchant-list-bot-trading';
export declare class MerchantV1BotTradingController {
    private merchantListBotTradingHandler;
    constructor(merchantListBotTradingHandler: MerchantListBotTradingHandler);
    listBot(query: any, req: any): Promise<import("../../../../application/usecase/bot/validate").MerchantBotValidate[]>;
}
