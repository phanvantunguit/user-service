"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1ProfileController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const validate_1 = require("../../../../application/usecase/merchant/admin-update-merchant/validate");
const get_merchant_1 = require("../../../../application/usecase/merchant/get-merchant");
const validate_2 = require("../../../../application/usecase/merchant/get-merchant/validate");
const merchant_check_password_1 = require("../../../../application/usecase/merchant/merchant-check-password");
const validate_3 = require("../../../../application/usecase/merchant/merchant-check-password/validate");
const merchant_create_wallet_1 = require("../../../../application/usecase/merchant/merchant-create-wallet");
const validate_4 = require("../../../../application/usecase/merchant/merchant-create-wallet/validate");
const merchant_send_otp_1 = require("../../../../application/usecase/merchant/merchant-send-otp");
const validate_5 = require("../../../../application/usecase/merchant/merchant-send-otp/validate");
const merchant_update_password_1 = require("../../../../application/usecase/merchant/merchant-update-password");
const validate_6 = require("../../../../application/usecase/merchant/merchant-update-password/validate");
const validate_7 = require("../../../../application/usecase/merchant/merchant-update-profile/validate");
const validate_8 = require("../../../../application/usecase/merchant/update-email-sender/validate");
const validate_9 = require("../../../../application/usecase/merchant/verify-email-sender/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1ProfileController = class MerchantV1ProfileController {
    constructor(getMerchantHandler, merchantUpdatePasswordHandler, merchantUpdateProfileHandler, verifyEmailSenderHandler, updateEmailSenderHandler, merchantVerifyPasswordHandler, sendOtpWalletHandler, createWalletHandler) {
        this.getMerchantHandler = getMerchantHandler;
        this.merchantUpdatePasswordHandler = merchantUpdatePasswordHandler;
        this.merchantUpdateProfileHandler = merchantUpdateProfileHandler;
        this.verifyEmailSenderHandler = verifyEmailSenderHandler;
        this.updateEmailSenderHandler = updateEmailSenderHandler;
        this.merchantVerifyPasswordHandler = merchantVerifyPasswordHandler;
        this.sendOtpWalletHandler = sendOtpWalletHandler;
        this.createWalletHandler = createWalletHandler;
    }
    async getProfile(req) {
        const user_id = req.user.user_id;
        const result = await this.getMerchantHandler.execute(user_id);
        return result.payload;
    }
    async updateProfile(req, body) {
        const user_id = req.user.user_id;
        const result = await this.merchantUpdateProfileHandler.execute(body, user_id);
        return result.payload;
    }
    async changePassword(req, body) {
        const user_id = req.user.user_id;
        const result = await this.merchantUpdatePasswordHandler.execute(body, user_id);
        return result.payload;
    }
    async verifySender(req, body) {
        const user_id = req.user.user_id;
        const result = await this.verifyEmailSenderHandler.execute(body, user_id);
        return result.payload;
    }
    async updateSender(req, body) {
        const user_id = req.user.user_id;
        const result = await this.updateEmailSenderHandler.execute(body, user_id);
        return result.payload;
    }
    async verifyPassWord(param, req) {
        const user_id = req.user.user_id;
        const result = await this.merchantVerifyPasswordHandler.execute(param, user_id);
        return result;
    }
    async sendOtpWallet(param, req) {
        const user_id = req.user.user_id;
        await this.sendOtpWalletHandler.execute(param, user_id);
        return true;
    }
    async createWallet(param, req) {
        const user_id = req.user.user_id;
        await this.createWalletHandler.execute(param, user_id);
        return true;
    }
};
__decorate([
    (0, common_1.Get)(''),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantGetProfileOutputMap extends (0, swagger_1.IntersectionType)(validate_2.GetMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get profile',
    }),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "getProfile", null);
__decorate([
    (0, common_1.Put)(''),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantUpdateProfileOutputMap extends (0, swagger_1.IntersectionType)(validate_7.MerchantUpdateProfileOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update Profile',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_7.MerchantUpdateProfileInput]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "updateProfile", null);
__decorate([
    (0, common_1.Put)('change-password'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantUpdatePasswordOutputMap extends (0, swagger_1.IntersectionType)(validate_6.MerchantUpdatePasswordOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Change password',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_6.MerchantUpdatePasswordInput]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "changePassword", null);
__decorate([
    (0, common_1.Put)('/verify-sender'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Verify sender',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_9.VerifyEmailSenderInput]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "verifySender", null);
__decorate([
    (0, common_1.Put)('/update-sender'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminUpdateMerchantOutputMap extends (0, swagger_1.IntersectionType)(validate_1.AdminUpdateMerchantOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update sender',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_8.UpdateEmailSenderInput]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "updateSender", null);
__decorate([
    (0, common_1.Put)('/verify-password'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantAddWalletOutputMap extends (0, swagger_1.IntersectionType)(validate_3.MerchantVerifyPasswordOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Verify password',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.MerchantVerifyPasswordInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "verifyPassWord", null);
__decorate([
    (0, common_1.Put)('/send-otp-wallet'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantSendOtpWalletOutput extends (0, swagger_1.IntersectionType)(validate_5.MerchantSendOtpOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Send OTP',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_5.MerchantSendOtpInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "sendOtpWallet", null);
__decorate([
    (0, common_1.Put)('/create-wallet'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantSendOtpWalletOutput extends (0, swagger_1.IntersectionType)(validate_4.MerchantCreateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create Wallet',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_4.MerchantCreateInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1ProfileController.prototype, "createWallet", null);
MerchantV1ProfileController = __decorate([
    (0, swagger_1.ApiTags)('merchant/profile'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/profile`),
    __param(0, (0, common_1.Inject)(get_merchant_1.GetMerchantHandler)),
    __param(1, (0, common_1.Inject)(merchant_update_password_1.MerchantUpdatePasswordHandler)),
    __param(2, (0, common_1.Inject)(application_1.MerchantUpdateProfileHandler)),
    __param(3, (0, common_1.Inject)(application_1.VerifyEmailSenderHandler)),
    __param(4, (0, common_1.Inject)(application_1.UpdateEmailSenderHandler)),
    __param(5, (0, common_1.Inject)(merchant_check_password_1.MerchantVerifyPasswordHandler)),
    __param(6, (0, common_1.Inject)(merchant_send_otp_1.SendOtpWalletHandler)),
    __param(7, (0, common_1.Inject)(merchant_create_wallet_1.CreateWalletHandler)),
    __metadata("design:paramtypes", [get_merchant_1.GetMerchantHandler,
        merchant_update_password_1.MerchantUpdatePasswordHandler,
        application_1.MerchantUpdateProfileHandler,
        application_1.VerifyEmailSenderHandler,
        application_1.UpdateEmailSenderHandler,
        merchant_check_password_1.MerchantVerifyPasswordHandler,
        merchant_send_otp_1.SendOtpWalletHandler,
        merchant_create_wallet_1.CreateWalletHandler])
], MerchantV1ProfileController);
exports.MerchantV1ProfileController = MerchantV1ProfileController;
//# sourceMappingURL=profile.controller.js.map