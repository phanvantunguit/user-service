"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1InvoiceController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const create_merchant_invoice_by_amount_1 = require("../../../../application/usecase/merchant/create-merchant-invoice-by-amount");
const validate_1 = require("../../../../application/usecase/merchant/create-merchant-invoice-by-amount/validate");
const get_merchant_invoice_1 = require("../../../../application/usecase/merchant/get-merchant-invoice");
const validate_2 = require("../../../../application/usecase/merchant/get-merchant-invoice/validate");
const validate_3 = require("../../../../application/usecase/merchant/list-merchant-invoice/validate");
const validate_4 = require("../../../../application/usecase/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1InvoiceController = class MerchantV1InvoiceController {
    constructor(listMerchantInvoiceHandler, getMerchantInvoiceHandler, createMerchantInvoiceByAmountHandler) {
        this.listMerchantInvoiceHandler = listMerchantInvoiceHandler;
        this.getMerchantInvoiceHandler = getMerchantInvoiceHandler;
        this.createMerchantInvoiceByAmountHandler = createMerchantInvoiceByAmountHandler;
    }
    async listMerchantInvoice(query, req) {
        const merchant_id = req.user.user_id;
        query.merchant_id = merchant_id;
        const result = await this.listMerchantInvoiceHandler.execute(query);
        return result.payload;
    }
    async getMerchantInvoice(id) {
        const result = await this.getMerchantInvoiceHandler.execute(id);
        return result.payload;
    }
    async createMerchantInvoice(body, req) {
        const merchant_id = req.user.user_id;
        const merchant_code = req.user.merchant_code;
        const result = await this.createMerchantInvoiceByAmountHandler.execute(body, merchant_id, merchant_code);
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('/list-invoice'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListMerchantInvoiceOutputMap extends (0, swagger_1.IntersectionType)(validate_3.ListMerchantInvoiceOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List invoice',
    }),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.ListMerchantInvoiceInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1InvoiceController.prototype, "listMerchantInvoice", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class AdminGetMerchantInvoiceOutputMap extends (0, swagger_1.IntersectionType)(validate_2.GetMerchantInvoiceIdOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get one invoice by id',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MerchantV1InvoiceController.prototype, "getMerchantInvoice", null);
__decorate([
    (0, common_1.Post)('/'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseCreateOutputMap extends (0, swagger_1.IntersectionType)(validate_4.BaseCreateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create invoice',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.CreateMerchantInvoiceByAmountInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1InvoiceController.prototype, "createMerchantInvoice", null);
MerchantV1InvoiceController = __decorate([
    (0, swagger_1.ApiTags)('merchant/invoices'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/invoices`),
    __param(0, (0, common_1.Inject)(application_1.ListMerchantInvoiceHandler)),
    __param(1, (0, common_1.Inject)(get_merchant_invoice_1.GetMerchantInvoiceHandler)),
    __param(2, (0, common_1.Inject)(create_merchant_invoice_by_amount_1.CreateMerchantInvoiceByAmountHandler)),
    __metadata("design:paramtypes", [application_1.ListMerchantInvoiceHandler,
        get_merchant_invoice_1.GetMerchantInvoiceHandler,
        create_merchant_invoice_by_amount_1.CreateMerchantInvoiceByAmountHandler])
], MerchantV1InvoiceController);
exports.MerchantV1InvoiceController = MerchantV1InvoiceController;
//# sourceMappingURL=invoices.controller.js.map