import { ReportTransactionChartHandler } from 'src/application';
import { MerchantGetTransactionHandler } from 'src/application/usecase/transaction/merchant-get-transaction';
import { MerchantListTransactionHandler } from 'src/application/usecase/transaction/merchant-list-transaction';
import { ListTransactionInput } from 'src/application/usecase/transaction/merchant-list-transaction/validate';
import { ReportTransactionHandler } from 'src/application/usecase/transaction/report-transaction';
import { ReportTransactionChartInput } from 'src/application/usecase/transaction/report-transaction-chart/validate';
import { ReportTransactionInput } from 'src/application/usecase/transaction/report-transaction/validate';
export declare class MerchantV1TransactionController {
    private merchantListTransactionHandler;
    private merchantGetTransactionHandler;
    private reportTransactionHandler;
    private reportTransactionChartHandler;
    constructor(merchantListTransactionHandler: MerchantListTransactionHandler, merchantGetTransactionHandler: MerchantGetTransactionHandler, reportTransactionHandler: ReportTransactionHandler, reportTransactionChartHandler: ReportTransactionChartHandler);
    listTransaction(req: any, query: ListTransactionInput): Promise<import("src/application/usecase/transaction/merchant-list-transaction/validate").PagingTransactionOutput>;
    report(req: any, query: ReportTransactionInput): Promise<import("src/application/usecase/transaction/report-transaction/validate").ReportTransactionValidate>;
    chart(req: any, query: ReportTransactionChartInput): Promise<import("src/application/usecase/transaction/report-transaction-chart/validate").ReportTransactionChartValidate[]>;
    getTransaction(id: string, req: any): Promise<import("../../../../application/usecase/transaction/validate").TransactionDetailValidate>;
}
