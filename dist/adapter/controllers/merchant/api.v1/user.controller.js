"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const list_inactive_by_system_1 = require("../../../../application/usecase/event-store/list-inactive-by-system");
const validate_1 = require("../../../../application/usecase/event-store/list-inactive-by-system/validate");
const get_number_bot_user_1 = require("../../../../application/usecase/merchant/get-number-bot-user");
const validate_2 = require("../../../../application/usecase/merchant/get-number-bot-user/validate");
const merchant_add_number_bot_1 = require("../../../../application/usecase/merchant/merchant-add-number-bot");
const validate_3 = require("../../../../application/usecase/merchant/merchant-add-number-bot/validate");
const merchant_update_user_tbot_status_1 = require("../../../../application/usecase/merchant/merchant-update-user-tbot-status");
const validate_4 = require("../../../../application/usecase/merchant/merchant-update-user-tbot-status/validate");
const merchant_add_user_asset_1 = require("../../../../application/usecase/merchant/user-asset/merchant-add-user-asset");
const validate_5 = require("../../../../application/usecase/merchant/user-asset/merchant-add-user-asset/validate");
const merchant_remove_user_asset_1 = require("../../../../application/usecase/merchant/user-asset/merchant-remove-user-asset");
const validate_6 = require("../../../../application/usecase/merchant/user-asset/merchant-remove-user-asset/validate");
const chart_visitor_1 = require("../../../../application/usecase/user/chart-visitor");
const create_user_1 = require("../../../../application/usecase/user/create-user");
const validate_7 = require("../../../../application/usecase/user/create-user/validate");
const get_user_1 = require("../../../../application/usecase/user/get-user");
const list_user_1 = require("../../../../application/usecase/user/list-user");
const validate_8 = require("../../../../application/usecase/user/list-user/validate");
const report_user_1 = require("../../../../application/usecase/user/report-user");
const validate_9 = require("../../../../application/usecase/user/report-user/validate");
const update_user_1 = require("../../../../application/usecase/user/update-user");
const validate_10 = require("../../../../application/usecase/user/update-user/validate");
const merchant_repository_1 = require("../../../../infrastructure/data/database/merchant.repository");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1UserController = class MerchantV1UserController {
    constructor(listUserHandler, reportUserHandler, reportChartVisitorHandler, merchantAddUserAssetHandler, merchantRemoveUserAssetHandler, merchantUserCreateServiceHandler, merchantRepository, getUserHandler, merchantUserUpdateServiceHandler, merchantAddBotForUserHandle, merchantUpdateUserBotTradingStatusHandler, getNumberOfBotUserHandler, listInactiveBySystemHandler) {
        this.listUserHandler = listUserHandler;
        this.reportUserHandler = reportUserHandler;
        this.reportChartVisitorHandler = reportChartVisitorHandler;
        this.merchantAddUserAssetHandler = merchantAddUserAssetHandler;
        this.merchantRemoveUserAssetHandler = merchantRemoveUserAssetHandler;
        this.merchantUserCreateServiceHandler = merchantUserCreateServiceHandler;
        this.merchantRepository = merchantRepository;
        this.getUserHandler = getUserHandler;
        this.merchantUserUpdateServiceHandler = merchantUserUpdateServiceHandler;
        this.merchantAddBotForUserHandle = merchantAddBotForUserHandle;
        this.merchantUpdateUserBotTradingStatusHandler = merchantUpdateUserBotTradingStatusHandler;
        this.getNumberOfBotUserHandler = getNumberOfBotUserHandler;
        this.listInactiveBySystemHandler = listInactiveBySystemHandler;
    }
    async listTransaction(req, query) {
        const merchant_code = req.user.merchant_code;
        const result = await this.listUserHandler.execute(query, merchant_code);
        return result.payload;
    }
    async report(req, query) {
        const merchant_code = req.user.merchant_code;
        const result = await this.reportUserHandler.execute(query, merchant_code);
        return result.payload;
    }
    async chart(req) {
        const merchant_code = req.user.merchant_code;
        const result = await this.reportChartVisitorHandler.execute(merchant_code);
        return result.payload;
    }
    async getUserById(req, id) {
        const merchant_code = req.user.merchant_code;
        const result = await this.getUserHandler.execute(id, merchant_code);
        return result.payload;
    }
    async addUserAsset(param, user_id, req) {
        const owner_created = req.user.user_id;
        const result = await this.merchantAddUserAssetHandler.execute(param, owner_created, user_id);
        return result.payload;
    }
    async removeUserAsset(param, req) {
        const owner_created = req.user.user_id;
        const result = await this.merchantRemoveUserAssetHandler.execute(param, owner_created);
        return result;
    }
    async createUserInput(param, req) {
        const owner_created = req.user.user_id;
        const result = await this.merchantUserCreateServiceHandler.execute(param, owner_created);
        return result;
    }
    async updateUserInput(param, user_id) {
        const result = await this.merchantUserUpdateServiceHandler.execute(param, user_id);
        return result;
    }
    async createAppSetting(param, id, req) {
        const ownerId = req.user.user_id;
        const result = await this.merchantAddBotForUserHandle.execute(param, id, ownerId);
        return result;
    }
    async updateStatusTbot(param, id, req) {
        const merchantId = req.user.user_id;
        const result = await this.merchantUpdateUserBotTradingStatusHandler.execute(param, id, merchantId);
        return result;
    }
    async getNumberBotOfUserById(req, id) {
        const merchant_id = req.user.user_id;
        const result = await this.getNumberOfBotUserHandler.execute(id, merchant_id);
        return result.payload;
    }
    async listInactiveBySystem(req, id, query) {
        const result = await this.listInactiveBySystemHandler.execute(Object.assign({ user_id: id }, query));
        return result.payload;
    }
};
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListUserOutputMap extends (0, swagger_1.IntersectionType)(validate_8.ListUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List user',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_8.ListUserInput]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "listTransaction", null);
__decorate([
    (0, common_1.Get)('/report'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ReportUserOutputMap extends (0, swagger_1.IntersectionType)(validate_9.ReportUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Report user',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validate_9.ReportUserInput]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "report", null);
__decorate([
    (0, common_1.Get)('/chart'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Report visitor chart',
    }),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "chart", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListUserOutputMap extends (0, swagger_1.IntersectionType)(validate_8.ListUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get user by id',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "getUserById", null);
__decorate([
    (0, common_1.Put)('/add-user-asset/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantAddUserAssetOutputMap extends (0, swagger_1.IntersectionType)(validate_5.MerchantCreateAssetOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Add user asset',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_5.MerchantCreateAssetInput, String, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "addUserAsset", null);
__decorate([
    (0, common_1.Delete)('/remove-user-asset'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantRemoveUserAssetOutputMap extends (0, swagger_1.IntersectionType)(validate_6.MerchantRemoveAssetOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Remove user asset',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_6.MerchantRemoveAssetInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "removeUserAsset", null);
__decorate([
    (0, common_1.Post)('/create-user'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantCreateUserInputMap extends (0, swagger_1.IntersectionType)(validate_7.MerchantCreateUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Create user input',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_7.PutUserProfileDto, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "createUserInput", null);
__decorate([
    (0, common_1.Put)('/:id/update-user'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantUpdateUserInputMap extends (0, swagger_1.IntersectionType)(validate_10.MerchantUpdateUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update user input',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_10.UpdateUserProfileDto, String]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "updateUserInput", null);
__decorate([
    (0, common_1.Put)('/add-number-bot-for-user/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantAddNumberBotDtoMAP extends (0, swagger_1.IntersectionType)(validate_3.MerchantAddNumberBotOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Add number bot for user',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_3.MerchantAddNumberBotDto, String, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "createAppSetting", null);
__decorate([
    (0, common_1.Put)('/setting-active-tbot/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class MerchantUpdateStatusInputMap extends (0, swagger_1.IntersectionType)(validate_4.MerchantUpdateStatusInputOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Update status tbot',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_4.MerchantUpdateStatusInput, String, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "updateStatusTbot", null);
__decorate([
    (0, common_1.Get)('/get-number-bot-for-user/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListNumberBotOfUserOutputMap extends (0, swagger_1.IntersectionType)(validate_2.GetNumberOfBotForUserOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get number of bot for by userId',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "getNumberBotOfUserById", null);
__decorate([
    (0, common_1.Get)('/:id/list-inactive-by-system'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListEventInactiveBySystemOutputMap extends (0, swagger_1.IntersectionType)(validate_1.ListEventInactiveBySystemOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List inactive by system',
    }),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('id')),
    __param(2, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, validate_1.ListEventInactiveBySystemInput]),
    __metadata("design:returntype", Promise)
], MerchantV1UserController.prototype, "listInactiveBySystem", null);
MerchantV1UserController = __decorate([
    (0, swagger_1.ApiTags)('merchant/user'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/user`),
    __param(0, (0, common_1.Inject)(list_user_1.ListUserHandler)),
    __param(1, (0, common_1.Inject)(report_user_1.ReportUserHandler)),
    __param(2, (0, common_1.Inject)(chart_visitor_1.ReportChartVisitorHandler)),
    __param(3, (0, common_1.Inject)(merchant_add_user_asset_1.MerchantAddUserAssetHandler)),
    __param(4, (0, common_1.Inject)(merchant_remove_user_asset_1.MerchantRemoveUserAssetHandler)),
    __param(5, (0, common_1.Inject)(create_user_1.MerchantUserCreateServiceHandler)),
    __param(6, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __param(7, (0, common_1.Inject)(get_user_1.GetUserHandler)),
    __param(8, (0, common_1.Inject)(update_user_1.MerchantUserUpdateServiceHandler)),
    __param(9, (0, common_1.Inject)(merchant_add_number_bot_1.MerchantAddBotForUserHandle)),
    __param(10, (0, common_1.Inject)(merchant_update_user_tbot_status_1.MerchantUpdateUserBotTradingStatusHandler)),
    __param(11, (0, common_1.Inject)(get_number_bot_user_1.GetNumberOfBotUserHandler)),
    __param(12, (0, common_1.Inject)(list_inactive_by_system_1.ListInactiveBySystemHandler)),
    __metadata("design:paramtypes", [list_user_1.ListUserHandler,
        report_user_1.ReportUserHandler,
        chart_visitor_1.ReportChartVisitorHandler,
        merchant_add_user_asset_1.MerchantAddUserAssetHandler,
        merchant_remove_user_asset_1.MerchantRemoveUserAssetHandler,
        create_user_1.MerchantUserCreateServiceHandler,
        merchant_repository_1.MerchantRepository,
        get_user_1.GetUserHandler,
        update_user_1.MerchantUserUpdateServiceHandler,
        merchant_add_number_bot_1.MerchantAddBotForUserHandle,
        merchant_update_user_tbot_status_1.MerchantUpdateUserBotTradingStatusHandler,
        get_number_bot_user_1.GetNumberOfBotUserHandler,
        list_inactive_by_system_1.ListInactiveBySystemHandler])
], MerchantV1UserController);
exports.MerchantV1UserController = MerchantV1UserController;
//# sourceMappingURL=user.controller.js.map