import { ListMerchantInvoiceHandler } from 'src/application';
import { CreateMerchantInvoiceByAmountHandler } from 'src/application/usecase/merchant/create-merchant-invoice-by-amount';
import { CreateMerchantInvoiceByAmountInput } from 'src/application/usecase/merchant/create-merchant-invoice-by-amount/validate';
import { GetMerchantInvoiceHandler } from 'src/application/usecase/merchant/get-merchant-invoice';
import { ListMerchantInvoiceInput } from 'src/application/usecase/merchant/list-merchant-invoice/validate';
export declare class MerchantV1InvoiceController {
    private listMerchantInvoiceHandler;
    private getMerchantInvoiceHandler;
    private createMerchantInvoiceByAmountHandler;
    constructor(listMerchantInvoiceHandler: ListMerchantInvoiceHandler, getMerchantInvoiceHandler: GetMerchantInvoiceHandler, createMerchantInvoiceByAmountHandler: CreateMerchantInvoiceByAmountHandler);
    listMerchantInvoice(query: ListMerchantInvoiceInput, req: any): Promise<import("src/application/usecase/merchant/list-merchant-invoice/validate").PagingListMerchantInvoiceOutput>;
    getMerchantInvoice(id: string): Promise<any>;
    createMerchantInvoice(body: CreateMerchantInvoiceByAmountInput, req: any): Promise<string>;
}
