"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantV1AdditionalDataController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const const_1 = require("../../const");
const application_1 = require("../../../../application");
const list_merchant_additional_data_1 = require("../../../../application/usecase/additional-data/list-merchant-additional-data");
const validate_1 = require("../../../../application/usecase/additional-data/list-merchant-additional-data/validate");
const validate_2 = require("../../../../application/usecase/additional-data/modify-merchant-additional-data/validate");
const validate_3 = require("../../../../application/usecase/validate");
const transform_interceptor_1 = require("../../transform.interceptor");
const auth_1 = require("../auth");
let MerchantV1AdditionalDataController = class MerchantV1AdditionalDataController {
    constructor(listMerchantAdditionalDataHandler, modifyMerchantAdditionalDataHandler) {
        this.listMerchantAdditionalDataHandler = listMerchantAdditionalDataHandler;
        this.modifyMerchantAdditionalDataHandler = modifyMerchantAdditionalDataHandler;
    }
    async listAdditionalData(query, req) {
        const user_id = req.user.user_id;
        const result = await this.listMerchantAdditionalDataHandler.execute(query, user_id);
        return result.payload;
    }
    async updateAdditionalData(body, req) {
        const user_id = req.user.user_id;
        const result = await this.modifyMerchantAdditionalDataHandler.execute(body, user_id);
        return result.payload;
    }
    async getAdditionalData(id) {
        const result = await this.listMerchantAdditionalDataHandler.execute({}, undefined, id);
        return result.payload[0];
    }
};
__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class ListMerchantAdditionalDataOutputMap extends (0, swagger_1.IntersectionType)(validate_1.ListMerchantAdditionalDataOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'List Additional Data',
    }),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_1.ListMerchantAdditionalDataInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1AdditionalDataController.prototype, "listAdditionalData", null);
__decorate([
    (0, common_1.Put)('/'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class BaseUpdateOutputMap extends (0, swagger_1.IntersectionType)(validate_3.BaseUpdateOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Modify Additional Data',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [validate_2.ModifyMerchantAdditionalDataInput, Object]),
    __metadata("design:returntype", Promise)
], MerchantV1AdditionalDataController.prototype, "updateAdditionalData", null);
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: class GetMerchantAdditionalDataOutputMap extends (0, swagger_1.IntersectionType)(validate_1.GetMerchantAdditionalDataOutput, transform_interceptor_1.BaseApiOutput) {
        },
        description: 'Get Additional Data',
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MerchantV1AdditionalDataController.prototype, "getAdditionalData", null);
MerchantV1AdditionalDataController = __decorate([
    (0, swagger_1.ApiTags)('merchant/additional-data'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(auth_1.MerchantAuthGuard),
    (0, common_1.UseInterceptors)(transform_interceptor_1.TransformInterceptor),
    (0, common_1.Controller)(`${const_1.ROOT_ROUTE.api_v1_merchant}/additional-data`),
    __param(0, (0, common_1.Inject)(list_merchant_additional_data_1.ListMerchantAdditionalDataHandler)),
    __param(1, (0, common_1.Inject)(application_1.ModifyMerchantAdditionalDataHandler)),
    __metadata("design:paramtypes", [list_merchant_additional_data_1.ListMerchantAdditionalDataHandler,
        application_1.ModifyMerchantAdditionalDataHandler])
], MerchantV1AdditionalDataController);
exports.MerchantV1AdditionalDataController = MerchantV1AdditionalDataController;
//# sourceMappingURL=additional-data.js.map