import { MerchantLoginHandler } from 'src/application/usecase/merchant/merchant-login';
import { MerchantLoginInput } from 'src/application/usecase/merchant/merchant-login/validate';
export declare class MerchantV1AuthController {
    private merchantLoginHandler;
    constructor(merchantLoginHandler: MerchantLoginHandler);
    login(body: MerchantLoginInput, query: any): Promise<string>;
}
