export declare enum VERIFY_TOKEN_TYPE {
    RESET_PASSWORD = "RESET_PASSWORD",
    VERIFY_EMAIL = "VERIFY_EMAIL",
    CHANGE_EMAIL = "CHANGE_EMAIL"
}
export declare const PasswordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*(),.~/?=|;:'\"{}<>]{8,}$";
export declare enum USER_TYPE {
    USER = "USER",
    ADMIN = "ADMIN"
}
export declare enum GENDER {
    MALE = "male",
    FEMALE = "female",
    OTHER = "other"
}
export declare enum USER_EVENT {
    LOGIN = "LOGIN"
}
