"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MERCHANT_COMMISION_STATUS = exports.MERCHANT_STATUS = exports.GENERAL_SETTING_LIMIT_TAB = exports.SYMBOL_STATUS = exports.BOT_STATUS = exports.ROLE_STATUS = exports.PACKAGE_TYPE = exports.USER_ROLE_STATUS = exports.ROLE_TYPE = exports.DEFAULT_ROLE = exports.ADMIN_PERMISSION_KEY = exports.ADMIN_PERMISSION_DATA = exports.WITHOUT_AUTHORIZATION = exports.ADMIN_PERMISSION = void 0;
exports.ADMIN_PERMISSION = {
    UPDATE_AUTH_USER_ROLE: {
        permission_id: 'UPDATE_AUTH_USER_ROLE',
        permission_name: 'Update admin auth user role',
        description: 'Admin are able to update admin auth user role',
    },
    CREATE_AUTH_ROLE: {
        permission_id: 'CREATE_AUTH_ROLE',
        permission_name: 'Create admin role',
        description: 'Admin are able to create new roles',
    },
    UPDATE_AUTH_ROLE: {
        permission_id: 'UPDATE_AUTH_ROLE',
        permission_name: 'Update admin role',
        description: 'Admin are able to update roles',
    },
    DELETE_AUTH_ROLE: {
        permission_id: 'DELETE_AUTH_ROLE',
        permission_name: 'Delete admin role',
        description: 'Admin are able to delete roles',
    },
    GET_AUTH_ROLE: {
        permission_id: 'GET_AUTH_ROLE',
        permission_name: 'View admin role',
        description: 'Admin are able to view roles',
    },
    UPDATE_USER_ROLE: {
        permission_id: 'UPDATE_USER_ROLE',
        permission_name: 'Update user role',
        description: 'Admin are able to user roles',
    },
    CREATE_FEATURE: {
        permission_id: 'CREATE_FEATURE',
        permission_name: 'Create feature',
        description: 'Admin are able to create feature',
    },
    UPDATE_FEATURE: {
        permission_id: 'UPDATE_FEATURE',
        permission_name: 'Update name of feature',
        description: 'Admin are able to update name of feature',
    },
    DELETE_FEATURE: {
        permission_id: 'DELETE_FEATURE',
        permission_name: 'Delete feature',
        description: 'Admin are able to delete feature',
    },
    GET_FEATURE: {
        permission_id: 'GET_FEATURE',
        permission_name: 'View features',
        description: 'Admin are able to view features',
    },
    CREATE_EXCHANGE: {
        permission_id: 'CREATE_EXCHANGE',
        permission_name: 'Create exchange',
        description: 'Admin are able to create exchange',
    },
    UPDATE_EXCHANGE: {
        permission_id: 'UPDATE_EXCHANGE',
        permission_name: 'Update description of exchange',
        description: 'Admin are able to update description of exchange',
    },
    DELETE_EXCHANGE: {
        permission_id: 'DELETE_EXCHANGE',
        permission_name: 'Delete exchange',
        description: 'Admin are able to delete exchange',
    },
    GET_EXCHANGE: {
        permission_id: 'GET_EXCHANGE',
        permission_name: 'View exchanges',
        description: 'Admin are able to view exchanges',
    },
    CREATE_GENERAL_SETTING: {
        permission_id: 'CREATE_GENERAL_SETTING',
        permission_name: 'Create general setting',
        description: 'Admin are able to create general setting',
    },
    UPDATE_GENERAL_SETTING: {
        permission_id: 'UPDATE_GENERAL_SETTING',
        permission_name: 'Update description of general setting',
        description: 'Admin are able to update general setting',
    },
    DELETE_GENERAL_SETTING: {
        permission_id: 'DELETE_GENERAL_SETTING',
        permission_name: 'Delete general setting',
        description: 'Admin are able to delete general setting',
    },
    GET_GENERAL_SETTING: {
        permission_id: 'GET_GENERAL_SETTING',
        permission_name: 'View general setting',
        description: 'Admin are able to view general setting',
    },
    CREATE_RESOLUTION: {
        permission_id: 'CREATE_RESOLUTION',
        permission_name: 'Create resolution',
        description: 'Admin are able to create resolution',
    },
    UPDATE_RESOLUTION: {
        permission_id: 'UPDATE_RESOLUTION',
        permission_name: 'Update resolution',
        description: 'Admin are able to update resolution',
    },
    DELETE_RESOLUTION: {
        permission_id: 'DELETE_RESOLUTION',
        permission_name: 'Delete resolution',
        description: 'Admin are able to delete resolution',
    },
    GET_RESOLUTION: {
        permission_id: 'GET_RESOLUTION',
        permission_name: 'View list resolution',
        description: 'Admin are able to view list resolution',
    },
    CREATE_SYMBOL: {
        permission_id: 'CREATE_SYMBOL',
        permission_name: 'Create symbol',
        description: 'Admin are able to create symbol',
    },
    UPDATE_SYMBOL: {
        permission_id: 'UPDATE_SYMBOL',
        permission_name: 'Update description of symbol',
        description: 'Admin are able to update symbol',
    },
    DELETE_SYMBOL: {
        permission_id: 'DELETE_SYMBOL',
        permission_name: 'Delete symbol',
        description: 'Admin are able to delete symbol',
    },
    GET_SYMBOL: {
        permission_id: 'GET_SYMBOL',
        permission_name: 'View symbol',
        description: 'Admin are able to view symbol',
    },
    GET_USER: {
        permission_id: 'GET_USER',
        permission_name: 'View user',
        description: 'Admin are able to view user',
    },
    CREATE_USER: {
        permission_id: 'CREATE_USER',
        permission_name: 'Create user',
        description: 'Admin are able to create user',
    },
    UPDATE_USER: {
        permission_id: 'UPDATE_USER',
        permission_name: 'Update user',
        description: 'Admin are able to update user',
    },
    GET_ADMIN: {
        permission_id: 'GET_ADMIN',
        permission_name: 'View admin',
        description: 'Admin are able to view admin',
    },
    CREATE_ADMIN: {
        permission_id: 'CREATE_ADMIN',
        permission_name: 'Create admin',
        description: 'Admin are able to create admin',
    },
    UPDATE_ADMIN: {
        permission_id: 'UPDATE_ADMIN',
        permission_name: 'Update admin',
        description: 'Admin are able to update admin',
    },
    GET_ROLE: {
        permission_id: 'GET_ROLE',
        permission_name: 'View role',
        description: 'Admin are able to view role',
    },
    CREATE_ROLE: {
        permission_id: 'CREATE_ROLE',
        permission_name: 'Create role',
        description: 'Admin are able to create role',
    },
    UPDATE_ROLE: {
        permission_id: 'UPDATE_ROLE',
        permission_name: 'Update role',
        description: 'Admin are able to update role',
    },
    DELETE_ROLE: {
        permission_id: 'DELETE_ROLE',
        permission_name: 'Delete role',
        description: 'Admin are able to delete role',
    },
    CREATE_APP_SETTING: {
        permission_id: 'CREATE_APP_SETTING',
        permission_name: 'Create app setting',
        description: 'Admin are able to create app setting',
    },
    UPDATE_APP_SETTING: {
        permission_id: 'UPDATE_APP_SETTING',
        permission_name: 'Update description of app setting',
        description: 'Admin are able to update app setting',
    },
    DELETE_APP_SETTING: {
        permission_id: 'DELETE_APP_SETTING',
        permission_name: 'Delete app setting',
        description: 'Admin are able to delete app setting',
    },
    GET_APP_SETTING: {
        permission_id: 'GET_APP_SETTING',
        permission_name: 'View app setting',
        description: 'Admin are able to view app setting',
    },
    GET_TRANSACTION: {
        permission_id: 'GET_TRANSACTION',
        permission_name: 'View transaction',
        description: 'Admin are able to view transaction',
    },
    UPDATE_TRANSACTION: {
        permission_id: 'UPDATE_TRANSACTION',
        permission_name: 'Update transaction',
        description: 'Admin are able to update transaction',
    },
    REQUEST_INTERVENTION: {
        permission_id: 'REQUEST_INTERVENTION',
        permission_name: 'Request intervention',
        description: 'Admin are able to request intervention',
    },
    GET_BOT_SETTING: {
        permission_id: 'GET_BOT_SETTING',
        permission_name: 'View bot setting',
        description: 'Admin are able to view bot setting',
    },
    CREATE_BOT_SETTING: {
        permission_id: 'CREATE_BOT_SETTING',
        permission_name: 'Create bot setting',
        description: 'Admin are able to create bot setting',
    },
    UPDATE_BOT_SETTING: {
        permission_id: 'UPDATE_BOT_SETTING',
        permission_name: 'Update bot setting',
        description: 'Admin are able to update bot setting',
    },
    DELETE_BOT_SETTING: {
        permission_id: 'DELETE_BOT_SETTING',
        permission_name: 'Delete bot setting',
        description: 'Admin are able to delete bot setting',
    },
    GET_PACKAGE: {
        permission_id: 'GET_PACKAGE',
        permission_name: 'View package',
        description: 'Admin are able to view package',
    },
    CREATE_PACKAGE: {
        permission_id: 'CREATE_PACKAGE',
        permission_name: 'Create package',
        description: 'Admin are able to create package',
    },
    UPDATE_PACKAGE: {
        permission_id: 'UPDATE_PACKAGE',
        permission_name: 'Update package',
        description: 'Admin are able to update package',
    },
    DELETE_PACKAGE: {
        permission_id: 'DELETE_PACKAGE',
        permission_name: 'Delete package',
        description: 'Admin are able to delete package',
    },
    GET_CURRENCY: {
        permission_id: 'GET_CURRENCY',
        permission_name: 'View currency',
        description: 'Admin are able to view currency',
    },
    CREATE_CURRENCY: {
        permission_id: 'CREATE_CURRENCY',
        permission_name: 'Create currency',
        description: 'Admin are able to create currency',
    },
    UPDATE_CURRENCY: {
        permission_id: 'UPDATE_CURRENCY',
        permission_name: 'Update currency',
        description: 'Admin are able to update currency',
    },
    DELETE_CURRENCY: {
        permission_id: 'DELETE_CURRENCY',
        permission_name: 'Delete currency',
        description: 'Admin are able to delete currency',
    },
    EXPORT_USER: {
        permission_id: 'EXPORT_USER',
        permission_name: 'Export user',
        description: 'Admin are able to export user',
    },
    EXPORT_TRANSACTION: {
        permission_id: 'EXPORT_TRANSACTION',
        permission_name: 'Export transaction',
        description: 'Admin are able to export transaction',
    },
    GET_BOT: {
        permission_id: 'GET_BOT',
        permission_name: 'View bot',
        description: 'Admin are able to view bot',
    },
    CREATE_BOT: {
        permission_id: 'CREATE_BOT',
        permission_name: 'Create bot',
        description: 'Admin are able to create bot',
    },
    UPDATE_BOT: {
        permission_id: 'UPDATE_BOT',
        permission_name: 'Update bot',
        description: 'Admin are able to update bot',
    },
    DELETE_BOT: {
        permission_id: 'DELETE_BOT',
        permission_name: 'Delete bot',
        description: 'Admin are able to delete bot',
    },
    GET_BOT_TRADING: {
        permission_id: 'GET_BOT_TRADING',
        permission_name: 'View bot trading',
        description: 'Admin are able to view bot trading',
    },
    CREATE_BOT_TRADING: {
        permission_id: 'CREATE_BOT_TRADING',
        permission_name: 'Create bot trading',
        description: 'Admin are able to create bot trading',
    },
    UPDATE_BOT_TRADING: {
        permission_id: 'UPDATE_BOT_TRADING',
        permission_name: 'Update bot trading',
        description: 'Admin are able to update bot trading',
    },
    DELETE_BOT_TRADING: {
        permission_id: 'DELETE_BOT_TRADING',
        permission_name: 'Delete bot trading',
        description: 'Admin are able to delete bot trading',
    },
    GET_BOT_TRADING_HISTORY: {
        permission_id: 'GET_BOT_TRADING_HISTORY',
        permission_name: 'View bot trading history',
        description: 'Admin are able to view bot trading history',
    },
    CREATE_BOT_TRADING_HISTORY: {
        permission_id: 'CREATE_BOT_TRADING_HISTORY',
        permission_name: 'Create bot trading history',
        description: 'Admin are able to create bot trading history',
    },
    UPDATE_BOT_TRADING_HISTORY: {
        permission_id: 'UPDATE_BOT_TRADING_HISTORY',
        permission_name: 'Update bot trading history',
        description: 'Admin are able to update bot trading history',
    },
    DELETE_BOT_TRADING_HISTORY: {
        permission_id: 'DELETE_BOT_TRADING_HISTORY',
        permission_name: 'Delete bot trading history',
        description: 'Admin are able to delete bot trading history',
    },
    GET_SYSTEM_TRADE_HISTORY: {
        permission_id: 'GET_SYSTEM_TRADE_HISTORY',
        permission_name: 'View system trade history',
        description: 'Admin are able to view system trade history',
    },
};
exports.WITHOUT_AUTHORIZATION = 'WITHOUT_AUTHORIZATION';
exports.ADMIN_PERMISSION_DATA = Object.values(exports.ADMIN_PERMISSION).map((permission) => permission);
exports.ADMIN_PERMISSION_KEY = Object.keys(exports.ADMIN_PERMISSION).map((key) => key);
exports.DEFAULT_ROLE = {
    id: '07b9102c-84bc-4170-b0e6-456a28cd820f',
    role_name: 'Free',
    root: {},
    description: 'free',
    owner_created: 'system',
    created_at: Date.now(),
    updated_at: Date.now(),
};
exports.ROLE_TYPE = {
    PACKAGE: 'PACKAGE',
    FEATURE: 'FEATURE',
    FREE: 'FREE',
    FREE_TRIAL: 'FREE_TRIAL',
    BEST_CHOICE: 'BEST_CHOICE',
};
var USER_ROLE_STATUS;
(function (USER_ROLE_STATUS) {
    USER_ROLE_STATUS["PROCESSING"] = "PROCESSING";
    USER_ROLE_STATUS["DISABLED"] = "DISABLED";
    USER_ROLE_STATUS["CURRENT_PLAN"] = "CURRENT_PLAN";
})(USER_ROLE_STATUS = exports.USER_ROLE_STATUS || (exports.USER_ROLE_STATUS = {}));
var PACKAGE_TYPE;
(function (PACKAGE_TYPE) {
    PACKAGE_TYPE["DAY"] = "DAY";
    PACKAGE_TYPE["MONTH"] = "MONTH";
})(PACKAGE_TYPE = exports.PACKAGE_TYPE || (exports.PACKAGE_TYPE = {}));
var ROLE_STATUS;
(function (ROLE_STATUS) {
    ROLE_STATUS["CLOSE"] = "CLOSE";
    ROLE_STATUS["OPEN"] = "OPEN";
    ROLE_STATUS["COMINGSOON"] = "COMINGSOON";
})(ROLE_STATUS = exports.ROLE_STATUS || (exports.ROLE_STATUS = {}));
var BOT_STATUS;
(function (BOT_STATUS) {
    BOT_STATUS["CLOSE"] = "CLOSE";
    BOT_STATUS["OPEN"] = "OPEN";
    BOT_STATUS["COMINGSOON"] = "COMINGSOON";
})(BOT_STATUS = exports.BOT_STATUS || (exports.BOT_STATUS = {}));
var SYMBOL_STATUS;
(function (SYMBOL_STATUS) {
    SYMBOL_STATUS["ON"] = "ON";
    SYMBOL_STATUS["OFF"] = "OFF";
})(SYMBOL_STATUS = exports.SYMBOL_STATUS || (exports.SYMBOL_STATUS = {}));
exports.GENERAL_SETTING_LIMIT_TAB = 'LIMIT_TAB';
var MERCHANT_STATUS;
(function (MERCHANT_STATUS) {
    MERCHANT_STATUS["ACTIVE"] = "ACTIVE";
    MERCHANT_STATUS["INACTIVE"] = "INACTIVE";
})(MERCHANT_STATUS = exports.MERCHANT_STATUS || (exports.MERCHANT_STATUS = {}));
var MERCHANT_COMMISION_STATUS;
(function (MERCHANT_COMMISION_STATUS) {
    MERCHANT_COMMISION_STATUS["ACTIVE"] = "ACTIVE";
    MERCHANT_COMMISION_STATUS["INACTIVE"] = "INACTIVE";
})(MERCHANT_COMMISION_STATUS = exports.MERCHANT_COMMISION_STATUS || (exports.MERCHANT_COMMISION_STATUS = {}));
//# sourceMappingURL=permission.js.map