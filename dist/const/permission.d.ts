export declare const ADMIN_PERMISSION: {
    UPDATE_AUTH_USER_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_AUTH_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_AUTH_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_AUTH_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_AUTH_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_USER_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_FEATURE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_FEATURE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_FEATURE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_FEATURE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_EXCHANGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_EXCHANGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_EXCHANGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_EXCHANGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_GENERAL_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_GENERAL_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_GENERAL_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_GENERAL_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_RESOLUTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_RESOLUTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_RESOLUTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_RESOLUTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_SYMBOL: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_SYMBOL: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_SYMBOL: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_SYMBOL: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_USER: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_USER: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_USER: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_ADMIN: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_ADMIN: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_ADMIN: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_ROLE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_APP_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_APP_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_APP_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_APP_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_TRANSACTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_TRANSACTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    REQUEST_INTERVENTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_BOT_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_BOT_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_BOT_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_BOT_SETTING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_PACKAGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_PACKAGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_PACKAGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_PACKAGE: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_CURRENCY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_CURRENCY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_CURRENCY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_CURRENCY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    EXPORT_USER: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    EXPORT_TRANSACTION: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_BOT: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_BOT: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_BOT: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_BOT: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_BOT_TRADING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_BOT_TRADING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_BOT_TRADING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_BOT_TRADING: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_BOT_TRADING_HISTORY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    CREATE_BOT_TRADING_HISTORY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    UPDATE_BOT_TRADING_HISTORY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    DELETE_BOT_TRADING_HISTORY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
    GET_SYSTEM_TRADE_HISTORY: {
        permission_id: string;
        permission_name: string;
        description: string;
    };
};
export declare const WITHOUT_AUTHORIZATION = "WITHOUT_AUTHORIZATION";
export declare const ADMIN_PERMISSION_DATA: ({
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
} | {
    permission_id: string;
    permission_name: string;
    description: string;
})[];
export declare const ADMIN_PERMISSION_KEY: string[];
export declare const DEFAULT_ROLE: {
    id: string;
    role_name: string;
    root: {};
    description: string;
    owner_created: string;
    created_at: number;
    updated_at: number;
};
export declare const ROLE_TYPE: {
    PACKAGE: string;
    FEATURE: string;
    FREE: string;
    FREE_TRIAL: string;
    BEST_CHOICE: string;
};
export declare enum USER_ROLE_STATUS {
    PROCESSING = "PROCESSING",
    DISABLED = "DISABLED",
    CURRENT_PLAN = "CURRENT_PLAN"
}
export declare enum PACKAGE_TYPE {
    DAY = "DAY",
    MONTH = "MONTH"
}
export declare enum ROLE_STATUS {
    CLOSE = "CLOSE",
    OPEN = "OPEN",
    COMINGSOON = "COMINGSOON"
}
export declare enum BOT_STATUS {
    CLOSE = "CLOSE",
    OPEN = "OPEN",
    COMINGSOON = "COMINGSOON"
}
export declare enum SYMBOL_STATUS {
    ON = "ON",
    OFF = "OFF"
}
export declare const GENERAL_SETTING_LIMIT_TAB = "LIMIT_TAB";
export declare enum MERCHANT_STATUS {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
export declare enum MERCHANT_COMMISION_STATUS {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
