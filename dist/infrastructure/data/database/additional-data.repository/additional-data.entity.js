"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdditionalDataEntitySubscriber = exports.AdditionalDataEntity = exports.ADDITIONAL_DATA_TABLE = void 0;
const types_1 = require("../../../../domain/additional-data/types");
const typeorm_1 = require("typeorm");
exports.ADDITIONAL_DATA_TABLE = 'additional_data';
let AdditionalDataEntity = class AdditionalDataEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], AdditionalDataEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], AdditionalDataEntity.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], AdditionalDataEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], AdditionalDataEntity.prototype, "data", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], AdditionalDataEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], AdditionalDataEntity.prototype, "updated_at", void 0);
AdditionalDataEntity = __decorate([
    (0, typeorm_1.Entity)(exports.ADDITIONAL_DATA_TABLE)
], AdditionalDataEntity);
exports.AdditionalDataEntity = AdditionalDataEntity;
let AdditionalDataEntitySubscriber = class AdditionalDataEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
AdditionalDataEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], AdditionalDataEntitySubscriber);
exports.AdditionalDataEntitySubscriber = AdditionalDataEntitySubscriber;
//# sourceMappingURL=additional-data.entity.js.map