"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdditionalDataRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const additional_data_entity_1 = require("./additional-data.entity");
class AdditionalDataRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(additional_data_entity_1.AdditionalDataEntity, database_1.ConnectionName.user_role);
    }
    list(param) {
        const { keyword, type } = param;
        const whereArray = [];
        if (keyword) {
            whereArray.push(`(name ILIKE '%${keyword}%')`);
        }
        if (type) {
            whereArray.push(`type = '${type}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
    SELECT *
    FROM ${additional_data_entity_1.ADDITIONAL_DATA_TABLE}
    ${where} 
    order by created_at DESC
  `;
        return this.repo().query(query);
    }
}
exports.AdditionalDataRepository = AdditionalDataRepository;
//# sourceMappingURL=index.js.map