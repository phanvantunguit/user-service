import { ADDITIONAL_DATA_TYPE } from 'src/domain/additional-data/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { AdditionalDataEntity } from './additional-data.entity';
export declare class AdditionalDataRepository extends BaseRepository<AdditionalDataEntity> {
    repo(): Repository<AdditionalDataEntity>;
    list(param: {
        keyword?: string;
        type?: ADDITIONAL_DATA_TYPE;
    }): Promise<any>;
}
