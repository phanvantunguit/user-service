import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const ROLES_TABLE = "roles";
export declare class RoleEntity {
    id: string;
    role_name: string;
    root: any;
    description: string;
    status: string;
    type: string;
    price: string;
    currency: string;
    owner_created: string;
    parent_id: string;
    is_best_choice: boolean;
    order: number;
    color: string;
    description_features: any;
    created_at: number;
    updated_at: number;
}
export declare class RoleEntitySubscriber implements EntitySubscriberInterface<RoleEntity> {
    beforeInsert(event: InsertEvent<RoleEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<RoleEntity>): Promise<void>;
}
