import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { RoleEntity } from './role.entity';
export declare class RoleRepository extends BaseRepository<RoleEntity> {
    repo(): Repository<RoleEntity>;
    list(): Promise<{
        id: string;
        name: string;
    }[]>;
}
