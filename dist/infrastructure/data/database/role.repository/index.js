"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleRepository = void 0;
const database_1 = require("../../../../const/database");
const types_1 = require("../../../../domain/bot/types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const role_entity_1 = require("./role.entity");
class RoleRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(role_entity_1.RoleEntity, database_1.ConnectionName.user_role);
    }
    list() {
        const queryBot = `
    SELECT *, role_name as name
    FROM ${role_entity_1.ROLES_TABLE}
    WHERE status = '${types_1.BOT_STATUS.OPEN}'
    ORDER BY name ASC
    `;
        return this.repo().query(queryBot);
    }
}
exports.RoleRepository = RoleRepository;
//# sourceMappingURL=index.js.map