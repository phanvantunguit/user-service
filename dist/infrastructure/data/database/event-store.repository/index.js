"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventStoreRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const event_store_entity_1 = require("./event-store.entity");
class EventStoreRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(event_store_entity_1.EventStoreEntity, database_1.ConnectionName.user_role);
    }
}
exports.EventStoreRepository = EventStoreRepository;
//# sourceMappingURL=index.js.map