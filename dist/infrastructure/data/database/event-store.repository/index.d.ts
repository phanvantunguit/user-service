import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { EventStoreEntity } from './event-store.entity';
export declare class EventStoreRepository extends BaseRepository<EventStoreEntity> {
    repo(): Repository<EventStoreEntity>;
}
