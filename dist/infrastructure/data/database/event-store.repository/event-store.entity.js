"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventStoreEntitySubscriber = exports.EventStoreEntity = exports.EVENT_STORE_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.EVENT_STORE_TABLE = 'event_stores';
let EventStoreEntity = class EventStoreEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], EventStoreEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], EventStoreEntity.prototype, "event_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], EventStoreEntity.prototype, "event_name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], EventStoreEntity.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], EventStoreEntity.prototype, "state", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], EventStoreEntity.prototype, "metadata", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], EventStoreEntity.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], EventStoreEntity.prototype, "created_at", void 0);
EventStoreEntity = __decorate([
    (0, typeorm_1.Entity)(exports.EVENT_STORE_TABLE),
    (0, typeorm_1.Unique)('event_id_event_name_state_es_un', ['event_id', 'event_name', 'state'])
], EventStoreEntity);
exports.EventStoreEntity = EventStoreEntity;
let EventStoreEntitySubscriber = class EventStoreEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
EventStoreEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], EventStoreEntitySubscriber);
exports.EventStoreEntitySubscriber = EventStoreEntitySubscriber;
//# sourceMappingURL=event-store.entity.js.map