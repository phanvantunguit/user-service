"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DBContext = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const database_1 = require("../../../const/database");
const typeorm_2 = require("typeorm");
let DBContext = class DBContext {
    constructor(connection) {
        this.connection = connection;
    }
    async runInTransaction(runInTransaction) {
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        return await runInTransaction(queryRunner)
            .then(async (result) => {
            await queryRunner.commitTransaction();
            await queryRunner.release();
            return result;
        })
            .catch(async (error) => {
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            throw error;
        });
    }
};
DBContext = __decorate([
    __param(0, (0, typeorm_1.InjectConnection)(database_1.ConnectionName.xtrading_user_service)),
    __metadata("design:paramtypes", [typeorm_2.Connection])
], DBContext);
exports.DBContext = DBContext;
//# sourceMappingURL=db-context.js.map