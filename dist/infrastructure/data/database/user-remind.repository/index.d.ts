import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserRemindEntity } from './user-remind.entity';
export declare class UserRemindRepository extends BaseRepository<UserRemindEntity> {
    repo(): Repository<UserRemindEntity>;
}
