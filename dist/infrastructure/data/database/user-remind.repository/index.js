"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRemindRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const user_remind_entity_1 = require("./user-remind.entity");
class UserRemindRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(user_remind_entity_1.UserRemindEntity, database_1.ConnectionName.xtrading_user_service);
    }
}
exports.UserRemindRepository = UserRemindRepository;
//# sourceMappingURL=index.js.map