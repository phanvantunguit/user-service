import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const USER_REMIND_TABLE = "user_reminds";
export declare class UserRemindEntity {
    id?: string;
    event_id?: string;
    user_id?: string;
    template_id?: string;
    remind_at?: number;
    remind?: boolean;
    created_at?: number;
    updated_at?: number;
}
export declare class UserRemindEntitySubscriber implements EntitySubscriberInterface<UserRemindEntity> {
    beforeInsert(event: InsertEvent<UserRemindEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserRemindEntity>): Promise<void>;
}
