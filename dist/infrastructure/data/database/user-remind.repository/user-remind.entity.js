"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRemindEntitySubscriber = exports.UserRemindEntity = exports.USER_REMIND_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.USER_REMIND_TABLE = 'user_reminds';
let UserRemindEntity = class UserRemindEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], UserRemindEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], UserRemindEntity.prototype, "event_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], UserRemindEntity.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], UserRemindEntity.prototype, "template_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint' }),
    __metadata("design:type", Number)
], UserRemindEntity.prototype, "remind_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], UserRemindEntity.prototype, "remind", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserRemindEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserRemindEntity.prototype, "updated_at", void 0);
UserRemindEntity = __decorate([
    (0, typeorm_1.Entity)(exports.USER_REMIND_TABLE),
    (0, typeorm_1.Unique)('event_id_user_id_template_id_remind_at_un', [
        'event_id',
        'user_id',
        'template_id',
        'remind_at',
    ])
], UserRemindEntity);
exports.UserRemindEntity = UserRemindEntity;
let UserRemindEntitySubscriber = class UserRemindEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
UserRemindEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], UserRemindEntitySubscriber);
exports.UserRemindEntitySubscriber = UserRemindEntitySubscriber;
//# sourceMappingURL=user-remind.entity.js.map