import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const ADMINS_TABLE = "admins";
export declare class AdminEntity {
    id?: string;
    email?: string;
    phone?: string;
    fullname?: string;
    super_admin?: boolean;
    email_confirmed?: boolean;
    status?: string;
    password?: string;
    created_by?: string;
    updated_by?: string;
    created_at?: number;
    updated_at?: number;
    deleted_at?: number;
}
export declare class AdminEntitySubscriber implements EntitySubscriberInterface<AdminEntity> {
    beforeInsert(event: InsertEvent<AdminEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<AdminEntity>): Promise<void>;
}
