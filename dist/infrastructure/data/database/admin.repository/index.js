"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const admin_entity_1 = require("./admin.entity");
class AdminRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(admin_entity_1.AdminEntity, database_1.ConnectionName.xtrading_user_service);
    }
}
exports.AdminRepository = AdminRepository;
//# sourceMappingURL=index.js.map