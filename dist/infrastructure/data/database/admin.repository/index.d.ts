import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { AdminEntity } from './admin.entity';
export declare class AdminRepository extends BaseRepository<AdminEntity> {
    repo(): Repository<AdminEntity>;
}
