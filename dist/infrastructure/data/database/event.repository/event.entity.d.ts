import { EVENT_STATUS } from 'src/domain/event/types';
import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const EVENTS_TABLE = "events";
export declare class EventEntity {
    id?: string;
    name?: string;
    code?: string;
    status?: EVENT_STATUS;
    attendees_number?: number;
    email_remind_at?: number;
    email_remind_template_id?: string;
    email_confirm?: boolean;
    email_confirm_template_id?: string;
    start_at?: number;
    finish_at?: number;
    remind?: boolean;
    create_qrcode?: boolean;
    created_by?: string;
    updated_by?: string;
    created_at?: number;
    updated_at?: number;
    deleted_at?: number;
}
export declare class EventEntitySubscriber implements EntitySubscriberInterface<EventEntity> {
    beforeInsert(event: InsertEvent<EventEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<EventEntity>): Promise<void>;
}
