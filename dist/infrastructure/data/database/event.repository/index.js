"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const event_entity_1 = require("./event.entity");
class EventRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(event_entity_1.EventEntity, database_1.ConnectionName.xtrading_user_service);
    }
    async getPaging(param) {
        let { page, size, deleted } = param;
        const { keyword, status } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        deleted = Number(deleted || 0);
        const whereArray = [];
        if (status) {
            whereArray.push(`status = '${status}'`);
        }
        if (deleted == 0) {
            whereArray.push(`deleted_at is null`);
        }
        if (keyword) {
            whereArray.push(`(name LIKE '%${keyword}%'
        OR code LIKE '%${keyword}%')`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryEvent = `
        SELECT  *
        FROM ${event_entity_1.EVENTS_TABLE}  
        ${where}  
        ORDER BY created_at DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${event_entity_1.EVENTS_TABLE}  
    ${where}
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryEvent),
            this.repo().query(queryCount),
        ]);
        return {
            rows: rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getEventToRemind(param) {
        const whereArray = [];
        if (param.to) {
            whereArray.push(`email_remind_at <= ${param.to}`);
        }
        if (param.remind) {
            whereArray.push(`(remind = ${param.remind === '1' ? true : false} OR remind is null)`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUserEvent = `
    SELECT *
    FROM ${event_entity_1.EVENTS_TABLE}
    ${where}
    `;
        return this.repo().query(queryUserEvent);
    }
}
exports.EventRepository = EventRepository;
//# sourceMappingURL=index.js.map