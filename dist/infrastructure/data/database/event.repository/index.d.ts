import { EventDomain } from 'src/domain/event/event.domain';
import { EVENT_STATUS } from 'src/domain/event/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { EventEntity } from './event.entity';
export declare class EventRepository extends BaseRepository<EventEntity> {
    repo(): Repository<EventEntity>;
    getPaging(param: {
        page?: number;
        size?: number;
        keyword?: string;
        status?: EVENT_STATUS;
        deleted?: number;
    }): Promise<{
        rows: EventDomain[];
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getEventToRemind(param: {
        remind?: '0' | '1';
        status?: EVENT_STATUS;
        from?: number;
        to?: number;
    }): Promise<EventEntity[]>;
}
