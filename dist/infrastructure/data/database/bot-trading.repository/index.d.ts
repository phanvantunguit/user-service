import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { BotTradingEntity } from './bot-trading.entity';
export declare class BotTradingRepository extends BaseRepository<BotTradingEntity> {
    repo(): Repository<BotTradingEntity>;
    list(param: {
        merchant_id?: string;
    }): Promise<{
        id: string;
        name: string;
    }[]>;
}
