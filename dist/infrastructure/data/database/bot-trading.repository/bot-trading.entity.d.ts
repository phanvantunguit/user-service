import { BOT_STATUS } from 'src/domain/bot/types';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const BOT_TRADING_TABLE = "bot_tradings";
export declare class BotTradingEntity {
    id?: string;
    name?: string;
    clone_name?: string;
    code?: string;
    type?: string;
    description?: string;
    work_based_on?: any;
    status?: BOT_STATUS;
    price?: string;
    display_price?: string;
    currency?: string;
    image_url?: string;
    owner_created?: string;
    order?: number;
    max_drawdown?: string;
    max_drawdown_change_percent?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class BotTradingEntitySubscriber implements EntitySubscriberInterface<BotTradingEntity> {
    beforeInsert(event: InsertEvent<BotTradingEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<BotTradingEntity>): Promise<void>;
}
