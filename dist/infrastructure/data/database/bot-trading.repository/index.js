"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotTradingRepository = void 0;
const database_1 = require("../../../../const/database");
const types_1 = require("../../../../domain/bot/types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const merchant_commission_entity_1 = require("../merchant-commission.repository/merchant-commission.entity");
const bot_trading_entity_1 = require("./bot-trading.entity");
class BotTradingRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(bot_trading_entity_1.BotTradingEntity, database_1.ConnectionName.user_role);
    }
    list(param) {
        const { merchant_id } = param;
        if (merchant_id) {
            const whereArray = [];
            whereArray.push(`mc.merchant_id = '${merchant_id}'`);
            whereArray.push(`bt.status = '${types_1.BOT_STATUS.OPEN}'`);
            const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
            const queryBot = `
      SELECT bt.*,
      mc.merchant_id
      FROM ${bot_trading_entity_1.BOT_TRADING_TABLE} bt
      LEFT JOIN ${merchant_commission_entity_1.MERCHANT_COMMISSION_TABLE} mc on mc.asset_id = bt.id
      ${where}
      ORDER BY name ASC
      `;
            return this.repo().query(queryBot);
        }
        else {
            const queryBot = `
      SELECT *
      FROM ${bot_trading_entity_1.BOT_TRADING_TABLE}
      WHERE status = '${types_1.BOT_STATUS.OPEN}'
      ORDER BY name ASC
      `;
            return this.repo().query(queryBot);
        }
    }
}
exports.BotTradingRepository = BotTradingRepository;
//# sourceMappingURL=index.js.map