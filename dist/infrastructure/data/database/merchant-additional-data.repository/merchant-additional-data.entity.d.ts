import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const MERCHANT_ADDITIONAL_DATA_TABLE = "merchant_additional_data";
export declare class MerchantAdditionalDataEntity {
    id?: string;
    merchant_id?: string;
    additional_data_id?: string;
    status?: string;
    created_at?: number;
    updated_at?: number;
    created_by?: string;
    updated_by?: string;
}
export declare class MerchantAdditionalDataEntitySubscriber implements EntitySubscriberInterface<MerchantAdditionalDataEntity> {
    beforeInsert(event: InsertEvent<MerchantAdditionalDataEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<MerchantAdditionalDataEntity>): Promise<void>;
}
