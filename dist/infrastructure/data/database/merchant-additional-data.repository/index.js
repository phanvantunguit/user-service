"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantAdditionalDataRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const additional_data_entity_1 = require("../additional-data.repository/additional-data.entity");
const base_repository_1 = require("../base.repository");
const merchant_additional_data_entity_1 = require("./merchant-additional-data.entity");
class MerchantAdditionalDataRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(merchant_additional_data_entity_1.MerchantAdditionalDataEntity, database_1.ConnectionName.user_role);
    }
    list(param) {
        const { id, keyword, merchant_id, type, status } = param;
        const whereArray = [];
        if (id) {
            whereArray.push(`madt.id = '${id}'`);
        }
        if (keyword) {
            whereArray.push(`(adt.name ILIKE '%${keyword}%')`);
        }
        if (merchant_id) {
            whereArray.push(`madt.merchant_id = '${merchant_id}'`);
        }
        if (type) {
            whereArray.push(`adt.type = '${type}'`);
        }
        if (status) {
            whereArray.push(`madt.status = '${status}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
    SELECT madt.*,
    adt.type as type,
    adt.name as name,
    adt.data as data
    FROM ${merchant_additional_data_entity_1.MERCHANT_ADDITIONAL_DATA_TABLE} madt
    LEFT JOIN ${additional_data_entity_1.ADDITIONAL_DATA_TABLE} adt on adt.id = madt.additional_data_id
    ${where} 
    order by created_at DESC
  `;
        return this.repo().query(query);
    }
}
exports.MerchantAdditionalDataRepository = MerchantAdditionalDataRepository;
//# sourceMappingURL=index.js.map