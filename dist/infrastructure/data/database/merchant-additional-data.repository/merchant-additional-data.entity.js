"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantAdditionalDataEntitySubscriber = exports.MerchantAdditionalDataEntity = exports.MERCHANT_ADDITIONAL_DATA_TABLE = void 0;
const types_1 = require("../../../../domain/additional-data/types");
const typeorm_1 = require("typeorm");
exports.MERCHANT_ADDITIONAL_DATA_TABLE = 'merchant_additional_data';
let MerchantAdditionalDataEntity = class MerchantAdditionalDataEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], MerchantAdditionalDataEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantAdditionalDataEntity.prototype, "merchant_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantAdditionalDataEntity.prototype, "additional_data_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', default: types_1.MERCHANT_ADDITIONAL_DATA_STATUS.ON }),
    __metadata("design:type", String)
], MerchantAdditionalDataEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantAdditionalDataEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantAdditionalDataEntity.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], MerchantAdditionalDataEntity.prototype, "created_by", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], MerchantAdditionalDataEntity.prototype, "updated_by", void 0);
MerchantAdditionalDataEntity = __decorate([
    (0, typeorm_1.Entity)(exports.MERCHANT_ADDITIONAL_DATA_TABLE),
    (0, typeorm_1.Unique)('merchant_id_additional_data_id_mdd_un', ['merchant_id', 'additional_data_id'])
], MerchantAdditionalDataEntity);
exports.MerchantAdditionalDataEntity = MerchantAdditionalDataEntity;
let MerchantAdditionalDataEntitySubscriber = class MerchantAdditionalDataEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
MerchantAdditionalDataEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], MerchantAdditionalDataEntitySubscriber);
exports.MerchantAdditionalDataEntitySubscriber = MerchantAdditionalDataEntitySubscriber;
//# sourceMappingURL=merchant-additional-data.entity.js.map