import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MerchantAdditionalDataEntity } from './merchant-additional-data.entity';
export declare class MerchantAdditionalDataRepository extends BaseRepository<MerchantAdditionalDataEntity> {
    repo(): Repository<MerchantAdditionalDataEntity>;
    list(param: {
        id?: string;
        merchant_id?: string;
        keyword?: string;
        type?: ADDITIONAL_DATA_TYPE;
        status?: MERCHANT_ADDITIONAL_DATA_STATUS;
    }): Promise<any>;
}
