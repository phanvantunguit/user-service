import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const USER_PROMOTION_TABLE = "user_promotions";
export declare class UserPromotionEntity {
    id?: string;
    email?: string;
    template_id?: string;
    send?: boolean;
    created_at?: number;
    updated_at?: number;
}
export declare class UserPromotionEntitySubscriber implements EntitySubscriberInterface<UserPromotionEntity> {
    beforeInsert(event: InsertEvent<UserPromotionEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserPromotionEntity>): Promise<void>;
}
