"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPromotionRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const user_promotion_entity_1 = require("./user-promotion.entity");
class UserPromotionRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(user_promotion_entity_1.UserPromotionEntity, database_1.ConnectionName.xtrading_user_service);
    }
}
exports.UserPromotionRepository = UserPromotionRepository;
//# sourceMappingURL=index.js.map