import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserPromotionEntity } from './user-promotion.entity';
export declare class UserPromotionRepository extends BaseRepository<UserPromotionEntity> {
    repo(): Repository<UserPromotionEntity>;
}
