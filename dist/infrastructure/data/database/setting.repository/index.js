"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const setting_entity_1 = require("./setting.entity");
class SettingRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(setting_entity_1.AppSettingEntity, database_1.ConnectionName.user_role);
    }
    async saveAppSetting(params) {
        return this.repo().save(params);
    }
    async findOneAppSetting(params) {
        return this.repo().findOne(params);
    }
    async findAppSetting(params) {
        const { id, name, user_id } = params;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}' OR user_id is null`);
        }
        else {
            whereArray.push(`user_id is null`);
        }
        if (id) {
            whereArray.push(`id = '${id}'`);
        }
        if (name) {
            whereArray.push(`name = '${name}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const querySetting = `
    SELECT  *
    FROM ${setting_entity_1.APP_SETTING_TABLE}  
    ${where}  
    ORDER BY updated_at DESC
  `;
        return this.repo().query(querySetting);
    }
    async deleteAppSettingById(id) {
        return this.repo().delete(id);
    }
}
exports.SettingRepository = SettingRepository;
//# sourceMappingURL=index.js.map