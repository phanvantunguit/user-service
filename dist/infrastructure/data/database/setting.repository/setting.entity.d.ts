import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const APP_SETTING_TABLE = "app_settings";
export declare class AppSettingEntity {
    id?: string;
    name?: string;
    value?: string;
    user_id?: string;
    description?: string;
    owner_created?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class AppSettingEntitySubscriber implements EntitySubscriberInterface<AppSettingEntity> {
    beforeInsert(event: InsertEvent<AppSettingEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<AppSettingEntity>): Promise<void>;
}
