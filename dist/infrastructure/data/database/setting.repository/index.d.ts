import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { AppSettingEntity } from './setting.entity';
export declare class SettingRepository extends BaseRepository<AppSettingEntity> {
    repo(): Repository<AppSettingEntity>;
    saveAppSetting(params: any): Promise<AppSettingEntity>;
    findOneAppSetting(params: {
        name?: string;
        user_id?: string;
        owner_created?: string;
    }): Promise<AppSettingEntity>;
    findAppSetting(params: any): Promise<AppSettingEntity[]>;
    deleteAppSettingById(id: string): Promise<import("typeorm").DeleteResult>;
}
