"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppSettingEntitySubscriber = exports.AppSettingEntity = exports.APP_SETTING_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.APP_SETTING_TABLE = 'app_settings';
let AppSettingEntity = class AppSettingEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], AppSettingEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Index)(),
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], AppSettingEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], AppSettingEntity.prototype, "value", void 0);
__decorate([
    (0, typeorm_1.Index)(),
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true, default: 'system' }),
    __metadata("design:type", String)
], AppSettingEntity.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], AppSettingEntity.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], AppSettingEntity.prototype, "owner_created", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], AppSettingEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], AppSettingEntity.prototype, "updated_at", void 0);
AppSettingEntity = __decorate([
    (0, typeorm_1.Unique)('name_user_id_as_un', ['name', 'user_id']),
    (0, typeorm_1.Entity)(exports.APP_SETTING_TABLE)
], AppSettingEntity);
exports.AppSettingEntity = AppSettingEntity;
let AppSettingEntitySubscriber = class AppSettingEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
AppSettingEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], AppSettingEntitySubscriber);
exports.AppSettingEntitySubscriber = AppSettingEntitySubscriber;
//# sourceMappingURL=setting.entity.js.map