import { BOT_STATUS } from 'src/domain/bot/types';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const BOT_TABLE = "bots";
export declare class BotEntity {
    id?: string;
    name?: string;
    code?: string;
    bot_setting_id?: string;
    type?: string;
    description?: string;
    work_based_on?: any;
    status?: BOT_STATUS;
    price?: string;
    currency?: string;
    image_url?: string;
    owner_created?: string;
    order?: number;
    created_at?: number;
    updated_at?: number;
}
export declare class BotEntitySubscriber implements EntitySubscriberInterface<BotEntity> {
    beforeInsert(event: InsertEvent<BotEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<BotEntity>): Promise<void>;
}
