import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { BotEntity } from './bot.entity';
export declare class BotRepository extends BaseRepository<BotEntity> {
    repo(): Repository<BotEntity>;
    list(): Promise<{
        id: string;
        name: string;
    }[]>;
}
