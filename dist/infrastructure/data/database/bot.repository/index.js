"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BotRepository = void 0;
const database_1 = require("../../../../const/database");
const types_1 = require("../../../../domain/bot/types");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const bot_entity_1 = require("./bot.entity");
class BotRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(bot_entity_1.BotEntity, database_1.ConnectionName.user_role);
    }
    list() {
        const queryBot = `
    SELECT *
    FROM ${bot_entity_1.BOT_TABLE}
    WHERE status = '${types_1.BOT_STATUS.OPEN}'
    ORDER BY name ASC
    `;
        return this.repo().query(queryBot);
    }
}
exports.BotRepository = BotRepository;
//# sourceMappingURL=index.js.map