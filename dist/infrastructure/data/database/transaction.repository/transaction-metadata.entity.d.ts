import { TRANSACTION_METADATA } from 'src/domain/transaction/types';
import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const TRANSACTION_METADATA_TABLE = "transaction_metadata";
export declare class TransactionMetadataEntity {
    id?: string;
    transaction_id?: string;
    attribute?: TRANSACTION_METADATA;
    value?: string;
    created_at?: number;
}
export declare class TransactionMetadataEntitySubscriber implements EntitySubscriberInterface<TransactionMetadataEntity> {
    beforeInsert(event: InsertEvent<TransactionMetadataEntity>): Promise<void>;
}
