"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogEntitySubscriber = exports.TransactionLogEntity = exports.TRANSACTION_LOGS_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.TRANSACTION_LOGS_TABLE = 'transaction_logs';
let TransactionLogEntity = class TransactionLogEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], TransactionLogEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], TransactionLogEntity.prototype, "transaction_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionLogEntity.prototype, "transaction_event", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], TransactionLogEntity.prototype, "transaction_status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], TransactionLogEntity.prototype, "metadata", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], TransactionLogEntity.prototype, "created_at", void 0);
TransactionLogEntity = __decorate([
    (0, typeorm_1.Entity)(exports.TRANSACTION_LOGS_TABLE)
], TransactionLogEntity);
exports.TransactionLogEntity = TransactionLogEntity;
let TransactionLogEntitySubscriber = class TransactionLogEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
TransactionLogEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], TransactionLogEntitySubscriber);
exports.TransactionLogEntitySubscriber = TransactionLogEntitySubscriber;
//# sourceMappingURL=transaction-log.entity.js.map