"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const transaction_log_entity_1 = require("./transaction-log.entity");
const transaction_metadata_entity_1 = require("./transaction-metadata.entity");
const transaction_entity_1 = require("./transaction.entity");
class TransactionRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(transaction_entity_1.TransactionEntity, database_1.ConnectionName.payment_service);
    }
    metadataRepo() {
        return (0, typeorm_1.getRepository)(transaction_metadata_entity_1.TransactionMetadataEntity, database_1.ConnectionName.payment_service);
    }
    logRepo() {
        return (0, typeorm_1.getRepository)(transaction_log_entity_1.TransactionLogEntity, database_1.ConnectionName.payment_service);
    }
    async findMultipleStatus(param) {
        const { status, user_id, email, phone, integrate_service } = param;
        const whereArray = [];
        if (status && status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        if (email) {
            whereArray.push(`email = '${email}'`);
        }
        if (phone) {
            whereArray.push(`phone = '${phone}'`);
        }
        if (integrate_service) {
            whereArray.push(`integrate_service = '${integrate_service}'`);
        }
        const where = `WHERE ${whereArray.join(' AND ')}`;
        const query = `
      SELECT *
      FROM ${transaction_entity_1.TRANSACTION_TABLE}  
      ${where} 
      order by created_at DESC
    `;
        return this.repo().query(query);
    }
    async getPaging(param) {
        let { page, size } = param;
        const { keyword, status, from, to, merchant_code, category, name } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (status) {
            whereArray.push(`status = '${status}'`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        if (keyword) {
            whereArray.push(`(email LIKE '%${keyword}%'
          OR fullname LIKE '%${keyword}%'
          OR order_id LIKE '%${keyword}%'
          OR payment_id LIKE '%${keyword}%')`);
        }
        if (category) {
            whereArray.push(`items like '%"category":"${category}"%'`);
        }
        if (name) {
            whereArray.push(`items like '%"name":"${name}"%'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
      SELECT id,
      integrate_service,
      order_id,
      order_type,
      user_id,
      fullname,
      email,
      amount,
      currency,
      payment_id,
      payment_method,
      status,
      ipn_url,
      merchant_code,
      created_at,
      updated_at,
      commission_cash,
      CAST(items AS JSONB) AS items    
      FROM
      (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
    ${where}  
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getDetail(param) {
        const { id, merchant_code } = param;
        const [transaction, metadatas, logs] = await Promise.all([
            this.repo().findOne({ id, merchant_code }),
            this.getMetadata({ transaction_id: id }),
            this.getLog({ transaction_id: id }),
        ]);
        return { transaction, metadatas, logs };
    }
    async getMetadata(param) {
        return this.metadataRepo().find({ transaction_id: param.transaction_id });
    }
    async getLog(param) {
        return this.logRepo().find({ transaction_id: param.transaction_id });
    }
    async report(param) {
        const { from, to, merchant_code } = param;
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryCount = `
    SELECT 
    COUNT(*) as count_total,
    MAX(created_at) as finish_at,
    MIN(created_at) as start_at,
    SUM(case when status='COMPLETE' then 1 else 0 end) as count_complete,
    SUM(case when status='FAILED' then 1 else 0 end) as count_failed,
    SUM(amount::float) amount_total,
    SUM(case when status='COMPLETE' then amount::float else 0 end) as amount_complete,
    SUM(case when status='FAILED' then amount::float else 0 end) as amount_failed,
    SUM(commission_cash) as commission_cash_total,
    SUM(case when status='FAILED' then commission_cash else 0 end) as commission_cash_failed,
    SUM(case when status='COMPLETE' then commission_cash else 0 end) as commission_cash
    FROM ${transaction_entity_1.TRANSACTION_TABLE}
    ${where} 
    `;
        return this.repo().query(queryCount);
    }
    async chart(param) {
        const { from, to, merchant_code, timezone, time_type } = param;
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const timeFormat = time_type === 'DAY' ? 'YYYY-MM-DD' : 'YYYY-MM';
        const queryChart = `
    SELECT to_char(to_timestamp(created_at / 1000) AT TIME ZONE '${timezone}', '${timeFormat}') as time, 
    COUNT(*) as count_total,
    MAX(created_at) as finish_at,
    MIN(created_at) as start_at,
    SUM(case when status='COMPLETE' then 1 else 0 end) as count_complete,
    SUM(case when status='FAILED' then 1 else 0 end) as count_failed,
    SUM(amount::float) amount_total,
    SUM(case when status='COMPLETE' then amount::float else 0 end) as amount_complete,
    SUM(case when status='FAILED' then amount::float else 0 end) as amount_failed,
    SUM(commission_cash) as commission_cash_total,
    SUM(case when status='FAILED' then commission_cash else 0 end) as commission_cash_failed,
    SUM(case when status='COMPLETE' then commission_cash else 0 end) as commission_cash
    from transactions
    ${where}  
    GROUP BY time
    ORDER BY time ASC
    `;
        return this.repo().query(queryChart);
    }
    async totalCommission(param) {
        const { merchant_code } = param;
        const whereArray = [];
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryCount = `
    SELECT 
    SUM(case when status='COMPLETE' then commission_cash else 0 end) as total
    FROM ${transaction_entity_1.TRANSACTION_TABLE}
    ${where} 
    `;
        return this.repo().query(queryCount);
    }
}
exports.TransactionRepository = TransactionRepository;
//# sourceMappingURL=index.js.map