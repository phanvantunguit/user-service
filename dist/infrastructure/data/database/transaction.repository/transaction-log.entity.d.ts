import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const TRANSACTION_LOGS_TABLE = "transaction_logs";
export declare class TransactionLogEntity {
    id?: string;
    transaction_id?: string;
    transaction_event?: string;
    transaction_status?: string;
    metadata?: any;
    created_at?: number;
}
export declare class TransactionLogEntitySubscriber implements EntitySubscriberInterface<TransactionLogEntity> {
    beforeInsert(event: InsertEvent<TransactionLogEntity>): Promise<void>;
}
