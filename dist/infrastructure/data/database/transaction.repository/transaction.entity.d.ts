import { PAYMENT_METHOD, TRANSACTION_STATUS } from 'src/domain/transaction/types';
import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const TRANSACTION_TABLE = "transactions";
export declare class TransactionEntity {
    id?: string;
    integrate_service?: string;
    order_id?: string;
    order_type?: string;
    user_id?: string;
    fullname?: string;
    email?: string;
    amount?: string;
    commission_cash?: number;
    currency?: string;
    payment_id?: string;
    payment_method?: PAYMENT_METHOD;
    status?: TRANSACTION_STATUS;
    ipn_url?: string;
    merchant_code?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class TransactionEntitySubscriber implements EntitySubscriberInterface<TransactionEntity> {
    beforeInsert(event: InsertEvent<TransactionEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<TransactionEntity>): Promise<void>;
}
