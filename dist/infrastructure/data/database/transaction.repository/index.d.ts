import { TRANSACTION_STATUS } from 'src/domain/transaction/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { TransactionLogEntity } from './transaction-log.entity';
import { TransactionMetadataEntity } from './transaction-metadata.entity';
import { TransactionEntity } from './transaction.entity';
export declare class TransactionRepository extends BaseRepository<TransactionEntity> {
    repo(): Repository<TransactionEntity>;
    metadataRepo(): Repository<TransactionMetadataEntity>;
    logRepo(): Repository<TransactionLogEntity>;
    findMultipleStatus(param: {
        status?: string[];
        integrate_service?: string;
        user_id?: string;
        email?: string;
        phone?: string;
    }): Promise<TransactionEntity[]>;
    getPaging(param: {
        page?: number;
        size?: number;
        keyword?: string;
        status?: TRANSACTION_STATUS;
        from?: number;
        to?: number;
        merchant_code?: string;
        category?: string;
        name?: string;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getDetail(param: {
        id: string;
        merchant_code: string;
    }): Promise<{
        transaction: TransactionEntity;
        metadatas: TransactionMetadataEntity[];
        logs: TransactionLogEntity[];
    }>;
    getMetadata(param: {
        transaction_id: string;
    }): Promise<TransactionMetadataEntity[]>;
    getLog(param: {
        transaction_id: string;
    }): Promise<TransactionLogEntity[]>;
    report(param: {
        from?: number;
        to?: number;
        merchant_code?: string;
    }): Promise<{
        count_total: string;
        count_complete: string;
        count_failed: string;
        amount_total: number;
        amount_complete: number;
        amount_failed: number;
        commission_cash_total: number;
        commission_cash_failed: number;
        commission_cash: number;
        start_at: number;
        finish_at: number;
    }>;
    chart(param: {
        from?: number;
        to?: number;
        merchant_code?: string;
        timezone: string;
        time_type: 'DAY' | 'MONTH';
    }): Promise<{
        time: string;
        count_total: string;
        count_complete: number;
        count_failed: string;
        amount_total: number;
        amount_complete: number;
        amount_failed: number;
        commission_cash_total: number;
        commission_cash_failed: number;
        commission_cash: number;
        start_at: number;
        finish_at: number;
    }[]>;
    totalCommission(param: {
        merchant_code?: string;
    }): Promise<{
        total: number;
    }[]>;
}
