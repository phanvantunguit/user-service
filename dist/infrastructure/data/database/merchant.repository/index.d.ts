import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MerchantEntity } from './merchant.entity';
export declare class MerchantRepository extends BaseRepository<MerchantEntity> {
    repo(): Repository<MerchantEntity>;
    queryMerchant(merchantCode?: string): Promise<any>;
    findMerchant(params: {
        status?: string;
        domain_type?: string;
    }): Promise<MerchantEntity[]>;
}
