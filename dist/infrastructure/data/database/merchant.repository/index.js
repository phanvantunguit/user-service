"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const merchant_entity_1 = require("./merchant.entity");
class MerchantRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(merchant_entity_1.MerchantEntity, database_1.ConnectionName.user_role);
    }
    async queryMerchant(merchantCode) {
        const queryConfig = `SELECT config
    FROM ${merchant_entity_1.MERCHANTS_TABLE}
    WHERE code = '${merchantCode}' `;
        return await this.repo().query(queryConfig);
    }
    findMerchant(params) {
        const { status, domain_type } = params;
        const whereArray = [];
        if (status) {
            whereArray.push(`status = '${status}'`);
        }
        if (domain_type) {
            whereArray.push(`config::json->>'domain_type' = '${domain_type}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryMerchant = `
      SELECT *
      FROM ${merchant_entity_1.MERCHANTS_TABLE}
      ${where}
    `;
        return this.repo().query(queryMerchant);
    }
}
exports.MerchantRepository = MerchantRepository;
//# sourceMappingURL=index.js.map