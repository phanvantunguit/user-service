"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantEntitySubscriber = exports.MerchantEntity = exports.MERCHANTS_TABLE = void 0;
const types_1 = require("../../../../domain/merchant/types");
const hash_util_1 = require("../../../../utils/hash.util");
const typeorm_1 = require("typeorm");
exports.MERCHANTS_TABLE = 'merchants';
let MerchantEntity = class MerchantEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], MerchantEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', unique: true }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', unique: true }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "code", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], MerchantEntity.prototype, "supper_merchant", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', default: types_1.MERCHANT_STATUS.ACTIVE }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', unique: true }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], MerchantEntity.prototype, "config", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', unique: true }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "domain", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "password", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "created_by", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantEntity.prototype, "updated_by", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantEntity.prototype, "updated_at", void 0);
MerchantEntity = __decorate([
    (0, typeorm_1.Entity)(exports.MERCHANTS_TABLE)
], MerchantEntity);
exports.MerchantEntity = MerchantEntity;
let MerchantEntitySubscriber = class MerchantEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
        if (event.entity.password) {
            event.entity.password = await (0, hash_util_1.hashPassword)(event.entity.password);
        }
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
        if (event.entity.password) {
            event.entity.password = await (0, hash_util_1.hashPassword)(event.entity.password);
        }
    }
};
MerchantEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], MerchantEntitySubscriber);
exports.MerchantEntitySubscriber = MerchantEntitySubscriber;
//# sourceMappingURL=merchant.entity.js.map