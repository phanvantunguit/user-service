import { MERCHANT_STATUS } from 'src/domain/merchant/types';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const MERCHANTS_TABLE = "merchants";
export declare class MerchantEntity {
    id?: string;
    name?: string;
    code?: string;
    supper_merchant?: boolean;
    status?: MERCHANT_STATUS;
    description?: string;
    email?: string;
    config?: any;
    domain?: string;
    password?: string;
    created_by?: string;
    updated_by?: string;
    created_at?: number;
    updated_at?: number;
}
export declare class MerchantEntitySubscriber implements EntitySubscriberInterface<MerchantEntity> {
    beforeInsert(event: InsertEvent<MerchantEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<MerchantEntity>): Promise<void>;
}
