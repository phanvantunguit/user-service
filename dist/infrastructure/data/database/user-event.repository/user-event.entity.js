"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEventEntitySubscriber = exports.UserEventEntity = exports.USERS_EVENTS_TABLE = void 0;
const types_1 = require("../../../../domain/event/types");
const typeorm_1 = require("typeorm");
exports.USERS_EVENTS_TABLE = 'users_events';
let UserEventEntity = class UserEventEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], UserEventEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "created_by", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "updated_by", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserEventEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserEventEntity.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "telegram", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "fullname", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', default: types_1.EVENT_CONFIRM_STATUS.WAITING }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "confirm_status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', default: false }),
    __metadata("design:type", Boolean)
], UserEventEntity.prototype, "attend", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], UserEventEntity.prototype, "event_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "invite_code", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'boolean', nullable: true, default: false }),
    __metadata("design:type", Boolean)
], UserEventEntity.prototype, "remind", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true, default: types_1.PAYMENT_STATUS.WAITING }),
    __metadata("design:type", String)
], UserEventEntity.prototype, "payment_status", void 0);
UserEventEntity = __decorate([
    (0, typeorm_1.Entity)(exports.USERS_EVENTS_TABLE),
    (0, typeorm_1.Unique)('event_id_invite_code_un', ['event_id', 'invite_code']),
    (0, typeorm_1.Unique)('event_id_email_un', ['event_id', 'email'])
], UserEventEntity);
exports.UserEventEntity = UserEventEntity;
let UserEventEntitySubscriber = class UserEventEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
UserEventEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], UserEventEntitySubscriber);
exports.UserEventEntitySubscriber = UserEventEntitySubscriber;
//# sourceMappingURL=user-event.entity.js.map