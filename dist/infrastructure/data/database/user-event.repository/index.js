"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEventRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const user_event_entity_1 = require("./user-event.entity");
class UserEventRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(user_event_entity_1.UserEventEntity, database_1.ConnectionName.xtrading_user_service);
    }
    async getPaging(param) {
        let { page, size, keyword } = param;
        const { type, event_id, confirm_status, attend, payment_status } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (confirm_status) {
            whereArray.push(`confirm_status = '${confirm_status}'`);
        }
        if (payment_status) {
            whereArray.push(`payment_status = '${payment_status}'`);
        }
        if (attend) {
            whereArray.push(`attend = ${attend == 1 ? true : false}`);
        }
        if (type) {
            whereArray.push(`invite_code LIKE '%${type}%'`);
        }
        if (event_id) {
            whereArray.push(`event_id = '${event_id}'`);
        }
        if (keyword) {
            keyword = keyword.toLowerCase();
            whereArray.push(`(lower(email) LIKE '%${keyword}%'
        OR lower(fullname) LIKE '%${keyword}%'
        OR phone LIKE '%${keyword}%'
        OR lower(telegram) LIKE '%${keyword}%'
        OR invite_code LIKE '%${keyword.toUpperCase()}%')`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUserEvent = `
        SELECT  *
        FROM ${user_event_entity_1.USERS_EVENTS_TABLE}  
        ${where}  
        ORDER BY created_at DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM ${user_event_entity_1.USERS_EVENTS_TABLE}  
    ${where}
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryUserEvent),
            this.repo().query(queryCount),
        ]);
        return {
            rows: rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getUserByDate(param) {
        const whereArray = [];
        if (param.event_id) {
            whereArray.push(`event_id = '${param.event_id}'`);
        }
        if (param.to) {
            whereArray.push(`created_at <= ${param.to}`);
        }
        if (param.from) {
            whereArray.push(`created_at >= ${param.from}`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUserEvent = `
    SELECT *
    FROM ${user_event_entity_1.USERS_EVENTS_TABLE}
    ${where}
    `;
        return this.repo().query(queryUserEvent);
    }
    async getEmails(param) {
        const whereArray = [];
        if (param.event_id) {
            whereArray.push(`event_id = '${param.event_id}'`);
        }
        if (param.confirm_status) {
            whereArray.push(`confirm_status = '${param.confirm_status}'`);
        }
        if (param.payment_status) {
            whereArray.push(`payment_status = '${param.payment_status}'`);
        }
        if (typeof param.attend === 'boolean') {
            whereArray.push(`attend = ${param.attend}`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUserEvent = `
    SELECT email, fullname 
    FROM ${user_event_entity_1.USERS_EVENTS_TABLE}
    ${where}
    group by email, fullname 
    `;
        return this.repo().query(queryUserEvent);
    }
}
exports.UserEventRepository = UserEventRepository;
//# sourceMappingURL=index.js.map