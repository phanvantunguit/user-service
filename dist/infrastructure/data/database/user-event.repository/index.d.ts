import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE, PAYMENT_STATUS } from 'src/domain/event/types';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserEventEntity } from './user-event.entity';
export declare class UserEventRepository extends BaseRepository<UserEventEntity> {
    repo(): Repository<UserEventEntity>;
    getPaging(param: {
        page?: number;
        size?: number;
        keyword?: string;
        type?: INVITE_CODE_TYPE;
        event_id?: string;
        confirm_status?: EVENT_CONFIRM_STATUS;
        payment_status?: PAYMENT_STATUS;
        attend?: number;
    }): Promise<{
        rows: UserEventDomain[];
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getUserByDate(param: {
        from?: number;
        to?: number;
        event_id: string;
    }): Promise<UserEventEntity[]>;
    getEmails(param: {
        event_id?: string;
        confirm_status?: EVENT_CONFIRM_STATUS;
        payment_status?: PAYMENT_STATUS;
        attend?: boolean;
    }): Promise<{
        email: string;
        fullname: string;
    }[]>;
}
