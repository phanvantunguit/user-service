import { EVENT_CONFIRM_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';
import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const USERS_EVENTS_TABLE = "users_events";
export declare class UserEventEntity {
    id?: string;
    created_by?: string;
    updated_by?: string;
    created_at?: number;
    updated_at?: number;
    email?: string;
    phone?: string;
    telegram?: string;
    fullname?: string;
    confirm_status?: EVENT_CONFIRM_STATUS;
    attend?: boolean;
    event_id?: string;
    invite_code?: string;
    remind?: boolean;
    payment_status?: PAYMENT_STATUS;
}
export declare class UserEventEntitySubscriber implements EntitySubscriberInterface<UserEventEntity> {
    beforeInsert(event: InsertEvent<UserEventEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserEventEntity>): Promise<void>;
}
