"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("../../../config");
const base_repository_1 = require("./base.repository");
const db_context_1 = require("./db-context");
const admin_repository_1 = require("./admin.repository");
const admin_entity_1 = require("./admin.repository/admin.entity");
const event_entity_1 = require("./event.repository/event.entity");
const user_event_entity_1 = require("./user-event.repository/user-event.entity");
const event_repository_1 = require("./event.repository");
const user_event_repository_1 = require("./user-event.repository");
const user_remind_repository_1 = require("./user-remind.repository");
const user_remind_entity_1 = require("./user-remind.repository/user-remind.entity");
const user_promotion_repository_1 = require("./user-promotion.repository");
const user_promotion_entity_1 = require("./user-promotion.repository/user-promotion.entity");
const database_1 = require("../../../const/database");
const merchant_entity_1 = require("./merchant.repository/merchant.entity");
const merchant_repository_1 = require("./merchant.repository");
const transaction_log_entity_1 = require("./transaction.repository/transaction-log.entity");
const transaction_metadata_entity_1 = require("./transaction.repository/transaction-metadata.entity");
const transaction_entity_1 = require("./transaction.repository/transaction.entity");
const transaction_repository_1 = require("./transaction.repository");
const user_repository_1 = require("./user.repository");
const user_entity_1 = require("./user.repository/user.entity");
const bot_repository_1 = require("./bot.repository");
const bot_entity_1 = require("./bot.repository/bot.entity");
const role_repository_1 = require("./role.repository");
const role_entity_1 = require("./role.repository/role.entity");
const bot_trading_repository_1 = require("./bot-trading.repository");
const bot_trading_entity_1 = require("./bot-trading.repository/bot-trading.entity");
const merchant_commission_repository_1 = require("./merchant-commission.repository");
const merchant_commission_entity_1 = require("./merchant-commission.repository/merchant-commission.entity");
const merchant_invoice_entity_1 = require("./merchant-invoice.repository/merchant-invoice.entity");
const merchant_invoice_repository_1 = require("./merchant-invoice.repository");
const user_bot_trading_entity_1 = require("./user.repository/user-bot-trading.entity");
const user_asset_log_entity_1 = require("./user.repository/user-asset-log.entity");
const verify_token_entity_1 = require("./user.repository/verify-token.entity");
const user_role_entity_1 = require("./user.repository/user-role.entity");
const setting_entity_1 = require("./setting.repository/setting.entity");
const setting_repository_1 = require("./setting.repository");
const event_store_repository_1 = require("./event-store.repository");
const event_store_entity_1 = require("./event-store.repository/event-store.entity");
const additional_data_repository_1 = require("./additional-data.repository");
const merchant_additional_data_repository_1 = require("./merchant-additional-data.repository");
const additional_data_entity_1 = require("./additional-data.repository/additional-data.entity");
const merchant_additional_data_entity_1 = require("./merchant-additional-data.repository/merchant-additional-data.entity");
const repo = [
    db_context_1.DBContext,
    base_repository_1.BaseRepository,
    admin_repository_1.AdminRepository,
    event_repository_1.EventRepository,
    user_event_repository_1.UserEventRepository,
    user_remind_repository_1.UserRemindRepository,
    user_promotion_repository_1.UserPromotionRepository,
    merchant_repository_1.MerchantRepository,
    transaction_repository_1.TransactionRepository,
    user_repository_1.UserRepository,
    bot_repository_1.BotRepository,
    role_repository_1.RoleRepository,
    bot_trading_repository_1.BotTradingRepository,
    merchant_commission_repository_1.MerchantCommissionRepository,
    merchant_invoice_repository_1.MerchantInvoiceRepository,
    setting_repository_1.SettingRepository,
    event_store_repository_1.EventStoreRepository,
    additional_data_repository_1.AdditionalDataRepository,
    merchant_additional_data_repository_1.MerchantAdditionalDataRepository,
];
let DatabaseModule = class DatabaseModule {
};
DatabaseModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                name: database_1.ConnectionName.xtrading_user_service,
                host: config_1.APP_CONFIG.POSTGRES_HOST,
                port: config_1.APP_CONFIG.POSTGRES_PORT,
                username: config_1.APP_CONFIG.POSTGRES_USER,
                password: config_1.APP_CONFIG.POSTGRES_PASS,
                database: config_1.APP_CONFIG.POSTGRES_DB,
                entities: [
                    admin_entity_1.AdminEntity,
                    event_entity_1.EventEntity,
                    user_event_entity_1.UserEventEntity,
                    user_remind_entity_1.UserRemindEntity,
                    user_promotion_entity_1.UserPromotionEntity,
                ],
                subscribers: [
                    admin_entity_1.AdminEntitySubscriber,
                    event_entity_1.EventEntitySubscriber,
                    user_event_entity_1.UserEventEntitySubscriber,
                    user_remind_entity_1.UserRemindEntitySubscriber,
                    user_promotion_entity_1.UserPromotionEntitySubscriber,
                ],
                synchronize: true,
                migrations: ['dist/migrations/*{.ts,.js}'],
                migrationsTableName: 'migrations',
                migrationsRun: false,
                keepConnectionAlive: true,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                name: database_1.ConnectionName.user_role,
                host: config_1.APP_CONFIG.UR_POSTGRES_HOST,
                port: config_1.APP_CONFIG.UR_POSTGRES_PORT,
                username: config_1.APP_CONFIG.UR_POSTGRES_USER,
                password: config_1.APP_CONFIG.UR_POSTGRES_PASS,
                database: config_1.APP_CONFIG.UR_POSTGRES_DB,
                entities: [
                    merchant_entity_1.MerchantEntity,
                    user_entity_1.UserEntity,
                    bot_entity_1.BotEntity,
                    role_entity_1.RoleEntity,
                    bot_trading_entity_1.BotTradingEntity,
                    merchant_commission_entity_1.MerchantCommissionEntity,
                    merchant_invoice_entity_1.MerchantInvoiceEntity,
                    user_bot_trading_entity_1.UserBotTradingEntity,
                    user_asset_log_entity_1.UserAssetLogEntity,
                    verify_token_entity_1.VerifyTokenEntity,
                    user_role_entity_1.UserRoleEntity,
                    setting_entity_1.AppSettingEntity,
                    event_store_entity_1.EventStoreEntity,
                    additional_data_entity_1.AdditionalDataEntity,
                    merchant_additional_data_entity_1.MerchantAdditionalDataEntity,
                ],
                subscribers: [
                    merchant_entity_1.MerchantEntitySubscriber,
                    user_entity_1.UserEntitySubscriber,
                    bot_entity_1.BotEntitySubscriber,
                    role_entity_1.RoleEntitySubscriber,
                    bot_trading_entity_1.BotTradingEntitySubscriber,
                    merchant_commission_entity_1.MerchantCommissionEntitySubscriber,
                    merchant_invoice_entity_1.MerchantInvoiceEntitySubscriber,
                    user_bot_trading_entity_1.UserBotTradingEntitySubscriber,
                    user_asset_log_entity_1.UserAssetLogEntitySubscriber,
                    verify_token_entity_1.VerifyTokenEntitySubscriber,
                    user_role_entity_1.UserRoleEntitySubscriber,
                    setting_entity_1.AppSettingEntitySubscriber,
                    event_store_entity_1.EventStoreEntitySubscriber,
                    additional_data_entity_1.AdditionalDataEntitySubscriber,
                    merchant_additional_data_entity_1.MerchantAdditionalDataEntitySubscriber,
                ],
                synchronize: true,
                migrationsRun: false,
                keepConnectionAlive: true,
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                name: database_1.ConnectionName.payment_service,
                host: config_1.APP_CONFIG.PAYMENT_POSTGRES_HOST,
                port: config_1.APP_CONFIG.PAYMENT_POSTGRES_PORT,
                username: config_1.APP_CONFIG.PAYMENT_POSTGRES_USER,
                password: config_1.APP_CONFIG.PAYMENT_POSTGRES_PASS,
                database: config_1.APP_CONFIG.PAYMENT_POSTGRES_DB,
                entities: [
                    transaction_log_entity_1.TransactionLogEntity,
                    transaction_metadata_entity_1.TransactionMetadataEntity,
                    transaction_entity_1.TransactionEntity,
                ],
                subscribers: [
                    transaction_entity_1.TransactionEntitySubscriber,
                    transaction_log_entity_1.TransactionLogEntitySubscriber,
                    transaction_metadata_entity_1.TransactionMetadataEntitySubscriber,
                ],
                synchronize: false,
                migrationsRun: false,
                keepConnectionAlive: true,
            }),
        ],
        providers: repo,
        exports: repo,
    })
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;
//# sourceMappingURL=index.js.map