import { QueryToken, RawVerifyToken } from 'src/application/usecase/verify-token/verify-token.types';
import { TBOT_TYPE } from 'src/domain/bot/types';
import { ITEM_STATUS } from 'src/domain/transaction/types';
import { Repository, QueryRunner } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserAssetLogEntity } from './user-asset-log.entity';
import { UserBotTradingEntity } from './user-bot-trading.entity';
import { UserEntity } from './user.entity';
import { VerifyTokenEntity } from './verify-token.entity';
export declare class UserRepository extends BaseRepository<UserEntity> {
    repo(): Repository<UserEntity>;
    getPaging(param: {
        page?: number;
        size?: number;
        keyword?: string;
        from?: number;
        to?: number;
        merchant_code?: string;
        tbots?: string[];
        email_confirmed?: boolean;
        tbot_status?: string[];
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getDetail(param: {
        id: string;
    }): Promise<any>;
    report(param: {
        from?: number;
        to?: number;
        merchant_code?: string;
    }): Promise<{
        count_confirmed: string;
        count_not_confirmed: string;
    }>;
    findOneUser(params: any): Promise<UserEntity>;
    saveUser(params: any): Promise<UserEntity>;
    findUser(params: {
        email?: string;
        keyword?: string;
        is_admin?: boolean;
        merchant_code?: string[];
    }): Promise<UserEntity[]>;
    private userRoleRepo;
    saveUserRole(params: any, queryRunner?: QueryRunner): Promise<any>;
    private userBotTradingRepo;
    saveUserBotTrading(params: any[], queryRunner?: QueryRunner): Promise<any[]>;
    saveStatusBotTrading(params: any): Promise<any>;
    findUserBotTrading(params: {
        user_id?: string;
        bot_id?: string;
    }): Promise<UserBotTradingEntity[]>;
    findUserBotTradingSQL(params: {
        user_id?: string;
        bot_ids?: string[];
        status?: ITEM_STATUS[];
        expires_at?: number;
        expired?: number;
        type?: TBOT_TYPE;
    }): Promise<UserBotTradingEntity[]>;
    findByAssetIds(ids: any): Promise<UserBotTradingEntity[]>;
    findSubscriberByIds(ids: string[]): Promise<any>;
    deleteAssetIds(assetIds: string[]): Promise<any>;
    private userAssetLogRepo;
    saveUserAssetLog(params: any[], queryRunner?: QueryRunner): Promise<any[]>;
    findAssetLogLatest(param: {
        user_id?: string;
        category?: string;
        status?: string[];
    }): Promise<UserAssetLogEntity[]>;
    private verifyTokenRepo;
    saveVerifyToken(params: RawVerifyToken): Promise<RawVerifyToken>;
    findOneToken(params: QueryToken): Promise<VerifyTokenEntity>;
}
