"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAssetLogEntitySubscriber = exports.UserAssetLogEntity = exports.USER_ASSET_LOG_TABLE = void 0;
const types_1 = require("../../../../domain/transaction/types");
const typeorm_1 = require("typeorm");
exports.USER_ASSET_LOG_TABLE = 'user_asset_logs';
let UserAssetLogEntity = class UserAssetLogEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "order_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "asset_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "owner_created", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "package_type", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], UserAssetLogEntity.prototype, "quantity", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], UserAssetLogEntity.prototype, "expires_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "price", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], UserAssetLogEntity.prototype, "discount_rate", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'float4', nullable: true }),
    __metadata("design:type", Number)
], UserAssetLogEntity.prototype, "discount_amount", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserAssetLogEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserAssetLogEntity.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], UserAssetLogEntity.prototype, "metadata", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserAssetLogEntity.prototype, "type", void 0);
UserAssetLogEntity = __decorate([
    (0, typeorm_1.Entity)(exports.USER_ASSET_LOG_TABLE)
], UserAssetLogEntity);
exports.UserAssetLogEntity = UserAssetLogEntity;
let UserAssetLogEntitySubscriber = class UserAssetLogEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
UserAssetLogEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], UserAssetLogEntitySubscriber);
exports.UserAssetLogEntitySubscriber = UserAssetLogEntitySubscriber;
//# sourceMappingURL=user-asset-log.entity.js.map