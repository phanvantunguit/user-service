import { EntitySubscriberInterface, InsertEvent } from 'typeorm';
export declare const VERIFY_TOKENS_TABLE = "verify_tokens";
export declare class VerifyTokenEntity {
    id: string;
    user_id: string;
    token: string;
    type: string;
    expires_at: number;
    metadata: any;
    created_at: number;
}
export declare class VerifyTokenEntitySubscriber implements EntitySubscriberInterface<VerifyTokenEntity> {
    beforeInsert(event: InsertEvent<VerifyTokenEntity>): Promise<void>;
}
