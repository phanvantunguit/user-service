"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyTokenEntitySubscriber = exports.VerifyTokenEntity = exports.VERIFY_TOKENS_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.VERIFY_TOKENS_TABLE = 'verify_tokens';
let VerifyTokenEntity = class VerifyTokenEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], VerifyTokenEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], VerifyTokenEntity.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], VerifyTokenEntity.prototype, "token", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], VerifyTokenEntity.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint' }),
    __metadata("design:type", Number)
], VerifyTokenEntity.prototype, "expires_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], VerifyTokenEntity.prototype, "metadata", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], VerifyTokenEntity.prototype, "created_at", void 0);
VerifyTokenEntity = __decorate([
    (0, typeorm_1.Entity)(exports.VERIFY_TOKENS_TABLE)
], VerifyTokenEntity);
exports.VerifyTokenEntity = VerifyTokenEntity;
let VerifyTokenEntitySubscriber = class VerifyTokenEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
    }
};
VerifyTokenEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], VerifyTokenEntitySubscriber);
exports.VerifyTokenEntitySubscriber = VerifyTokenEntitySubscriber;
//# sourceMappingURL=verify-token.entity.js.map