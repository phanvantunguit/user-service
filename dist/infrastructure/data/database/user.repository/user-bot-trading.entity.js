"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserBotTradingEntitySubscriber = exports.UserBotTradingEntity = exports.USER_BOT_TRADING_TABLE = void 0;
const types_1 = require("../../../../domain/transaction/types");
const typeorm_1 = require("typeorm");
exports.USER_BOT_TRADING_TABLE = 'user_bot_tradings';
let UserBotTradingEntity = class UserBotTradingEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "bot_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "user_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "owner_created", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "broker", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "broker_server", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "broker_account", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "subscriber_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], UserBotTradingEntity.prototype, "expires_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "type", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], UserBotTradingEntity.prototype, "balance", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', nullable: true }),
    __metadata("design:type", Number)
], UserBotTradingEntity.prototype, "connected_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserBotTradingEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], UserBotTradingEntity.prototype, "updated_at", void 0);
UserBotTradingEntity = __decorate([
    (0, typeorm_1.Entity)(exports.USER_BOT_TRADING_TABLE),
    (0, typeorm_1.Unique)('tbot_id_user_id_un', ['bot_id', 'user_id'])
], UserBotTradingEntity);
exports.UserBotTradingEntity = UserBotTradingEntity;
let UserBotTradingEntitySubscriber = class UserBotTradingEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
UserBotTradingEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], UserBotTradingEntitySubscriber);
exports.UserBotTradingEntitySubscriber = UserBotTradingEntitySubscriber;
//# sourceMappingURL=user-bot-trading.entity.js.map