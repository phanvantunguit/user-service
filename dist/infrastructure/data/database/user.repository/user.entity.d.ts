import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const USERS_TABLE = "users";
export declare class UserEntity {
    id: string;
    email: string;
    username: string;
    password: string;
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    affiliate_code: string;
    link_affiliate: string;
    referral_code: string;
    profile_pic: string;
    active: boolean;
    email_confirmed: boolean;
    note_updated: string;
    date_registered: string;
    super_user: boolean;
    is_admin: boolean;
    country: string;
    year_of_birth: string;
    gender: string;
    phone_code: string;
    merchant_code: string;
    created_at: number;
    updated_at: number;
}
export declare class UserEntitySubscriber implements EntitySubscriberInterface<UserEntity> {
    beforeInsert(event: InsertEvent<UserEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserEntity>): Promise<void>;
}
