import { ITEM_STATUS, ORDER_CATEGORY } from 'src/domain/transaction/types';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const USER_ASSET_LOG_TABLE = "user_asset_logs";
export declare class UserAssetLogEntity {
    id?: string;
    order_id?: string;
    asset_id?: string;
    user_id?: string;
    owner_created?: string;
    package_type?: string;
    category?: ORDER_CATEGORY;
    quantity?: number;
    expires_at?: number;
    status?: ITEM_STATUS;
    name?: string;
    price?: string;
    discount_rate?: number;
    discount_amount?: number;
    created_at?: number;
    updated_at?: number;
    metadata?: any;
    type?: string;
}
export declare class UserAssetLogEntitySubscriber implements EntitySubscriberInterface<UserAssetLogEntity> {
    beforeInsert(event: InsertEvent<UserAssetLogEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserAssetLogEntity>): Promise<void>;
}
