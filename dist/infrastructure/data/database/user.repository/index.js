"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const database_1 = require("../../../../const/database");
const types_1 = require("../../../../domain/transaction/types");
const format_data_util_1 = require("../../../../utils/format-data.util");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const user_asset_log_entity_1 = require("./user-asset-log.entity");
const user_bot_trading_entity_1 = require("./user-bot-trading.entity");
const user_role_entity_1 = require("./user-role.entity");
const user_entity_1 = require("./user.entity");
const verify_token_entity_1 = require("./verify-token.entity");
class UserRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(user_entity_1.UserEntity, database_1.ConnectionName.user_role);
    }
    async getPaging(param) {
        let { page, size, keyword } = param;
        const { from, to, merchant_code, tbots, email_confirmed, tbot_status } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        if (email_confirmed) {
            whereArray.push(`email_confirmed = ${email_confirmed}`);
        }
        if (keyword) {
            keyword = keyword.toLowerCase();
            const keywords = keyword.split(' ');
            if (keywords.length > 1) {
                const last_name = keywords[keywords.length - 1];
                const first_name = keywords
                    .filter((e, i) => i < keywords.length - 1)
                    .join(' ');
                whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR (lower(first_name) LIKE '%${first_name}%'
            AND lower(last_name) LIKE '%${last_name}%'))
            `);
            }
            else {
                whereArray.push(`(email LIKE '%${keyword}%'
          OR lower(username) LIKE '%${keyword}%'
          OR phone LIKE '%${keyword}%'
          OR lower(first_name) LIKE '%${keyword}%'
          OR lower(last_name) LIKE '%${keyword}%')
          `);
            }
        }
        if (tbots.length > 0) {
            const whereBots = tbots.map((name) => `tbots::jsonb ? '${name}'`);
            whereArray.push(`(${whereBots.join(' OR ')})`);
        }
        if (tbot_status && tbot_status.length > 0) {
            const whereTbotArray = tbot_status.map((status) => `tbot_status::jsonb ? '${status}'`);
            whereArray.push(`(${whereTbotArray.join(' OR ')})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUser = `
    SELECT *
    FROM
    (SELECT
      COALESCE(json_agg(DISTINCT ubt.status) FILTER (WHERE ubt.status IS NOT NULL), '[]') AS tbot_status,
      COALESCE(json_agg(DISTINCT ualb.asset_id) FILTER (WHERE ualb.asset_id IS NOT NULL), '[]') AS tbots,
  u.id,
      u.email,
      u.username,
      u.phone,
      u.first_name,
      u.last_name,
      u.address,
      u.affiliate_code,
      u.link_affiliate,
      u.referral_code,
      u.profile_pic,
      u.active,
      u.email_confirmed,
      u.note_updated,
      u.date_registered,
      u.country,
      u.year_of_birth,
      u.gender,
      u.phone_code,
      u.merchant_code,
      u.created_at,
      u.updated_at
      FROM users as u
      LEFT JOIN user_bot_tradings ubt on ubt.user_id = u.id
      LEFT JOIN (select * from (select distinct on (asset_id, user_id) * from user_asset_logs
      where category = 'TBOT'
      order by asset_id desc, user_id desc, updated_at desc) as ual) as ualb on ualb.user_id = u.id
      GROUP BY u.id) as utemp
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT u.*,
      COALESCE(json_agg(DISTINCT ubt.status) FILTER (WHERE ubt.status IS NOT NULL), '[]') AS tbot_status,
      COALESCE(json_agg(DISTINCT ualb.asset_id) FILTER (WHERE ualb.asset_id IS NOT NULL), '[]') AS tbots
      FROM users as u
      LEFT JOIN user_bot_tradings ubt on ubt.user_id = u.id
      LEFT JOIN (select * from (select distinct on (asset_id, user_id) * from user_asset_logs
      where category = 'TBOT'
      order by asset_id desc, user_id desc, updated_at desc) as ual) as ualb on ualb.user_id = u.id
      GROUP BY u.id) as utemp
    ${where} 
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryUser),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getDetail(param) {
        const { id } = param;
        const whereArray = [];
        if (id) {
            whereArray.push(`id = '${id}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUser = `
    SELECT *
    FROM
    (SELECT u.id,
      u.email,
      u.username,
      u.phone,
      u.first_name,
      u.last_name,
      u.address,
      u.affiliate_code,
      u.link_affiliate,
      u.referral_code,
      u.profile_pic,
      u.active,
      u.email_confirmed,
      u.note_updated,
      u.date_registered,
      u.country,
      u.year_of_birth,
      u.gender,
      u.phone_code,
      u.merchant_code,
      u.created_at,
      u.updated_at,
      COALESCE(json_agg(DISTINCT ualb.name) FILTER (WHERE ualb.name IS NOT NULL), '[]') AS tbots
      FROM users as u
      LEFT JOIN (select * from (select distinct on (asset_id, user_id) * from user_asset_logs
      where category = 'TBOT'
      order by asset_id desc, user_id desc, updated_at desc) as ual) as ualb on ualb.user_id = u.id
      GROUP BY u.id) as utemp
    ${where}
    ORDER BY created_at DESC
    LIMIT 1
  `;
        return this.repo().query(queryUser);
    }
    async report(param) {
        const { from, to, merchant_code } = param;
        const whereArray = [];
        if (from) {
            whereArray.push(`created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`created_at <= ${to}`);
        }
        if (merchant_code) {
            whereArray.push(`merchant_code = '${merchant_code}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryCount = `
    SELECT 
    SUM(case when email_confirmed = true then 1 else 0 end) as count_confirmed,
    SUM(case when email_confirmed= false then 1 else 0 end) as count_not_confirmed
    FROM ${user_entity_1.USERS_TABLE}
    ${where} 
    `;
        return this.repo().query(queryCount);
    }
    async findOneUser(params) {
        const query = (0, format_data_util_1.cleanObject)(params);
        return this.repo().findOne(query);
    }
    async saveUser(params) {
        if (params.email) {
            params.email = params.email.toLocaleLowerCase();
        }
        return this.repo().save(params);
    }
    async findUser(params) {
        let { keyword, is_admin, email, merchant_code } = params;
        const whereArray = [];
        if (is_admin) {
            whereArray.push(`is_admin = ${is_admin}`);
        }
        if (keyword) {
            keyword = keyword.toLowerCase();
            whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR lower(first_name) LIKE '%${keyword}%'
            OR lower(last_name) LIKE '%${keyword}%')
            `);
        }
        if (email) {
            whereArray.push(`email = '${email}'`);
        }
        if (merchant_code && merchant_code.length > 0) {
            whereArray.push(`merchant_code in (${merchant_code.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryUser = `
    SELECT  *
    FROM ${user_entity_1.USERS_TABLE}  
    ${where}  
    ORDER BY created_at DESC`;
        return this.repo().query(queryUser);
    }
    userRoleRepo() {
        return (0, typeorm_1.getRepository)(user_role_entity_1.UserRoleEntity, database_1.ConnectionName.user_role);
    }
    async saveUserRole(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userRoleRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userRoleRepo().save(params);
        }
    }
    userBotTradingRepo() {
        return (0, typeorm_1.getRepository)(user_bot_trading_entity_1.UserBotTradingEntity, database_1.ConnectionName.user_role);
    }
    async saveUserBotTrading(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userBotTradingRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userBotTradingRepo().save(params);
        }
    }
    async saveStatusBotTrading(params) {
        return this.userBotTradingRepo().save(params);
    }
    async findUserBotTrading(params) {
        return this.userBotTradingRepo().find({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async findUserBotTradingSQL(params) {
        const { user_id, bot_ids, status, expires_at, expired, type } = params;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        if (status && status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        if (expires_at) {
            whereArray.push(`expires_at > ${expires_at}`);
        }
        if (expired) {
            whereArray.push(`expires_at < ${expired}`);
            whereArray.push(`status != '${types_1.ITEM_STATUS.EXPIRED}'`);
        }
        if (bot_ids && bot_ids.length > 0) {
            whereArray.push(`bot_id in (${bot_ids.map((e) => `'${e}'`)})`);
        }
        if (type) {
            whereArray.push(`type = '${type}'`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      SELECT *
      FROM ${user_bot_trading_entity_1.USER_BOT_TRADING_TABLE}  
      ${where} 
      order by created_at DESC
    `;
        return this.userBotTradingRepo().query(query);
    }
    async findByAssetIds(ids) {
        return this.userBotTradingRepo().findByIds(ids);
    }
    async findSubscriberByIds(ids) {
        const queryRoles = `
        SELECT COUNT(subscriber_id)
        FROM ${user_bot_trading_entity_1.USER_BOT_TRADING_TABLE} rv 
        WHERE id in (${ids.map((id) => `'${id}'`)})
        `;
        const data = await this.userBotTradingRepo().query(queryRoles);
        return data;
    }
    async deleteAssetIds(assetIds) {
        const whereArray = [];
        if (assetIds.length > 0) {
            whereArray.push(`id in (${assetIds.map((id) => `'${id}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      DELETE
      FROM ${user_bot_trading_entity_1.USER_BOT_TRADING_TABLE}  
      ${where}
    `;
        return this.userBotTradingRepo().query(query);
    }
    userAssetLogRepo() {
        return (0, typeorm_1.getRepository)(user_asset_log_entity_1.UserAssetLogEntity, database_1.ConnectionName.user_role);
    }
    async saveUserAssetLog(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.userAssetLogRepo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.userAssetLogRepo().save(params);
        }
    }
    findAssetLogLatest(param) {
        const { user_id, category, status } = param;
        const whereArray = [];
        if (user_id) {
            whereArray.push(`user_id = '${user_id}'`);
        }
        if (category) {
            whereArray.push(`category = '${category}'`);
        }
        if (status && status.length > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const query = `
      select distinct on (asset_id) *
      from user_asset_logs
		  ${where}
      order by asset_id desc,  updated_at desc
    `;
        return this.userBotTradingRepo().query(query);
    }
    verifyTokenRepo() {
        return (0, typeorm_1.getRepository)(verify_token_entity_1.VerifyTokenEntity, database_1.ConnectionName.user_role);
    }
    async saveVerifyToken(params) {
        return this.verifyTokenRepo().save(params);
    }
    async findOneToken(params) {
        const query = (0, format_data_util_1.cleanObject)(params);
        return this.verifyTokenRepo().findOne(query);
    }
}
exports.UserRepository = UserRepository;
//# sourceMappingURL=index.js.map