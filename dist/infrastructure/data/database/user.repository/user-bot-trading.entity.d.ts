import { ITEM_STATUS } from 'src/domain/transaction/types';
import { EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
export declare const USER_BOT_TRADING_TABLE = "user_bot_tradings";
export declare class UserBotTradingEntity {
    id: string;
    bot_id: string;
    user_id: string;
    owner_created: string;
    broker: string;
    broker_server: string;
    broker_account: string;
    subscriber_id: string;
    expires_at: number;
    status: ITEM_STATUS;
    type?: string;
    balance?: string;
    connected_at?: number;
    created_at: number;
    updated_at: number;
}
export declare class UserBotTradingEntitySubscriber implements EntitySubscriberInterface<UserBotTradingEntity> {
    beforeInsert(event: InsertEvent<UserBotTradingEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<UserBotTradingEntity>): Promise<void>;
}
