"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantInvoiceRepository = void 0;
const database_1 = require("../../../../const/database");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const merchant_entity_1 = require("../merchant.repository/merchant.entity");
const merchant_invoice_entity_1 = require("./merchant-invoice.entity");
class MerchantInvoiceRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(merchant_invoice_entity_1.MerchantInvoiceEntity, database_1.ConnectionName.user_role);
    }
    async getPaging(param) {
        let { page, size, sort_by, keyword } = param;
        const { status, from, to, order_by, merchant_id } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (sort_by && sort_by === 'created_at') {
            sort_by = 'mi.created_at';
        }
        if (from) {
            whereArray.push(`mi.created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`mi.created_at <= ${to}`);
        }
        if (status) {
            whereArray.push(`mi.status = '${status}'`);
        }
        if (merchant_id) {
            whereArray.push(`mi.merchant_id = '${merchant_id}'`);
        }
        if (keyword) {
            whereArray.push(`(ms.name LIKE '%${keyword}%' OR ms.email LIKE '%${keyword}%')`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
    SELECT mi.*,
    ms.code as merchant_code,
    ms.email as merchant_email,
    ms.name as merchant_name,
    ms.config::json#>>'{wallet,wallet_address}' as wallet_address,
    ms.config::json#>>'{wallet,status}' as wallet_status
    FROM 
    ${merchant_invoice_entity_1.MERCHANT_INVOICES_TABLE} mi 
    left join ${merchant_entity_1.MERCHANTS_TABLE} ms on ms.id = mi.merchant_id
    ${where}  
    ORDER BY ${sort_by ? sort_by : 'mi.created_at'} ${order_by ? order_by : 'DESC'} 
    OFFSET ${(page - 1) * size}
    LIMIT ${size}
  `;
        const queryCount = `
  SELECT COUNT(*)
  FROM 
  ${merchant_invoice_entity_1.MERCHANT_INVOICES_TABLE} mi 
  left join ${merchant_entity_1.MERCHANTS_TABLE} ms on ms.id = mi.merchant_id
  ${where}  
  `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
    async getDetail(id) {
        const queryTransaction = `
    SELECT mi.*,
    ms.code as merchant_code,
    ms.email as merchant_email,
    ms.name as merchant_name,
    ms.config::json#>>'{wallet,wallet_address}' as wallet_address,
    ms.config::json#>>'{wallet,status}' as wallet_status
    FROM 
    ${merchant_invoice_entity_1.MERCHANT_INVOICES_TABLE} mi 
    left join ${merchant_entity_1.MERCHANTS_TABLE} ms on ms.id = mi.merchant_id
    where m.id = '${id}'
    `;
        return this.repo().query(queryTransaction);
    }
    async getTransaction(transaction_id) {
        const queryTransaction = `
    SELECT COUNT(mi.transaction_id)
    FROM 
    ${merchant_invoice_entity_1.MERCHANT_INVOICES_TABLE} mi 
    where mi.transaction_id = '${transaction_id}' AND mi.status ='COMPLETED'
    `;
        return this.repo().query(queryTransaction);
    }
    async totalCommission(param) {
        const { merchant_id, status } = param;
        const whereArray = [];
        if (merchant_id) {
            whereArray.push(`merchant_id = '${merchant_id}'`);
        }
        if ((status === null || status === void 0 ? void 0 : status.length) > 0) {
            whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
        }
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryCount = `
    SELECT 
    SUM(amount_commission_complete) as total
    FROM ${merchant_invoice_entity_1.MERCHANT_INVOICES_TABLE}
    ${where} 
    `;
        return this.repo().query(queryCount);
    }
}
exports.MerchantInvoiceRepository = MerchantInvoiceRepository;
//# sourceMappingURL=index.js.map