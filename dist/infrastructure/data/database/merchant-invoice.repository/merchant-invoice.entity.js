"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantInvoiceEntitySubscriber = exports.MerchantInvoiceEntity = exports.MERCHANT_INVOICES_TABLE = void 0;
const typeorm_1 = require("typeorm");
exports.MERCHANT_INVOICES_TABLE = 'merchant_invoices';
let MerchantInvoiceEntity = class MerchantInvoiceEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "merchant_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'float4' }),
    __metadata("design:type", Number)
], MerchantInvoiceEntity.prototype, "amount_commission_complete", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "transaction_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "wallet_address", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "wallet_network", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', nullable: true }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantInvoiceEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantInvoiceEntity.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], MerchantInvoiceEntity.prototype, "updated_by", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'jsonb', default: {} }),
    __metadata("design:type", Object)
], MerchantInvoiceEntity.prototype, "metadata", void 0);
MerchantInvoiceEntity = __decorate([
    (0, typeorm_1.Entity)(exports.MERCHANT_INVOICES_TABLE)
], MerchantInvoiceEntity);
exports.MerchantInvoiceEntity = MerchantInvoiceEntity;
let MerchantInvoiceEntitySubscriber = class MerchantInvoiceEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        event.entity.updated_at = Date.now();
    }
};
MerchantInvoiceEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], MerchantInvoiceEntitySubscriber);
exports.MerchantInvoiceEntitySubscriber = MerchantInvoiceEntitySubscriber;
//# sourceMappingURL=merchant-invoice.entity.js.map