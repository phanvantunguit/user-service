import { MERCHANT_INVOICES_STATUS } from 'src/domain/invoices/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MerchantInvoiceEntity } from './merchant-invoice.entity';
export declare class MerchantInvoiceRepository extends BaseRepository<MerchantInvoiceEntity> {
    repo(): Repository<MerchantInvoiceEntity>;
    getPaging(param: {
        keyword?: string;
        page?: number;
        size?: number;
        from?: number;
        to?: number;
        sort_by?: string;
        order_by?: string;
        status?: MERCHANT_INVOICES_STATUS;
        merchant_id?: string;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
    getDetail(id: string): Promise<any>;
    getTransaction(transaction_id: string): Promise<any>;
    totalCommission(param: {
        merchant_id?: string;
        status?: string[];
    }): Promise<{
        total: number;
    }[]>;
}
