"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepository = void 0;
const typeorm_1 = require("typeorm");
class BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(typeorm_1.BaseEntity);
    }
    async findOne(params) {
        return this.repo().findOne({
            where: params,
            order: { created_at: 'DESC' },
        });
    }
    async find(params) {
        return this.repo().find({ where: params, order: { created_at: 'DESC' } });
    }
    async findById(id) {
        return this.repo().findOne(id);
    }
    async findByIds(ids) {
        return this.repo().findByIds(ids);
    }
    async save(params, queryRunner) {
        if (queryRunner) {
            const transaction = await this.repo().create(params);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.repo().save(params);
        }
    }
    async deleteById(id, queryRunner) {
        return this.repo().delete(id);
    }
    async deleteMultipleByIds(ids, queryRunner) {
        return this.repo().delete(ids);
    }
    async delete(params, queryRunner) {
        return this.repo().delete(params);
    }
    async count(params) {
        return this.repo().count(params);
    }
    async softDeleteById(id, userId, queryRunner) {
        const data = { id, deleted_at: Date.now(), updated_by: userId };
        if (queryRunner) {
            const transaction = await this.repo().create(data);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.repo().save(data);
        }
    }
    async softDeleteMultipleByIds(ids, userId, queryRunner) {
        const data = ids.map((id) => {
            return { id, deleted_at: Date.now(), updated_by: userId };
        });
        if (queryRunner) {
            const transaction = await this.repo().create(data);
            return queryRunner.manager.save(transaction);
        }
        else {
            return this.repo().save(data);
        }
    }
}
exports.BaseRepository = BaseRepository;
//# sourceMappingURL=index.js.map