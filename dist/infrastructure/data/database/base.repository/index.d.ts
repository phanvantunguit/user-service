import { DeleteResult, Repository, QueryRunner } from 'typeorm';
import { EntityId } from 'typeorm/repository/EntityId';
import { IBaseRepository } from './types';
export declare class BaseRepository<T> implements IBaseRepository<T> {
    repo(): Repository<any>;
    findOne(params: T | T[]): Promise<T>;
    find(params: T | T[]): Promise<T[]>;
    findById(id: EntityId): Promise<T>;
    findByIds(ids: any[]): Promise<T[]>;
    save(params: T | T[], queryRunner?: QueryRunner): Promise<T>;
    deleteById(id: EntityId, queryRunner?: QueryRunner): Promise<DeleteResult>;
    deleteMultipleByIds(ids: EntityId[], queryRunner?: QueryRunner): Promise<DeleteResult>;
    delete(params: T, queryRunner?: QueryRunner): Promise<DeleteResult>;
    count(params: T): Promise<number>;
    softDeleteById(id: EntityId, userId?: string, queryRunner?: QueryRunner): Promise<T>;
    softDeleteMultipleByIds(ids: EntityId[], userId?: string, queryRunner?: QueryRunner): Promise<T[]>;
}
