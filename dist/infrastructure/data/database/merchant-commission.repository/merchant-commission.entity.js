"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantCommissionEntitySubscriber = exports.MerchantCommissionEntity = exports.MERCHANT_COMMISSION_TABLE = void 0;
const types_1 = require("../../../../domain/commission/types");
const typeorm_1 = require("typeorm");
exports.MERCHANT_COMMISSION_TABLE = 'merchant_commissions';
let MerchantCommissionEntity = class MerchantCommissionEntity {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, typeorm_1.Generated)('uuid'),
    __metadata("design:type", String)
], MerchantCommissionEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantCommissionEntity.prototype, "merchant_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid' }),
    __metadata("design:type", String)
], MerchantCommissionEntity.prototype, "asset_id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar' }),
    __metadata("design:type", String)
], MerchantCommissionEntity.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', default: types_1.MERCHANT_COMMISION_STATUS.ACTIVE }),
    __metadata("design:type", String)
], MerchantCommissionEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'float4', default: 0 }),
    __metadata("design:type", Number)
], MerchantCommissionEntity.prototype, "commission", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantCommissionEntity.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'bigint', default: Date.now() }),
    __metadata("design:type", Number)
], MerchantCommissionEntity.prototype, "updated_at", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'uuid', nullable: true }),
    __metadata("design:type", String)
], MerchantCommissionEntity.prototype, "updated_by", void 0);
MerchantCommissionEntity = __decorate([
    (0, typeorm_1.Entity)(exports.MERCHANT_COMMISSION_TABLE, { synchronize: false }),
    (0, typeorm_1.Unique)('merchant_id_asset_id_un', ['merchant_id', 'asset_id'])
], MerchantCommissionEntity);
exports.MerchantCommissionEntity = MerchantCommissionEntity;
let MerchantCommissionEntitySubscriber = class MerchantCommissionEntitySubscriber {
    async beforeInsert(event) {
        event.entity.created_at = Date.now();
        event.entity.updated_at = Date.now();
    }
    async beforeUpdate(event) {
        if (!event.entity.deleted_at) {
            event.entity.updated_at = Date.now();
        }
    }
};
MerchantCommissionEntitySubscriber = __decorate([
    (0, typeorm_1.EventSubscriber)()
], MerchantCommissionEntitySubscriber);
exports.MerchantCommissionEntitySubscriber = MerchantCommissionEntitySubscriber;
//# sourceMappingURL=merchant-commission.entity.js.map