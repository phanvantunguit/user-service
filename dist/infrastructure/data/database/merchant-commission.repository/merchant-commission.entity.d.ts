import { EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
export declare const MERCHANT_COMMISSION_TABLE = "merchant_commissions";
export declare class MerchantCommissionEntity {
    id?: string;
    merchant_id?: string;
    asset_id?: string;
    category?: string;
    status?: string;
    commission?: number;
    created_at?: number;
    updated_at?: number;
    updated_by?: string;
}
export declare class MerchantCommissionEntitySubscriber implements EntitySubscriberInterface<MerchantCommissionEntity> {
    beforeInsert(event: InsertEvent<MerchantCommissionEntity>): Promise<void>;
    beforeUpdate(event: UpdateEvent<MerchantCommissionEntity>): Promise<void>;
}
