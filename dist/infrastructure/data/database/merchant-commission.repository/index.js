"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantCommissionRepository = void 0;
const database_1 = require("../../../../const/database");
const permission_1 = require("../../../../const/permission");
const typeorm_1 = require("typeorm");
const base_repository_1 = require("../base.repository");
const bot_trading_entity_1 = require("../bot-trading.repository/bot-trading.entity");
const merchant_commission_entity_1 = require("./merchant-commission.entity");
class MerchantCommissionRepository extends base_repository_1.BaseRepository {
    repo() {
        return (0, typeorm_1.getRepository)(merchant_commission_entity_1.MerchantCommissionEntity, database_1.ConnectionName.user_role);
    }
    async getPaging(param) {
        let { page, size, sort_by } = param;
        const { status, from, to, merchant_id, order_by, category } = param;
        page = Number(page || 1);
        size = Number(size || 50);
        const whereArray = [];
        if (sort_by && sort_by === 'created_at') {
            sort_by = 'mc.created_at';
        }
        if (from) {
            whereArray.push(`mc.created_at >= ${from}`);
        }
        if (to) {
            whereArray.push(`mc.created_at <= ${to}`);
        }
        if (status) {
            whereArray.push(`mc.status = '${status}'`);
        }
        if (merchant_id) {
            whereArray.push(`mc.merchant_id = '${merchant_id}'`);
        }
        if (category) {
            whereArray.push(`mc.category = '${category}'`);
        }
        whereArray.push(`bt.status = '${permission_1.BOT_STATUS.OPEN}'`);
        const where = whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
        const queryTransaction = `
      SELECT mc.*, bt.name, bt.pnl, bt.max_drawdown, bt.price, bt.image_url, bt.order
      FROM 
      ${merchant_commission_entity_1.MERCHANT_COMMISSION_TABLE} mc 
      LEFT JOIN ${bot_trading_entity_1.BOT_TRADING_TABLE} bt on bt.id = mc.asset_id
      ${where}  
      ORDER BY ${sort_by ? sort_by : 'mc.created_at'} ${order_by ? order_by : 'DESC'} 
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
        const queryCount = `
    SELECT COUNT(*)
    FROM 
    ${merchant_commission_entity_1.MERCHANT_COMMISSION_TABLE} mc 
    LEFT JOIN ${bot_trading_entity_1.BOT_TRADING_TABLE} bt on bt.id = mc.asset_id
    ${where}  
    `;
        const [rows, total] = await Promise.all([
            this.repo().query(queryTransaction),
            this.repo().query(queryCount),
        ]);
        return {
            rows,
            page,
            size,
            count: rows.length,
            total: Number(total[0].count),
        };
    }
}
exports.MerchantCommissionRepository = MerchantCommissionRepository;
//# sourceMappingURL=index.js.map