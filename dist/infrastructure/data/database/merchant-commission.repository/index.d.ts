import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import { Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MerchantCommissionEntity } from './merchant-commission.entity';
export declare class MerchantCommissionRepository extends BaseRepository<MerchantCommissionEntity> {
    repo(): Repository<MerchantCommissionEntity>;
    getPaging(param: {
        page?: number;
        size?: number;
        from?: number;
        to?: number;
        sort_by?: string;
        order_by?: string;
        status?: MERCHANT_COMMISION_STATUS;
        category?: string;
        merchant_id: string;
    }): Promise<{
        rows: any;
        page: number;
        size: number;
        count: number;
        total: number;
    }>;
}
