import { GCloudTransport } from './google.transport';
export declare class StorageService {
    private gCloudTransport;
    constructor(gCloudTransport: GCloudTransport);
    upload(file: any): Promise<string>;
    uploadBuffer(data: string, fileName: string): Promise<string>;
}
