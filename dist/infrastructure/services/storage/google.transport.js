"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GCloudTransport = void 0;
const config_1 = require("../../../config");
const storage_1 = require("@google-cloud/storage");
const app_setting_1 = require("../../../const/app-setting");
class GCloudTransport {
    constructor() {
        this.storage = new storage_1.Storage({
            projectId: config_1.APP_CONFIG.GOOGLE_CLOUD_PROJECT_ID || 'coinmap-k8s',
        });
        this.bucket = this.storage.bucket(config_1.APP_CONFIG.GOOGLE_CLOUD_STORAGE_BUCKET || 'static-dev.cextrading.io');
    }
    async upload(file) {
        return new Promise((resolve, reject) => {
            const mimetype = file.originalname.split('.').pop();
            const fileName = `images/${app_setting_1.SERVICE_NAME}/${Date.now()}.${mimetype}`;
            const blob = this.bucket.file(fileName);
            const blobStream = blob.createWriteStream();
            blobStream.on('error', (err) => {
                reject(err);
            });
            blobStream.on('finish', () => {
                resolve(`https://${this.bucket.name}/${blob.name}`);
            });
            blobStream.end(file.buffer);
        });
    }
    async uploadBuffer(data, fileName) {
        return new Promise((resolve, reject) => {
            const base64EncodedString = data.replace(/^data:\w+\/\w+;base64,/, '');
            const fileBuffer = Buffer.from(base64EncodedString, 'base64');
            const blob = this.bucket.file(fileName);
            const blobStream = blob.createWriteStream();
            blobStream.on('error', (err) => {
                reject(err);
            });
            blobStream.on('finish', () => {
                resolve(`https://${this.bucket.name}/${blob.name}`);
            });
            blobStream.end(fileBuffer);
        });
    }
}
exports.GCloudTransport = GCloudTransport;
//# sourceMappingURL=google.transport.js.map