import { Storage } from '@google-cloud/storage';
export declare class GCloudTransport {
    storage: Storage;
    bucket: import("@google-cloud/storage/build/src/bucket").Bucket;
    upload(file: any): Promise<string>;
    uploadBuffer(data: string, fileName: string): Promise<string>;
}
