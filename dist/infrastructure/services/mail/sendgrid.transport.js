"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendGridTransport = void 0;
const http_transport_util_1 = require("../../../utils/http-transport.util");
const config_1 = require("../../../config");
class SendGridTransport {
    async sendGridSendEmail(params, email, name) {
        const data = {
            from: {
                email: email || config_1.APP_CONFIG.SENDGRID_SENDER_EMAIL,
                name: name || config_1.APP_CONFIG.SENDGRID_SENDER_NAME,
            },
            personalizations: [
                {
                    to: [
                        {
                            email: params.to,
                        },
                    ],
                },
            ],
            subject: params.subject,
            content: [
                {
                    type: 'text/html',
                    value: params.html,
                },
            ],
        };
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/mail/send`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            const result = await (0, http_transport_util_1.requestPOST)(url, data, headers);
            console.log('SendGrid result', result);
            return true;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
    async sendGridSendDynamicTemplate(params) {
        console.log("params", params);
        const data = {
            from: {
                email: params.from_email || config_1.APP_CONFIG.SENDGRID_SENDER_EMAIL,
                name: params.from_name || config_1.APP_CONFIG.SENDGRID_SENDER_NAME,
            },
            personalizations: [
                {
                    to: [
                        {
                            email: params.to,
                        },
                    ],
                    dynamic_template_data: params.dynamic_template_data,
                },
            ],
            template_id: params.template_id,
        };
        if (params.send_at) {
            data['send_at'] = params.send_at;
        }
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/mail/send`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            const result = await (0, http_transport_util_1.requestPOST)(url, data, headers);
            console.log('SendGrid result', result);
            return true;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
    async sendRequestVerifySender(param) {
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            console.log('sendRequestVerifySender data', param);
            const result = await (0, http_transport_util_1.requestPOST)(url, param, headers);
            console.log('SendGrid result', result);
            return result;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
    async sendVerifySender(param) {
        const { token } = param;
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders/verify/${token}`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            const result = await (0, http_transport_util_1.requestGET)(url, headers);
            console.log('SendGrid result', result);
            return true;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
    async updateSender(param) {
        const { id, nickname, from_name, reply_to, address, city, country } = param;
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders/${id}`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            const data = {
                nickname,
                from_name,
                reply_to,
                address,
                city,
                country,
            };
            const result = await (0, http_transport_util_1.requestPATCH)(url, data, headers);
            console.log('SendGrid result', result);
            return result;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
    async deleteSender(param) {
        const { id } = param;
        const url = `${config_1.APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders/${id}`;
        const headers = {
            Authorization: `Bearer ${config_1.APP_CONFIG.SENDGRID_API_TOKEN}`,
            'Content-Type': 'application/json',
        };
        try {
            const result = await (0, http_transport_util_1.requestDELETE)(url, headers);
            console.log('SendGrid result', result);
            return result;
        }
        catch (error) {
            console.log('SendGrid ERROR', error);
            return false;
        }
    }
}
exports.SendGridTransport = SendGridTransport;
//# sourceMappingURL=sendgrid.transport.js.map