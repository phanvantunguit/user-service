import { ISendDynamincTemplate, ISendEmail } from './types';
export declare class SendGridTransport {
    sendGridSendEmail(params: ISendEmail, email?: string, name?: string): Promise<any>;
    sendGridSendDynamicTemplate(params: ISendDynamincTemplate): Promise<boolean>;
    sendRequestVerifySender(param: {
        nickname: string;
        from_email: string;
        from_name: string;
        reply_to: string;
        address: string;
        city: string;
        country: string;
    }): Promise<any>;
    sendVerifySender(param: {
        token: string;
    }): Promise<boolean>;
    updateSender(param: {
        id: string;
        nickname: string;
        from_name: string;
        reply_to: string;
        address: string;
        city: string;
        country: string;
    }): Promise<any>;
    deleteSender(param: {
        id: string;
    }): Promise<any>;
}
