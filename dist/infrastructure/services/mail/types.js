"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMAIL_SUBJECT = void 0;
var EMAIL_SUBJECT;
(function (EMAIL_SUBJECT) {
    EMAIL_SUBJECT["confirm_attend_to_event"] = "Th\u01B0 m\u1EDDi tham gia s\u1EF1 ki\u1EC7n";
    EMAIL_SUBJECT["verify_email"] = "X\u00E1c nh\u1EADn email";
    EMAIL_SUBJECT["remind_attend_to_event"] = "Th\u01B0 nh\u1EAFc tham gia s\u1EF1 ki\u1EC7n v\u00E0o ng\u00E0y mai";
    EMAIL_SUBJECT["create_account"] = "Account Information";
    EMAIL_SUBJECT["reset_password"] = "Reset Password";
    EMAIL_SUBJECT["send_otp"] = "Send OTP";
})(EMAIL_SUBJECT = exports.EMAIL_SUBJECT || (exports.EMAIL_SUBJECT = {}));
//# sourceMappingURL=types.js.map