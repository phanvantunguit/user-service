import { SendGridTransport } from './sendgrid.transport';
import { IEmailDynamicTemplate } from './types';
export declare class MailService {
    private sendGridTransport;
    constructor(sendGridTransport: SendGridTransport);
    sendEmailConfirmAccount(email: string, token: string, password: string, from_email?: string, from_name?: string, logo_url?: string, header_url?: string, main_content?: string, twitter_url?: string, youtube_url?: string, facebook_url?: string, telegram_url?: string, company_name?: string, fullname?: string): Promise<any>;
    sendEmailCreateAccount(email: string, token: string, password: string, from_email?: string, from_name?: string): Promise<any>;
    sendEmailCreateMerchant(email: string, password: string): Promise<any>;
    sendEmailResetPassword(email: string, password: string): Promise<any>;
    sendEmailRemindEvent(email: string, code: string): Promise<any>;
    sendEmailDynamicTemplate(email: string, data: IEmailDynamicTemplate, template_id: string, send_at?: number, from_email?: string, from_name?: string): Promise<any>;
    sendRequestVerifySender(param: {
        nickname: string;
        from_email: string;
        from_name: string;
        reply_to: string;
        address: string;
        city: string;
        country: string;
    }): Promise<any>;
    sendVerifySender(param: {
        token: string;
    }): Promise<boolean>;
    updateSender(param: {
        id: string;
        nickname: string;
        from_name: string;
        reply_to: string;
        address: string;
        city: string;
        country: string;
    }): Promise<any>;
    deleteSender(param: {
        id: string;
    }): Promise<any>;
    sendEmailOTP(email: string, otp: string, wallet_address: string): Promise<any>;
}
