export interface ISendEmail {
    to: string;
    subject: string;
    html: string;
}
export declare enum EMAIL_SUBJECT {
    confirm_attend_to_event = "Th\u01B0 m\u1EDDi tham gia s\u1EF1 ki\u1EC7n",
    verify_email = "X\u00E1c nh\u1EADn email",
    remind_attend_to_event = "Th\u01B0 nh\u1EAFc tham gia s\u1EF1 ki\u1EC7n v\u00E0o ng\u00E0y mai",
    create_account = "Account Information",
    reset_password = "Reset Password",
    send_otp = "Send OTP"
}
export interface IEmailPaymentInfo {
    email: string;
    role_name: string;
    package_name: string;
    amount: string;
    currency: string;
    buy_amount: string;
    buy_currency: string;
    wallet_address?: string;
    time?: string;
    checkout_url?: string;
    qrcode_url?: string;
    status_url?: string;
    sell_amount_received?: string;
}
export interface IEmailDynamicTemplate {
    email: string;
    name: string;
    phone?: string;
    event_name?: string;
    event_code?: string;
    invite_code?: string;
    attendees_number?: number;
    start_at?: string;
    finish_at?: string;
    qrcode_url?: string;
    callback_confirm_url?: string;
    countdown?: string;
    count_user?: number;
}
export interface ISendDynamincTemplate {
    to: string;
    dynamic_template_data: any;
    template_id: string;
    send_at?: number;
    from_email?: string;
    from_name?: string;
}
