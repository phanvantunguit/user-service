"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailService = void 0;
const common_1 = require("@nestjs/common");
const handlebars = require("handlebars");
const config_1 = require("../../../config");
const sendgrid_transport_1 = require("./sendgrid.transport");
const types_1 = require("./types");
const types_2 = require("../../../domain/event/types");
const email_verify_account_create_1 = require("../../../templates/email-verify-account-create");
const email_remind_event_vip_1 = require("../../../templates/email-remind-event-vip");
const email_remind_event_1 = require("../../../templates/email-remind-event");
const email_send_merchant_password_1 = require("../../../templates/email-send-merchant-password");
const email_send_otp_wallet_1 = require("../../../templates/email-send-otp-wallet");
const email_verify_1 = require("../../../templates/email-verify");
let MailService = class MailService {
    constructor(sendGridTransport) {
        this.sendGridTransport = sendGridTransport;
    }
    async sendEmailConfirmAccount(email, token, password, from_email, from_name, logo_url, header_url, main_content, twitter_url, youtube_url, facebook_url, telegram_url, company_name, fullname) {
        const logoUrl = logo_url || 'https://static.cextrading.io/images/1/logo.png';
        const headerUrl = header_url || 'https://static.cextrading.io/images/1/header-bg.png';
        const companyName = company_name || 'Coinmap';
        const companyNameUpperCase = company_name || 'COINMAP';
        const mainContent = main_content ||
            `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.`;
        let footerContent;
        if (company_name) {
            footerContent = `
          ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.\n
          ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
          `;
        }
        else {
            footerContent =
                'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.';
        }
        const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
        const facebookUrl = facebook_url || 'https://www.facebook.com/CoinmapTrading';
        const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
        const telegramUrl = telegram_url || 'https://t.me/trading8x';
        const template = handlebars.compile(email_verify_1.emailVerify);
        const replacements = {
            linkVerify: `${config_1.APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/user/verify-email/${token}`,
            password,
            logoUrl,
            headerUrl,
            companyName,
            companyNameUpperCase: companyNameUpperCase.toUpperCase(),
            mainContent,
            footerContent,
            twitterUrl,
            facebookUrl,
            youtubeUrl,
            telegramUrl,
            fullname
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: types_1.EMAIL_SUBJECT.verify_email,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailCreateAccount(email, token, password, from_email, from_name) {
        const txtPassword = `Your password: ${password}`;
        const template = handlebars.compile(email_verify_account_create_1.emailVerifyAccountCreate);
        const replacements = {
            linkVerify: `${config_1.APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/user/verify-email/${token}`,
            password: txtPassword,
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: types_1.EMAIL_SUBJECT.verify_email,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail, from_email, from_name);
    }
    async sendEmailCreateMerchant(email, password) {
        const txtPassword = `Your password: ${password}`;
        const txtAccount = `Your account: ${email}`;
        const template = handlebars.compile(email_send_merchant_password_1.emailSendMerchantPassword);
        const replacements = {
            title: 'Account Information',
            account: txtAccount,
            password: txtPassword,
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: types_1.EMAIL_SUBJECT.create_account,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
    }
    async sendEmailResetPassword(email, password) {
        const txtPassword = `Your password: ${password}`;
        const txtAccount = `Your account: ${email}`;
        const template = handlebars.compile(email_send_merchant_password_1.emailSendMerchantPassword);
        const replacements = {
            title: 'Reset Password',
            account: txtAccount,
            password: txtPassword,
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: types_1.EMAIL_SUBJECT.reset_password,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
    }
    async sendEmailRemindEvent(email, code) {
        const vip = code.includes(types_2.INVITE_CODE_TYPE.VIP);
        const template = vip
            ? handlebars.compile(email_remind_event_vip_1.emailRemindEventVip)
            : handlebars.compile(email_remind_event_1.emailRemindEvent);
        const html = template({});
        const dataSendEmail = {
            to: email,
            subject: types_1.EMAIL_SUBJECT.remind_attend_to_event,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
    }
    async sendEmailDynamicTemplate(email, data, template_id, send_at, from_email, from_name) {
        return this.sendGridTransport.sendGridSendDynamicTemplate({
            to: email,
            dynamic_template_data: data,
            template_id,
            send_at,
            from_email,
            from_name,
        });
    }
    async sendRequestVerifySender(param) {
        return this.sendGridTransport.sendRequestVerifySender(param);
    }
    async sendVerifySender(param) {
        return this.sendGridTransport.sendVerifySender(param);
    }
    async updateSender(param) {
        return this.sendGridTransport.updateSender(param);
    }
    async deleteSender(param) {
        return this.sendGridTransport.deleteSender(param);
    }
    async sendEmailOTP(email, otp, wallet_address) {
        const txtOTP = `Your OTP: ${otp}`;
        const txtWalletAddress = `Your Wallet: ${wallet_address}`;
        const template = handlebars.compile(email_send_otp_wallet_1.emailSendOTPWallet);
        const replacements = {
            otp: txtOTP,
            wallet_address: txtWalletAddress,
        };
        const html = template(replacements);
        const dataSendEmail = {
            to: email,
            subject: types_1.EMAIL_SUBJECT.send_otp,
            html,
        };
        return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
    }
};
MailService = __decorate([
    __param(0, (0, common_1.Inject)(sendgrid_transport_1.SendGridTransport)),
    __metadata("design:paramtypes", [sendgrid_transport_1.SendGridTransport])
], MailService);
exports.MailService = MailService;
//# sourceMappingURL=index.js.map