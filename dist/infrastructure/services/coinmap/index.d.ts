import { ITEM_STATUS } from 'src/domain/transaction/types';
export declare class CoinmapService {
    updateUserBotTradingStatus(param: {
        id: string;
        status: ITEM_STATUS;
        merchant_code: string;
        password: string;
    }): Promise<{
        status: boolean;
        response: any;
    }>;
    deleteUserBotTrading(param: {
        id: string;
        merchant_code: string;
        password: string;
    }): Promise<{
        status: boolean;
        response: any;
    }>;
}
