"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoinmapService = void 0;
const config_1 = require("../../../config");
const hash_util_1 = require("../../../utils/hash.util");
const http_transport_util_1 = require("../../../utils/http-transport.util");
class CoinmapService {
    async updateUserBotTradingStatus(param) {
        const { id, status, merchant_code, password } = param;
        try {
            const data = {
                status,
            };
            const headers = {
                'Content-Type': 'application/json',
                Authorization: `Basic ${(0, hash_util_1.encodeBase64)(`${merchant_code}:${password}`)}`,
            };
            const url = `${config_1.APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/admin/user/tbot/${id}`;
            const result = await (0, http_transport_util_1.requestPUT)(url, data, headers);
            return { status: true, response: result };
        }
        catch (error) {
            console.log("error", error);
            return { status: false, response: error };
        }
    }
    async deleteUserBotTrading(param) {
        const { id, merchant_code, password } = param;
        try {
            const headers = {
                'Content-Type': 'application/json',
                Authorization: `Basic ${(0, hash_util_1.encodeBase64)(`${merchant_code}:${password}`)}`,
            };
            const url = `${config_1.APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/admin/user/tbot/${id}`;
            const result = await (0, http_transport_util_1.requestDELETE)(url, headers);
            return { status: true, response: result };
        }
        catch (error) {
            console.log("error", error);
            return { status: false, response: error };
        }
    }
}
exports.CoinmapService = CoinmapService;
//# sourceMappingURL=index.js.map