export declare enum ENVIRONMENT {
    DEVELOPMENT = "DEVELOPMENT",
    STAGING = "STAGING",
    PRODUCTION = "PRODUCTION"
}
export interface Iconfig {
    PORT: number;
    BASE_URL: string;
    ENVIRONMENT: ENVIRONMENT;
    POSTGRES_HOST: string;
    POSTGRES_PORT: number;
    POSTGRES_USER: string;
    POSTGRES_PASS: string;
    POSTGRES_DB: string;
    PAYMENT_POSTGRES_HOST: string;
    PAYMENT_POSTGRES_PORT: number;
    PAYMENT_POSTGRES_USER: string;
    PAYMENT_POSTGRES_PASS: string;
    PAYMENT_POSTGRES_DB: string;
    UR_POSTGRES_HOST: string;
    UR_POSTGRES_PORT: number;
    UR_POSTGRES_USER: string;
    UR_POSTGRES_PASS: string;
    UR_POSTGRES_DB: string;
    SENDGRID_BASE_URL: string;
    SENDGRID_API_TOKEN: string;
    SENDGRID_SENDER_EMAIL: string;
    SENDGRID_SENDER_NAME: string;
    SENDGRID_SENDER_ALGO_EMAIL: string;
    SENDGRID_SENDER_ALGO_NAME: string;
    INVITE_EVENT_SIMPLE_TEMPLATE_ID: string;
    INVITE_EVENT_QRCODE_TEMPLATE_ID: string;
    REMIND_EVENT_TEMPLATE_ID: string;
    PAYMENT_EVENT_TEMPLATE_ID: string;
    REMIND_PAYMENT_TEMPLATE_ID: string;
    PROMOTION_TEMPLATE_ID: string;
    GOOGLE_CLOUD_PROJECT_ID: string;
    GOOGLE_CLOUD_STORAGE_BUCKET: string;
    SECRET_KEY: string;
    TIME_EXPIRED_LOGIN: number;
    DASHBOARD_ADMIN_BASE_URL: string;
    PRIVATE_KEY: string;
    CLIENT_EMAIL: string;
    VERSION: string;
    USER_ROLE_BASE_URL: string;
    MIN_INVOICE_AMOUNT: number;
    ALGO_LOGO_EMAIL: string;
    ALGO_BANNER_EMAIL: string;
}
