"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertEvent1664420722422 = void 0;
const types_1 = require("../domain/event/types");
class InsertEvent1664420722422 {
    async up(queryRunner) {
        const queryString = `
        INSERT INTO events ("name", code, status, start_at, finish_at, created_at, updated_at, deleted_at, attendees_number)
            VALUES ('Coinmap vs Axi', 'coinmapaxi', '${types_1.EVENT_STATUS.OPEN_REGISTRATION}', 1665766800000, null, ${Date.now()},${Date.now()}, null, 1000)
        `;
        await queryRunner.query(queryString);
    }
    down(queryRunner) {
        return;
    }
}
exports.InsertEvent1664420722422 = InsertEvent1664420722422;
//# sourceMappingURL=insert-event-1664420722422.js.map