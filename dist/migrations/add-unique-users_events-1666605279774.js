"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddUniqueUsersEvents1666605279774 = void 0;
class AddUniqueUsersEvents1666605279774 {
    async up(queryRunner) {
        const queryString = `
    DELETE FROM
    users_events a
        USING users_events b
    WHERE
        a.id < b.id
        AND
        a.event_id = b.event_id
        AND (a.invite_code = b.invite_code or a.email = b.email)`;
        const addConstraint = `
    ALTER TABLE users_events ADD CONSTRAINT event_id_invite_code_un UNIQUE (event_id, invite_code);
    ALTER TABLE users_events ADD CONSTRAINT event_id_email_un UNIQUE (event_id, email);
    `;
        await queryRunner.query(queryString);
        await queryRunner.query(addConstraint);
    }
    down(queryRunner) {
        return;
    }
}
exports.AddUniqueUsersEvents1666605279774 = AddUniqueUsersEvents1666605279774;
//# sourceMappingURL=add-unique-users_events-1666605279774.js.map