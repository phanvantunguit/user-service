"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertAdmin1664523922143 = void 0;
const hash_util_1 = require("../utils/hash.util");
class InsertAdmin1664523922143 {
    async up(queryRunner) {
        const password = await (0, hash_util_1.hashPassword)('123456Aa');
        const queryString = `
    INSERT INTO admins
    (email, phone, fullname, super_admin, email_confirmed, "password", created_at, updated_at, deleted_at)
    VALUES
    ('nguyentoan@coinmap.tech', null, 'Nguyen Toan', true, true, '${password}', 1664519091464, 1664519091464, null),
    ('trongnguyen@coinmap.tech', null, 'Nguyen Trong', true, true, '${password}', 1664519091465, 1664519091465, null);`;
        await queryRunner.query(queryString);
    }
    down(queryRunner) {
        return;
    }
}
exports.InsertAdmin1664523922143 = InsertAdmin1664523922143;
//# sourceMappingURL=insert-admin-1664523922143.js.map