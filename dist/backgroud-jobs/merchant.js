"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantJob = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const application_1 = require("../application");
const types_1 = require("../domain/merchant/types");
const merchant_repository_1 = require("../infrastructure/data/database/merchant.repository");
const _1 = require(".");
let MerchantJob = class MerchantJob {
    constructor(backgroudJob, createMerchantInvoiceHandler, merchantRepository) {
        this.backgroudJob = backgroudJob;
        this.createMerchantInvoiceHandler = createMerchantInvoiceHandler;
        this.merchantRepository = merchantRepository;
    }
    run() {
        this.backgroudJob.addNewJob(this.createMerchantInvoice.name, this.createMerchantInvoice.bind(this), schedule_1.CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_NOON, this.createMerchantInvoice.bind(this));
    }
    async createMerchantInvoice() {
        const merchants = await this.merchantRepository.find({
            status: types_1.MERCHANT_STATUS.ACTIVE,
        });
        for (let m of merchants) {
            const invoices = await this.createMerchantInvoiceHandler.execute(m.id, m.code);
            if (invoices) {
                console.log('MerchantJob createMerchantInvoice:', invoices.length);
            }
        }
    }
};
MerchantJob = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, common_1.Inject)(application_1.CreateMerchantInvoiceHandler)),
    __param(2, (0, common_1.Inject)(merchant_repository_1.MerchantRepository)),
    __metadata("design:paramtypes", [_1.BackgroudJob,
        application_1.CreateMerchantInvoiceHandler,
        merchant_repository_1.MerchantRepository])
], MerchantJob);
exports.MerchantJob = MerchantJob;
//# sourceMappingURL=merchant.js.map