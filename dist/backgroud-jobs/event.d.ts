import { RemindAllAttendEventHandler, RemindDynamicHandler } from 'src/application';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { BackgroudJob } from '.';
export declare class EventJob {
    private backgroudJob;
    private eventRepository;
    private remindAllAttendEventHandler;
    private userEventRepository;
    private remindDynamicHandler;
    constructor(backgroudJob: BackgroudJob, eventRepository: EventRepository, remindAllAttendEventHandler: RemindAllAttendEventHandler, userEventRepository: UserEventRepository, remindDynamicHandler: RemindDynamicHandler);
    run(): void;
    remindAttendEvent(): Promise<void>;
    remindPaymentEventFisrt(): Promise<void>;
    remindPaymentEvent(): Promise<void>;
}
