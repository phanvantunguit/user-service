import { CreateMerchantInvoiceHandler } from 'src/application';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { BackgroudJob } from '.';
export declare class MerchantJob {
    private backgroudJob;
    private createMerchantInvoiceHandler;
    private merchantRepository;
    constructor(backgroudJob: BackgroudJob, createMerchantInvoiceHandler: CreateMerchantInvoiceHandler, merchantRepository: MerchantRepository);
    run(): void;
    createMerchantInvoice(): Promise<void>;
}
