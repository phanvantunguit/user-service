"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var BackgroudJob_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackgroudJob = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const cron_1 = require("cron");
let BackgroudJob = BackgroudJob_1 = class BackgroudJob {
    constructor(schedulerRegistry) {
        this.schedulerRegistry = schedulerRegistry;
        this.logger = new common_1.Logger(BackgroudJob_1.name);
    }
    stopCronJob(name) {
        const job = this.schedulerRegistry.getCronJob(name);
        if (job) {
            job.stop();
        }
    }
    startCronJob(name) {
        const job = this.schedulerRegistry.getCronJob(name);
        if (!job.running) {
            job.start();
        }
    }
    deleteCronJob(name) {
        this.schedulerRegistry.deleteCronJob(name);
    }
    addNewJob(name, taks, time, preTask) {
        this.logger.log(`${name} was Scheduled`);
        if (preTask) {
            this.logger.log(`${preTask.name} is runing...`);
            preTask();
        }
        const job = new cron_1.CronJob(time, () => {
            this.logger.log(`${taks.name} is runing...`);
            taks();
        });
        this.schedulerRegistry.addCronJob(name, job);
        job.start();
    }
};
BackgroudJob = BackgroudJob_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [schedule_1.SchedulerRegistry])
], BackgroudJob);
exports.BackgroudJob = BackgroudJob;
//# sourceMappingURL=index.js.map