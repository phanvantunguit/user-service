import { Logger } from '@nestjs/common';
import { CronExpression, SchedulerRegistry } from '@nestjs/schedule';
export declare class BackgroudJob {
    private schedulerRegistry;
    readonly logger: Logger;
    constructor(schedulerRegistry: SchedulerRegistry);
    stopCronJob(name: string): void;
    startCronJob(name: string): void;
    deleteCronJob(name: string): void;
    addNewJob(name: string, taks: any, time: CronExpression, preTask?: any): void;
}
