"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventJob = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const application_1 = require("../application");
const config_1 = require("../config");
const types_1 = require("../domain/event/types");
const event_repository_1 = require("../infrastructure/data/database/event.repository");
const user_event_repository_1 = require("../infrastructure/data/database/user-event.repository");
const _1 = require(".");
let EventJob = class EventJob {
    constructor(backgroudJob, eventRepository, remindAllAttendEventHandler, userEventRepository, remindDynamicHandler) {
        this.backgroudJob = backgroudJob;
        this.eventRepository = eventRepository;
        this.remindAllAttendEventHandler = remindAllAttendEventHandler;
        this.userEventRepository = userEventRepository;
        this.remindDynamicHandler = remindDynamicHandler;
    }
    run() {
        this.backgroudJob.addNewJob(this.remindAttendEvent.name, this.remindAttendEvent.bind(this), schedule_1.CronExpression.EVERY_5_MINUTES, this.remindAttendEvent.bind(this));
    }
    async remindAttendEvent() {
        const to = new Date();
        const events = await this.eventRepository.getEventToRemind({
            remind: '0',
            to: to.valueOf(),
        });
        this.backgroudJob.logger.log(`${this.remindAttendEvent.name} events number: ${events.length}`);
        for (const event of events) {
            const result = await this.remindAllAttendEventHandler.execute({
                event_id: event.id,
            });
            await this.eventRepository.save({ id: event.id, remind: true });
            this.backgroudJob.logger.log(`${this.remindAttendEvent.name}: ${event.name} was finished ${result.payload}`);
        }
        this.backgroudJob.logger.log(`${this.remindAttendEvent.name} was finished`);
    }
    async remindPaymentEventFisrt() {
        const event = (await this.eventRepository.findOne({
            code: 'COINMAP_CHARITY_HEATMAP',
        }));
        if (event) {
            const to = new Date();
            const users = (await this.userEventRepository.getUserByDate({
                to: to.setDate(to.getDate() - 1),
                event_id: event.id,
            }));
            for (const user of users) {
                if (user.payment_status !== types_1.PAYMENT_STATUS.COMPLETED) {
                    await this.remindDynamicHandler.execute({
                        user,
                        event,
                        template_id: config_1.APP_CONFIG.REMIND_PAYMENT_TEMPLATE_ID,
                    });
                }
            }
            this.backgroudJob.logger.log(`${this.remindPaymentEvent.name} was finished`);
        }
    }
    async remindPaymentEvent() {
        const event = (await this.eventRepository.findOne({
            code: 'COINMAP_CHARITY_HEATMAP',
        }));
        if (event) {
            const to = new Date();
            const from = new Date();
            const users = (await this.userEventRepository.getUserByDate({
                from: from.setDate(from.getDate() - 2),
                to: to.setDate(to.getDate() - 1),
                event_id: event.id,
            }));
            for (const user of users) {
                if (user.payment_status !== types_1.PAYMENT_STATUS.COMPLETED) {
                    await this.remindDynamicHandler.execute({
                        user,
                        event,
                        template_id: config_1.APP_CONFIG.REMIND_PAYMENT_TEMPLATE_ID,
                    });
                }
            }
            this.backgroudJob.logger.log(`${this.remindPaymentEvent.name} was finished`);
        }
    }
};
EventJob = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, common_1.Inject)(event_repository_1.EventRepository)),
    __param(2, (0, common_1.Inject)(application_1.RemindAllAttendEventHandler)),
    __param(3, (0, common_1.Inject)(user_event_repository_1.UserEventRepository)),
    __param(4, (0, common_1.Inject)(application_1.RemindDynamicHandler)),
    __metadata("design:paramtypes", [_1.BackgroudJob,
        event_repository_1.EventRepository,
        application_1.RemindAllAttendEventHandler,
        user_event_repository_1.UserEventRepository,
        application_1.RemindDynamicHandler])
], EventJob);
exports.EventJob = EventJob;
//# sourceMappingURL=event.js.map