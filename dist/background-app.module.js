"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackgroudAppModule = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const application_1 = require("./application");
const backgroud_jobs_1 = require("./backgroud-jobs");
const event_1 = require("./backgroud-jobs/event");
const merchant_1 = require("./backgroud-jobs/merchant");
const database_1 = require("./infrastructure/data/database");
const services_1 = require("./infrastructure/services");
let BackgroudAppModule = class BackgroudAppModule {
};
BackgroudAppModule = __decorate([
    (0, common_1.Module)({
        imports: [database_1.DatabaseModule],
        providers: [
            backgroud_jobs_1.BackgroudJob,
            event_1.EventJob,
            application_1.RemindAllAttendEventHandler,
            application_1.RemindAttendEventHandler,
            services_1.MailService,
            services_1.SendGridTransport,
            schedule_1.SchedulerRegistry,
            application_1.RemindDynamicHandler,
            merchant_1.MerchantJob,
            application_1.CreateMerchantInvoiceHandler,
        ],
    })
], BackgroudAppModule);
exports.BackgroudAppModule = BackgroudAppModule;
//# sourceMappingURL=background-app.module.js.map