"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emailConfirmEventVip = void 0;
exports.emailConfirmEventVip = `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>
  </title>
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if mso]>
    <noscript>
    <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    </noscript>
    <![endif]-->
  <!--[if lte mso 11]>
    <style type="text/css">
      .mj-outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
  <!--[if !mso]><!-->
  <link href="https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,500;0,700;1,400&display=swap" rel="stylesheet" type="text/css">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,500;0,700;1,400&display=swap);

  </style>
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }

      .mj-column-per-50 {
        width: 50% !important;
        max-width: 50%;
      }
    }

  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }

    .moz-text-html .mj-column-per-50 {
      width: 50% !important;
      max-width: 50%;
    }

  </style>
  <style type="text/css">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }

  </style>
  <style type="text/css">
    .footer-link:hover {
      background: linear-gradient(90deg, #1C53DF 0%, #3511C5 100%);
    }

    .body {
      background: url("https://static-dev.cextrading.io/1664592293766.png");
      background-repeat: no-repeat;
      background-size: cover;
    }

    .body-content {
      display: flex;
      flex-direction: column;
      align-items: center;
      min-height: 800px;
      justify-content: center;
    }

  </style>
</head>

<body style="word-spacing:normal;">
  <div style="">
    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="body-outlook" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div class="body" style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="body-content-outlook" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="body-content-outlook" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div class="body-content" style="margin:0px auto;max-width:600px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                  <tbody>
                    <tr>
                      <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                        <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:600px;" ><![endif]-->
                        <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                          <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:600px;" ><![endif]-->
                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;margin-top:135px;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                              <tbody>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:16px;font-weight:bold;line-height:24px;text-align:left;color:#9DB3FF;">Kính gửi: Anh/Chị {{name}},</div>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:16px;line-height:24px;text-align:left;color:#9DB3FF;">Xin chân thành cảm ơn anh/chị đã dành thời gian tham dự sự kiện <span style="font-weight: bold;">THE NEW TASTE OF TRADING của COINMAP</span>, thân mời anh/chị tham dự theo thông tin bên dưới :</div>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:16px;line-height:24px;text-align:left;color:#9DB3FF;">- Thời gian: 13h30 ngày 15 tháng 10 năm 2022 <br></br> - Địa điểm: Tầng 5 - CASTOR, GEM Center, số 8 Nguyễn Bỉnh Khiêm, Đa Kao, Quận 1, TP.HCM</div>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:16px;line-height:24px;text-align:left;color:#9DB3FF;">Xin vui lòng tham dự đúng giờ để tránh bỏ sót những thông tin quan trọng. Khi đi vui lòng mang theo điện thoại di động có kèm QR CODE bên dưới để check-in.</div>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="center" valign="middle" style="padding-bottom:5px;">
                                  <img src="{{qrcode_url}}"/>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:16px;line-height:24px;text-align:center;color:#9DB3FF;">Code: {{code}}</div>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="center" role="presentation" valign="middle">
                                    <a href={{linkApprove}} style="display:inline-block;background:linear-gradient(270deg, #5D3293 0%, #A23293 100%);color:#ffffff;font-family:Archivo;font-size:16px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:2px;" target="_blank">XÁC NHẬN</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                      <tbody>
                                        <tr>
                                          <td style="width:200px;">
                                            <img height="auto" src="https://event.coinmap.tech/images/partnership.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="200" />
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:16px;font-weight:bold;line-height:1;text-align:left;color:#9DB3FF;">Regards,<br></br> Coinmap Team.</div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </div>
                        <!--[if mso | IE]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#290D4B" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div style="background:#290D4B;background-color:#290D4B;margin:0px auto;max-width:600px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#290D4B;background-color:#290D4B;width:100%;">
                  <tbody>
                    <tr>
                      <td style="direction:ltr;font-size:0px;padding:24px;text-align:center;">
                        <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                        <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                            <tbody>
                              <tr>
                                <td style="vertical-align:top;padding-bottom:12px;">
                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                    <tbody>
                                      <tr>
                                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <div style="font-family:Archivo;font-size:24px;line-height:1;text-align:center;color:#FFFFFF;">In partnership and sponsored</div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                            <tbody>
                                              <tr>
                                                <td style="width:200px;">
                                                  <img height="auto" src="https://event.coinmap.tech/images/partnership.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="200" />
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]></td><td class="" style="width:552px;" ><![endif]-->
                        <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                          <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:276px;" ><![endif]-->
                          <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="vertical-align:top;padding:0px;padding-bottom:12px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0px;padding-right:6px;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:12px;line-height:1;text-align:left;color:#000000;"><a class="footer-link" href="https://t.me/coinmapcextrading" style="text-decoration: none; color: #B8C3F7; border:1px solid #5151E8; border-radius: 4px; padding: 12px; display: flex; align-items: center;" target="_blank">
                                                <svg width="17" style="margin-right: 12px;" height="15" viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                  <path d="M6.60281 13.2444C6.12242 13.2444 6.20851 13.0596 6.03604 12.6038L4.63159 7.9713L13.3543 2.52576L14.3768 2.79694L13.5268 5.11307L6.60281 13.2444Z" fill="#5F61C4" />
                                                  <path d="M6.60315 13.2443C6.97277 13.2443 7.13288 13.0719 7.34236 12.8747C7.66262 12.5668 11.7778 8.56271 11.7778 8.56271L9.25213 7.94659L6.91132 9.42506L6.60342 13.1212V13.2443H6.60315Z" fill="#26298A" />
                                                  <path d="M6.84921 9.4749L12.8121 13.8733C13.4897 14.2429 13.9827 14.0581 14.1552 13.2448L16.5823 1.81167C16.8288 0.813872 16.2004 0.370218 15.5475 0.665802L1.30523 6.16069C0.331838 6.55499 0.344169 7.09708 1.13276 7.33124L4.7919 8.4771L13.2558 3.14234C13.6501 2.89584 14.0197 3.03157 13.7239 3.30247L6.84921 9.4749Z" fill="#B8C3F7" />
                                                </svg>
                                                <mj-text> Coinmap CEX Trading</mj-text>
                                              </a></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td style="vertical-align:top;width:276px;" ><![endif]-->
                          <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="vertical-align:top;padding:0px;padding-bottom:12px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0px;padding-left:6px;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:13px;line-height:1;text-align:left;color:#000000;"><a class="footer-link" href="https://www.facebook.com/CoinmapTrading" style="text-decoration: none; color: #B8C3F7; border:1px solid #5151E8; border-radius: 4px; padding: 12px; display: flex; align-items: center;" target="_blank">
                                                <svg style="margin-right: 12px;" width="19" height="17" viewBox="0 0 19 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                  <path d="M9.88782 4.5842C9.88782 6.80953 11.7317 8.61332 14.0051 8.61332C16.2768 8.61332 18.1203 6.80912 18.1203 4.5842C18.1203 2.35969 16.2764 0.555908 14.0051 0.555908C11.7313 0.555908 9.88782 2.35969 9.88782 4.5842Z" fill="#B8C3F7" />
                                                  <path d="M4.73304 9.35758C6.77907 9.35758 8.43966 7.73391 8.43966 5.73302C8.43966 3.72964 6.77907 2.10681 4.73304 2.10681C2.68701 2.10681 1.03027 3.72964 1.03027 5.73302C1.03027 7.73391 2.68701 9.35758 4.73304 9.35758Z" fill="#B8C3F7" />
                                                  <path d="M8.43452 16.0748C8.63825 16.0748 8.80514 15.9113 8.80514 15.7108V14.3029C8.80514 12.2101 7.08417 10.1852 4.73337 10.1852C2.38587 10.1852 0.663208 12.2101 0.663208 14.3029V15.7108C0.663208 15.9108 0.828439 16.0748 1.03383 16.0748H1.16505H8.30532H8.43452Z" fill="#B8C3F7" />
                                                  <path d="M14.0052 9.53088C11.3948 9.53088 9.48218 11.7806 9.48218 14.1074V15.6715C9.48218 15.893 9.66569 16.0752 9.89344 16.0752H10.0382H17.9715H18.1165C18.3438 16.0752 18.5305 15.8934 18.5305 15.6715V14.1074C18.5301 11.7806 16.6157 9.53088 14.0052 9.53088Z" fill="#B8C3F7" />
                                                </svg>
                                                <mj-text> Fanpage Coinmap</mj-text>
                                              </a></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td style="vertical-align:top;width:276px;" ><![endif]-->
                          <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="vertical-align:top;padding:0px;padding-bottom:12px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0px;padding-right:6px;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:12px;line-height:1;text-align:left;color:#000000;"><a class="footer-link" href="https://twitter.com/CoinmapTrading" style="text-decoration: none; color: #B8C3F7; border:1px solid #5151E8; border-radius: 4px; padding: 12px; display: flex; align-items: center;" target="_blank">
                                                <svg style="margin-right: 12px;" width="18" height="15" viewBox="0 0 18 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                  <path d="M17.5969 1.96461C16.9728 2.24356 16.3038 2.42864 15.6033 2.50496C16.3231 2.06794 16.881 1.36643 17.1511 0.523206C16.4772 0.929415 15.7335 1.21926 14.9441 1.36928C14.324 0.656162 13.431 0.202077 12.4361 0.18691C10.5277 0.157996 8.95788 1.75676 8.9303 3.75747C8.92668 4.04186 8.9527 4.31868 9.00831 4.58482C6.13798 4.39048 3.61057 2.90927 1.93612 0.690292C1.63116 1.22116 1.45257 1.84185 1.44353 2.50472C1.42613 3.76222 2.02089 4.88082 2.93916 5.54465C2.37265 5.51692 1.84161 5.34652 1.37976 5.06758C1.37885 5.08251 1.37886 5.09792 1.37841 5.11285C1.35399 6.86874 2.52615 8.35113 4.10205 8.70781C3.81155 8.78625 3.50545 8.82584 3.19009 8.82062C2.96764 8.8173 2.75177 8.79171 2.54085 8.74644C2.96065 10.1928 4.22297 11.26 5.73488 11.3121C4.53855 12.2656 3.04022 12.823 1.42024 12.7983C1.14173 12.7941 0.866166 12.772 0.596924 12.7348C2.11178 13.7866 3.9205 14.4135 5.87234 14.4433C12.2304 14.54 15.7834 9.07208 15.8499 4.28289C15.8519 4.12576 15.8508 3.96911 15.8454 3.81436C16.529 3.31406 17.1233 2.6846 17.5969 1.96461Z" fill="#B8C3F7" />
                                                </svg>
                                                <mj-text> Twitter Coinmap</mj-text>
                                              </a></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td style="vertical-align:top;width:276px;" ><![endif]-->
                          <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="vertical-align:top;padding:0px;padding-bottom:12px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0px;padding-left:6px;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:12px;line-height:1;text-align:left;color:#000000;"><a class="footer-link" href="https://www.coinmap.tech/" style="text-decoration: none; color: #B8C3F7; border:1px solid #5151E8; border-radius: 4px; padding: 12px; display: flex; align-items: center;" target="_blank">
                                                <svg style="margin-right: 12px;" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                  <path d="M7.64233 0.315552H7.55043C3.70859 0.316001 0.596924 3.452 0.596924 7.31668C0.596924 8.2568 0.782934 9.15204 1.1161 9.96916V10.1362H1.18644C2.26442 12.5974 4.70762 14.3151 7.55086 14.3156H7.64277C10.486 14.3151 12.9288 12.5978 14.0072 10.1362H14.0773V9.96916C14.4109 9.15204 14.5969 8.2568 14.5969 7.31668C14.5965 3.45155 11.4842 0.316001 7.64233 0.315552ZM12.9577 4.70461H11.2055C10.9602 3.59567 10.5659 2.62861 10.0642 1.88558C11.3241 2.45531 12.3512 3.45739 12.9577 4.70461ZM10.4446 7.31668C10.4446 7.93894 10.3947 8.53697 10.3055 9.09324H8.0291V5.7498H10.3362C10.4058 6.24366 10.4446 6.76894 10.4446 7.31668ZM10.1352 4.70461H8.02953V1.43122C8.86533 1.72978 9.69944 2.95725 10.1352 4.70461ZM7.06436 1.47117V4.70461H5.05779C5.47659 3.02639 6.26245 1.8281 7.06436 1.47117ZM7.06436 5.74936V9.09277H4.88793C4.79873 8.53695 4.74877 7.93895 4.74877 7.31624C4.74877 6.7685 4.787 6.2432 4.85657 5.7489H7.06436V5.74936ZM5.12879 1.88558C4.62704 2.62861 4.23318 3.59567 3.98744 4.70461H2.23502C2.84202 3.45739 3.86928 2.45531 5.12879 1.88558ZM1.6344 7.31668C1.6344 6.77433 1.70786 6.24906 1.84389 5.74936H3.80778C3.74534 6.25175 3.70999 6.77747 3.70999 7.31668C3.70999 7.92996 3.75426 8.5235 3.83543 9.09324H1.90378C1.72895 8.53203 1.6344 7.93535 1.6344 7.31668ZM2.34172 10.1366H4.036C4.28353 11.1594 4.6596 12.0514 5.12879 12.7455C3.9402 12.2077 2.95854 11.286 2.34172 10.1366ZM5.11267 10.1366H7.06436V13.1599C6.29635 12.8178 5.54262 11.7039 5.11267 10.1366ZM8.02953 13.1999V10.1366H10.0807C9.63206 11.7713 8.83144 12.9134 8.02953 13.1999ZM10.0646 12.7455C10.5334 12.0514 10.9103 11.1594 11.1574 10.1366H12.8521C12.2344 11.286 11.2532 12.2077 10.0646 12.7455ZM13.2892 9.09324H11.3575C11.4392 8.5235 11.4834 7.92996 11.4834 7.31668C11.4834 6.77747 11.4481 6.25175 11.3852 5.74936H13.3493C13.4853 6.24861 13.559 6.77433 13.559 7.31668C13.5586 7.93535 13.4645 8.53203 13.2892 9.09324Z" fill="#B8C3F7" />
                                                </svg>
                                                <mj-text> Website Coinmap</mj-text>
                                              </a></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </div>
                        <!--[if mso | IE]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><![endif]-->
  </div>
</body>

</html>
`;
//# sourceMappingURL=email-confirm-event-vip.js.map