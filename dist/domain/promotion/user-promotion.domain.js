"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPromotionDomain = void 0;
class UserPromotionDomain {
    constructor(param) {
        this.template_id = param.template_id;
        this.email = param.email;
        this.send = param.send;
    }
}
exports.UserPromotionDomain = UserPromotionDomain;
//# sourceMappingURL=user-promotion.domain.js.map