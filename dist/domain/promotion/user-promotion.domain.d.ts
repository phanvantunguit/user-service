export declare class UserPromotionDomain {
    constructor(param: {
        template_id: string;
        email: string;
        send: boolean;
    });
    id?: string;
    template_id: string;
    email: string;
    send: boolean;
    created_at: number;
    updated_at: number;
}
