"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEventDomain = void 0;
const base_domain_1 = require("../base/base.domain");
class UserEventDomain extends base_domain_1.BaseDomain {
    setInviteCode(type, count, length) {
        count = count.toString();
        while (count.length < length)
            count = '0' + count;
        this.invite_code = `${type}${count}`;
    }
}
exports.UserEventDomain = UserEventDomain;
//# sourceMappingURL=user-event.domain.js.map