"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PAYMENT_STATUS = exports.EVENT_CONFIRM_STATUS = exports.EVENT_STATUS = exports.INVITE_CODE_TYPE = void 0;
var INVITE_CODE_TYPE;
(function (INVITE_CODE_TYPE) {
    INVITE_CODE_TYPE["VIP"] = "VIP";
    INVITE_CODE_TYPE["GUEST"] = "GUEST";
})(INVITE_CODE_TYPE = exports.INVITE_CODE_TYPE || (exports.INVITE_CODE_TYPE = {}));
var EVENT_STATUS;
(function (EVENT_STATUS) {
    EVENT_STATUS["CLOSE"] = "CLOSE";
    EVENT_STATUS["HAPPENING"] = "HAPPENING";
    EVENT_STATUS["CLOSE_REGISTRATION"] = "CLOSE_REGISTRATION";
    EVENT_STATUS["OPEN_REGISTRATION"] = "OPEN_REGISTRATION";
    EVENT_STATUS["COMINGSOON"] = "COMINGSOON";
})(EVENT_STATUS = exports.EVENT_STATUS || (exports.EVENT_STATUS = {}));
var EVENT_CONFIRM_STATUS;
(function (EVENT_CONFIRM_STATUS) {
    EVENT_CONFIRM_STATUS["WAITING"] = "WAITING";
    EVENT_CONFIRM_STATUS["APPROVED"] = "APPROVED";
    EVENT_CONFIRM_STATUS["REJECTED"] = "REJECTED";
})(EVENT_CONFIRM_STATUS = exports.EVENT_CONFIRM_STATUS || (exports.EVENT_CONFIRM_STATUS = {}));
var PAYMENT_STATUS;
(function (PAYMENT_STATUS) {
    PAYMENT_STATUS["WAITING"] = "WAITING";
    PAYMENT_STATUS["PROCESSING"] = "PROCESSING";
    PAYMENT_STATUS["COMPLETED"] = "COMPLETED";
})(PAYMENT_STATUS = exports.PAYMENT_STATUS || (exports.PAYMENT_STATUS = {}));
//# sourceMappingURL=types.js.map