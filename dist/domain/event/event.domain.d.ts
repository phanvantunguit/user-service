import { BaseDomain } from '../base/base.domain';
import { EVENT_STATUS } from './types';
export declare class EventDomain extends BaseDomain {
    name: string;
    code?: string;
    create_qrcode?: boolean;
    status: EVENT_STATUS;
    attendees_number?: number;
    remind?: boolean;
    email_remind_at?: number;
    email_remind_template_id?: string;
    email_confirm?: boolean;
    email_confirm_template_id?: string;
    start_at?: number;
    finish_at?: number;
    created_by?: string;
    updated_by?: string;
}
