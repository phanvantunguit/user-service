import { BaseDomain } from '../base/base.domain';
import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE, PAYMENT_STATUS } from './types';
export declare class UserEventDomain extends BaseDomain {
    created_by?: string;
    updated_by?: string;
    email: string;
    phone?: string;
    telegram?: string;
    fullname: string;
    confirm_status: EVENT_CONFIRM_STATUS;
    attend: boolean;
    remind: boolean;
    event_id: string;
    invite_code: string;
    payment_status: PAYMENT_STATUS;
    setInviteCode(type: INVITE_CODE_TYPE, count: number | string, length: number): void;
}
