"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PACKAGE_TYPE = exports.MERCHANT_COMMISION_STATUS = void 0;
var MERCHANT_COMMISION_STATUS;
(function (MERCHANT_COMMISION_STATUS) {
    MERCHANT_COMMISION_STATUS["ACTIVE"] = "ACTIVE";
    MERCHANT_COMMISION_STATUS["INACTIVE"] = "INACTIVE";
})(MERCHANT_COMMISION_STATUS = exports.MERCHANT_COMMISION_STATUS || (exports.MERCHANT_COMMISION_STATUS = {}));
var PACKAGE_TYPE;
(function (PACKAGE_TYPE) {
    PACKAGE_TYPE["DAY"] = "DAY";
    PACKAGE_TYPE["MONTH"] = "MONTH";
})(PACKAGE_TYPE = exports.PACKAGE_TYPE || (exports.PACKAGE_TYPE = {}));
//# sourceMappingURL=types.js.map