export declare enum MERCHANT_COMMISION_STATUS {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
export declare enum PACKAGE_TYPE {
    DAY = "DAY",
    MONTH = "MONTH"
}
