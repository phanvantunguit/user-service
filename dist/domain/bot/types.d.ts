export declare enum BOT_STATUS {
    CLOSE = "CLOSE",
    OPEN = "OPEN",
    COMINGSOON = "COMINGSOON"
}
export declare enum USER_ASSET_TYPE {
    EXPIRED = "EXPIRED",
    DELETED = "DELETED",
    USING = "USING"
}
export declare enum TBOT_TYPE {
    SELECT = "SELECT",
    BUY = "BUY",
    ASSIGN = "ASSIGN"
}
