"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TBOT_TYPE = exports.USER_ASSET_TYPE = exports.BOT_STATUS = void 0;
var BOT_STATUS;
(function (BOT_STATUS) {
    BOT_STATUS["CLOSE"] = "CLOSE";
    BOT_STATUS["OPEN"] = "OPEN";
    BOT_STATUS["COMINGSOON"] = "COMINGSOON";
})(BOT_STATUS = exports.BOT_STATUS || (exports.BOT_STATUS = {}));
var USER_ASSET_TYPE;
(function (USER_ASSET_TYPE) {
    USER_ASSET_TYPE["EXPIRED"] = "EXPIRED";
    USER_ASSET_TYPE["DELETED"] = "DELETED";
    USER_ASSET_TYPE["USING"] = "USING";
})(USER_ASSET_TYPE = exports.USER_ASSET_TYPE || (exports.USER_ASSET_TYPE = {}));
var TBOT_TYPE;
(function (TBOT_TYPE) {
    TBOT_TYPE["SELECT"] = "SELECT";
    TBOT_TYPE["BUY"] = "BUY";
    TBOT_TYPE["ASSIGN"] = "ASSIGN";
})(TBOT_TYPE = exports.TBOT_TYPE || (exports.TBOT_TYPE = {}));
//# sourceMappingURL=types.js.map