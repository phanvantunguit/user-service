export declare enum PAYMENT_METHOD {
    COIN_PAYMENT = "COIN_PAYMENT"
}
export declare enum TRANSACTION_STATUS {
    CREATED = "CREATED",
    PROCESSING = "PROCESSING",
    FAILED = "FAILED",
    TIMEOUT = "TIMEOUT",
    COMPLETE = "COMPLETE"
}
export declare enum TRANSACTION_EVENT {
    PAYMENT_INIT = "PAYMENT_INIT",
    PAYMENT_CREATED = "PAYMENT_CREATED",
    PAYMENT_PROCESSING = "PAYMENT_PROCESSING",
    PAYMENT_COMPLETE = "PAYMENT_COMPLETE",
    PAYMENT_FAILED = "PAYMENT_FAILED",
    CALLBACK_SUCCEED = "CALLBACK_SUCCEED",
    CALLBACK_FAILED = "CALLBACK_FAILED"
}
export declare enum METADATA_STRING_OBJECT {
    items = "items"
}
export declare enum COIN_PAYMENT_METADATA {
    wallet_address = "wallet_address",
    timeout = "timeout",
    qrcode_url = "qrcode_url",
    status_url = "status_url",
    checkout_url = "checkout_url"
}
export declare type TRANSACTION_METADATA = COIN_PAYMENT_METADATA | METADATA_STRING_OBJECT;
export declare enum ORDER_CATEGORY {
    PKG = "PKG",
    SBOT = "SBOT",
    TBOT = "TBOT"
}
export declare enum ITEM_STATUS {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE",
    INACTIVE_BY_SYSTEM = "INACTIVE_BY_SYSTEM",
    PROCESSING = "PROCESSING",
    NOT_CONNECT = "NOT_CONNECT",
    CONNECTING = "CONNECTING",
    DISCONNECTED = "DISCONNECTED",
    EXPIRED = "EXPIRED",
    DELETED = "DELETED",
    FAILED = "FAILED",
    STOP_OUT = "STOP_OUT"
}
