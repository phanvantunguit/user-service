import { TRANSACTION_EVENT, TRANSACTION_STATUS } from './types';
export declare class TransactionLogDomain {
    constructor(param: {
        transaction_id: string;
        transaction_event: TRANSACTION_EVENT;
        transaction_status: TRANSACTION_STATUS;
        metadata: any;
    });
    id?: string;
    transaction_id?: string;
    transaction_event?: TRANSACTION_EVENT;
    transaction_status?: TRANSACTION_STATUS;
    metadata?: any;
    created_at?: number;
}
