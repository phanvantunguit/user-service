import { TRANSACTION_METADATA } from './types';
export declare class TransactionMetadataDomain {
    constructor(param: {
        transaction_id: string;
        attribute: TRANSACTION_METADATA;
        value: string;
    });
    id?: string;
    transaction_id?: string;
    attribute: TRANSACTION_METADATA;
    value?: string;
    created_at?: number;
}
