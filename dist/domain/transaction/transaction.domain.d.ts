import { BaseDomain } from '../base/base.domain';
import { PAYMENT_METHOD, TRANSACTION_STATUS } from './types';
export declare class TransactionDomain extends BaseDomain {
    constructor(param: {
        id?: string;
        order_id: string;
        order_type?: string;
        amount: string;
        currency: string;
        payment_method: PAYMENT_METHOD;
        integrate_service: string;
        status: TRANSACTION_STATUS;
        ipn_url?: string;
        user_id?: string;
        email?: string;
        fullname?: string;
        merchant_code?: string;
    });
    id: string;
    order_id: string;
    order_type?: string;
    amount: string;
    currency: string;
    payment_method: PAYMENT_METHOD;
    integrate_service: string;
    ipn_url?: string;
    payment_id?: string;
    status: TRANSACTION_STATUS;
    user_id?: string;
    email?: string;
    fullname?: string;
    merchant_code?: string;
}
