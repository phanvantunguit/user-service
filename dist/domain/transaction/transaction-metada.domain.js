"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionMetadataDomain = void 0;
class TransactionMetadataDomain {
    constructor(param) {
        this.transaction_id = param.transaction_id;
        this.attribute = param.attribute;
        this.value = param.value;
    }
}
exports.TransactionMetadataDomain = TransactionMetadataDomain;
//# sourceMappingURL=transaction-metada.domain.js.map