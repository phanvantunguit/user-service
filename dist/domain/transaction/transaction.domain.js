"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionDomain = void 0;
const base_domain_1 = require("../base/base.domain");
class TransactionDomain extends base_domain_1.BaseDomain {
    constructor(param) {
        super();
        this.id = param.id;
        this.order_id = param.order_id;
        this.order_type = param.order_type;
        this.amount = param.amount;
        this.currency = param.currency;
        this.payment_method = param.payment_method;
        this.integrate_service = param.integrate_service;
        this.status = param.status;
        this.ipn_url = param.ipn_url;
        this.user_id = param.user_id;
        this.email = param.email;
        this.fullname = param.fullname;
        this.merchant_code = param.merchant_code;
    }
}
exports.TransactionDomain = TransactionDomain;
//# sourceMappingURL=transaction.domain.js.map