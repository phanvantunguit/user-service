"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogDomain = void 0;
class TransactionLogDomain {
    constructor(param) {
        this.transaction_id = param.transaction_id;
        this.transaction_event = param.transaction_event;
        this.transaction_status = param.transaction_status;
        this.metadata = param.metadata;
    }
}
exports.TransactionLogDomain = TransactionLogDomain;
//# sourceMappingURL=transaction-log.domain.js.map