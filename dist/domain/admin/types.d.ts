export declare enum ADMIN_STATUS {
    ACTIVATE = "ACTIVATE",
    DEACTIVATE = "DEACTIVATE"
}
