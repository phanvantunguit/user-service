"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADMIN_STATUS = void 0;
var ADMIN_STATUS;
(function (ADMIN_STATUS) {
    ADMIN_STATUS["ACTIVATE"] = "ACTIVATE";
    ADMIN_STATUS["DEACTIVATE"] = "DEACTIVATE";
})(ADMIN_STATUS = exports.ADMIN_STATUS || (exports.ADMIN_STATUS = {}));
//# sourceMappingURL=types.js.map