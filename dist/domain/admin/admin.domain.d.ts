import { BaseDomain } from '../base/base.domain';
import { ADMIN_STATUS } from './types';
export declare class AdminDomain extends BaseDomain {
    email: string;
    phone: string;
    fullname: string;
    super_admin: boolean;
    email_confirmed: boolean;
    status: ADMIN_STATUS;
    password: string;
    created_by?: string;
    updated_by?: string;
}
