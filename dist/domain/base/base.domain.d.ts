export declare abstract class BaseDomain {
    id: string;
    created_at: number;
    updated_at: number;
    deleted_at?: number;
}
