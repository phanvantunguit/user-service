import { BaseDomain } from '../base/base.domain';
import { MERCHANT_STATUS } from './types';
export declare class MerchantDomain extends BaseDomain {
    constructor(param: {
        id?: string;
        name: string;
        code: string;
        status: MERCHANT_STATUS;
        description?: string;
        email: string;
        password?: string;
        domain?: string;
        created_by: string;
        updated_by: string;
        created_at?: number;
        updated_at?: number;
        config?: any;
    });
    id: string;
    name: string;
    code: string;
    status: MERCHANT_STATUS;
    description: string;
    email: string;
    password: string;
    config: any;
    domain: string;
    created_by: string;
    updated_by: string;
    created_at: number;
    updated_at: number;
}
