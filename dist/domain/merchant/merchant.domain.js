"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantDomain = void 0;
const base_domain_1 = require("../base/base.domain");
class MerchantDomain extends base_domain_1.BaseDomain {
    constructor(param) {
        super();
        this.id = param.id;
        this.name = param.name;
        this.code = param.code;
        this.status = param.status;
        this.description = param.description;
        this.email = param.email;
        this.password = param.password;
        this.created_by = param.created_by;
        this.updated_by = param.updated_by;
        this.created_at = param.created_at;
        this.created_at = param.created_at;
        this.config = param.config;
        this.domain = param.domain;
    }
}
exports.MerchantDomain = MerchantDomain;
//# sourceMappingURL=merchant.domain.js.map