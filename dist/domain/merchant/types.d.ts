export declare enum MERCHANT_STATUS {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
export declare enum MERCHANT_TYPE {
    COINMAP = "COINMAP",
    OTHERS = "OTHERS"
}
export declare const MERCHANT_CONFIG_ONLY_ADMIN: string[];
export declare const MERCHANT_CONFIG_ONLY_SYSTEM: string[];
export declare enum MERCHANT_INVOICE_STATUS {
    COMPLETED = "COMPLETED",
    WAITING_PAYMENT = "WAITING_PAYMENT",
    WALLET_INVALID = "WALLET_INVALID"
}
export declare enum MERCHANT_WALLET_TYPE {
    TRC20 = "TRC20",
    BSC20 = "BSC20"
}
