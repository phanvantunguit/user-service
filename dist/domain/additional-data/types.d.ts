export declare enum MERCHANT_ADDITIONAL_DATA_STATUS {
    ON = "ON",
    OFF = "OFF"
}
export declare enum ADDITIONAL_DATA_TYPE {
    FAQ = "FAQ",
    PKG_PERIOD = "PKG_PERIOD",
    TBOT_PERIOD = "TBOT_PERIOD",
    SBOT_PERIOD = "SBOT_PERIOD",
    TBOT_FEE = "TBOT_FEE"
}
