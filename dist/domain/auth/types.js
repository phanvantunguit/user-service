"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordRegex = exports.WITHOUT_AUTHORIZATION = void 0;
exports.WITHOUT_AUTHORIZATION = 'WITHOUT_AUTHORIZATION';
exports.PasswordRegex = `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*(),.~/?=|;:'"{}<>]{8,}$`;
//# sourceMappingURL=types.js.map