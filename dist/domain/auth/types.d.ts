export declare const WITHOUT_AUTHORIZATION = "WITHOUT_AUTHORIZATION";
export declare const PasswordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*(),.~/?=|;:'\"{}<>]{8,}$";
