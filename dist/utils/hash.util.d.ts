export declare function hashPassword(password: string): Promise<string>;
export declare function comparePassword(password: string, hash: string): Promise<boolean>;
export declare function generateToken(data: any, private_key: string, expires_in: number): string;
export declare function verifyToken(token: string, public_key: string): Promise<any>;
export declare function generateTokenSecret(data: any, secret_key: string, expires_in: number): string;
export declare function verifyTokenSecret(token: string, secret_key: string): Promise<any>;
export declare function decodeBase64(encrypt_data: string): string;
export declare function encodeBase64(decrypt_data: string): string;
export declare function createChecksum(data: string, secret_key: string): string;
export declare function validateChecksum(data: string, checksum: string, secret_key: string): boolean;
export declare function tripleDESEncrypt(data: string, secret_key: string, output_type: 'base64' | 'hex'): string | false;
export declare function tripleDESDecrypt(data: string, secret_key: string, input_type: 'base64' | 'hex'): string | false;
export declare function createChecksumSHA512HMAC(data: string, key: string, output: 'hex' | 'base64'): string;
export declare function generatePassword(): any;
