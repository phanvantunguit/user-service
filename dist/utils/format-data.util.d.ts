export declare function cleanObject(obj: object): {};
export declare function generatePassword(): any;
export declare function formatRequestDataToString(params: object): string;
export declare function formatRequestDataToStringV2(params: object): string;
export declare function formatQueryArray(params: object): string[];
