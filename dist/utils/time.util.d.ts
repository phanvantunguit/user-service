export declare const SECOND = 1000;
export declare const MINUTE: number;
export declare const HOUR: number;
export declare const DAY: number;
export declare function setBeginDate(time?: number): number;
export declare function setEndDate(time?: number): number;
