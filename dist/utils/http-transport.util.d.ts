export declare function requestPOST(url: string, data: any, headers: any): Promise<any>;
export declare function requestPUT(url: string, data: any, headers: any): Promise<any>;
export declare function requestPATCH(url: string, data: any, headers: any): Promise<any>;
export declare function requestGET(url: string, headers: any): Promise<any>;
export declare function requestDELETE(url: string, headers: any): Promise<any>;
