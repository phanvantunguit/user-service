"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generatePassword = exports.createChecksumSHA512HMAC = exports.tripleDESDecrypt = exports.tripleDESEncrypt = exports.validateChecksum = exports.createChecksum = exports.encodeBase64 = exports.decodeBase64 = exports.verifyTokenSecret = exports.generateTokenSecret = exports.verifyToken = exports.generateToken = exports.comparePassword = exports.hashPassword = void 0;
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const types_1 = require("../domain/auth/types");
function hashPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, 10, function (err, hash) {
            if (err)
                return reject(err);
            return resolve(hash);
        });
    });
}
exports.hashPassword = hashPassword;
function comparePassword(password, hash) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hash, function (err, res) {
            if (err) {
                return reject(err);
            }
            else {
                return resolve(res);
            }
        });
    });
}
exports.comparePassword = comparePassword;
function generateToken(data, private_key, expires_in) {
    return jwt.sign(data, private_key, {
        expiresIn: expires_in,
        algorithm: 'RS256',
    });
}
exports.generateToken = generateToken;
function verifyToken(token, public_key) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, public_key, function (err, decoded) {
            if (err)
                return reject(err);
            return resolve(decoded);
        });
    });
}
exports.verifyToken = verifyToken;
function generateTokenSecret(data, secret_key, expires_in) {
    return jwt.sign(data, secret_key, {
        expiresIn: expires_in,
    });
}
exports.generateTokenSecret = generateTokenSecret;
function verifyTokenSecret(token, secret_key) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret_key, function (err, decoded) {
            if (err)
                return reject(err);
            return resolve(decoded);
        });
    });
}
exports.verifyTokenSecret = verifyTokenSecret;
function decodeBase64(encrypt_data) {
    return Buffer.from(encrypt_data, 'base64').toString('utf8');
}
exports.decodeBase64 = decodeBase64;
function encodeBase64(decrypt_data) {
    return Buffer.from(decrypt_data).toString('base64');
}
exports.encodeBase64 = encodeBase64;
function createChecksum(data, secret_key) {
    const hash = crypto
        .createHash('sha256')
        .update(data + secret_key)
        .digest('hex');
    return hash;
}
exports.createChecksum = createChecksum;
function validateChecksum(data, checksum, secret_key) {
    const hash = crypto
        .createHash('sha256')
        .update(data + secret_key)
        .digest('hex');
    return hash === checksum;
}
exports.validateChecksum = validateChecksum;
function create3DESKey(key) {
    let hashKey = crypto.createHash('md5').update(key).digest();
    hashKey = Buffer.concat([hashKey, hashKey.slice(0, 8)]);
    return hashKey;
}
function tripleDESEncrypt(data, secret_key, output_type) {
    try {
        const threeDESKey = create3DESKey(secret_key);
        const cipher = crypto.createCipheriv('des-ede3', threeDESKey, '');
        const encrypted = cipher.update(data, 'utf8', output_type);
        return encrypted + cipher.final(output_type);
    }
    catch (error) {
        return false;
    }
}
exports.tripleDESEncrypt = tripleDESEncrypt;
function tripleDESDecrypt(data, secret_key, input_type) {
    try {
        const threeDESKey = create3DESKey(secret_key);
        const decipher = crypto.createDecipheriv('des-ede3', threeDESKey, '');
        const decrypted = decipher.update(data, input_type);
        return decrypted.toString() + decipher.final().toString();
    }
    catch (error) {
        return false;
    }
}
exports.tripleDESDecrypt = tripleDESDecrypt;
function createChecksumSHA512HMAC(data, key, output) {
    const hmac = crypto.createHmac('sha512', key);
    const signed = hmac.update(data).digest(output);
    return signed;
}
exports.createChecksumSHA512HMAC = createChecksumSHA512HMAC;
function generatePassword() {
    const password = Math.random().toString(36).substring(2, 7) +
        Math.random().toString(36).substring(2, 7).toUpperCase();
    const test = new RegExp(types_1.PasswordRegex).test(password);
    if (test) {
        return password;
    }
    return generatePassword();
}
exports.generatePassword = generatePassword;
//# sourceMappingURL=hash.util.js.map