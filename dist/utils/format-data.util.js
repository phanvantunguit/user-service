"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatQueryArray = exports.formatRequestDataToStringV2 = exports.formatRequestDataToString = exports.generatePassword = exports.cleanObject = void 0;
const user_1 = require("../const/user");
function cleanObject(obj) {
    const newObj = {};
    for (const key in obj) {
        if (obj[key]) {
            newObj[key] = obj[key];
        }
    }
    return newObj;
}
exports.cleanObject = cleanObject;
function generatePassword() {
    const password = Math.random().toString(36).substring(2, 7) +
        Math.random().toString(36).substring(2, 7).toUpperCase();
    const test = new RegExp(user_1.PasswordRegex).test(password);
    if (test) {
        return password;
    }
    return generatePassword();
}
exports.generatePassword = generatePassword;
function formatRequestDataToString(params) {
    return Object.keys(params)
        .map((key) => `${key}=${params[key]}`)
        .join('&');
}
exports.formatRequestDataToString = formatRequestDataToString;
function formatRequestDataToStringV2(params) {
    try {
        return Object.keys(params)
            .map((key) => {
            if (typeof params[key] === 'object') {
                return `${key}=${JSON.stringify(params[key])}`;
            }
            else {
                return `${key}=${params[key]}`;
            }
        })
            .join('&');
    }
    catch (error) {
        console.log('error', error);
    }
}
exports.formatRequestDataToStringV2 = formatRequestDataToStringV2;
function formatQueryArray(params) {
    return Object.keys(params).map((key) => {
        if (typeof params[key] === 'string') {
            return `${key}='${params[key]}'`;
        }
        else {
            return `${key}=${params[key]}`;
        }
    });
}
exports.formatQueryArray = formatQueryArray;
//# sourceMappingURL=format-data.util.js.map