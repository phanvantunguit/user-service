"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resFailed = exports.resSuccess = void 0;
const common_1 = require("@nestjs/common");
function resSuccess(params) {
    const { payload, res, message, status } = params;
    const result = Object.assign(Object.assign({}, ERROR_CODE.SUCCESS), { payload });
    if (message) {
        result.message = message;
    }
    res.status(status || common_1.HttpStatus.OK).send(result);
}
exports.resSuccess = resSuccess;
function resFailed(params) {
    const { payload, res, reason } = params;
    const result = {
        error_code: reason.error_code,
        message: reason.message,
        payload,
    };
    const status = reason.status || common_1.HttpStatus.UNPROCESSABLE_ENTITY;
    res.status(status).send(result);
}
exports.resFailed = resFailed;
//# sourceMappingURL=response.util.js.map