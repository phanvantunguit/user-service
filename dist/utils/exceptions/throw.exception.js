"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serverError = exports.accessDeniedError = exports.unauthorizedError = exports.badRequestError = exports.notFoundError = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("./const");
function notFoundError(data) {
    throw {
        status: common_1.HttpStatus.NOT_FOUND,
        error_code: const_1.ERROR_CODE.NOT_FOUND.error_code,
        message: `${data} ${const_1.ERROR_CODE.NOT_FOUND.message}`,
    };
}
exports.notFoundError = notFoundError;
function badRequestError(data, error) {
    throw {
        status: common_1.HttpStatus.BAD_REQUEST,
        error_code: error.error_code,
        message: `${data} ${error.message}`,
    };
}
exports.badRequestError = badRequestError;
function unauthorizedError(data, error) {
    throw {
        status: common_1.HttpStatus.UNAUTHORIZED,
        error_code: error.error_code,
        message: `${data} ${error.message}`,
    };
}
exports.unauthorizedError = unauthorizedError;
function accessDeniedError() {
    throw Object.assign({ status: common_1.HttpStatus.FORBIDDEN }, const_1.ERROR_CODE.ACCESS_DENIED);
}
exports.accessDeniedError = accessDeniedError;
function serverError(data, error) {
    throw {
        status: common_1.HttpStatus.SERVICE_UNAVAILABLE,
        error_code: error.error_code,
        message: `${data} ${error.message}`,
    };
}
exports.serverError = serverError;
//# sourceMappingURL=throw.exception.js.map