import { HttpStatus } from '@nestjs/common';
export interface IThrowExceptionOutput {
    status: HttpStatus;
    error_code: string;
    message: string | string[];
}
export interface IBadRequestError {
    error_code: string;
    message: string | string[];
}
export interface IUnauthoriedError {
    error_code: string;
    message: string | string[];
}
