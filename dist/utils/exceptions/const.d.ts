export declare const ERROR_CODE: {
    SUCCESS: {
        error_code: string;
        message: string;
    };
    UNKNOWN: {
        error_code: string;
        message: string;
    };
    BAD_REQUEST: {
        error_code: string;
        message: string;
    };
    NOT_FOUND: {
        error_code: string;
        message: string;
    };
    EXISTED: {
        error_code: string;
        message: string;
    };
    INVALID: {
        error_code: string;
        message: string;
    };
    INCORRECT: {
        error_code: string;
        message: string;
    };
    UNAUTHORIZED: {
        error_code: string;
        message: string;
    };
    ACCESS_DENIED: {
        error_code: string;
        message: string;
    };
    NOT_VERIFIED: {
        error_code: string;
        message: string;
    };
    EXPIRED: {
        error_code: string;
        message: string;
    };
    UNPROCESSABLE: {
        error_code: string;
        message: string;
    };
    PROCESSING: {
        error_code: string;
        message: string;
    };
    EXPIRES: {
        error_code: string;
        message: string;
    };
    NOT_TYPE_TRC20: {
        error_code: string;
        message: string;
    };
    SEND_EMAIL_FAILED: {
        error_code: string;
        message: string;
    };
    SESSION_INVALID: {
        error_code: string;
        message: string;
    };
    NOT_TYPE_BSC20: {
        error_code: string;
        message: string;
    };
    RANGES_INVALID: {
        error_code: string;
        message: string;
    };
    RANGES_FROM_TO: {
        error_code: string;
        message: string;
    };
};
export declare enum BAD_REQUEST_CODE {
    BAD_REQUEST = "BAD_REQUEST",
    EXISTED = "EXISTED",
    INVALID = "INVALID",
    INCORRECT = "INCORRECT",
    EXPIRED = "EXPIRED"
}
export declare enum UNAUTHORIZED_CODE {
    UNAUTHORIZED = "UNAUTHORIZED",
    EXPIRED = "EXPIRED"
}
