import { IBadRequestError, IThrowExceptionOutput, IUnauthoriedError } from './type';
export declare function notFoundError(data: string): IThrowExceptionOutput;
export declare function badRequestError(data: string, error: IBadRequestError): IThrowExceptionOutput;
export declare function unauthorizedError(data: string, error: IUnauthoriedError): IThrowExceptionOutput;
export declare function accessDeniedError(): IThrowExceptionOutput;
export declare function serverError(data: string, error: IBadRequestError): IThrowExceptionOutput;
