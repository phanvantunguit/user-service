"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionsUtil = void 0;
const common_1 = require("@nestjs/common");
const const_1 = require("./const");
let ExceptionsUtil = class ExceptionsUtil {
    constructor() {
        this.logger = new common_1.Logger('EXCEPTION');
    }
    catch(exception, host) {
        if (typeof exception === 'object') {
            this.logger.log(JSON.stringify(exception));
        }
        else {
            this.logger.log(`${exception}`);
        }
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        let status = exception instanceof common_1.HttpException
            ? exception.getStatus()
            : common_1.HttpStatus.UNPROCESSABLE_ENTITY;
        const reason = Object.assign({}, const_1.ERROR_CODE.UNKNOWN);
        switch (status) {
            case common_1.HttpStatus.BAD_REQUEST: {
                reason.error_code = const_1.ERROR_CODE.BAD_REQUEST.error_code;
                reason.message = exception.response.message;
                break;
            }
            case common_1.HttpStatus.NOT_FOUND: {
                reason.error_code = const_1.ERROR_CODE.NOT_FOUND.error_code;
                reason.message = exception.response.message;
                break;
            }
            default:
                if (exception.status) {
                    status = exception.status;
                    reason.error_code = exception.error_code;
                    reason.message = exception.message;
                }
                else {
                    reason.message = `${exception}`;
                }
                break;
        }
        response.status(status).json(Object.assign({}, reason));
    }
};
ExceptionsUtil = __decorate([
    (0, common_1.Catch)()
], ExceptionsUtil);
exports.ExceptionsUtil = ExceptionsUtil;
//# sourceMappingURL=index.js.map