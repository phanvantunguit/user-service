"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateQRCode = void 0;
const qrcode = require("qrcode");
async function generateQRCode(data) {
    return qrcode.toDataURL(data);
}
exports.generateQRCode = generateQRCode;
//# sourceMappingURL=qrcode.util.js.map