"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestDELETE = exports.requestGET = exports.requestPATCH = exports.requestPUT = exports.requestPOST = void 0;
const axios = require("axios");
const https = require("https");
function requestPOST(url, data, headers) {
    const agent = new https.Agent({
        rejectUnauthorized: false,
    });
    return new Promise((resolve, reject) => {
        axios.default
            .post(url, data, {
            httpsAgent: agent,
            headers,
        })
            .then((resp) => {
            resolve(resp.data);
        })
            .catch((error) => {
            const dataError = handleError(error);
            reject(dataError);
        });
    });
}
exports.requestPOST = requestPOST;
function requestPUT(url, data, headers) {
    return new Promise((resolve, reject) => {
        axios.default
            .put(url, data, {
            headers,
        })
            .then((resp) => {
            resolve(resp.data);
        })
            .catch((error) => {
            const dataError = handleError(error);
            reject(dataError);
        });
    });
}
exports.requestPUT = requestPUT;
function requestPATCH(url, data, headers) {
    return new Promise((resolve, reject) => {
        axios.default
            .patch(url, data, {
            headers,
        })
            .then((resp) => {
            resolve(resp.data);
        })
            .catch((error) => {
            const dataError = handleError(error);
            reject(dataError);
        });
    });
}
exports.requestPATCH = requestPATCH;
function requestGET(url, headers) {
    return new Promise((resolve, reject) => {
        axios.default
            .get(url, {
            headers,
        })
            .then((resp) => {
            resolve(resp.data);
        })
            .catch((error) => {
            const dataError = handleError(error);
            reject(dataError);
        });
    });
}
exports.requestGET = requestGET;
function requestDELETE(url, headers) {
    return new Promise((resolve, reject) => {
        axios.default
            .delete(url, {
            headers,
        })
            .then((resp) => {
            resolve(resp.data);
        })
            .catch((error) => {
            const dataError = handleError(error);
            reject(dataError);
        });
    });
}
exports.requestDELETE = requestDELETE;
function handleError(error) {
    if (error.response) {
        if (typeof error.response.data == 'object') {
            return JSON.stringify(error.response.data);
        }
        return error.response.data;
    }
    return error.message;
}
//# sourceMappingURL=http-transport.util.js.map