"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setEndDate = exports.setBeginDate = exports.DAY = exports.HOUR = exports.MINUTE = exports.SECOND = void 0;
exports.SECOND = 1000;
exports.MINUTE = 60 * exports.SECOND;
exports.HOUR = 60 * exports.MINUTE;
exports.DAY = 24 * exports.HOUR;
function setBeginDate(time = Date.now()) {
    const timeDefault = new Date(Number(time));
    timeDefault.setHours(0);
    timeDefault.setMinutes(0);
    timeDefault.setSeconds(0);
    timeDefault.setMilliseconds(0);
    return timeDefault.valueOf();
}
exports.setBeginDate = setBeginDate;
function setEndDate(time = Date.now()) {
    const timeDefault = new Date(Number(time));
    timeDefault.setHours(24);
    timeDefault.setMinutes(0);
    timeDefault.setSeconds(0);
    timeDefault.setMilliseconds(0);
    return timeDefault.valueOf();
}
exports.setEndDate = setEndDate;
//# sourceMappingURL=time.util.js.map