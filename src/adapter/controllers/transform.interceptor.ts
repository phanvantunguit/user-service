import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ERROR_CODE } from 'src/utils/exceptions/const';

export interface Response<T> {
  error_code: string;
  message: string;
  payload: T;
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    return next.handle().pipe(
      map((data) => ({
        ...ERROR_CODE.SUCCESS,
        payload: data,
      })),
    );
  }
}
export class BaseApiOutput {
  @ApiProperty({
    required: true,
    description: 'Error code of request',
    example: 'SUCCESS',
  })
  @IsString()
  error_code?: string;

  @ApiProperty({
    required: true,
    description: 'Message describe error code',
    example: 'Success',
  })
  @IsString()
  message?: string;
}
