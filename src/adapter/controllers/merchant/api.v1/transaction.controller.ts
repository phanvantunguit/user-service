import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { ReportTransactionChartHandler } from 'src/application';
import { MerchantGetTransactionHandler } from 'src/application/usecase/transaction/merchant-get-transaction';
import { MerchantListTransactionHandler } from 'src/application/usecase/transaction/merchant-list-transaction';
import {
  ListTransactionInput,
  ListTransactionOutput,
} from 'src/application/usecase/transaction/merchant-list-transaction/validate';
import { ReportTransactionHandler } from 'src/application/usecase/transaction/report-transaction';
import {
  ReportTransactionChartInput,
  ReportTransactionChartOutput,
} from 'src/application/usecase/transaction/report-transaction-chart/validate';
import {
  ReportTransactionInput,
  ReportTransactionOutput,
} from 'src/application/usecase/transaction/report-transaction/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/transaction')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/transaction`)
export class MerchantV1TransactionController {
  constructor(
    @Inject(MerchantListTransactionHandler)
    private merchantListTransactionHandler: MerchantListTransactionHandler,
    @Inject(MerchantGetTransactionHandler)
    private merchantGetTransactionHandler: MerchantGetTransactionHandler,
    @Inject(ReportTransactionHandler)
    private reportTransactionHandler: ReportTransactionHandler,
    @Inject(ReportTransactionChartHandler)
    private reportTransactionChartHandler: ReportTransactionChartHandler,
  ) {}

  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListTransactionOutputMap extends IntersectionType(
      ListTransactionOutput,
      BaseApiOutput,
    ) {},
    description: 'List transaction',
  })
  async listTransaction(@Req() req: any, @Query() query: ListTransactionInput) {
    const merchant_code = req.user.merchant_code;
    const result = await this.merchantListTransactionHandler.execute(
      query,
      merchant_code,
    );
    return result.payload;
  }
  @Get('/report')
  @ApiResponse({
    status: 200,
    type: class ReportTransactionOutputMap extends IntersectionType(
      ReportTransactionOutput,
      BaseApiOutput,
    ) {},
    description: 'Report transaction',
  })
  async report(@Req() req: any, @Query() query: ReportTransactionInput) {
    const merchant_code = req.user.merchant_code;
    const merchant_id = req.user.user_id;
    const result = await this.reportTransactionHandler.execute(
      query,
      merchant_id,
      merchant_code,
    );
    return result.payload;
  }
  @Get('/chart')
  @ApiResponse({
    status: 200,
    type: class ReportTransactionChartOutputMap extends IntersectionType(
      ReportTransactionChartOutput,
      BaseApiOutput,
    ) {},
    description: 'Report transaction chart',
  })
  async chart(@Req() req: any, @Query() query: ReportTransactionChartInput) {
    const merchant_code = req.user.merchant_code;
    const result = await this.reportTransactionChartHandler.execute(
      query,
      merchant_code,
    );
    return result.payload;
  }
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class ListTransactionOutputMap extends IntersectionType(
      ListTransactionOutput,
      BaseApiOutput,
    ) {},
    description: 'Get transaction',
  })
  async getTransaction(@Param('id') id: string, @Req() req: any) {
    const merchant_code = req.user.merchant_code;
    const result = await this.merchantGetTransactionHandler.execute({
      id,
      merchant_code,
    });
    return result.payload;
  }
}
