import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import {
  MerchantUpdateProfileHandler,
  UpdateEmailSenderHandler,
  VerifyEmailSenderHandler,
} from 'src/application';
import { AdminUpdateMerchantOutput } from 'src/application/usecase/merchant/admin-update-merchant/validate';
import { GetMerchantHandler } from 'src/application/usecase/merchant/get-merchant';
import { GetMerchantOutput } from 'src/application/usecase/merchant/get-merchant/validate';
import { MerchantVerifyPasswordHandler } from 'src/application/usecase/merchant/merchant-check-password';
import {
  MerchantVerifyPasswordInput,
  MerchantVerifyPasswordOutput,
} from 'src/application/usecase/merchant/merchant-check-password/validate';
import { CreateWalletHandler } from 'src/application/usecase/merchant/merchant-create-wallet';
import {
  MerchantCreateInput,
  MerchantCreateOutput,
} from 'src/application/usecase/merchant/merchant-create-wallet/validate';
import { SendOtpWalletHandler } from 'src/application/usecase/merchant/merchant-send-otp';
import {
  MerchantSendOtpInput,
  MerchantSendOtpOutput,
} from 'src/application/usecase/merchant/merchant-send-otp/validate';
import { MerchantUpdatePasswordHandler } from 'src/application/usecase/merchant/merchant-update-password';
import {
  MerchantUpdatePasswordInput,
  MerchantUpdatePasswordOutput,
} from 'src/application/usecase/merchant/merchant-update-password/validate';
import {
  MerchantUpdateProfileInput,
  MerchantUpdateProfileOutput,
} from 'src/application/usecase/merchant/merchant-update-profile/validate';
import { UpdateEmailSenderInput } from 'src/application/usecase/merchant/update-email-sender/validate';
import { VerifyEmailSenderInput } from 'src/application/usecase/merchant/verify-email-sender/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/profile')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/profile`)
export class MerchantV1ProfileController {
  constructor(
    @Inject(GetMerchantHandler)
    private getMerchantHandler: GetMerchantHandler,
    @Inject(MerchantUpdatePasswordHandler)
    private merchantUpdatePasswordHandler: MerchantUpdatePasswordHandler,
    @Inject(MerchantUpdateProfileHandler)
    private merchantUpdateProfileHandler: MerchantUpdateProfileHandler,
    @Inject(VerifyEmailSenderHandler)
    private verifyEmailSenderHandler: VerifyEmailSenderHandler,
    @Inject(UpdateEmailSenderHandler)
    private updateEmailSenderHandler: UpdateEmailSenderHandler,
    @Inject(MerchantVerifyPasswordHandler)
    private merchantVerifyPasswordHandler: MerchantVerifyPasswordHandler,
    @Inject(SendOtpWalletHandler)
    private sendOtpWalletHandler: SendOtpWalletHandler,
    @Inject(CreateWalletHandler)
    private createWalletHandler: CreateWalletHandler,
  ) {}

  @Get('')
  @ApiResponse({
    status: 200,
    type: class MerchantGetProfileOutputMap extends IntersectionType(
      GetMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Get profile',
  })
  async getProfile(@Req() req: any) {
    const user_id = req.user.user_id;
    const result = await this.getMerchantHandler.execute(user_id);
    return result.payload;
  }

  @Put('')
  @ApiResponse({
    status: 200,
    type: class MerchantUpdateProfileOutputMap extends IntersectionType(
      MerchantUpdateProfileOutput,
      BaseApiOutput,
    ) {},
    description: 'Update Profile',
  })
  async updateProfile(
    @Req() req: any,
    @Body() body: MerchantUpdateProfileInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.merchantUpdateProfileHandler.execute(
      body,
      user_id,
    );
    return result.payload;
  }

  @Put('change-password')
  @ApiResponse({
    status: 200,
    type: class MerchantUpdatePasswordOutputMap extends IntersectionType(
      MerchantUpdatePasswordOutput,
      BaseApiOutput,
    ) {},
    description: 'Change password',
  })
  async changePassword(
    @Req() req: any,
    @Body() body: MerchantUpdatePasswordInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.merchantUpdatePasswordHandler.execute(
      body,
      user_id,
    );
    return result.payload;
  }

  @Put('/verify-sender')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Verify sender',
  })
  async verifySender(@Req() req: any, @Body() body: VerifyEmailSenderInput) {
    const user_id = req.user.user_id;
    const result = await this.verifyEmailSenderHandler.execute(body, user_id);
    return result.payload;
  }

  @Put('/update-sender')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Update sender',
  })
  async updateSender(@Req() req: any, @Body() body: UpdateEmailSenderInput) {
    const user_id = req.user.user_id;
    const result = await this.updateEmailSenderHandler.execute(body, user_id);
    return result.payload;
  }
  @Put('/verify-password')
  @ApiResponse({
    status: 200,
    type: class MerchantAddWalletOutputMap extends IntersectionType(
      MerchantVerifyPasswordOutput,
      BaseApiOutput,
    ) {},
    description: 'Verify password',
  })
  async verifyPassWord(
    @Body() param: MerchantVerifyPasswordInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.merchantVerifyPasswordHandler.execute(
      param,
      user_id,
    );
    return result;
  }

  @Put('/send-otp-wallet')
  @ApiResponse({
    status: 200,
    type: class MerchantSendOtpWalletOutput extends IntersectionType(
      MerchantSendOtpOutput,
      BaseApiOutput,
    ) {},
    description: 'Send OTP',
  })
  async sendOtpWallet(@Body() param: MerchantSendOtpInput, @Req() req: any) {
    const user_id = req.user.user_id;
    await this.sendOtpWalletHandler.execute(param, user_id);
    return true;
  }

  @Put('/create-wallet')
  @ApiResponse({
    status: 200,
    type: class MerchantSendOtpWalletOutput extends IntersectionType(
      MerchantCreateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create Wallet',
  })
  async createWallet(@Body() param: MerchantCreateInput, @Req() req: any) {
    const user_id = req.user.user_id;
    await this.createWalletHandler.execute(param, user_id);
    return true;
  }
}
