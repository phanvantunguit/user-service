import {
  Controller,
  Get,
  Inject,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { MerchantListRoleHandler } from 'src/application';
import { MerchantListBotOuput } from 'src/application/usecase/bot/merchant-list-bot/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/role')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/role`)
export class MerchantV1RoleController {
  constructor(
    @Inject(MerchantListRoleHandler)
    private merchantListRoleHandler: MerchantListRoleHandler,
  ) {}
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class MerchantListBotOuputMap extends IntersectionType(
      MerchantListBotOuput,
      BaseApiOutput,
    ) {},
    description: 'List role',
  })
  async listRole() {
    const result = await this.merchantListRoleHandler.execute();
    return result.payload;
  }
}
