import {
  Controller,
  Get,
  Inject,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { MerchantListBotTradingHandler } from 'src/application/usecase/bot/merchant-list-bot-trading';
import { MerchantListBotTradingOuput } from 'src/application/usecase/bot/merchant-list-bot-trading/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/bot-trading')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/bot-trading`)
export class MerchantV1BotTradingController {
  constructor(
    @Inject(MerchantListBotTradingHandler)
    private merchantListBotTradingHandler: MerchantListBotTradingHandler,
  ) {}
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class MerchantListBotTradingOuputMap extends IntersectionType(
      MerchantListBotTradingOuput,
      BaseApiOutput,
    ) {},
    description: 'List bot trading',
  })
  async listBot(@Query() query: any, @Req() req: any) {
    const merchant_id = req.user.user_id;
    const { user_id } = query;
    const result = await this.merchantListBotTradingHandler.execute(user_id, merchant_id);
    return result.payload;
  }
}
