import {
  Body,
  Controller,
  Inject,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { MerchantLoginHandler } from 'src/application/usecase/merchant/merchant-login';
import {
  MerchantLoginInput,
  MerchantLoginOutput,
} from 'src/application/usecase/merchant/merchant-login/validate';
import { WITHOUT_AUTHORIZATION } from 'src/domain/auth/types';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard, MerchantPermission } from '../auth';

@ApiTags('merchant/auth')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/auth`)
export class MerchantV1AuthController {
  constructor(
    @Inject(MerchantLoginHandler)
    private merchantLoginHandler: MerchantLoginHandler,
  ) {}

  @MerchantPermission(WITHOUT_AUTHORIZATION)
  @Post('login')
  @ApiResponse({
    status: 200,
    type: class AdminLoginOutputMap extends IntersectionType(
      MerchantLoginOutput,
      BaseApiOutput,
    ) {},
    description: 'Login',
  })
  async login(@Body() body: MerchantLoginInput, @Query() query: any,) {
    const result = await this.merchantLoginHandler.execute(body, query.merchant_code);
    return result.payload;
  }
}
