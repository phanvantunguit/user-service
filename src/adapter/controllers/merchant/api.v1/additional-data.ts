import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { ModifyMerchantAdditionalDataHandler } from 'src/application';
import { ListMerchantAdditionalDataHandler } from 'src/application/usecase/additional-data/list-merchant-additional-data';
import { GetMerchantAdditionalDataOutput, ListMerchantAdditionalDataInput, ListMerchantAdditionalDataOutput } from 'src/application/usecase/additional-data/list-merchant-additional-data/validate';
import { ModifyMerchantAdditionalData, ModifyMerchantAdditionalDataInput } from 'src/application/usecase/additional-data/modify-merchant-additional-data/validate';
import { BaseUpdateOutput } from 'src/application/usecase/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/additional-data')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/additional-data`)
export class MerchantV1AdditionalDataController {
  constructor(
    @Inject(ListMerchantAdditionalDataHandler)
    private listMerchantAdditionalDataHandler: ListMerchantAdditionalDataHandler,
    @Inject(ModifyMerchantAdditionalDataHandler)
    private modifyMerchantAdditionalDataHandler: ModifyMerchantAdditionalDataHandler,
  ) {}
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListMerchantAdditionalDataOutputMap extends IntersectionType(
      ListMerchantAdditionalDataOutput,
      BaseApiOutput,
    ) {},
    description: 'List Additional Data',
  })
  async listAdditionalData(
    @Query() query: ListMerchantAdditionalDataInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.listMerchantAdditionalDataHandler.execute(query, user_id);
    return result.payload;
  }

  @Put('/')
  @ApiResponse({
    status: 200,
    type: class BaseUpdateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Modify Additional Data',
  })
  async updateAdditionalData(
    @Body() body: ModifyMerchantAdditionalDataInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.modifyMerchantAdditionalDataHandler.execute(body, user_id);
    return result.payload;
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class GetMerchantAdditionalDataOutputMap extends IntersectionType(
      GetMerchantAdditionalDataOutput,
      BaseApiOutput,
    ) {},
    description: 'Get Additional Data',
  })
  async getAdditionalData(
    @Param('id') id: string,
  ) {
    const result = await this.listMerchantAdditionalDataHandler.execute({}, undefined, id)
    return result.payload[0];
  }
}
