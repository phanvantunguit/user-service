import {
  Body,
  Controller,
  Get,
  Inject,
  Post,
  Param,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
  Delete,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { ListInactiveBySystemHandler } from 'src/application/usecase/event-store/list-inactive-by-system';
import {
  ListEventInactiveBySystemInput,
  ListEventInactiveBySystemOutput,
} from 'src/application/usecase/event-store/list-inactive-by-system/validate';
import { GetNumberOfBotUserHandler } from 'src/application/usecase/merchant/get-number-bot-user';
import { GetNumberOfBotForUserOutput } from 'src/application/usecase/merchant/get-number-bot-user/validate';
import { MerchantAddBotForUserHandle } from 'src/application/usecase/merchant/merchant-add-number-bot';
import {
  MerchantAddNumberBotDto,
  MerchantAddNumberBotOutput,
} from 'src/application/usecase/merchant/merchant-add-number-bot/validate';
import { MerchantUpdateUserBotTradingStatusHandler } from 'src/application/usecase/merchant/merchant-update-user-tbot-status';
import {
  MerchantUpdateStatusInput,
  MerchantUpdateStatusInputOutput,
} from 'src/application/usecase/merchant/merchant-update-user-tbot-status/validate';
import { MerchantAddUserAssetHandler } from 'src/application/usecase/merchant/user-asset/merchant-add-user-asset';
import {
  MerchantCreateAssetInput,
  MerchantCreateAssetOutput,
} from 'src/application/usecase/merchant/user-asset/merchant-add-user-asset/validate';
import { MerchantRemoveUserAssetHandler } from 'src/application/usecase/merchant/user-asset/merchant-remove-user-asset';
import {
  MerchantRemoveAssetInput,
  MerchantRemoveAssetOutput,
} from 'src/application/usecase/merchant/user-asset/merchant-remove-user-asset/validate';
import { ReportChartVisitorHandler } from 'src/application/usecase/user/chart-visitor';
import { MerchantUserCreateServiceHandler } from 'src/application/usecase/user/create-user';
import {
  MerchantCreateUserOutput,
  PutUserProfileDto,
} from 'src/application/usecase/user/create-user/validate';
import { GetUserHandler } from 'src/application/usecase/user/get-user';
import { ListUserHandler } from 'src/application/usecase/user/list-user';
import {
  ListUserInput,
  ListUserOutput,
} from 'src/application/usecase/user/list-user/validate';
import { ReportUserHandler } from 'src/application/usecase/user/report-user';
import {
  ReportUserInput,
  ReportUserOutput,
} from 'src/application/usecase/user/report-user/validate';
import { MerchantUserUpdateServiceHandler } from 'src/application/usecase/user/update-user';
import {
  MerchantUpdateUserOutput,
  UpdateUserProfileDto,
} from 'src/application/usecase/user/update-user/validate';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/user')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/user`)
export class MerchantV1UserController {
  constructor(
    @Inject(ListUserHandler)
    private listUserHandler: ListUserHandler,
    @Inject(ReportUserHandler)
    private reportUserHandler: ReportUserHandler,
    @Inject(ReportChartVisitorHandler)
    private reportChartVisitorHandler: ReportChartVisitorHandler,
    @Inject(MerchantAddUserAssetHandler)
    private merchantAddUserAssetHandler: MerchantAddUserAssetHandler,
    @Inject(MerchantRemoveUserAssetHandler)
    private merchantRemoveUserAssetHandler: MerchantRemoveUserAssetHandler,
    @Inject(MerchantUserCreateServiceHandler)
    private merchantUserCreateServiceHandler: MerchantUserCreateServiceHandler,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
    @Inject(GetUserHandler) private getUserHandler: GetUserHandler,
    @Inject(MerchantUserUpdateServiceHandler)
    private merchantUserUpdateServiceHandler: MerchantUserUpdateServiceHandler,
    @Inject(MerchantAddBotForUserHandle)
    private merchantAddBotForUserHandle: MerchantAddBotForUserHandle,
    @Inject(MerchantUpdateUserBotTradingStatusHandler)
    private merchantUpdateUserBotTradingStatusHandler: MerchantUpdateUserBotTradingStatusHandler,
    @Inject(GetNumberOfBotUserHandler)
    private getNumberOfBotUserHandler: GetNumberOfBotUserHandler,
    @Inject(ListInactiveBySystemHandler)
    private listInactiveBySystemHandler: ListInactiveBySystemHandler,
  ) {}

  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListUserOutputMap extends IntersectionType(
      ListUserOutput,
      BaseApiOutput,
    ) {},
    description: 'List user',
  })
  async listTransaction(@Req() req: any, @Query() query: ListUserInput) {
    const merchant_code = req.user.merchant_code;
    const result = await this.listUserHandler.execute(query, merchant_code);
    return result.payload;
  }

  @Get('/report')
  @ApiResponse({
    status: 200,
    type: class ReportUserOutputMap extends IntersectionType(
      ReportUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Report user',
  })
  async report(@Req() req: any, @Query() query: ReportUserInput) {
    const merchant_code = req.user.merchant_code;
    const result = await this.reportUserHandler.execute(query, merchant_code);
    return result.payload;
  }

  @Get('/chart')
  @ApiResponse({
    status: 200,
    description: 'Report visitor chart',
  })
  async chart(@Req() req: any) {
    const merchant_code = req.user.merchant_code;
    const result = await this.reportChartVisitorHandler.execute(merchant_code);
    return result.payload;
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class ListUserOutputMap extends IntersectionType(
      ListUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Get user by id',
  })
  async getUserById(@Req() req: any, @Param('id') id: string) {
    const merchant_code = req.user.merchant_code;
    const result = await this.getUserHandler.execute(id, merchant_code);
    return result.payload;
  }

  @Put('/add-user-asset/:id')
  @ApiResponse({
    status: 200,
    type: class MerchantAddUserAssetOutputMap extends IntersectionType(
      MerchantCreateAssetOutput,
      BaseApiOutput,
    ) {},
    description: 'Add user asset',
  })
  async addUserAsset(
    @Body() param: MerchantCreateAssetInput,
    @Param('id') user_id: string,
    @Req() req: any,
  ) {
    const owner_created = req.user.user_id;

    const result = await this.merchantAddUserAssetHandler.execute(
      param,
      owner_created,
      user_id,
    );
    return result.payload;
  }

  @Delete('/remove-user-asset')
  @ApiResponse({
    status: 200,
    type: class MerchantRemoveUserAssetOutputMap extends IntersectionType(
      MerchantRemoveAssetOutput,
      BaseApiOutput,
    ) {},
    description: 'Remove user asset',
  })
  async removeUserAsset(
    @Body() param: MerchantRemoveAssetInput,
    @Req() req: any,
  ) {
    const owner_created = req.user.user_id;
    const result = await this.merchantRemoveUserAssetHandler.execute(
      param,
      owner_created,
    );
    return result;
  }

  @Post('/create-user')
  @ApiResponse({
    status: 200,
    type: class MerchantCreateUserInputMap extends IntersectionType(
      MerchantCreateUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Create user input',
  })
  async createUserInput(@Body() param: PutUserProfileDto, @Req() req: any) {
    const owner_created = req.user.user_id;
    const result = await this.merchantUserCreateServiceHandler.execute(
      param,
      owner_created,
    );
    return result;
  }

  @Put('/:id/update-user')
  @ApiResponse({
    status: 200,
    type: class MerchantUpdateUserInputMap extends IntersectionType(
      MerchantUpdateUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Update user input',
  })
  async updateUserInput(
    @Body() param: UpdateUserProfileDto,
    @Param('id') user_id: string,
  ) {
    const result = await this.merchantUserUpdateServiceHandler.execute(
      param,
      user_id,
    );
    return result;
  }
  @Put('/add-number-bot-for-user/:id')
  @ApiResponse({
    status: 200,
    type: class MerchantAddNumberBotDtoMAP extends IntersectionType(
      MerchantAddNumberBotOutput,
      BaseApiOutput,
    ) {},
    description: 'Add number bot for user',
  })
  async createAppSetting(
    @Body() param: MerchantAddNumberBotDto,
    @Param('id') id: string,
    @Req() req: any,
  ) {
    const ownerId = req.user.user_id;
    const result = await this.merchantAddBotForUserHandle.execute(
      param,
      id,
      ownerId,
    );
    return result;
  }
  @Put('/setting-active-tbot/:id')
  @ApiResponse({
    status: 200,
    type: class MerchantUpdateStatusInputMap extends IntersectionType(
      MerchantUpdateStatusInputOutput,
      BaseApiOutput,
    ) {},
    description: 'Update status tbot',
  })
  async updateStatusTbot(
    @Body() param: MerchantUpdateStatusInput,
    @Param('id') id: string,
    @Req() req: any,
  ) {
    const merchantId = req.user.user_id;
    const result = await this.merchantUpdateUserBotTradingStatusHandler.execute(
      param,
      id,
      merchantId,
    );
    return result;
  }

  @Get('/get-number-bot-for-user/:id')
  @ApiResponse({
    status: 200,
    type: class ListNumberBotOfUserOutputMap extends IntersectionType(
      GetNumberOfBotForUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Get number of bot for by userId',
  })
  async getNumberBotOfUserById(@Req() req: any, @Param('id') id: string) {
    const merchant_id = req.user.user_id;

    const result = await this.getNumberOfBotUserHandler.execute(
      id,
      merchant_id,
    );
    return result.payload;
  }

  @Get('/:id/list-inactive-by-system')
  @ApiResponse({
    status: 200,
    type: class ListEventInactiveBySystemOutputMap extends IntersectionType(
      ListEventInactiveBySystemOutput,
      BaseApiOutput,
    ) {},
    description: 'List inactive by system',
  })
  async listInactiveBySystem(
    @Req() req: any,
    @Param('id') id: string,
    @Query() query: ListEventInactiveBySystemInput,
  ) {
    const result = await this.listInactiveBySystemHandler.execute({
      user_id: id,
      ...query,
    });
    return result.payload;
  }
}
