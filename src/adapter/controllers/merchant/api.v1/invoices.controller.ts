import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import {
  CreateMerchantInvoiceHandler,
  ListMerchantInvoiceHandler,
} from 'src/application';
import { CreateMerchantInvoiceByAmountHandler } from 'src/application/usecase/merchant/create-merchant-invoice-by-amount';
import { CreateMerchantInvoiceByAmountInput } from 'src/application/usecase/merchant/create-merchant-invoice-by-amount/validate';
import { GetMerchantInvoiceHandler } from 'src/application/usecase/merchant/get-merchant-invoice';
import { GetMerchantInvoiceIdOutput } from 'src/application/usecase/merchant/get-merchant-invoice/validate';
import {
  ListMerchantInvoiceInput,
  ListMerchantInvoiceOutput,
} from 'src/application/usecase/merchant/list-merchant-invoice/validate';
import { BaseCreateOutput } from 'src/application/usecase/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/invoices')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/invoices`)
export class MerchantV1InvoiceController {
  constructor(
    @Inject(ListMerchantInvoiceHandler)
    private listMerchantInvoiceHandler: ListMerchantInvoiceHandler,
    @Inject(GetMerchantInvoiceHandler)
    private getMerchantInvoiceHandler: GetMerchantInvoiceHandler,
    @Inject(CreateMerchantInvoiceByAmountHandler)
    private createMerchantInvoiceByAmountHandler: CreateMerchantInvoiceByAmountHandler,
  ) {}

  @Get('/list-invoice')
  @ApiResponse({
    status: 200,
    type: class ListMerchantInvoiceOutputMap extends IntersectionType(
      ListMerchantInvoiceOutput,
      BaseApiOutput,
    ) {},
    description: 'List invoice',
  })
  async listMerchantInvoice(
    @Query() query: ListMerchantInvoiceInput,
    @Req() req: any,
  ) {
    const merchant_id = req.user.user_id;
    query.merchant_id = merchant_id;
    const result = await this.listMerchantInvoiceHandler.execute(query);
    return result.payload;
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminGetMerchantInvoiceOutputMap extends IntersectionType(
      GetMerchantInvoiceIdOutput,
      BaseApiOutput,
    ) {},
    description: 'Get one invoice by id',
  })
  async getMerchantInvoice(@Param('id') id: string) {
    const result = await this.getMerchantInvoiceHandler.execute(id);
    return result.payload;
  }

  @Post('/')
  @ApiResponse({
    status: 200,
    type: class BaseCreateOutputMap extends IntersectionType(
      BaseCreateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create invoice',
  })
  async createMerchantInvoice(
    @Body() body: CreateMerchantInvoiceByAmountInput,
    @Req() req: any,
  ) {
    const merchant_id = req.user.user_id;
    const merchant_code = req.user.merchant_code;
    const result = await this.createMerchantInvoiceByAmountHandler.execute(
      body,
      merchant_id,
      merchant_code,
    );
    return result.payload;
  }
}
