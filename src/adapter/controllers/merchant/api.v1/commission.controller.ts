import {
  Body,
  Controller,
  Get,
  Inject,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import {
  UpdateMerchantCommissionHandler,
  ListMerchantCommissionHandler,
} from 'src/application';
import { AdminUpdateMerchantOutput } from 'src/application/usecase/merchant/admin-update-merchant/validate';
import {
  ListMerchantCommissioOutput,
  ListMerchantCommissionInput,
} from 'src/application/usecase/merchant/list-merchant-commission/validate';
import { MerchantUpdateCommissionInput } from 'src/application/usecase/merchant/update-merchant-commission/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/commission')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/commission`)
export class MerchantV1CommissionController {
  constructor(
    @Inject(UpdateMerchantCommissionHandler)
    private updateMerchantCommissionHandler: UpdateMerchantCommissionHandler,
    @Inject(ListMerchantCommissionHandler)
    private listMerchantCommissionHandler: ListMerchantCommissionHandler,
  ) {}
  @Put('/update')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Update commission',
  })
  async updateCommission(
    @Req() req: any,
    @Body() body: MerchantUpdateCommissionInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.updateMerchantCommissionHandler.execute(
      body,
      user_id,
    );
    return result.payload;
  }

  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListMerchantCommissioOutputMap extends IntersectionType(
      ListMerchantCommissioOutput,
      BaseApiOutput,
    ) {},
    description: 'List bot trading',
  })
  async listBotCommission(
    @Req() req: any,
    @Query() query: ListMerchantCommissionInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.listMerchantCommissionHandler.execute(
      query,
      user_id,
    );
    return result.payload;
  }
}
