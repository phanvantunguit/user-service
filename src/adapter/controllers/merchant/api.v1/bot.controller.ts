import {
  Controller,
  Get,
  Inject,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { MerchantListBotHandler } from 'src/application/usecase/bot/merchant-list-bot';
import { MerchantListBotOuput } from 'src/application/usecase/bot/merchant-list-bot/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { MerchantAuthGuard } from '../auth';

@ApiTags('merchant/bot')
@ApiBearerAuth()
@UseGuards(MerchantAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_merchant}/bot`)
export class MerchantV1BotController {
  constructor(
    @Inject(MerchantListBotHandler)
    private merchantListBotHandler: MerchantListBotHandler,
  ) {}
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class MerchantListBotOuputMap extends IntersectionType(
      MerchantListBotOuput,
      BaseApiOutput,
    ) {},
    description: 'List bot',
  })
  async listBot() {
    const result = await this.merchantListBotHandler.execute();
    return result.payload;
  }
}
