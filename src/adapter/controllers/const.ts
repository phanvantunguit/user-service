export enum ROOT_ROUTE {
  api_v1_admin = 'api/v1/admin',
  api_v1 = 'api/v1',
  api_v1_merchant = 'api/v1/merchant',
}
