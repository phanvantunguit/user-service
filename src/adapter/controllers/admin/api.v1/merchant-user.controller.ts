import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { GetUserHandler, ListUserHandler } from 'src/application';
import { GetUserOutput } from 'src/application/usecase/user/get-user/validate';
import {
  ListUserInput,
  ListUserOutput,
} from 'src/application/usecase/user/list-user/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';

@ApiTags('admin/merchant-user')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/merchant-user`)
export class AdminV1MerchantUserController {
  constructor(
    @Inject(ListUserHandler)
    private listUserHandler: ListUserHandler,
    @Inject(GetUserHandler)
    private getUserHandler: GetUserHandler,
  ) {}

  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListUserOutputMap extends IntersectionType(
      ListUserOutput,
      BaseApiOutput,
    ) {},
    description: 'List user of merchant',
  })
  async listTransaction(@Req() req: any, @Query() query: ListUserInput) {
    const result = await this.listUserHandler.execute(query, query.merchant_code);
    return result.payload;
  }
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class GetUserOutputMap extends IntersectionType(
      GetUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Detail user',
  })
  async getEvent(@Param('id') id: string) {
    const result = await this.getUserHandler.execute(id);
    return result.payload;
  }
}
