import {
  UseGuards,
  UseInterceptors,
  Controller,
  Inject,
  Body,
  Post,
  Req,
  Put,
  Param,
  Get,
  Query,
  Delete,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  IntersectionType,
  ApiResponse,
} from '@nestjs/swagger';
import {
  CreateMerchantInvoiceByAmountHandler,
  GetMerchantInvoiceHandler,
  ReportTransactionHandler,
  VerifyEmailSenderHandler,
} from 'src/application';
import { ModifyMerchantAdditionalDataHandler } from 'src/application/usecase/additional-data/modify-merchant-additional-data';
import { DeleteMerchantAdditionalDataHandler } from 'src/application/usecase/additional-data/delete-merchant-additional-data';
import { ListMerchantAdditionalDataHandler } from 'src/application/usecase/additional-data/list-merchant-additional-data';
import { GetMerchantAdditionalDataOutput, ListMerchantAdditionalDataInput, ListMerchantAdditionalDataOutput } from 'src/application/usecase/additional-data/list-merchant-additional-data/validate';
import { AdminCreateMerchantHandler } from 'src/application/usecase/merchant/admin-create-merchant';
import {
  AdminCreateMerchantInput,
  AdminCreateMerchantOutput,
} from 'src/application/usecase/merchant/admin-create-merchant/validate';
import { AdminListMerchantHandler } from 'src/application/usecase/merchant/admin-list-merchant';
import { AdminListMerchantOutput } from 'src/application/usecase/merchant/admin-list-merchant/validate';
import { AdminUpdateMerchantHandler } from 'src/application/usecase/merchant/admin-update-merchant';
import {
  AdminUpdateMerchantOutput,
  AdminUpdateMerchantInput,
} from 'src/application/usecase/merchant/admin-update-merchant/validate';
import { CreateMerchantInvoiceByAmountInput } from 'src/application/usecase/merchant/create-merchant-invoice-by-amount/validate';
import { GetMerchantHandler } from 'src/application/usecase/merchant/get-merchant';
import { GetMerchantInvoiceIdOutput } from 'src/application/usecase/merchant/get-merchant-invoice/validate';
import { GetMerchantOutput } from 'src/application/usecase/merchant/get-merchant/validate';
import { ListMerchantCommissionHandler } from 'src/application/usecase/merchant/list-merchant-commission';
import {
  ListMerchantCommissionInput,
  ListMerchantCommissioOutput,
} from 'src/application/usecase/merchant/list-merchant-commission/validate';
import { ListMerchantInvoiceHandler } from 'src/application/usecase/merchant/list-merchant-invoice';
import {
  ListMerchantInvoiceInput,
  ListMerchantInvoiceOutput,
} from 'src/application/usecase/merchant/list-merchant-invoice/validate';
import { UpdateMerchantInvoiceHandler } from 'src/application/usecase/merchant/merchant-update-invoice';
import {
  MerchantUpdateInvoiceInput,
  MerchantUpdateInvoiceOutput,
} from 'src/application/usecase/merchant/merchant-update-invoice/validate';
import { ResetMerchantPasswordHandler } from 'src/application/usecase/merchant/reset-merchant-password';
import { UpdateEmailSenderHandler } from 'src/application/usecase/merchant/update-email-sender';
import { UpdateEmailSenderInput } from 'src/application/usecase/merchant/update-email-sender/validate';
import { UpdateMerchantCommissionHandler } from 'src/application/usecase/merchant/update-merchant-commission';
import { AdminUpdateCommissionInput } from 'src/application/usecase/merchant/update-merchant-commission/validate';
import { VerifyEmailSenderInput } from 'src/application/usecase/merchant/verify-email-sender/validate';
import {
  ReportTransactionInput,
  ReportTransactionOutput,
} from 'src/application/usecase/transaction/report-transaction/validate';
import { BaseCreateOutput, BaseUpdateOutput } from 'src/application/usecase/validate';
import { MerchantCommissionRepository } from 'src/infrastructure/data/database/merchant-commission.repository';
import { ROOT_ROUTE } from '../../const';
import {
  BaseApiOutput,
  TransformInterceptor,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';
import { ModifyMerchantAdditionalDataInput } from 'src/application/usecase/additional-data/modify-merchant-additional-data/validate';
import { ModifyMerchantStandaloneAdditionalDataHandler } from 'src/application/usecase/additional-data/modify-merchant-standalone-additional-data';
import { ModifyMerchantStandaloneAdditionalDataInput } from 'src/application/usecase/additional-data/modify-merchant-standalone-additional-data/validate';

@ApiTags('admin/merchant')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/merchant`)
export class AdminV1MerchantController {
  constructor(
    @Inject(AdminUpdateMerchantHandler)
    private adminUpdateMerchantHandler: AdminUpdateMerchantHandler,
    @Inject(AdminCreateMerchantHandler)
    private adminCreateMerchantHandler: AdminCreateMerchantHandler,
    @Inject(GetMerchantHandler)
    private getMerchantHandler: GetMerchantHandler,
    @Inject(AdminListMerchantHandler)
    private adminListMerchantHandler: AdminListMerchantHandler,
    @Inject(ResetMerchantPasswordHandler)
    private resetMerchantPasswordHandler: ResetMerchantPasswordHandler,
    @Inject(VerifyEmailSenderHandler)
    private verifyEmailSenderHandler: VerifyEmailSenderHandler,
    @Inject(UpdateEmailSenderHandler)
    private updateEmailSenderHandler: UpdateEmailSenderHandler,
    @Inject(UpdateMerchantCommissionHandler)
    private updateMerchantCommissionHandler: UpdateMerchantCommissionHandler,
    @Inject(ListMerchantCommissionHandler)
    private listMerchantCommissionHandler: ListMerchantCommissionHandler,
    @Inject(MerchantCommissionRepository)
    private merchantCommissionRepository: MerchantCommissionRepository,
    @Inject(ListMerchantInvoiceHandler)
    private listMerchantInvoiceHandler: ListMerchantInvoiceHandler,
    @Inject(GetMerchantInvoiceHandler)
    private getMerchantInvoiceHandler: GetMerchantInvoiceHandler,
    @Inject(UpdateMerchantInvoiceHandler)
    private updateMerchantInvoiceHandler: UpdateMerchantInvoiceHandler,
    @Inject(CreateMerchantInvoiceByAmountHandler)
    private createMerchantInvoiceByAmountHandler: CreateMerchantInvoiceByAmountHandler,
    @Inject(ReportTransactionHandler)
    private reportTransactionHandler: ReportTransactionHandler,
    @Inject(ModifyMerchantAdditionalDataHandler)
    private modifyMerchantAdditionalDataHandler: ModifyMerchantAdditionalDataHandler,
    @Inject(DeleteMerchantAdditionalDataHandler)
    private deleteMerchantAdditionalDataHandler: DeleteMerchantAdditionalDataHandler,
    @Inject(ListMerchantAdditionalDataHandler)
    private listMerchantAdditionalDataHandler: ListMerchantAdditionalDataHandler,
    @Inject(ModifyMerchantStandaloneAdditionalDataHandler) private modifyMerchantStandaloneAdditionalDataHandler: ModifyMerchantStandaloneAdditionalDataHandler
  ) {}

  @Post('')
  @ApiResponse({
    status: 200,
    type: class AdminCreateMerchantOutputMap extends IntersectionType(
      AdminCreateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Create merchant',
  })
  async createMerchant(
    @Body() body: AdminCreateMerchantInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.adminCreateMerchantHandler.execute(body, user_id);
    return result.payload;
  }

  @Put('/:id/reset-password')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Update merchant',
  })
  async resetMerchantPassword(@Param('id') id: string, @Req() req: any) {
    const user_id = req.user.user_id;
    const result = await this.resetMerchantPasswordHandler.execute(id, user_id);
    return result.payload;
  }

  @Put('/:id/verify-sender')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Verify sender',
  })
  async verifySender(
    @Param('id') id: string,
    @Req() req: any,
    @Body() body: VerifyEmailSenderInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.verifyEmailSenderHandler.execute(
      body,
      id,
      user_id,
    );
    return result.payload;
  }

  @Delete('/commission/:id')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Delete commission',
  })
  async deleteCommission(@Param('id') id: string, @Req() req: any) {
    const result = await this.merchantCommissionRepository.deleteById(id);
    return true;
  }

  @Put('/:id/update-sender')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Update sender',
  })
  async updateSender(
    @Param('id') id: string,
    @Req() req: any,
    @Body() body: UpdateEmailSenderInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.updateEmailSenderHandler.execute(
      body,
      id,
      user_id,
    );
    return result.payload;
  }

  @Put('/:id/commission')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Update commission',
  })
  async updateCommission(
    @Param('id') id: string,
    @Req() req: any,
    @Body() body: AdminUpdateCommissionInput,
  ) {
    const user_id = req.user.user_id;
    const result = await this.updateMerchantCommissionHandler.execute(
      body,
      id,
      user_id,
    );
    return result.payload;
  }
  @Post('/:id/invoice')
  @ApiResponse({
    status: 200,
    type: class BaseCreateOutputMap extends IntersectionType(
      BaseCreateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create invoice',
  })
  async createMerchantInvoice(
    @Param('id') id: string,
    @Body() body: CreateMerchantInvoiceByAmountInput,
    @Req() req: any,
  ) {
    const adminId = req.user.user_id;
    const result = await this.createMerchantInvoiceByAmountHandler.execute(
      body,
      id,
      undefined,
      adminId,
    );
    return result.payload;
  }

  @Get('/:id/list-commission')
  @ApiResponse({
    status: 200,
    type: class ListMerchantCommissioOutputMap extends IntersectionType(
      ListMerchantCommissioOutput,
      BaseApiOutput,
    ) {},
    description: 'List bot trading',
  })
  async listBotCommission(
    @Param('id') id: string,
    @Query() query: ListMerchantCommissionInput,
  ) {
    const result = await this.listMerchantCommissionHandler.execute(query, id);
    return result.payload;
  }

  @Get('/:id/transaction-report')
  @ApiResponse({
    status: 200,
    type: class ReportTransactionOutputMap extends IntersectionType(
      ReportTransactionOutput,
      BaseApiOutput,
    ) {},
    description: 'Report transaction',
  })
  async report(
    @Param('id') id: string,
    @Query() query: ReportTransactionInput,
  ) {
    const result = await this.reportTransactionHandler.execute(query, id);
    return result.payload;
  }

  @Put('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateMerchantOutputMap extends IntersectionType(
      AdminUpdateMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Update merchant',
  })
  async updateMerchant(
    @Param('id') id: string,
    @Body() body: AdminUpdateMerchantInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.adminUpdateMerchantHandler.execute(
      body,
      id,
      user_id,
    );
    return result.payload;
  }

  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class AdminListMerchantOutputMap extends IntersectionType(
      AdminListMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'List merchant',
  })
  async listMerchant() {
    const result = await this.adminListMerchantHandler.execute();
    return result.payload;
  }

  // Merchant invoice
  @Get('list-invoice')
  @ApiResponse({
    status: 200,
    type: class ListMerchantInvoiceOutputMap extends IntersectionType(
      ListMerchantInvoiceOutput,
      BaseApiOutput,
    ) {},
    description: 'List invoice',
  })
  async listMerchantInvoice(@Query() query: ListMerchantInvoiceInput) {
    const result = await this.listMerchantInvoiceHandler.execute(query);
    return result.payload;
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminGetMerchantOutputMap extends IntersectionType(
      GetMerchantOutput,
      BaseApiOutput,
    ) {},
    description: 'Get one merchant',
  })
  async getMerchant(@Param('id') id: string) {
    const result = await this.getMerchantHandler.execute(id);
    return result.payload;
  }

  @Get('/invoice/:id')
  @ApiResponse({
    status: 200,
    type: class AdminGetMerchantInvoiceOutputMap extends IntersectionType(
      GetMerchantInvoiceIdOutput,
      BaseApiOutput,
    ) {},
    description: 'Get one invoice by id',
  })
  async getMerchantInvoice(@Param('id') id: string) {
    const result = await this.getMerchantInvoiceHandler.execute(id);
    return result.payload;
  }

  @Put('/:id/update-invoice')
  @ApiResponse({
    status: 200,
    type: class MerchantUpdateInvoiceOutputMap extends IntersectionType(
      MerchantUpdateInvoiceOutput,
      BaseApiOutput,
    ) {},
    description: 'Admin update merchant invoice',
  })
  async updateMerchantInvoice(
    @Param('id') id: string,
    @Body() body: MerchantUpdateInvoiceInput,
  ) {
    const result = await this.updateMerchantInvoiceHandler.execute(id, body);
    return result;
  }

  @Put('/:id/additional-data')
  @ApiResponse({
    status: 200,
    type: class BaseUpdateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create Merchant Additional Data',
  })
  async modifyAdditionalData(
    @Param('id') id: string,
    @Body() body: ModifyMerchantAdditionalDataInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.modifyMerchantAdditionalDataHandler.execute(
      body,
      id,
      user_id,
    );
    return result.payload;
  }
  @Delete('/additional-data/:id')
  @ApiResponse({
    status: 200,
    type: class BaseUpdateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Delete Merchant Additional Data',
  })
  async deleteAdditionalData(@Param('id') id: string) {
    const result = await this.deleteMerchantAdditionalDataHandler.execute(id);
    return result.payload;
  }

  @Put('/:id/standalone-additional-data')
  @ApiResponse({
    status: 200,
    type: class BaseUpdateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create Merchant Additional Data',
  })
  async modifyStandaloneAdditionalData(
    @Param('id') id: string,
    @Body() body: ModifyMerchantStandaloneAdditionalDataInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.modifyMerchantStandaloneAdditionalDataHandler.execute(
      body,
      id,
      user_id,
    );
    return result.payload;
  }

  @Get('/:id/additional-data/list')
  @ApiResponse({
    status: 200,
    type: class ListMerchantAdditionalDataOutputMap extends IntersectionType(
      ListMerchantAdditionalDataOutput,
      BaseApiOutput,
    ) {},
    description: 'List Additional Data',
  })
  async listAdditionalData(
    @Param('id') id: string,
    @Query() query: ListMerchantAdditionalDataInput,
  ) {
    const result = await this.listMerchantAdditionalDataHandler.execute(query, id);
    return result.payload;
  }

  @Get('/additional-data/:id')
  @ApiResponse({
    status: 200,
    type: class GetMerchantAdditionalDataOutputMap extends IntersectionType(
      GetMerchantAdditionalDataOutput,
      BaseApiOutput,
    ) {},
    description: 'Get Additional Data',
  })
  async getAdditionalData(
    @Param('id') id: string,
  ) {
    const result = await this.listMerchantAdditionalDataHandler.execute({}, id)
    return result.payload[0];
  }
}
