import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Req,
  Res,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { AdminCreateUserHandler } from 'src/application/usecase/auth/admin-create-user';
import {
  AdminCreateUserInput,
  AdminCreateUserOutput,
} from 'src/application/usecase/auth/admin-create-user/validate';
import { AdminLoginHandler } from 'src/application/usecase/auth/admin-login';
import {
  AdminLoginInput,
  AdminLoginOutput,
} from 'src/application/usecase/auth/admin-login/validate';
import { AdminVerifyEmailHandler } from 'src/application/usecase/auth/verify-email';
import {
  AdminVerifyEmailInput,
  AdminVerifyEmailOutput,
} from 'src/application/usecase/auth/verify-email/validate';
import { APP_CONFIG } from 'src/config';
import { WITHOUT_AUTHORIZATION } from 'src/domain/auth/types';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { AdminAuthGuard, AdminPermission } from '../auth';

@ApiTags('admin/auth')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/auth`)
export class AdminV1AuthController {
  constructor(
    @Inject(AdminLoginHandler) private adminLoginHandler: AdminLoginHandler,
    @Inject(AdminCreateUserHandler)
    private adminCreateUserHandler: AdminCreateUserHandler,
    @Inject(AdminVerifyEmailHandler)
    private adminVerifyEmailHandler: AdminVerifyEmailHandler,
  ) {}

  @AdminPermission(WITHOUT_AUTHORIZATION)
  @Post('login')
  @ApiResponse({
    status: 200,
    type: class AdminLoginOutputMap extends IntersectionType(
      AdminLoginOutput,
      BaseApiOutput,
    ) {},
    description: 'Login',
  })
  async login(@Body() body: AdminLoginInput) {
    const result = await this.adminLoginHandler.execute(body);
    return result.payload;
  }

  @Post('create')
  @ApiResponse({
    status: 200,
    type: class AdminCreateUserOutputMap extends IntersectionType(
      AdminCreateUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Create new admin',
  })
  async create(@Body() body: AdminCreateUserInput, @Req() req: any) {
    const user_id = req.user.user_id;
    const result = await this.adminCreateUserHandler.execute(body, user_id);
    return result.payload;
  }

  @AdminPermission(WITHOUT_AUTHORIZATION)
  @Get('verify-email/:token')
  @ApiResponse({
    status: 200,
    type: class AdminVerifyEmailOutputMap extends IntersectionType(
      AdminVerifyEmailOutput,
      BaseApiOutput,
    ) {},
    description: 'Admin verify email',
  })
  async verify(@Param() param: AdminVerifyEmailInput, @Res() res: Response) {
    await this.adminVerifyEmailHandler.execute(param);
    const urlReplace = `${APP_CONFIG.DASHBOARD_ADMIN_BASE_URL}/login`;
    res.redirect(urlReplace);
  }
}
