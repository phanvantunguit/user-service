import {
  UseGuards,
  UseInterceptors,
  Controller,
  Inject,
  Get,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  IntersectionType,
  ApiResponse,
} from '@nestjs/swagger';
import { MerchantListBotTradingHandler } from 'src/application';
import { MerchantListBotTradingOuput } from 'src/application/usecase/bot/merchant-list-bot-trading/validate';
import { ROOT_ROUTE } from '../../const';
import {
  BaseApiOutput,
  TransformInterceptor,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';

@ApiTags('admin/bot-trading')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/bot-trading`)
export class AdminV1BotTradingController {
  constructor(
    @Inject(MerchantListBotTradingHandler)
    private merchantListBotTradingHandler: MerchantListBotTradingHandler,
  ) {}
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class MerchantListBotTradingOuputMap extends IntersectionType(
      MerchantListBotTradingOuput,
      BaseApiOutput,
    ) {},
    description: 'List bot trading',
  })
  async listBot() {
    const result = await this.merchantListBotTradingHandler.execute();
    return result.payload;
  }
}
