import {
  Controller,
  Get,
  Inject,
  Param,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { AdminGetUserHandler } from 'src/application/usecase/event/admin-get-user';
import {
  AdminGetUserInput,
  AdminGetUserOutput,
} from 'src/application/usecase/event/admin-get-user/validate';
import { AdminListUserHandler } from 'src/application/usecase/event/admin-list-user';
import {
  AdminListUserInput,
  AdminListUserOutput,
} from 'src/application/usecase/event/admin-list-user/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';

@ApiTags('admin/user')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/user`)
export class AdminV1UserController {
  constructor(
    @Inject(AdminListUserHandler)
    private adminListUserHandler: AdminListUserHandler,
    @Inject(AdminGetUserHandler)
    private adminGetUserHandler: AdminGetUserHandler,
  ) {}

  @Get('list')
  @ApiResponse({
    status: 200,
    type: class AdminListUserOutputMap extends IntersectionType(
      AdminListUserOutput,
      BaseApiOutput,
    ) {},
    description: 'List user invite',
  })
  async login(@Query() query: AdminListUserInput) {
    const result = await this.adminListUserHandler.execute(query);
    return result.payload;
  }
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminGetUserOutputMap extends IntersectionType(
      AdminGetUserOutput,
      BaseApiOutput,
    ) {},
    description: 'Detail event',
  })
  async getEvent(@Param() param: AdminGetUserInput) {
    const result = await this.adminGetUserHandler.execute(param);
    return result.payload;
  }
}
