import {
  UseGuards,
  UseInterceptors,
  Controller,
  Inject,
  Body,
  Post,
  Get,
  Res,
  Query,
  Param,
  Req,
  Put,
  Delete,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  IntersectionType,
  ApiResponse,
} from '@nestjs/swagger';
import { CreateAppSettingHandler } from 'src/application/usecase/setting/create-app-setting';
import { CreateAppSettingInput } from 'src/application/usecase/setting/create-app-setting/validate';
import { DeleteAppSettingHandler } from 'src/application/usecase/setting/delete-app-setting';
import { GetAppSettingHandler } from 'src/application/usecase/setting/get-app-setting';
import { GetAppSettingOutput } from 'src/application/usecase/setting/get-app-setting/validate';
import { ListAppSettingHandler } from 'src/application/usecase/setting/list-app-setting';
import {
  ListAppSettingInput,
  ListAppSettingOutput,
} from 'src/application/usecase/setting/list-app-setting/validate';
import { UpdateAppSettingHandler } from 'src/application/usecase/setting/update-app-setting';
import { UpdateAppSettingInput } from 'src/application/usecase/setting/update-app-setting/validate';
import {
  BaseCreateOutput,
  BaseUpdateOutput,
} from 'src/application/usecase/validate';
import { ROOT_ROUTE } from '../../const';
import {
  BaseApiOutput,
  TransformInterceptor,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';

@ApiTags('admin/app-setting')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/app-setting`)
export class AdminV1SettingController {
  constructor(
    @Inject(ListAppSettingHandler)
    private listAppSettingHandler: ListAppSettingHandler,
    @Inject(GetAppSettingHandler)
    private getAppSettingHandler: GetAppSettingHandler,
    @Inject(CreateAppSettingHandler)
    private createAppSettingHandler: CreateAppSettingHandler,
    @Inject(UpdateAppSettingHandler)
    private updateAppSettingHandler: UpdateAppSettingHandler,
    @Inject(DeleteAppSettingHandler)
    private deleteAppSettingHandler: DeleteAppSettingHandler,
  ) {}

  @Get('list')
  @ApiResponse({
    status: 200,
    type: class ListAppSettingOutputMap extends IntersectionType(
      ListAppSettingOutput,
      BaseApiOutput,
    ) {},
    description: 'List app-setting',
  })
  async listAppSetting(@Query() query: ListAppSettingInput) {
    const result = await this.listAppSettingHandler.execute(query);
    return result.payload;
  }
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class GetAppSettingOutputMap extends IntersectionType(
      GetAppSettingOutput,
      BaseApiOutput,
    ) {},
    description: 'Get app-setting by id',
  })
  async getAppSetting(@Param('id') id: string) {
    const result = await this.getAppSettingHandler.execute(id);
    return result.payload;
  }
  @Post('')
  @ApiResponse({
    status: 200,
    type: class BaseCreateOutputMap extends IntersectionType(
      BaseCreateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create new app-setting',
  })
  async createAppSetting(@Body() body: CreateAppSettingInput, @Req() req: any) {
    const owner_created = req.user.user_id;
    const result = await this.createAppSettingHandler.execute({
      ...body,
      owner_created,
    });
    return result.payload;
  }
  @Put('/:id')
  @ApiResponse({
    status: 200,
    type: class BaseCreateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Update app-setting',
  })
  async updateAppSetting(
    @Param('id') id: string,
    @Body() body: UpdateAppSettingInput,
    @Req() req: any,
  ) {
    const owner_created = req.user.user_id;
    const result = await this.updateAppSettingHandler.execute({
      ...body,
      id,
      owner_created,
    });
    return result.payload;
  }
  @Delete('/:id')
  @ApiResponse({
    status: 200,
    type: class BaseCreateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Delete app-setting',
  })
  async deleteAppSetting(@Param('id') id: string) {
    const result = await this.deleteAppSettingHandler.execute(id);
    return result.payload;
  }
}
