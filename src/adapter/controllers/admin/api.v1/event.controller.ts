import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiResponse,
  ApiTags,
  IntersectionType,
} from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { AdminListEventHandler } from 'src/application/usecase/event/admin-list-event';
import {
  AdminListEventInput,
  AdminListEventOutput,
} from 'src/application/usecase/event/admin-list-event/validate';
import { AdminInviteEventHandler } from 'src/application/usecase/event/admin-invite';
import {
  AdminInviteEventInput,
  AdminInviteEventOutput,
} from 'src/application/usecase/event/admin-invite/validate';
import {
  AdminGetEventInput,
  AdminGetEventOutput,
} from 'src/application/usecase/event/admin-get-event/validate';
import { AdminGetEventHandler } from 'src/application/usecase/event/admin-get-event';
import {
  AdminCreateEventHandler,
  AdminDeleteEventHandler,
  AdminUpdateEventHandler,
  RemindAllDynamicHandler,
} from 'src/application';
import {
  AdminCreateEventInput,
  AdminCreateEventOutput,
} from 'src/application/usecase/event/admin-create-event/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  AdminInviteEventCSVInput,
  AdminInviteEventCSVOutput,
} from 'src/application/usecase/event/admin-invite-scv/validate';
import { AdminUpdateStatusInviteEventHandler } from 'src/application/usecase/event/admin-update-status-invite';
import {
  AdminUpdateStatusInviteEventInput,
  AdminUpdateStatusInviteEventOutput,
} from 'src/application/usecase/event/admin-update-status-invite/validate';
import { AdminInviteEventCSVHandler } from 'src/application/usecase/event/admin-invite-scv';
import {
  AdminUpdateEventInput,
  AdminUpdateEventOutput,
} from 'src/application/usecase/event/admin-update-event/validate';
import {
  RemindAllAttendEventInput,
  RemindAllAttendEventOutput,
} from 'src/application/usecase/event/remind-all-attend/validate';
import { RemindAllAttendEventHandler } from 'src/application/usecase/event/remind-all-attend';
import {
  AdminDeleteEventInput,
  AdminDeleteEventOutput,
} from 'src/application/usecase/event/admin-delete-event/validate';
import {
  RemindAllDynamicInput,
  RemindAllDynamicOutput,
} from 'src/application/usecase/event/remind-all-dynamic/validate';

@ApiTags('admin/event')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/event`)
export class AdminV1EventController {
  constructor(
    @Inject(AdminInviteEventHandler)
    private adminInviteEventHandler: AdminInviteEventHandler,
    @Inject(AdminListEventHandler)
    private adminListEventHandler: AdminListEventHandler,
    @Inject(AdminGetEventHandler)
    private adminGetEventHandler: AdminGetEventHandler,
    @Inject(AdminCreateEventHandler)
    private adminCreateEventHandler: AdminCreateEventHandler,
    @Inject(AdminUpdateStatusInviteEventHandler)
    private adminUpdateStatusInviteEventHandler: AdminUpdateStatusInviteEventHandler,
    @Inject(AdminInviteEventCSVHandler)
    private adminInviteEventCSVHandler: AdminInviteEventCSVHandler,
    @Inject(AdminUpdateEventHandler)
    private adminUpdateEventHandler: AdminUpdateEventHandler,
    @Inject(RemindAllAttendEventHandler)
    private remindAllAttendEventHandler: RemindAllAttendEventHandler,
    @Inject(AdminDeleteEventHandler)
    private adminDeleteEventHandler: AdminDeleteEventHandler,
    @Inject(RemindAllDynamicHandler)
    private remindAllDynamicHandler: RemindAllDynamicHandler,
  ) {}
  @Post('invite-csv')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        callback_confirm_url: { type: 'string' },
        callback_attend_url: { type: 'string' },
        event_id: { type: 'string' },
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiResponse({
    status: 200,
    type: class AdminInviteEventCSVOutputMap extends IntersectionType(
      AdminInviteEventCSVOutput,
      BaseApiOutput,
    ) {},
    description: 'Save user and send email invite',
  })
  @UseInterceptors(FileInterceptor('file'))
  async inviteCSV(
    @UploadedFile() file,
    @Body() body: AdminInviteEventCSVInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.adminInviteEventCSVHandler.execute(
      file,
      body,
      user_id,
    );
    return result.payload;
  }
  @Post('invite')
  @ApiResponse({
    status: 200,
    type: class AdminInviteEventOutputMap extends IntersectionType(
      AdminInviteEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Save user and send email invite',
  })
  async invite(@Body() body: AdminInviteEventInput, @Req() req: any) {
    const user_id = req.user.user_id;
    const result = await this.adminInviteEventHandler.execute(body, user_id);
    return result.payload;
  }
  @Put('invite')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateStatusInviteEventOutputMap extends IntersectionType(
      AdminUpdateStatusInviteEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Save user and send email invite',
  })
  async updateInvite(
    @Body() body: AdminUpdateStatusInviteEventInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.adminUpdateStatusInviteEventHandler.execute(
      body,
      user_id,
    );
    return result.payload;
  }
  @Put('/remind')
  @ApiResponse({
    status: 200,
    type: class RemindAllAttendEventOutputputMap extends IntersectionType(
      RemindAllAttendEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Remind attend event',
  })
  async remind(@Body() body: RemindAllAttendEventInput) {
    const result = await this.remindAllAttendEventHandler.execute(body);
    return result.payload;
  }
  @Put('/remind-dynaminc')
  @ApiResponse({
    status: 200,
    type: class RemindAllDynamicOutputMap extends IntersectionType(
      RemindAllDynamicOutput,
      BaseApiOutput,
    ) {},
    description: 'Remind attend event',
  })
  async remindDynamic(@Body() body: RemindAllDynamicInput) {
    const result = await this.remindAllDynamicHandler.execute(body);
    return result.payload;
  }
  @Post('')
  @ApiResponse({
    status: 200,
    type: class AdminCreateEventOutputMap extends IntersectionType(
      AdminCreateEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Create new event',
  })
  async create(@Body() body: AdminCreateEventInput, @Req() req: any) {
    const user_id = req.user.user_id;
    const result = await this.adminCreateEventHandler.execute(body, user_id);
    return result.payload;
  }
  @Put('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminUpdateEventOutputMap extends IntersectionType(
      AdminUpdateEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Update event',
  })
  async update(
    @Param('id') id: string,
    @Body() body: AdminUpdateEventInput,
    @Req() req: any,
  ) {
    const user_id = req.user.user_id;
    const result = await this.adminUpdateEventHandler.execute(
      id,
      body,
      user_id,
    );
    return result.payload;
  }
  @Delete('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminDeleteEventOutputMap extends IntersectionType(
      AdminDeleteEventOutput,
      BaseApiOutput,
    ) {},
    description: 'List event',
  })
  async deleteEvent(@Param() param: AdminDeleteEventInput) {
    const result = await this.adminDeleteEventHandler.execute(param);
    return result.payload;
  }

  @Get('list')
  @ApiResponse({
    status: 200,
    type: class AdminListEventOutputMap extends IntersectionType(
      AdminListEventOutput,
      BaseApiOutput,
    ) {},
    description: 'List event',
  })
  async listEvent(@Query() query: AdminListEventInput) {
    const result = await this.adminListEventHandler.execute(query);
    return result.payload;
  }
  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class AdminGetEventOutputMap extends IntersectionType(
      AdminGetEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Detail event',
  })
  async getEvent(@Param() param: AdminGetEventInput) {
    const result = await this.adminGetEventHandler.execute(param);
    return result.payload;
  }
}
