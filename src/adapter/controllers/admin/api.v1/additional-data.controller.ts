import {
  UseGuards,
  UseInterceptors,
  Controller,
  Inject,
  Get,
  Post,
  Param,
  Req,
  Body,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  IntersectionType,
  ApiResponse,
} from '@nestjs/swagger';
import { ModifyAdditionalDataHandler } from 'src/application';
import { DeleteAdditionalDataHandler } from 'src/application/usecase/additional-data/delete-additional-data';
import { GetAdditionalDataHandler } from 'src/application/usecase/additional-data/get-additional-data';
import { GetAdditionalDataOutput } from 'src/application/usecase/additional-data/get-additional-data/validate';
import { ListAdditionalDataHandler } from 'src/application/usecase/additional-data/list-additional-data';
import {
  ListAdditionalDataInput,
  ListAdditionalDataOutput,
} from 'src/application/usecase/additional-data/list-additional-data/validate';
import { ModifyAdditionalDataInput } from 'src/application/usecase/additional-data/modify-additional-data/validate';
import {
  BaseCreateOutput,
  BaseUpdateOutput,
} from 'src/application/usecase/validate';
import { ROOT_ROUTE } from '../../const';
import {
  BaseApiOutput,
  TransformInterceptor,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';

@ApiTags('admin/additional-data')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/additional-data`)
export class AdminAdditionalDataController {
  constructor(
    @Inject(ModifyAdditionalDataHandler)
    private modifyAdditionalDataHandler: ModifyAdditionalDataHandler,
    @Inject(DeleteAdditionalDataHandler)
    private deleteAdditionalDataHandler: DeleteAdditionalDataHandler,
    @Inject(ListAdditionalDataHandler)
    private listAdditionalDataHandler: ListAdditionalDataHandler,
    @Inject(GetAdditionalDataHandler)
    private getAdditionalDataHandler: GetAdditionalDataHandler,
  ) {}

  @Post('/')
  @ApiResponse({
    status: 200,
    type: class BaseCreateOutputMap extends IntersectionType(
      BaseCreateOutput,
      BaseApiOutput,
    ) {},
    description: 'Create Additional Data',
  })
  async createAdditionalData(@Body() body: ModifyAdditionalDataInput) {
    const result = await this.modifyAdditionalDataHandler.execute(body);
    return result.payload;
  }
  @Put('/:id')
  @ApiResponse({
    status: 200,
    type: class BaseUpdateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Update Additional Data',
  })
  async updateAdditionalData(
    @Param('id') id: string,
    @Body() body: ModifyAdditionalDataInput,
  ) {
    const result = await this.modifyAdditionalDataHandler.execute(body, id);
    return result.payload;
  }
  @Delete('/:id')
  @ApiResponse({
    status: 200,
    type: class BaseUpdateOutputMap extends IntersectionType(
      BaseUpdateOutput,
      BaseApiOutput,
    ) {},
    description: 'Delete Additional Data',
  })
  async deleteAdditionalData(@Param('id') id: string) {
    const result = await this.deleteAdditionalDataHandler.execute(id);
    return result.payload;
  }
  @Get('/list')
  @ApiResponse({
    status: 200,
    type: class ListAdditionalDataOutputMap extends IntersectionType(
      ListAdditionalDataOutput,
      BaseApiOutput,
    ) {},
    description: 'List Additional Data',
  })
  async listAdditionalData(@Query() query: ListAdditionalDataInput) {
    const result = await this.listAdditionalDataHandler.execute(query);
    return result.payload;
  }

  @Get('/:id')
  @ApiResponse({
    status: 200,
    type: class GetAdditionalDataOutputMap extends IntersectionType(
      GetAdditionalDataOutput,
      BaseApiOutput,
    ) {},
    description: 'Get Additional Data',
  })
  async getAdditionalData(@Param('id') id: string) {
    const result = await this.getAdditionalDataHandler.execute(id);
    return result.payload;
  }
}
