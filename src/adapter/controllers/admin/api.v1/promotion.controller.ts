import {
  UseGuards,
  UseInterceptors,
  Controller,
  Inject,
  Body,
  Post,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  IntersectionType,
  ApiResponse,
} from '@nestjs/swagger';
import { AdminSendPromotionHandler } from 'src/application';
import {
  AdminSendPromotionInput,
  AdminSendPromotionOutput,
} from 'src/application/usecase/promotion/admin-send-promotion/validate';
import { ROOT_ROUTE } from '../../const';
import {
  BaseApiOutput,
  TransformInterceptor,
} from '../../transform.interceptor';
import { AdminAuthGuard } from '../auth';

@ApiTags('admin/promotion')
@ApiBearerAuth()
@UseGuards(AdminAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1_admin}/promotion`)
export class AdminV1PromotionController {
  constructor(
    @Inject(AdminSendPromotionHandler)
    private adminSendPromotionHandler: AdminSendPromotionHandler,
  ) {}

  @Post('send')
  @ApiResponse({
    status: 200,
    type: class AdminInviteEventOutputMap extends IntersectionType(
      AdminSendPromotionOutput,
      BaseApiOutput,
    ) {},
    description: 'Send Promotion for user',
  })
  async send(@Body() body: AdminSendPromotionInput) {
    const result = await this.adminSendPromotionHandler.execute(body);
    return result.payload;
  }
}
