import { ExecutionContext, Injectable, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { APP_CONFIG } from 'src/config';
import {
  accessDeniedError,
  unauthorizedError,
} from 'src/utils/exceptions/throw.exception';
import { verifyTokenSecret } from 'src/utils/hash.util';
import { SetMetadata } from '@nestjs/common';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { WITHOUT_AUTHORIZATION } from 'src/domain/auth/types';

export const AdminPermission = (permission: string) =>
  SetMetadata('permission', permission);

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const permission = this.reflector.get<string>(
      'permission',
      context.getHandler(),
    );
    if (permission === WITHOUT_AUTHORIZATION) {
      return true;
    }
    const request = context.switchToHttp().getRequest();

    const authHeader: string | undefined = request.get('Authorization');
    if (authHeader) {
      const [type, token] = authHeader.split(' ');
      if (type.toLowerCase() === 'bearer' && token) {
        let decode;
        try {
          decode = await verifyTokenSecret(token, APP_CONFIG.SECRET_KEY);
        } catch (error) {
          unauthorizedError('Token', ERROR_CODE.UNAUTHORIZED);
        }
        // const ipAddress =
        //   request.headers['x-forwarded-for'] ||
        //   request.socket.remoteAddress ||
        //   null
        // await this.sessionService.verifySession(decode)
        if (decode.admin) {
          request.user = decode;
          return true;
        }
        // if (decode.user_id && decode.roles && decode.roles.length > 0) {
        //   const userRoles = await this.userRepo.findAuthUserRole({
        //     user_id: decode.user_id,
        //   });
        //   const roleIds = userRoles.map((e) => e.auth_role_id);
        //   const roles = await this.permissionRepo.findAuthRoleByIds(roleIds);
        //   const isValid = roles.some((r) =>
        //     r.root.permissions.some((p) => p.permission_id === permission),
        //   );
        //   if (isValid) {
        //     request.user = decode;
        //     return true;
        //   }
        // }
        accessDeniedError();
      }
    }
    unauthorizedError('Token', ERROR_CODE.UNAUTHORIZED);
  }
}
