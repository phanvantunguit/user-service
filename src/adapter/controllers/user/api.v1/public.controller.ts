import {
  Controller,
  Inject,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import { TransformInterceptor } from '../../transform.interceptor';
import { StorageService } from 'src/infrastructure/services';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { ERROR_CODE } from 'src/utils/exceptions/const';

@ApiTags('public')
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1}`)
export class UserPublicController {
  constructor(@Inject(StorageService) private storageService: StorageService) {}
  @Post('/upload')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file) {
    if (file) {
      const file_url = await this.storageService.upload(file);
      return { file_url };
    } else {
      badRequestError('Upload', ERROR_CODE.BAD_REQUEST);
    }
  }
}
