import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { ApiResponse, ApiTags, IntersectionType } from '@nestjs/swagger';
import { ROOT_ROUTE } from 'src/adapter/controllers/const';
import {
  UserCheckParamHandler,
  UserRegisterEventHandler,
} from 'src/application';
import {
  UserCheckParamInput,
  UserCheckParamOutput,
} from 'src/application/usecase/event/user-check-param/validate';
import { UserCheckinEventHandler } from 'src/application/usecase/event/user-checkin';
import {
  UserCheckinEventInput,
  UserCheckinEventOutput,
} from 'src/application/usecase/event/user-checkin/validate';
import { UserConfirmEventHandler } from 'src/application/usecase/event/user-confirm';
import {
  UserConfirmEventInput,
  UserConfirmEventOutput,
} from 'src/application/usecase/event/user-confirm/validate';
import { UserGetEventHandler } from 'src/application/usecase/event/user-get-event';
import {
  UserGetEventInput,
  UserGetEventOutput,
} from 'src/application/usecase/event/user-get-event/validate';
import {
  UserRegisterEventInput,
  UserRegisterEventOutput,
} from 'src/application/usecase/event/user-register/validate';
import {
  TransformInterceptor,
  BaseApiOutput,
} from '../../transform.interceptor';

@ApiTags('public/event')
@UseInterceptors(TransformInterceptor)
@Controller(`${ROOT_ROUTE.api_v1}/event`)
export class UserV1EventController {
  constructor(
    @Inject(UserRegisterEventHandler)
    private userRegisterEventHandler: UserRegisterEventHandler,
    @Inject(UserConfirmEventHandler)
    private userConfirmEventHandler: UserConfirmEventHandler,
    @Inject(UserCheckinEventHandler)
    private userCheckinEventHandler: UserCheckinEventHandler,
    @Inject(UserGetEventHandler)
    private userGetEventHandler: UserGetEventHandler,
    @Inject(UserCheckParamHandler)
    private userCheckParamHandler: UserCheckParamHandler,
  ) {}
  @Post('register')
  @ApiResponse({
    status: 200,
    type: class UserRegisterEventOutputMap extends IntersectionType(
      UserRegisterEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Save user and send email invite',
  })
  async register(@Body() body: UserRegisterEventInput) {
    const result = await this.userRegisterEventHandler.execute(body);
    return result.payload;
  }
  @Post('confirm')
  @ApiResponse({
    status: 200,
    type: class UserConfirmEventOutputMap extends IntersectionType(
      UserConfirmEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Save status confirm',
  })
  async confirm(@Body() query: UserConfirmEventInput) {
    const result = await this.userConfirmEventHandler.execute(query);
    return result.payload;
  }
  @Post('checkin')
  @ApiResponse({
    status: 200,
    type: class UserCheckinEventOutputMap extends IntersectionType(
      UserCheckinEventOutput,
      BaseApiOutput,
    ) {},
    description: 'User checkin',
  })
  async attend(@Body() query: UserCheckinEventInput) {
    const result = await this.userCheckinEventHandler.execute(query);
    return result.payload;
  }
  @Put('/check-param')
  @ApiResponse({
    status: 200,
    type: class UserCheckParamOutputMap extends IntersectionType(
      UserCheckParamOutput,
      BaseApiOutput,
    ) {},
    description: 'Detail event',
  })
  async checkParam(@Body() body: UserCheckParamInput) {
    const result = await this.userCheckParamHandler.execute(body);
    return result.payload;
  }
  @Get('/:code')
  @ApiResponse({
    status: 200,
    type: class UserGetEventOutputMap extends IntersectionType(
      UserGetEventOutput,
      BaseApiOutput,
    ) {},
    description: 'Detail event',
  })
  async getEvent(@Param() param: UserGetEventInput) {
    const result = await this.userGetEventHandler.execute(param);
    return result.payload;
  }
}
