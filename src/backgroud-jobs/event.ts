import { Inject, Injectable } from '@nestjs/common';
import { CronExpression } from '@nestjs/schedule';
import {
  RemindAllAttendEventHandler,
  RemindDynamicHandler,
} from 'src/application';
import { APP_CONFIG } from 'src/config';
import { EventDomain } from 'src/domain/event/event.domain';
import { PAYMENT_STATUS } from 'src/domain/event/types';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { UserRemindRepository } from 'src/infrastructure/data/database/user-remind.repository';
import { BackgroudJob } from '.';

@Injectable()
export class EventJob {
  constructor(
    private backgroudJob: BackgroudJob,
    @Inject(EventRepository) private eventRepository: EventRepository,
    @Inject(RemindAllAttendEventHandler)
    private remindAllAttendEventHandler: RemindAllAttendEventHandler,
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(RemindDynamicHandler)
    private remindDynamicHandler: RemindDynamicHandler,
  ) {}
  run() {
    this.backgroudJob.addNewJob(
      this.remindAttendEvent.name,
      this.remindAttendEvent.bind(this),
      CronExpression.EVERY_5_MINUTES,
      this.remindAttendEvent.bind(this),
    );
    // this.backgroudJob.addNewJob(
    //   this.remindPaymentEvent.name,
    //   this.remindPaymentEvent.bind(this),
    //   CronExpression.EVERY_12_HOURS,
    //   this.remindPaymentEventFisrt.bind(this),
    // );
  }
  async remindAttendEvent() {
    const to = new Date();
    const events = await this.eventRepository.getEventToRemind({
      remind: '0',
      to: to.valueOf(),
    });
    this.backgroudJob.logger.log(
      `${this.remindAttendEvent.name} events number: ${events.length}`,
    );
    for (const event of events) {
      const result = await this.remindAllAttendEventHandler.execute({
        event_id: event.id,
      });
      await this.eventRepository.save({ id: event.id, remind: true });
      this.backgroudJob.logger.log(
        `${this.remindAttendEvent.name}: ${event.name} was finished ${result.payload}`,
      );
    }
    this.backgroudJob.logger.log(`${this.remindAttendEvent.name} was finished`);
  }

  async remindPaymentEventFisrt() {
    const event = (await this.eventRepository.findOne({
      code: 'COINMAP_CHARITY_HEATMAP',
    })) as EventDomain;
    if (event) {
      const to = new Date();
      const users = (await this.userEventRepository.getUserByDate({
        to: to.setDate(to.getDate() - 1),
        event_id: event.id,
      })) as UserEventDomain[];
      for (const user of users) {
        if (user.payment_status !== PAYMENT_STATUS.COMPLETED) {
          await this.remindDynamicHandler.execute({
            user,
            event,
            template_id: APP_CONFIG.REMIND_PAYMENT_TEMPLATE_ID,
          });
        }
      }
      this.backgroudJob.logger.log(
        `${this.remindPaymentEvent.name} was finished`,
      );
    }
  }

  async remindPaymentEvent() {
    const event = (await this.eventRepository.findOne({
      code: 'COINMAP_CHARITY_HEATMAP',
    })) as EventDomain;
    if (event) {
      const to = new Date();
      const from = new Date();
      const users = (await this.userEventRepository.getUserByDate({
        from: from.setDate(from.getDate() - 2),
        to: to.setDate(to.getDate() - 1),
        event_id: event.id,
      })) as UserEventDomain[];
      for (const user of users) {
        if (user.payment_status !== PAYMENT_STATUS.COMPLETED) {
          await this.remindDynamicHandler.execute({
            user,
            event,
            template_id: APP_CONFIG.REMIND_PAYMENT_TEMPLATE_ID,
          });
        }
      }
      this.backgroudJob.logger.log(
        `${this.remindPaymentEvent.name} was finished`,
      );
    }
  }
}
