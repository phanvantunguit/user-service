import { Inject, Injectable } from '@nestjs/common';
import { CronExpression } from '@nestjs/schedule';
import { CreateMerchantInvoiceHandler } from 'src/application';
import { MERCHANT_STATUS } from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { BackgroudJob } from '.';

@Injectable()
export class MerchantJob {
  constructor(
    private backgroudJob: BackgroudJob,
    @Inject(CreateMerchantInvoiceHandler)
    private createMerchantInvoiceHandler: CreateMerchantInvoiceHandler,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  run() {
    this.backgroudJob.addNewJob(
      this.createMerchantInvoice.name,
      this.createMerchantInvoice.bind(this),
      CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_NOON,
      this.createMerchantInvoice.bind(this),
    );
  }
  async createMerchantInvoice() {
    const merchants = await this.merchantRepository.find({
      status: MERCHANT_STATUS.ACTIVE,
    });
    for (let m of merchants) {
      const invoices = await this.createMerchantInvoiceHandler.execute(
        m.id,
        m.code,
      );
      if (invoices) {
        console.log('MerchantJob createMerchantInvoice:', invoices.length);
      }
    }
  }
}
