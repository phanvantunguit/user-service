import { Injectable, Logger } from '@nestjs/common';
import { CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';

@Injectable()
export class BackgroudJob {
  readonly logger = new Logger(BackgroudJob.name);
  constructor(private schedulerRegistry: SchedulerRegistry) {}

  stopCronJob(name: string) {
    const job = this.schedulerRegistry.getCronJob(name);
    if (job) {
      job.stop();
    }
  }

  startCronJob(name: string) {
    const job = this.schedulerRegistry.getCronJob(name);
    if (!job.running) {
      job.start();
    }
  }

  deleteCronJob(name: string) {
    this.schedulerRegistry.deleteCronJob(name);
  }

  addNewJob(name: string, taks: any, time: CronExpression, preTask?: any) {
    this.logger.log(`${name} was Scheduled`);
    if (preTask) {
      this.logger.log(`${preTask.name} is runing...`);
      preTask();
    }
    const job = new CronJob(time, () => {
      this.logger.log(`${taks.name} is runing...`);
      taks();
    });
    this.schedulerRegistry.addCronJob(name, job);
    job.start();
  }
}
