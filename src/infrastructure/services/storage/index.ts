import { Inject } from '@nestjs/common';
import { GCloudTransport } from './google.transport';

export class StorageService {
  constructor(
    @Inject(GCloudTransport)
    private gCloudTransport: GCloudTransport,
  ) {}
  async upload(file): Promise<string> {
    return this.gCloudTransport.upload(file);
  }
  async uploadBuffer(data: string, fileName: string): Promise<string> {
    return this.gCloudTransport.uploadBuffer(data, fileName);
  }
}
