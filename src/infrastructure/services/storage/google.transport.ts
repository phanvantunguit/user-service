import { APP_CONFIG } from 'src/config';
import { Storage } from '@google-cloud/storage';
import { SERVICE_NAME } from 'src/const/app-setting';

export class GCloudTransport {
  storage = new Storage({
    projectId: APP_CONFIG.GOOGLE_CLOUD_PROJECT_ID || 'coinmap-k8s',
  });
  //   bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET)
  bucket = this.storage.bucket(
    APP_CONFIG.GOOGLE_CLOUD_STORAGE_BUCKET || 'static-dev.cextrading.io',
  );
  async upload(file): Promise<string> {
    return new Promise((resolve, reject) => {
      const mimetype = file.originalname.split('.').pop();
      // const fileName = `${Date.now()}.${mimetype}`;
      const fileName = `images/${SERVICE_NAME}/${Date.now()}.${mimetype}`;
      const blob = this.bucket.file(fileName);
      const blobStream = blob.createWriteStream();
      blobStream.on('error', (err) => {
        reject(err);
      });
      blobStream.on('finish', () => {
        resolve(`https://${this.bucket.name}/${blob.name}`);
      });

      blobStream.end(file.buffer);
    });
  }
  async uploadBuffer(data: string, fileName: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const base64EncodedString = data.replace(/^data:\w+\/\w+;base64,/, '');
      const fileBuffer = Buffer.from(base64EncodedString, 'base64');
      const blob = this.bucket.file(fileName);
      const blobStream = blob.createWriteStream();
      blobStream.on('error', (err) => {
        reject(err);
      });
      blobStream.on('finish', () => {
        resolve(`https://${this.bucket.name}/${blob.name}`);
      });

      blobStream.end(fileBuffer);
    });
  }
}
