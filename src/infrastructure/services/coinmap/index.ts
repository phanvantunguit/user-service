import { APP_CONFIG } from 'src/config';
import { ITEM_STATUS } from 'src/domain/transaction/types';
import { encodeBase64 } from 'src/utils/hash.util';
import { requestDELETE, requestPUT } from 'src/utils/http-transport.util';
export class CoinmapService {
  async updateUserBotTradingStatus(param: {
    id: string;
    status: ITEM_STATUS;
    merchant_code: string;
    password: string;
  }): Promise<{ status: boolean; response: any }> {
    const { id, status, merchant_code, password } = param;
    try {
      const data = {
        status,
      };
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeBase64(`${merchant_code}:${password}`)}`,
      };
      const url = `${APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/admin/user/tbot/${id}`;
      const result = await requestPUT(url, data, headers);
      return { status: true, response: result };
    } catch (error) {
      console.log("error", error)
      return { status: false, response: error };
    }
  }
  async deleteUserBotTrading(param: {
    id: string;
    merchant_code: string;
    password: string;
  }): Promise<{ status: boolean; response: any }> {
    const { id, merchant_code, password } = param;
    try {
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeBase64(`${merchant_code}:${password}`)}`,
      };
      const url = `${APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/admin/user/tbot/${id}`;
      const result = await requestDELETE(url, headers);
      return { status: true, response: result };
    } catch (error) {
      console.log("error", error)
      return { status: false, response: error };
    }
  }
}
