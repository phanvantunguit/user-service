import { Inject } from '@nestjs/common';
import * as handlebars from 'handlebars';
import { APP_CONFIG } from 'src/config';
import { SendGridTransport } from './sendgrid.transport';
import { EMAIL_SUBJECT, IEmailDynamicTemplate } from './types';
import { INVITE_CODE_TYPE } from 'src/domain/event/types';
import { emailVerifyAccountCreate } from 'src/templates/email-verify-account-create';
import { emailRemindEventVip } from 'src/templates/email-remind-event-vip';
import { emailRemindEvent } from 'src/templates/email-remind-event';
import { emailSendMerchantPassword } from 'src/templates/email-send-merchant-password';
import { emailSendOTPWallet } from 'src/templates/email-send-otp-wallet';
import { emailVerify } from 'src/templates/email-verify';
export class MailService {
  constructor(
    @Inject(SendGridTransport) private sendGridTransport: SendGridTransport,
  ) {}
  async sendEmailConfirmAccount(
    email: string,
    token: string,
    password: string,
    from_email?: string,
    from_name?: string,
    logo_url?: string,
    header_url?: string,
    main_content?: string,
    twitter_url?: string,
    youtube_url?: string,
    facebook_url?: string,
    telegram_url?: string,
    company_name?: string,
    fullname?: string
  ): Promise<any> {
    const logoUrl =
      logo_url || 'https://static.cextrading.io/images/1/logo.png';
    const headerUrl =
      header_url || 'https://static.cextrading.io/images/1/header-bg.png';
    const companyName = company_name || 'Coinmap';
    const companyNameUpperCase = company_name || 'COINMAP';
    const mainContent =
      main_content ||
      `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our CEX and DEX Trading applications will help you trade safety and in comfort.`;
      let footerContent
      if (company_name) {
        footerContent = `
          ${company_name.toUpperCase()} is an organization providing trading-related services for professional investors.\n
          ${company_name.toUpperCase()} prioritizes the benefit of high-end technology applied to Trading; we are committed to being top in providing trading-related services and helping our investors optimize profits.
          `
      } else {
        footerContent =
          'Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.'
      }
    const twitterUrl = twitter_url || 'https://twitter.com/CoinmapTrading';
    const facebookUrl =
      facebook_url || 'https://www.facebook.com/CoinmapTrading';
    const youtubeUrl = youtube_url || 'https://www.youtube.com/c/8xTRADING';
    const telegramUrl = telegram_url || 'https://t.me/trading8x';

    const template = handlebars.compile(emailVerify);
    const replacements = {
      linkVerify: `${APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/user/verify-email/${token}`,
      password,
      logoUrl,
      headerUrl,
      companyName,
      companyNameUpperCase: companyNameUpperCase.toUpperCase(),
      mainContent,
      footerContent,
      twitterUrl,
      facebookUrl,
      youtubeUrl,
      telegramUrl,
      fullname
    };
    const html = template(replacements);
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.verify_email,
      html,
    };
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    );
  }
  // async sendEmailConfirmEvent(
  //   email: string,
  //   name: string,
  //   token: string,
  //   callbackUrl: string,
  //   qrcode_url: string,
  //   code: string,
  // ): Promise<any> {
  //   const vip = code.includes(INVITE_CODE_TYPE.VIP);
  //   const template = vip
  //     ? handlebars.compile(emailConfirmEventVip)
  //     : handlebars.compile(emailConfirmEvent);
  //   const baseUrl = callbackUrl.includes('?')
  //     ? callbackUrl + `&token=${token}`
  //     : callbackUrl + `?token=${token}`;
  //   const replacements = {
  //     name,
  //     linkApprove: `${baseUrl}&status=${EVENT_CONFIRM_STATUS.APPROVED}`,
  //     linkReject: `${baseUrl}&status=${EVENT_CONFIRM_STATUS.REJECTED}`,
  //     qrcode_url,
  //     code,
  //   };
  //   const html = template(replacements);
  //   const dataSendEmail = {
  //     to: email,
  //     subject: EMAIL_SUBJECT.confirm_attend_to_event,
  //     html,
  //   };
  //   return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
  // }
  async sendEmailCreateAccount(
    email: string,
    token: string,
    password: string,
    from_email?: string,
    from_name?: string,
  ): Promise<any> {
    const txtPassword = `Your password: ${password}`;
    const template = handlebars.compile(emailVerifyAccountCreate);
    const replacements = {
      linkVerify: `${APP_CONFIG.USER_ROLE_BASE_URL}/api/v1/user/verify-email/${token}`,
      password: txtPassword,
    };
    const html = template(replacements);
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.verify_email,
      html,
    };
    return this.sendGridTransport.sendGridSendEmail(
      dataSendEmail,
      from_email,
      from_name,
    );
  }
  async sendEmailCreateMerchant(email: string, password: string): Promise<any> {
    const txtPassword = `Your password: ${password}`;
    const txtAccount = `Your account: ${email}`;
    const template = handlebars.compile(emailSendMerchantPassword);
    const replacements = {
      title: 'Account Information',
      account: txtAccount,
      password: txtPassword,
    };
    const html = template(replacements);
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.create_account,
      html,
    };
    return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
  }
  async sendEmailResetPassword(email: string, password: string): Promise<any> {
    const txtPassword = `Your password: ${password}`;
    const txtAccount = `Your account: ${email}`;
    const template = handlebars.compile(emailSendMerchantPassword);
    const replacements = {
      title: 'Reset Password',
      account: txtAccount,
      password: txtPassword,
    };
    const html = template(replacements);
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.reset_password,
      html,
    };
    return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
  }
  async sendEmailRemindEvent(email: string, code: string): Promise<any> {
    const vip = code.includes(INVITE_CODE_TYPE.VIP);
    const template = vip
      ? handlebars.compile(emailRemindEventVip)
      : handlebars.compile(emailRemindEvent);
    const html = template({});
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.remind_attend_to_event,
      html,
    };
    return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
  }
  async sendEmailDynamicTemplate(
    email: string,
    data: IEmailDynamicTemplate,
    template_id: string,
    send_at?: number,
    from_email?: string,
    from_name?: string,
  ): Promise<any> {
    return this.sendGridTransport.sendGridSendDynamicTemplate({
      to: email,
      dynamic_template_data: data,
      template_id,
      send_at,
      from_email,
      from_name,
    });
  }
  async sendRequestVerifySender(param: {
    nickname: string;
    from_email: string;
    from_name: string;
    reply_to: string;
    address: string;
    city: string;
    country: string;
  }) {
    return this.sendGridTransport.sendRequestVerifySender(param);
  }
  async sendVerifySender(param: { token: string }) {
    return this.sendGridTransport.sendVerifySender(param);
  }

  async updateSender(param: {
    id: string;
    nickname: string;
    from_name: string;
    reply_to: string;
    address: string;
    city: string;
    country: string;
  }) {
    return this.sendGridTransport.updateSender(param);
  }
  async deleteSender(param: { id: string }) {
    return this.sendGridTransport.deleteSender(param);
  }

  async sendEmailOTP(
    email: string,
    otp: string,
    wallet_address: string,
  ): Promise<any> {
    const txtOTP = `Your OTP: ${otp}`;
    const txtWalletAddress = `Your Wallet: ${wallet_address}`;
    const template = handlebars.compile(emailSendOTPWallet);
    const replacements = {
      otp: txtOTP,
      wallet_address: txtWalletAddress,
    };
    const html = template(replacements);
    const dataSendEmail = {
      to: email,
      subject: EMAIL_SUBJECT.send_otp,
      html,
    };
    return this.sendGridTransport.sendGridSendEmail(dataSendEmail);
  }
}
