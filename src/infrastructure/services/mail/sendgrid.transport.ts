import { ISendDynamincTemplate, ISendEmail } from './types';
import {
  requestPOST,
  requestGET,
  requestPATCH,
  requestDELETE,
} from 'src/utils/http-transport.util';
import { APP_CONFIG } from 'src/config';
export class SendGridTransport {
  async sendGridSendEmail(
    params: ISendEmail,
    email?: string,
    name?: string,
  ): Promise<any> {
    const data = {
      from: {
        email: email || APP_CONFIG.SENDGRID_SENDER_EMAIL,
        name: name || APP_CONFIG.SENDGRID_SENDER_NAME,
      },
      personalizations: [
        {
          to: [
            {
              email: params.to,
            },
          ],
        },
      ],
      subject: params.subject,
      content: [
        {
          type: 'text/html',
          value: params.html,
        },
      ],
    };
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/mail/send`;
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    };
    try {
      const result = await requestPOST(url, data, headers);
      console.log('SendGrid result', result);
      return true;
    } catch (error) {
      console.log('SendGrid ERROR', error);
      return false;
    }
  }
  async sendGridSendDynamicTemplate(params: ISendDynamincTemplate) {
    console.log("params", params)
    const data = {
      from: {
        email: params.from_email || APP_CONFIG.SENDGRID_SENDER_EMAIL,
        name: params.from_name || APP_CONFIG.SENDGRID_SENDER_NAME,
      },
      personalizations: [
        {
          to: [
            {
              email: params.to,
            },
          ],
          dynamic_template_data: params.dynamic_template_data,
        },
      ],
      template_id: params.template_id,
    };
    if (params.send_at) {
      data['send_at'] = params.send_at;
    }
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/mail/send`;
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    };
    try {
      const result = await requestPOST(url, data, headers);
      console.log('SendGrid result', result);
      return true;
    } catch (error) {
      console.log('SendGrid ERROR', error);
      return false;
    }
  }
  async sendRequestVerifySender(param: {
    nickname: string;
    from_email: string;
    from_name: string;
    reply_to: string;
    address: string;
    city: string;
    country: string;
  }) {
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders`;
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    };
    try {
      console.log('sendRequestVerifySender data', param);
      const result = await requestPOST(url, param, headers);
      console.log('SendGrid result', result);
      return result;
    } catch (error) {
      console.log('SendGrid ERROR', error);
      return false;
    }
  }
  async sendVerifySender(param: { token: string }) {
    const { token } = param;
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders/verify/${token}`;
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    };
    try {
      const result = await requestGET(url, headers);
      console.log('SendGrid result', result);
      return true;
    } catch (error) {
      console.log('SendGrid ERROR', error);
      return false;
    }
  }

  async updateSender(param: {
    id: string;
    nickname: string;
    from_name: string;
    reply_to: string;
    address: string;
    city: string;
    country: string;
  }) {
    const { id, nickname, from_name, reply_to, address, city, country } = param;
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders/${id}`;
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    };
    try {
      const data = {
        nickname,
        from_name,
        reply_to,
        address,
        city,
        country,
      };
      const result = await requestPATCH(url, data, headers);
      console.log('SendGrid result', result);
      return result;
    } catch (error) {
      console.log('SendGrid ERROR', error);
      return false;
    }
  }

  async deleteSender(param: { id: string }) {
    const { id } = param;
    const url = `${APP_CONFIG.SENDGRID_BASE_URL}/v3/verified_senders/${id}`;
    const headers = {
      Authorization: `Bearer ${APP_CONFIG.SENDGRID_API_TOKEN}`,
      'Content-Type': 'application/json',
    };
    try {
      const result = await requestDELETE(url, headers);
      console.log('SendGrid result', result);
      return result;
    } catch (error) {
      console.log('SendGrid ERROR', error);
      return false;
    }
  }
}
