import { ConnectionName } from 'src/const/database';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserRemindEntity } from './user-remind.entity';

export class UserRemindRepository extends BaseRepository<UserRemindEntity> {
  repo(): Repository<UserRemindEntity> {
    return getRepository(
      UserRemindEntity,
      ConnectionName.xtrading_user_service,
    );
  }
}
