import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm';

export const USER_REMIND_TABLE = 'user_reminds';
@Entity(USER_REMIND_TABLE)
@Unique('event_id_user_id_template_id_remind_at_un', [
  'event_id',
  'user_id',
  'template_id',
  'remind_at',
])
export class UserRemindEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'uuid' })
  event_id?: string;

  @Column({ type: 'uuid' })
  user_id?: string;

  @Column({ type: 'varchar' })
  template_id?: string;

  @Column({ type: 'bigint' })
  remind_at?: number;

  @Column({ type: 'boolean', default: false })
  remind?: boolean;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;
}

@EventSubscriber()
export class UserRemindEntitySubscriber
  implements EntitySubscriberInterface<UserRemindEntity>
{
  async beforeInsert(event: InsertEvent<UserRemindEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }

  async beforeUpdate(event: UpdateEvent<UserRemindEntity>) {
    event.entity.updated_at = Date.now();
  }
}
