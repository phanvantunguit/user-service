import { ConnectionName } from 'src/const/database';
import { getRepository, Repository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { AppSettingEntity, APP_SETTING_TABLE } from './setting.entity';

export class SettingRepository extends BaseRepository<AppSettingEntity> {
  // app setting
  repo(): Repository<AppSettingEntity> {
    return getRepository(AppSettingEntity, ConnectionName.user_role);
  }
  async saveAppSetting(params: any): Promise<AppSettingEntity> {
    return this.repo().save(params);
  }
  async findOneAppSetting(params: {
    name?: string;
    user_id?: string;
    owner_created?: string;
  }): Promise<AppSettingEntity> {
    return this.repo().findOne(params);
  }
  async findAppSetting(params: any): Promise<AppSettingEntity[]> {
    const { id, name, user_id } = params;

    const whereArray = [];
    if (user_id) {
      whereArray.push(`user_id = '${user_id}' OR user_id is null`);
    } else {
      whereArray.push(`user_id is null`);
    }
    if (id) {
      whereArray.push(`id = '${id}'`);
    }
    if (name) {
      whereArray.push(`name = '${name}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const querySetting = `
    SELECT  *
    FROM ${APP_SETTING_TABLE}  
    ${where}  
    ORDER BY updated_at DESC
  `;
    return this.repo().query(querySetting);
  }
  async deleteAppSettingById(id: string) {
    return this.repo().delete(id);
  }
}
