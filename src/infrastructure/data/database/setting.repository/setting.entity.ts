import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Generated,
  Index,
  Unique,
} from 'typeorm';

export const APP_SETTING_TABLE = 'app_settings';

@Unique('name_user_id_as_un', ['name', 'user_id'])
@Entity(APP_SETTING_TABLE, { synchronize: false })
export class AppSettingEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Index()
  @Column({ type: 'varchar' })
  name?: string;

  @Column({ type: 'varchar' })
  value?: string;

  @Index()
  @Column({ type: 'varchar', nullable: true, default: 'system' })
  user_id?: string;

  @Column({ type: 'varchar', nullable: true })
  description?: string;

  @Column({ type: 'varchar' })
  owner_created?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;
}

@EventSubscriber()
export class AppSettingEntitySubscriber
  implements EntitySubscriberInterface<AppSettingEntity>
{
  async beforeInsert(event: InsertEvent<AppSettingEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<AppSettingEntity>) {
    event.entity.updated_at = Date.now();
  }
}
