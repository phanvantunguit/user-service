import { ConnectionName } from 'src/const/database';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { AdminEntity } from './admin.entity';

export class AdminRepository extends BaseRepository<AdminEntity> {
  repo(): Repository<AdminEntity> {
    return getRepository(AdminEntity, ConnectionName.xtrading_user_service);
  }
}
