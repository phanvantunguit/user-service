import { hashPassword } from 'src/utils/hash.util';
import {
  Entity,
  Column,
  PrimaryColumn,
  Index,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
} from 'typeorm';

export const ADMINS_TABLE = 'admins';
@Entity(ADMINS_TABLE)
export class AdminEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'varchar', unique: true })
  @Index()
  email?: string;

  @Column({ type: 'varchar', nullable: true })
  phone?: string;

  @Column({ type: 'varchar' })
  fullname?: string;

  @Column({ type: 'boolean', default: false })
  super_admin?: boolean;

  @Column({ type: 'boolean', default: false })
  email_confirmed?: boolean;

  @Column({ type: 'varchar', nullable: true })
  status?: string;

  @Column({ type: 'varchar' })
  password?: string;

  @Column({ type: 'varchar', nullable: true })
  created_by?: string;

  @Column({ type: 'varchar', nullable: true })
  updated_by?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;

  @Column({ type: 'bigint', nullable: true })
  deleted_at?: number;
}

@EventSubscriber()
export class AdminEntitySubscriber
  implements EntitySubscriberInterface<AdminEntity>
{
  async beforeInsert(event: InsertEvent<AdminEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
    if (event.entity.password) {
      event.entity.password = await hashPassword(event.entity.password);
    }
  }

  async beforeUpdate(event: UpdateEvent<AdminEntity>) {
    event.entity.updated_at = Date.now();
    if (event.entity.password) {
      event.entity.password = await hashPassword(event.entity.password);
    }
  }
}
