import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm';

export const USER_PROMOTION_TABLE = 'user_promotions';
@Entity(USER_PROMOTION_TABLE)
@Unique('email_template_id_un', ['email', 'template_id'])
export class UserPromotionEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'varchar' })
  email?: string;

  @Column({ type: 'varchar' })
  template_id?: string;

  @Column({ type: 'boolean', default: false })
  send?: boolean;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;
}

@EventSubscriber()
export class UserPromotionEntitySubscriber
  implements EntitySubscriberInterface<UserPromotionEntity>
{
  async beforeInsert(event: InsertEvent<UserPromotionEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }

  async beforeUpdate(event: UpdateEvent<UserPromotionEntity>) {
    event.entity.updated_at = Date.now();
  }
}
