import { ConnectionName } from 'src/const/database';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserPromotionEntity } from './user-promotion.entity';

export class UserPromotionRepository extends BaseRepository<UserPromotionEntity> {
  repo(): Repository<UserPromotionEntity> {
    return getRepository(
      UserPromotionEntity,
      ConnectionName.xtrading_user_service,
    );
  }
}
