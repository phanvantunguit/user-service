import { MERCHANT_STATUS } from 'src/domain/merchant/types';
import { hashPassword } from 'src/utils/hash.util';
import {
  Entity,
  Column,
  PrimaryColumn,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Generated,
} from 'typeorm';

export const MERCHANTS_TABLE = 'merchants';
@Entity(MERCHANTS_TABLE)
export class MerchantEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'varchar', unique: true })
  name?: string;

  @Column({ type: 'varchar', unique: true })
  code?: string;

  @Column({ type: 'boolean', default: false })
  supper_merchant?: boolean;

  @Column({ type: 'varchar', default: MERCHANT_STATUS.ACTIVE })
  status?: MERCHANT_STATUS;

  @Column({ type: 'varchar', nullable: true })
  description?: string;

  @Column({ type: 'varchar', unique: true })
  email?: string;

  @Column({ type: 'jsonb', default: {} })
  config?: any;

  @Column({ type: 'varchar', unique: true })
  domain?: string;

  @Column({ type: 'varchar' })
  password?: string;

  @Column({ type: 'uuid' })
  created_by?: string;

  @Column({ type: 'uuid' })
  updated_by?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;
}

@EventSubscriber()
export class MerchantEntitySubscriber
  implements EntitySubscriberInterface<MerchantEntity>
{
  async beforeInsert(event: InsertEvent<MerchantEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
    if (event.entity.password) {
      event.entity.password = await hashPassword(event.entity.password);
    }
  }

  async beforeUpdate(event: UpdateEvent<MerchantEntity>) {
    event.entity.updated_at = Date.now();
    if (event.entity.password) {
      event.entity.password = await hashPassword(event.entity.password);
    }
  }
}
