import { ConnectionName } from 'src/const/database';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MerchantEntity, MERCHANTS_TABLE } from './merchant.entity';

export class MerchantRepository extends BaseRepository<MerchantEntity> {
  repo(): Repository<MerchantEntity> {
    return getRepository(MerchantEntity, ConnectionName.user_role);
  }

  async queryMerchant(merchantCode?: string): Promise<any> {
    const queryConfig = `SELECT config
    FROM ${MERCHANTS_TABLE}
    WHERE code = '${merchantCode}' `;
    return await this.repo().query(queryConfig);
  }

  findMerchant(params: { status?: string; domain_type?: string }): Promise<MerchantEntity[]> {
    const { status, domain_type } = params
    const whereArray = []
    if (status) {
      whereArray.push(`status = '${status}'`)
    }
    if (domain_type) {
      whereArray.push(`config::json->>'domain_type' = '${domain_type}'`)
    }
    const where =
    whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : ''
    const queryMerchant = `
      SELECT *
      FROM ${MERCHANTS_TABLE}
      ${where}
    `
    return this.repo().query(queryMerchant)
  }
}
