import { ITEM_STATUS } from 'src/domain/transaction/types';
import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Unique,
} from 'typeorm';

export const USER_BOT_TRADING_TABLE = 'user_bot_tradings';
@Entity(USER_BOT_TRADING_TABLE, { synchronize: false })
@Unique('tbot_id_user_id_un', ['bot_id', 'user_id'])
export class UserBotTradingEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string;

  @Column({ type: 'uuid' })
  bot_id: string;

  @Column({ type: 'uuid' })
  @Index()
  user_id: string;

  @Column({ type: 'uuid' })
  owner_created: string;

  @Column({ type: 'varchar', nullable: true })
  broker: string;

  @Column({ type: 'varchar', nullable: true })
  broker_server: string;

  @Column({ type: 'varchar', nullable: true })
  broker_account: string;

  @Column({ type: 'varchar', nullable: true })
  subscriber_id: string;

  @Column({ type: 'bigint', nullable: true })
  expires_at: number;

  @Column({ type: 'varchar' })
  status: ITEM_STATUS;

  @Column({ type: 'varchar', nullable: true })
  type?: string

  @Column({ type: 'varchar', nullable: true })
  balance?: string

  @Column({ type: 'bigint', nullable: true })
  connected_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number;
}

@EventSubscriber()
export class UserBotTradingEntitySubscriber
  implements EntitySubscriberInterface<UserBotTradingEntity>
{
  async beforeInsert(event: InsertEvent<UserBotTradingEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<UserBotTradingEntity>) {
    event.entity.updated_at = Date.now();
  }
}
