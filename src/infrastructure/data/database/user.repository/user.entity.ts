import {
  Entity,
  Column,
  PrimaryColumn,
  Index,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm';
import { hashPassword } from 'src/utils/hash.util';

export const USERS_TABLE = 'users';
@Entity(USERS_TABLE, { synchronize: false })
@Unique('merchant_code_email_un', ['merchant_code', 'email'])
export class UserEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string;

  @Column({ type: 'varchar' })
  @Index()
  email: string;

  @Column({ type: 'varchar', nullable: true })
  username: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'varchar', nullable: true })
  phone: string;

  @Column({ type: 'varchar', nullable: true })
  first_name: string;

  @Column({ type: 'varchar', nullable: true })
  last_name: string;

  @Column({ type: 'varchar', nullable: true })
  address: string;

  @Column({ type: 'varchar', nullable: true })
  affiliate_code: string;

  @Column({ type: 'varchar', nullable: true })
  link_affiliate: string;

  @Column({ type: 'varchar', nullable: true })
  referral_code: string;

  @Column({ type: 'varchar', nullable: true })
  profile_pic: string;

  @Column({ type: 'boolean', default: false })
  active: boolean;

  @Column({ type: 'boolean', default: false })
  email_confirmed: boolean;

  @Column({ type: 'varchar', nullable: true })
  note_updated: string;

  @Column({ type: 'varchar', nullable: true })
  date_registered: string;

  @Column({ type: 'boolean', default: false })
  super_user: boolean;

  @Column({ type: 'boolean', default: false })
  is_admin: boolean;

  @Column({ type: 'varchar', nullable: true })
  country: string;

  @Column({ type: 'varchar', nullable: true })
  year_of_birth: string;

  @Column({ type: 'varchar', nullable: true })
  gender: string;

  @Column({ type: 'varchar', nullable: true })
  phone_code: string;

  @Column({ type: 'varchar', nullable: true, default: 'CM' })
  merchant_code: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number;
}

@EventSubscriber()
export class UserEntitySubscriber
  implements EntitySubscriberInterface<UserEntity>
{
  async beforeInsert(event: InsertEvent<UserEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
    if (event.entity.password) {
      event.entity.password = await hashPassword(event.entity.password);
    }
  }

  async beforeUpdate(event: UpdateEvent<UserEntity>) {
    event.entity.updated_at = Date.now();
    if (event.entity.password) {
      event.entity.password = await hashPassword(event.entity.password);
    }
  }
}
