import { ITEM_STATUS, ORDER_CATEGORY } from 'src/domain/transaction/types';
import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm';

export const USER_ASSET_LOG_TABLE = 'user_asset_logs';
@Entity(USER_ASSET_LOG_TABLE, { synchronize: false })
export class UserAssetLogEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'varchar', nullable: true })
  order_id?: string;

  @Column({ type: 'uuid' })
  asset_id?: string;

  @Column({ type: 'uuid' })
  @Index()
  user_id?: string;

  @Column({ type: 'uuid' })
  owner_created?: string;

  @Column({ type: 'varchar', nullable: true })
  package_type?: string;

  @Column({ type: 'varchar', nullable: true })
  category?: ORDER_CATEGORY;

  @Column({ type: 'bigint', nullable: true })
  quantity?: number;

  @Column({ type: 'bigint', nullable: true })
  expires_at?: number;

  @Column({ type: 'varchar' })
  status?: ITEM_STATUS;

  @Column({ type: 'varchar', nullable: true })
  name?: string;

  @Column({ type: 'varchar', nullable: true })
  price?: string;

  @Column({ type: 'float4', nullable: true })
  discount_rate?: number;

  @Column({ type: 'float4', nullable: true })
  discount_amount?: number;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;

  @Column({ type: 'jsonb', default: {} })
  metadata?: any

  @Column({ type: 'varchar', nullable: true })
  type?: string
}

@EventSubscriber()
export class UserAssetLogEntitySubscriber
  implements EntitySubscriberInterface<UserAssetLogEntity>
{
  async beforeInsert(event: InsertEvent<UserAssetLogEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<UserAssetLogEntity>) {
    event.entity.updated_at = Date.now();
  }
}
