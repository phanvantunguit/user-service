import {
  QueryToken,
  RawVerifyToken,
} from 'src/application/usecase/verify-token/verify-token.types';
import { ConnectionName } from 'src/const/database';
import { TBOT_TYPE } from 'src/domain/bot/types';
import { ITEM_STATUS } from 'src/domain/transaction/types';
import { cleanObject } from 'src/utils/format-data.util';
import { Repository, getRepository, QueryRunner } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserAssetLogEntity } from './user-asset-log.entity';
import {
  UserBotTradingEntity,
  USER_BOT_TRADING_TABLE,
} from './user-bot-trading.entity';
import { UserRoleEntity } from './user-role.entity';
import { UserEntity, USERS_TABLE } from './user.entity';
import { VerifyTokenEntity } from './verify-token.entity';

export class UserRepository extends BaseRepository<UserEntity> {
  repo(): Repository<UserEntity> {
    return getRepository(UserEntity, ConnectionName.user_role);
  }
  async getPaging(param: {
    page?: number;
    size?: number;
    keyword?: string;
    from?: number;
    to?: number;
    merchant_code?: string;
    tbots?: string[];
    email_confirmed?: boolean;
    tbot_status?: string[];
  }) {
    let { page, size, keyword } = param;
    const { from, to, merchant_code, tbots, email_confirmed, tbot_status } =
      param;
    page = Number(page || 1);
    size = Number(size || 50);
    const whereArray = [];
    if (from) {
      whereArray.push(`created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`);
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`);
    }
    if (email_confirmed) {
      whereArray.push(`email_confirmed = ${email_confirmed}`);
    }
    if (keyword) {
      keyword = keyword.toLowerCase();
      const keywords = keyword.split(' ');
      if (keywords.length > 1) {
        const last_name = keywords[keywords.length - 1];
        const first_name = keywords
          .filter((e, i) => i < keywords.length - 1)
          .join(' ');
        whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR (lower(first_name) LIKE '%${first_name}%'
            AND lower(last_name) LIKE '%${last_name}%'))
            `);
      } else {
        whereArray.push(`(email LIKE '%${keyword}%'
          OR lower(username) LIKE '%${keyword}%'
          OR phone LIKE '%${keyword}%'
          OR lower(first_name) LIKE '%${keyword}%'
          OR lower(last_name) LIKE '%${keyword}%')
          `);
      }
    }
    if (tbots.length > 0) {
      const whereBots = tbots.map((name) => `tbots::jsonb ? '${name}'`);
      whereArray.push(`(${whereBots.join(' OR ')})`);
    }

    if (tbot_status && tbot_status.length > 0) {
      const whereTbotArray = tbot_status.map(
        (status) => `tbot_status::jsonb ? '${status}'`,
      );
      whereArray.push(`(${whereTbotArray.join(' OR ')})`);
    }

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUser = `
    SELECT *
    FROM
    (SELECT
      COALESCE(json_agg(DISTINCT ubt.status) FILTER (WHERE ubt.status IS NOT NULL), '[]') AS tbot_status,
      COALESCE(json_agg(DISTINCT ualb.asset_id) FILTER (WHERE ualb.asset_id IS NOT NULL), '[]') AS tbots,
  u.id,
      u.email,
      u.username,
      u.phone,
      u.first_name,
      u.last_name,
      u.address,
      u.affiliate_code,
      u.link_affiliate,
      u.referral_code,
      u.profile_pic,
      u.active,
      u.email_confirmed,
      u.note_updated,
      u.date_registered,
      u.country,
      u.year_of_birth,
      u.gender,
      u.phone_code,
      u.merchant_code,
      u.created_at,
      u.updated_at
      FROM users as u
      LEFT JOIN user_bot_tradings ubt on ubt.user_id = u.id
      LEFT JOIN (select * from (select distinct on (asset_id, user_id) * from user_asset_logs
      where category = 'TBOT'
      order by asset_id desc, user_id desc, updated_at desc) as ual) as ualb on ualb.user_id = u.id
      GROUP BY u.id) as utemp
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
    const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT u.*,
      COALESCE(json_agg(DISTINCT ubt.status) FILTER (WHERE ubt.status IS NOT NULL), '[]') AS tbot_status,
      COALESCE(json_agg(DISTINCT ualb.asset_id) FILTER (WHERE ualb.asset_id IS NOT NULL), '[]') AS tbots
      FROM users as u
      LEFT JOIN user_bot_tradings ubt on ubt.user_id = u.id
      LEFT JOIN (select * from (select distinct on (asset_id, user_id) * from user_asset_logs
      where category = 'TBOT'
      order by asset_id desc, user_id desc, updated_at desc) as ual) as ualb on ualb.user_id = u.id
      GROUP BY u.id) as utemp
    ${where} 
    `;
    const [rows, total] = await Promise.all([
      this.repo().query(queryUser),
      this.repo().query(queryCount),
    ]);
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    };
  }
  async getDetail(param: { id: string }) {
    const { id } = param;
    const whereArray = [];
    if (id) {
      whereArray.push(`id = '${id}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUser = `
    SELECT *
    FROM
    (SELECT u.id,
      u.email,
      u.username,
      u.phone,
      u.first_name,
      u.last_name,
      u.address,
      u.affiliate_code,
      u.link_affiliate,
      u.referral_code,
      u.profile_pic,
      u.active,
      u.email_confirmed,
      u.note_updated,
      u.date_registered,
      u.country,
      u.year_of_birth,
      u.gender,
      u.phone_code,
      u.merchant_code,
      u.created_at,
      u.updated_at,
      COALESCE(json_agg(DISTINCT ualb.name) FILTER (WHERE ualb.name IS NOT NULL), '[]') AS tbots
      FROM users as u
      LEFT JOIN (select * from (select distinct on (asset_id, user_id) * from user_asset_logs
      where category = 'TBOT'
      order by asset_id desc, user_id desc, updated_at desc) as ual) as ualb on ualb.user_id = u.id
      GROUP BY u.id) as utemp
    ${where}
    ORDER BY created_at DESC
    LIMIT 1
  `;

    return this.repo().query(queryUser);
  }
  async report(param: {
    from?: number;
    to?: number;
    merchant_code?: string;
  }): Promise<{
    count_confirmed: string;
    count_not_confirmed: string;
  }> {
    const { from, to, merchant_code } = param;
    const whereArray = [];
    if (from) {
      whereArray.push(`created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`);
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryCount = `
    SELECT 
    SUM(case when email_confirmed = true then 1 else 0 end) as count_confirmed,
    SUM(case when email_confirmed= false then 1 else 0 end) as count_not_confirmed
    FROM ${USERS_TABLE}
    ${where} 
    `;
    return this.repo().query(queryCount);
  }
  async findOneUser(params: any): Promise<UserEntity> {
    const query = cleanObject(params);
    return this.repo().findOne(query);
  }
  async saveUser(params: any): Promise<UserEntity> {
    if (params.email) {
      params.email = params.email.toLocaleLowerCase();
    }
    return this.repo().save(params);
  }
  async findUser(params: {
    email?: string;
    keyword?: string;
    is_admin?: boolean;
    merchant_code?: string[];
  }): Promise<UserEntity[]> {
    let { keyword, is_admin, email, merchant_code } = params;
    const whereArray = [];
    if (is_admin) {
      whereArray.push(`is_admin = ${is_admin}`);
    }
    if (keyword) {
      keyword = keyword.toLowerCase();
      whereArray.push(`(email LIKE '%${keyword}%'
            OR lower(username) LIKE '%${keyword}%'
            OR phone LIKE '%${keyword}%'
            OR lower(first_name) LIKE '%${keyword}%'
            OR lower(last_name) LIKE '%${keyword}%')
            `);
    }
    if (email) {
      whereArray.push(`email = '${email}'`);
    }
    if (merchant_code && merchant_code.length > 0) {
      whereArray.push(
        `merchant_code in (${merchant_code.map((e) => `'${e}'`)})`,
      );
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUser = `
    SELECT  *
    FROM ${USERS_TABLE}  
    ${where}  
    ORDER BY created_at DESC`;
    return this.repo().query(queryUser);
  }

  // user role
  private userRoleRepo(): Repository<UserRoleEntity> {
    return getRepository(UserRoleEntity, ConnectionName.user_role);
  }

  async saveUserRole(params: any, queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.userRoleRepo().create(params);
      return queryRunner.manager.save(transaction);
    } else {
      return this.userRoleRepo().save(params);
    }
  }

  // user bot trading
  private userBotTradingRepo(): Repository<UserBotTradingEntity> {
    return getRepository(UserBotTradingEntity, ConnectionName.user_role);
  }
  async saveUserBotTrading(params: any[], queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.userBotTradingRepo().create(params);
      return queryRunner.manager.save(transaction);
    } else {
      return this.userBotTradingRepo().save(params);
    }
  }
  async saveStatusBotTrading(params: any) {
    return this.userBotTradingRepo().save(params);
  }
  async findUserBotTrading(params: {
    user_id?: string;
    bot_id?: string;
  }): Promise<UserBotTradingEntity[]> {
    return this.userBotTradingRepo().find({
      where: params,
      order: { created_at: 'DESC' },
    });
  }
  async findUserBotTradingSQL(params: {
    user_id?: string;
    bot_ids?: string[];
    status?: ITEM_STATUS[];
    expires_at?: number;
    expired?: number;
    type?: TBOT_TYPE;
  }): Promise<UserBotTradingEntity[]> {
    const { user_id, bot_ids, status, expires_at, expired, type } = params;
    const whereArray = [];
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`);
    }
    if (status && status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
    }
    if (expires_at) {
      whereArray.push(`expires_at > ${expires_at}`);
    }
    if (expired) {
      whereArray.push(`expires_at < ${expired}`);
      whereArray.push(`status != '${ITEM_STATUS.EXPIRED}'`);
    }
    if (bot_ids && bot_ids.length > 0) {
      whereArray.push(`bot_id in (${bot_ids.map((e) => `'${e}'`)})`);
    }
    if (type) {
      whereArray.push(`type = '${type}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const query = `
      SELECT *
      FROM ${USER_BOT_TRADING_TABLE}  
      ${where} 
      order by created_at DESC
    `;
    return this.userBotTradingRepo().query(query);
  }

  async findByAssetIds(ids: any): Promise<UserBotTradingEntity[]> {
    return this.userBotTradingRepo().findByIds(ids);
  }
  async findSubscriberByIds(ids: string[]): Promise<any> {
    const queryRoles = `
        SELECT COUNT(subscriber_id)
        FROM ${USER_BOT_TRADING_TABLE} rv 
        WHERE id in (${ids.map((id) => `'${id}'`)})
        `;
    const data = await this.userBotTradingRepo().query(queryRoles);
    return data;
  }
  async deleteAssetIds(assetIds: string[]) {
    const whereArray = [];
    if (assetIds.length > 0) {
      whereArray.push(`id in (${assetIds.map((id) => `'${id}'`)})`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const query = `
      DELETE
      FROM ${USER_BOT_TRADING_TABLE}  
      ${where}
    `;
    return this.userBotTradingRepo().query(query);
  }

  // user asset log
  private userAssetLogRepo(): Repository<UserAssetLogEntity> {
    return getRepository(UserAssetLogEntity, ConnectionName.user_role);
  }
  async saveUserAssetLog(params: any[], queryRunner?: QueryRunner) {
    if (queryRunner) {
      const transaction = await this.userAssetLogRepo().create(params);
      return queryRunner.manager.save(transaction);
    } else {
      return this.userAssetLogRepo().save(params);
    }
  }
  findAssetLogLatest(param: {
    user_id?: string;
    category?: string;
    status?: string[];
  }): Promise<UserAssetLogEntity[]> {
    const { user_id, category, status } = param;
    const whereArray = [];
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`);
    }
    if (category) {
      whereArray.push(`category = '${category}'`);
    }
    if (status && status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const query = `
      select distinct on (asset_id) *
      from user_asset_logs
		  ${where}
      order by asset_id desc,  updated_at desc
    `;
    return this.userBotTradingRepo().query(query);
  }
  // verify token
  private verifyTokenRepo(): Repository<VerifyTokenEntity> {
    return getRepository(VerifyTokenEntity, ConnectionName.user_role);
  }
  async saveVerifyToken(params: RawVerifyToken): Promise<RawVerifyToken> {
    return this.verifyTokenRepo().save(params);
  }
  async findOneToken(params: QueryToken): Promise<VerifyTokenEntity> {
    const query = cleanObject(params);
    return this.verifyTokenRepo().findOne(query);
  }
}
