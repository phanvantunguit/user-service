import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
} from 'typeorm';

export const VERIFY_TOKENS_TABLE = 'verify_tokens';
@Entity(VERIFY_TOKENS_TABLE, { synchronize: false })
export class VerifyTokenEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string;

  @Column({ type: 'uuid' })
  @Index()
  user_id: string;

  @Column({ type: 'varchar' })
  @Index()
  token: string;

  @Column({ type: 'varchar' })
  @Index()
  type: string;

  @Column({ type: 'bigint' })
  expires_at: number;

  @Column({ type: 'jsonb', default: {} })
  metadata: any;

  @Column({ type: 'bigint', nullable: true })
  created_at: number;
}

@EventSubscriber()
export class VerifyTokenEntitySubscriber
  implements EntitySubscriberInterface<VerifyTokenEntity>
{
  async beforeInsert(event: InsertEvent<VerifyTokenEntity>) {
    event.entity.created_at = Date.now();
  }
}
