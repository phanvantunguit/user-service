import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
  Unique,
} from 'typeorm';

export const USER_ROLE_TABLE = 'user_roles';
@Entity(USER_ROLE_TABLE, { synchronize: false })
@Unique('role_id_user_id_un', ['role_id', 'user_id'])
export class UserRoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string;

  @Column({ type: 'uuid' })
  role_id: string;

  @Column({ type: 'uuid' })
  @Index()
  user_id: string;

  @Column({ type: 'varchar', nullable: true })
  description: string;

  @Column({ type: 'varchar' })
  owner_created: string;

  @Column({ type: 'varchar', nullable: true })
  package_id: string;

  @Column({ type: 'varchar', nullable: true })
  package_type: string;

  @Column({ type: 'bigint', nullable: true })
  quantity: number;

  @Column({ type: 'varchar', nullable: true })
  package_name: string;

  @Column({ type: 'bigint', nullable: true })
  expires_at: number;

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number;
}

@EventSubscriber()
export class UserRoleEntitySubscriber
  implements EntitySubscriberInterface<UserRoleEntity>
{
  async beforeInsert(event: InsertEvent<UserRoleEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<UserRoleEntity>) {
    event.entity.updated_at = Date.now();
  }
}
