import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm';

export const ROLES_TABLE = 'roles';
@Entity(ROLES_TABLE, { synchronize: false })
export class RoleEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id: string;

  @Column({ type: 'varchar' })
  role_name: string;

  @Column({ type: 'jsonb', default: {} })
  root: any;

  @Column({ type: 'varchar', nullable: true })
  description: string;

  @Column({ type: 'varchar', nullable: true })
  status: string;

  @Column({ type: 'varchar', nullable: true })
  type: string;

  @Column({ type: 'varchar', nullable: true })
  price: string;

  @Column({ type: 'varchar', nullable: true })
  currency: string;

  @Column({ type: 'varchar' })
  owner_created: string;

  @Column({ type: 'uuid', nullable: true })
  parent_id: string;

  @Column({ type: 'boolean', default: false })
  is_best_choice: boolean;

  @Column({ type: 'int4', nullable: true })
  order: number;

  @Column({ type: 'varchar', nullable: true })
  color: string;

  @Column({ type: 'jsonb', default: {} })
  description_features: any;

  @Column({ type: 'bigint', default: Date.now() })
  created_at: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at: number;
}

@EventSubscriber()
export class RoleEntitySubscriber
  implements EntitySubscriberInterface<RoleEntity>
{
  async beforeInsert(event: InsertEvent<RoleEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<RoleEntity>) {
    event.entity.updated_at = Date.now();
  }
}
