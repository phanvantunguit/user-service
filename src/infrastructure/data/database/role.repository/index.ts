import { ConnectionName } from 'src/const/database';
import { BOT_STATUS } from 'src/domain/bot/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { RoleEntity, ROLES_TABLE } from './role.entity';

export class RoleRepository extends BaseRepository<RoleEntity> {
  repo(): Repository<RoleEntity> {
    return getRepository(RoleEntity, ConnectionName.user_role);
  }
  list(): Promise<{ id: string; name: string }[]> {
    const queryBot = `
    SELECT *, role_name as name
    FROM ${ROLES_TABLE}
    WHERE status = '${BOT_STATUS.OPEN}'
    ORDER BY name ASC
    `;
    return this.repo().query(queryBot);
  }
}
