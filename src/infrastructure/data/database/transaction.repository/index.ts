import { ConnectionName } from 'src/const/database';
import { TRANSACTION_STATUS } from 'src/domain/transaction/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { TransactionLogEntity } from './transaction-log.entity';
import { TransactionMetadataEntity } from './transaction-metadata.entity';
import { TransactionEntity, TRANSACTION_TABLE } from './transaction.entity';

export class TransactionRepository extends BaseRepository<TransactionEntity> {
  repo(): Repository<TransactionEntity> {
    return getRepository(TransactionEntity, ConnectionName.payment_service);
  }
  metadataRepo(): Repository<TransactionMetadataEntity> {
    return getRepository(
      TransactionMetadataEntity,
      ConnectionName.payment_service,
    );
  }
  logRepo(): Repository<TransactionLogEntity> {
    return getRepository(TransactionLogEntity, ConnectionName.payment_service);
  }

  async findMultipleStatus(param: {
    status?: string[];
    integrate_service?: string;
    user_id?: string;
    email?: string;
    phone?: string;
  }): Promise<TransactionEntity[]> {
    const { status, user_id, email, phone, integrate_service } = param;
    const whereArray = [];
    if (status && status.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`);
    }
    if (user_id) {
      whereArray.push(`user_id = '${user_id}'`);
    }
    if (email) {
      whereArray.push(`email = '${email}'`);
    }
    if (phone) {
      whereArray.push(`phone = '${phone}'`);
    }
    if (integrate_service) {
      whereArray.push(`integrate_service = '${integrate_service}'`);
    }
    const where = `WHERE ${whereArray.join(' AND ')}`;
    const query = `
      SELECT *
      FROM ${TRANSACTION_TABLE}  
      ${where} 
      order by created_at DESC
    `;
    return this.repo().query(query);
  }
  async getPaging(param: {
    page?: number;
    size?: number;
    keyword?: string;
    status?: TRANSACTION_STATUS;
    from?: number;
    to?: number;
    merchant_code?: string;
    category?: string;
    name?: string;
  }) {
    let { page, size } = param;
    const { keyword, status, from, to, merchant_code, category, name } = param;
    page = Number(page || 1);
    size = Number(size || 50);
    const whereArray = [];
    if (from) {
      whereArray.push(`created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`);
    }

    if (status) {
      whereArray.push(`status = '${status}'`);
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`);
    }
    if (keyword) {
      whereArray.push(`(email LIKE '%${keyword}%'
          OR fullname LIKE '%${keyword}%'
          OR order_id LIKE '%${keyword}%'
          OR payment_id LIKE '%${keyword}%')`);
    }
    if (category) {
      whereArray.push(`items like '%"category":"${category}"%'`);
    }
    if (name) {
      whereArray.push(`items like '%"name":"${name}"%'`);
    }

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryTransaction = `
      SELECT id,
      integrate_service,
      order_id,
      order_type,
      user_id,
      fullname,
      email,
      amount,
      currency,
      payment_id,
      payment_method,
      status,
      ipn_url,
      merchant_code,
      created_at,
      updated_at,
      commission_cash,
      CAST(items AS JSONB) AS items    
      FROM
      (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
      ${where}  
      ORDER BY created_at DESC
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
    const queryCount = `
    SELECT COUNT(*)
    FROM (SELECT t.*, tm.value as items
      FROM transactions t
      LEFT JOIN transaction_metadata tm on tm.transaction_id = t.id and tm.attribute = 'items') as tmix
    ${where}  
    `;
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ]);
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    };
  }
  async getDetail(param: { id: string; merchant_code: string }) {
    const { id, merchant_code } = param;
    const [transaction, metadatas, logs] = await Promise.all([
      this.repo().findOne({ id, merchant_code }),
      this.getMetadata({ transaction_id: id }),
      this.getLog({ transaction_id: id }),
    ]);
    return { transaction, metadatas, logs };
  }
  async getMetadata(param: { transaction_id: string }) {
    return this.metadataRepo().find({ transaction_id: param.transaction_id });
  }
  async getLog(param: { transaction_id: string }) {
    return this.logRepo().find({ transaction_id: param.transaction_id });
  }
  async report(param: {
    from?: number;
    to?: number;
    merchant_code?: string;
  }): Promise<{
    count_total: string;
    count_complete: string;
    count_failed: string;
    amount_total: number;
    amount_complete: number;
    amount_failed: number;

    commission_cash_total: number;
    commission_cash_failed: number;
    commission_cash: number;
    start_at: number;
    finish_at: number;
  }> {
    const { from, to, merchant_code } = param;
    const whereArray = [];
    if (from) {
      whereArray.push(`created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`);
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryCount = `
    SELECT 
    COUNT(*) as count_total,
    MAX(created_at) as finish_at,
    MIN(created_at) as start_at,
    SUM(case when status='COMPLETE' then 1 else 0 end) as count_complete,
    SUM(case when status='FAILED' then 1 else 0 end) as count_failed,
    SUM(amount::float) amount_total,
    SUM(case when status='COMPLETE' then amount::float else 0 end) as amount_complete,
    SUM(case when status='FAILED' then amount::float else 0 end) as amount_failed,
    SUM(commission_cash) as commission_cash_total,
    SUM(case when status='FAILED' then commission_cash else 0 end) as commission_cash_failed,
    SUM(case when status='COMPLETE' then commission_cash else 0 end) as commission_cash
    FROM ${TRANSACTION_TABLE}
    ${where} 
    `;
    return this.repo().query(queryCount);
  }
  async chart(param: {
    from?: number;
    to?: number;
    merchant_code?: string;
    timezone: string;
    time_type: 'DAY' | 'MONTH';
  }): Promise<
    {
      time: string;
      count_total: string;
      count_complete: number;
      count_failed: string;
      amount_total: number;
      amount_complete: number;
      amount_failed: number;

      commission_cash_total: number;
      commission_cash_failed: number;
      commission_cash: number;
      start_at: number;
      finish_at: number;
    }[]
  > {
    const { from, to, merchant_code, timezone, time_type } = param;
    const whereArray = [];
    if (from) {
      whereArray.push(`created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`created_at <= ${to}`);
    }
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const timeFormat = time_type === 'DAY' ? 'YYYY-MM-DD' : 'YYYY-MM';
    const queryChart = `
    SELECT to_char(to_timestamp(created_at / 1000) AT TIME ZONE '${timezone}', '${timeFormat}') as time, 
    COUNT(*) as count_total,
    MAX(created_at) as finish_at,
    MIN(created_at) as start_at,
    SUM(case when status='COMPLETE' then 1 else 0 end) as count_complete,
    SUM(case when status='FAILED' then 1 else 0 end) as count_failed,
    SUM(amount::float) amount_total,
    SUM(case when status='COMPLETE' then amount::float else 0 end) as amount_complete,
    SUM(case when status='FAILED' then amount::float else 0 end) as amount_failed,
    SUM(commission_cash) as commission_cash_total,
    SUM(case when status='FAILED' then commission_cash else 0 end) as commission_cash_failed,
    SUM(case when status='COMPLETE' then commission_cash else 0 end) as commission_cash
    from transactions
    ${where}  
    GROUP BY time
    ORDER BY time ASC
    `;
    return this.repo().query(queryChart);
  }

  async totalCommission(param: {
    merchant_code?: string;
  }): Promise<{
    total: number;
  }[]> {
    const {merchant_code } = param;
    const whereArray = [];
    if (merchant_code) {
      whereArray.push(`merchant_code = '${merchant_code}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryCount = `
    SELECT 
    SUM(case when status='COMPLETE' then commission_cash else 0 end) as total
    FROM ${TRANSACTION_TABLE}
    ${where} 
    `;
    return this.repo().query(queryCount);
  }
}
