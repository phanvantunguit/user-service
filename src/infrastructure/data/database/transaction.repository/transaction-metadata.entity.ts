import { TRANSACTION_METADATA } from 'src/domain/transaction/types';
import {
  Entity,
  Column,
  Index,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm';

export const TRANSACTION_METADATA_TABLE = 'transaction_metadata';
@Entity(TRANSACTION_METADATA_TABLE)
export class TransactionMetadataEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'uuid' })
  @Index()
  transaction_id?: string;

  @Column({ type: 'varchar' })
  attribute?: TRANSACTION_METADATA;

  @Column({ type: 'varchar' })
  value?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;
}
@EventSubscriber()
export class TransactionMetadataEntitySubscriber
  implements EntitySubscriberInterface<TransactionMetadataEntity>
{
  async beforeInsert(event: InsertEvent<TransactionMetadataEntity>) {
    event.entity.created_at = Date.now();
  }
}
