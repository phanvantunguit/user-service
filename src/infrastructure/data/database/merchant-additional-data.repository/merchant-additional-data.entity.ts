import { MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types'
import {
    Entity,
    Column,
    PrimaryColumn,
    Generated,
    EntitySubscriberInterface,
    EventSubscriber,
    InsertEvent,
    UpdateEvent,
    Unique,
  } from 'typeorm'
  
  export const MERCHANT_ADDITIONAL_DATA_TABLE = 'merchant_additional_data'
  @Entity(MERCHANT_ADDITIONAL_DATA_TABLE)
  @Unique('merchant_id_additional_data_id_mdd_un', ['merchant_id', 'additional_data_id'])
  export class MerchantAdditionalDataEntity {
    @PrimaryColumn()
    @Generated('uuid')
    id?: string
  
    @Column({ type: 'uuid' })
    merchant_id?: string

    @Column({ type: 'uuid' })
    additional_data_id?: string
  
    @Column({ type: 'varchar', default: MERCHANT_ADDITIONAL_DATA_STATUS.ON })
    status?: string

    @Column({ type: 'bigint', default: Date.now() })
    created_at?: number
  
    @Column({ type: 'bigint', default: Date.now() })
    updated_at?: number

    @Column({ type: 'uuid', nullable: true })
    created_by?: string
  
    @Column({ type: 'uuid', nullable: true })
    updated_by?: string

    @Column({ type: 'int4', nullable: true })
    order?: number;
  }
  
  @EventSubscriber()
  export class MerchantAdditionalDataEntitySubscriber
    implements EntitySubscriberInterface<MerchantAdditionalDataEntity>
  {
    async beforeInsert(event: InsertEvent<MerchantAdditionalDataEntity>) {
      event.entity.created_at = Date.now()
      event.entity.updated_at = Date.now()
    }
    async beforeUpdate(event: UpdateEvent<MerchantAdditionalDataEntity>) {
      event.entity.updated_at = Date.now()
    }
  }
  