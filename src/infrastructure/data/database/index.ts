import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_CONFIG } from 'src/config';
import { BaseRepository } from './base.repository';
import { DBContext } from './db-context';
import { AdminRepository } from './admin.repository';
import {
  AdminEntity,
  AdminEntitySubscriber,
} from './admin.repository/admin.entity';
import {
  EventEntity,
  EventEntitySubscriber,
} from './event.repository/event.entity';
import {
  UserEventEntity,
  UserEventEntitySubscriber,
} from './user-event.repository/user-event.entity';
import { EventRepository } from './event.repository';
import { UserEventRepository } from './user-event.repository';
import { UserRemindRepository } from './user-remind.repository';
import {
  UserRemindEntity,
  UserRemindEntitySubscriber,
} from './user-remind.repository/user-remind.entity';
import { UserPromotionRepository } from './user-promotion.repository';
import {
  UserPromotionEntity,
  UserPromotionEntitySubscriber,
} from './user-promotion.repository/user-promotion.entity';
import { ConnectionName } from 'src/const/database';
import {
  MerchantEntity,
  MerchantEntitySubscriber,
} from './merchant.repository/merchant.entity';
import { MerchantRepository } from './merchant.repository';
import {
  TransactionLogEntity,
  TransactionLogEntitySubscriber,
} from './transaction.repository/transaction-log.entity';
import {
  TransactionMetadataEntity,
  TransactionMetadataEntitySubscriber,
} from './transaction.repository/transaction-metadata.entity';
import {
  TransactionEntity,
  TransactionEntitySubscriber,
} from './transaction.repository/transaction.entity';
import { TransactionRepository } from './transaction.repository';
import { UserRepository } from './user.repository';
import {
  UserEntity,
  UserEntitySubscriber,
} from './user.repository/user.entity';
import { BotRepository } from './bot.repository';
import { BotEntity, BotEntitySubscriber } from './bot.repository/bot.entity';
import { RoleRepository } from './role.repository';
import {
  RoleEntity,
  RoleEntitySubscriber,
} from './role.repository/role.entity';
import { BotTradingRepository } from './bot-trading.repository';
import {
  BotTradingEntity,
  BotTradingEntitySubscriber,
} from './bot-trading.repository/bot-trading.entity';
import { MerchantCommissionRepository } from './merchant-commission.repository';
import {
  MerchantCommissionEntity,
  MerchantCommissionEntitySubscriber,
} from './merchant-commission.repository/merchant-commission.entity';
import {
  MerchantInvoiceEntity,
  MerchantInvoiceEntitySubscriber,
} from './merchant-invoice.repository/merchant-invoice.entity';
import { MerchantInvoiceRepository } from './merchant-invoice.repository';
import {
  UserBotTradingEntity,
  UserBotTradingEntitySubscriber,
} from './user.repository/user-bot-trading.entity';
import {
  UserAssetLogEntity,
  UserAssetLogEntitySubscriber,
} from './user.repository/user-asset-log.entity';
import {
  VerifyTokenEntity,
  VerifyTokenEntitySubscriber,
} from './user.repository/verify-token.entity';
import {
  UserRoleEntity,
  UserRoleEntitySubscriber,
} from './user.repository/user-role.entity';
import {
  AppSettingEntity,
  AppSettingEntitySubscriber,
} from './setting.repository/setting.entity';
import { SettingRepository } from './setting.repository';
import { EventStoreRepository } from './event-store.repository';
import { EventStoreEntity, EventStoreEntitySubscriber } from './event-store.repository/event-store.entity';
import { AdditionalDataRepository } from './additional-data.repository';
import { MerchantAdditionalDataRepository } from './merchant-additional-data.repository';
import { AdditionalDataEntity, AdditionalDataEntitySubscriber } from './additional-data.repository/additional-data.entity';
import { MerchantAdditionalDataEntity, MerchantAdditionalDataEntitySubscriber } from './merchant-additional-data.repository/merchant-additional-data.entity';
const repo = [
  DBContext,
  BaseRepository,
  AdminRepository,
  EventRepository,
  UserEventRepository,
  UserRemindRepository,
  UserPromotionRepository,
  MerchantRepository,
  TransactionRepository,
  UserRepository,
  BotRepository,
  RoleRepository,
  BotTradingRepository,
  MerchantCommissionRepository,
  MerchantInvoiceRepository,
  SettingRepository,
  EventStoreRepository,
  AdditionalDataRepository,
  MerchantAdditionalDataRepository
];
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      name: ConnectionName.xtrading_user_service,
      host: APP_CONFIG.POSTGRES_HOST,
      port: APP_CONFIG.POSTGRES_PORT,
      username: APP_CONFIG.POSTGRES_USER,
      password: APP_CONFIG.POSTGRES_PASS,
      database: APP_CONFIG.POSTGRES_DB,
      entities: [
        AdminEntity,
        EventEntity,
        UserEventEntity,
        UserRemindEntity,
        UserPromotionEntity,
      ],
      subscribers: [
        AdminEntitySubscriber,
        EventEntitySubscriber,
        UserEventEntitySubscriber,
        UserRemindEntitySubscriber,
        UserPromotionEntitySubscriber,
      ],
      synchronize: true,
      migrations: ['dist/migrations/*{.ts,.js}'],
      migrationsTableName: 'migrations',
      migrationsRun: true,
      keepConnectionAlive: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      name: ConnectionName.user_role,
      host: APP_CONFIG.UR_POSTGRES_HOST,
      port: APP_CONFIG.UR_POSTGRES_PORT,
      username: APP_CONFIG.UR_POSTGRES_USER,
      password: APP_CONFIG.UR_POSTGRES_PASS,
      database: APP_CONFIG.UR_POSTGRES_DB,
      entities: [
        MerchantEntity,
        UserEntity,
        BotEntity,
        RoleEntity,
        BotTradingEntity,
        MerchantCommissionEntity,
        MerchantInvoiceEntity,
        UserBotTradingEntity,
        UserAssetLogEntity,
        VerifyTokenEntity,
        UserRoleEntity,
        AppSettingEntity,
        EventStoreEntity,
        AdditionalDataEntity,
        MerchantAdditionalDataEntity,
      ],
      subscribers: [
        MerchantEntitySubscriber,
        UserEntitySubscriber,
        BotEntitySubscriber,
        RoleEntitySubscriber,
        BotTradingEntitySubscriber,
        MerchantCommissionEntitySubscriber,
        MerchantInvoiceEntitySubscriber,
        UserBotTradingEntitySubscriber,
        UserAssetLogEntitySubscriber,
        VerifyTokenEntitySubscriber,
        UserRoleEntitySubscriber,
        AppSettingEntitySubscriber,
        EventStoreEntitySubscriber,
        AdditionalDataEntitySubscriber,
        MerchantAdditionalDataEntitySubscriber,
      ],
      synchronize: true,
      // migrations: ['dist/migrations/*{.ts,.js}'],
      // migrationsTableName: 'migrations',
      migrationsRun: false,
      keepConnectionAlive: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      name: ConnectionName.payment_service,
      host: APP_CONFIG.PAYMENT_POSTGRES_HOST,
      port: APP_CONFIG.PAYMENT_POSTGRES_PORT,
      username: APP_CONFIG.PAYMENT_POSTGRES_USER,
      password: APP_CONFIG.PAYMENT_POSTGRES_PASS,
      database: APP_CONFIG.PAYMENT_POSTGRES_DB,
      entities: [
        TransactionLogEntity,
        TransactionMetadataEntity,
        TransactionEntity,
      ],
      subscribers: [
        TransactionEntitySubscriber,
        TransactionLogEntitySubscriber,
        TransactionMetadataEntitySubscriber,
      ],
      synchronize: false,
      migrationsRun: false,
      keepConnectionAlive: true,
    }),
  ],
  providers: repo,
  exports: repo,
})
export class DatabaseModule {}
