import { ConnectionName } from 'src/const/database';
import { BOT_STATUS } from 'src/domain/bot/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MERCHANT_COMMISSION_TABLE } from '../merchant-commission.repository/merchant-commission.entity';
import { BotTradingEntity, BOT_TRADING_TABLE } from './bot-trading.entity';

export class BotTradingRepository extends BaseRepository<BotTradingEntity> {
  repo(): Repository<BotTradingEntity> {
    return getRepository(BotTradingEntity, ConnectionName.user_role);
  }
  list(param: {
    merchant_id?: string;
  }): Promise<{ id: string; name: string }[]> {
    const { merchant_id } = param;
    if (merchant_id) {
      const whereArray = [];
      whereArray.push(`mc.merchant_id = '${merchant_id}'`);
      whereArray.push(`bt.status = '${BOT_STATUS.OPEN}'`);
      const where =
        whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';

      const queryBot = `
      SELECT bt.*,
      mc.merchant_id
      FROM ${BOT_TRADING_TABLE} bt
      LEFT JOIN ${MERCHANT_COMMISSION_TABLE} mc on mc.asset_id = bt.id
      ${where}
      ORDER BY name ASC
      `;
      return this.repo().query(queryBot);
    } else {
      const queryBot = `
      SELECT *
      FROM ${BOT_TRADING_TABLE}
      WHERE status = '${BOT_STATUS.OPEN}'
      ORDER BY name ASC
      `;
      return this.repo().query(queryBot);
    }
  }
}
