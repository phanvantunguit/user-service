import { BOT_STATUS } from 'src/domain/bot/types';
import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
  InsertEvent,
} from 'typeorm';

export const BOT_TRADING_TABLE = 'bot_tradings';
@Entity(BOT_TRADING_TABLE, { synchronize: false })
export class BotTradingEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'varchar' })
  name?: string;

  @Column({ type: 'varchar', nullable: true })
  clone_name?: string

  @Column({ type: 'varchar', nullable: true })
  code?: string;

  @Column({ type: 'varchar' })
  type?: string;

  @Column({ type: 'varchar' })
  description?: string;

  @Column({ type: 'jsonb', default: {} })
  work_based_on?: any;

  @Column({ type: 'varchar' })
  status?: BOT_STATUS;

  @Column({ type: 'varchar' })
  price?: string;

  @Column({ type: 'varchar', nullable: true })
  display_price?: string

  @Column({ type: 'varchar' })
  currency?: string;

  @Column({ type: 'varchar' })
  image_url?: string;

  @Column({ type: 'varchar' })
  owner_created?: string;

  @Column({ type: 'int4', nullable: true })
  order?: number;

  @Column({ type: 'varchar', nullable: true })
  max_drawdown?: string

  @Column({ type: 'varchar', nullable: true })
  max_drawdown_change_percent?: string

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;
}

@EventSubscriber()
export class BotTradingEntitySubscriber
  implements EntitySubscriberInterface<BotTradingEntity>
{
  async beforeInsert(event: InsertEvent<BotTradingEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<BotTradingEntity>) {
    event.entity.updated_at = Date.now();
  }
}
