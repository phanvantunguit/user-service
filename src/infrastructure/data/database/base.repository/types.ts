import { DeleteResult } from 'typeorm';
import { EntityId } from 'typeorm/repository/EntityId';

export interface IBaseRepository<T> {
  findOne(params: any): Promise<T>;
  find(params: any): Promise<T[]>;
  findById(id: EntityId): Promise<T>;
  findByIds(ids: [EntityId]): Promise<T[]>;
  save(params: any): Promise<T>;
  delete(params: any): Promise<DeleteResult>;
  deleteById(id: EntityId): Promise<DeleteResult>;
  count(params: any): Promise<number>;
}
