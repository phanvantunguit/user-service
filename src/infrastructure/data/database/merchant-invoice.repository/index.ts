import { ConnectionName } from 'src/const/database';
import { BOT_STATUS } from 'src/domain/bot/types';
import { MERCHANT_INVOICES_STATUS } from 'src/domain/invoices/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { MERCHANTS_TABLE } from '../merchant.repository/merchant.entity';
import {
  MerchantInvoiceEntity,
  MERCHANT_INVOICES_TABLE,
} from './merchant-invoice.entity';

export class MerchantInvoiceRepository extends BaseRepository<MerchantInvoiceEntity> {
  repo(): Repository<MerchantInvoiceEntity> {
    return getRepository(MerchantInvoiceEntity, ConnectionName.user_role);
  }
  async getPaging(param: {
    keyword?: string;
    page?: number;
    size?: number;
    from?: number;
    to?: number;
    sort_by?: string;
    order_by?: string;
    status?: MERCHANT_INVOICES_STATUS;
    merchant_id?: string;
  }) {
    let { page, size, sort_by, keyword } = param;
    const { status, from, to, order_by, merchant_id } = param;
    page = Number(page || 1);
    size = Number(size || 50);
    const whereArray = [];
    if (sort_by && sort_by === 'created_at') {
      sort_by = 'mi.created_at';
    }
    if (from) {
      whereArray.push(`mi.created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`mi.created_at <= ${to}`);
    }
    if (status) {
      whereArray.push(`mi.status = '${status}'`);
    }
    if (merchant_id) {
      whereArray.push(`mi.merchant_id = '${merchant_id}'`);
    }
    if (keyword) {
      whereArray.push(
        `(ms.name LIKE '%${keyword}%' OR ms.email LIKE '%${keyword}%')`,
      );
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryTransaction = `
    SELECT mi.*,
    ms.code as merchant_code,
    ms.email as merchant_email,
    ms.name as merchant_name,
    ms.config::json#>>'{wallet,wallet_address}' as wallet_address,
    ms.config::json#>>'{wallet,status}' as wallet_status
    FROM 
    ${MERCHANT_INVOICES_TABLE} mi 
    left join ${MERCHANTS_TABLE} ms on ms.id = mi.merchant_id
    ${where}  
    ORDER BY ${sort_by ? sort_by : 'mi.created_at'} ${
      order_by ? order_by : 'DESC'
    } 
    OFFSET ${(page - 1) * size}
    LIMIT ${size}
  `;
    const queryCount = `
  SELECT COUNT(*)
  FROM 
  ${MERCHANT_INVOICES_TABLE} mi 
  left join ${MERCHANTS_TABLE} ms on ms.id = mi.merchant_id
  ${where}  
  `;
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ]);

    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    };
  }
  async getDetail(id: string) {
    const queryTransaction = `
    SELECT mi.*,
    ms.code as merchant_code,
    ms.email as merchant_email,
    ms.name as merchant_name,
    ms.config::json#>>'{wallet,wallet_address}' as wallet_address,
    ms.config::json#>>'{wallet,status}' as wallet_status
    FROM 
    ${MERCHANT_INVOICES_TABLE} mi 
    left join ${MERCHANTS_TABLE} ms on ms.id = mi.merchant_id
    where m.id = '${id}'
    `;
    return this.repo().query(queryTransaction);
  }
  async getTransaction(transaction_id: string) {
    const queryTransaction = `
    SELECT COUNT(mi.transaction_id)
    FROM 
    ${MERCHANT_INVOICES_TABLE} mi 
    where mi.transaction_id = '${transaction_id}' AND mi.status ='COMPLETED'
    `;
    return this.repo().query(queryTransaction);
  }
  async totalCommission(param: {
    merchant_id?: string;
    status?: string[];
  }): Promise<{
    total: number;
  }[]> {
    const { merchant_id, status } = param;
    const whereArray = [];
    if (merchant_id) {
      whereArray.push(`merchant_id = '${merchant_id}'`);
    }
    if (status?.length > 0) {
      whereArray.push(`status in (${status.map((e) => `'${e}'`)})`)
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryCount = `
    SELECT 
    SUM(amount_commission_complete) as total
    FROM ${MERCHANT_INVOICES_TABLE}
    ${where} 
    `;
    return this.repo().query(queryCount);
  }
}
