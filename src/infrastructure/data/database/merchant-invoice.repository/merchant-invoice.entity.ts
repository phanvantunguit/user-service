import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
} from 'typeorm';

export const MERCHANT_INVOICES_TABLE = 'merchant_invoices';
@Entity(MERCHANT_INVOICES_TABLE)
export class MerchantInvoiceEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'uuid' })
  merchant_id?: string;

  @Column({ type: 'float4' })
  amount_commission_complete?: number;

  @Column({ type: 'varchar' })
  status?: string;

  @Column({ type: 'varchar', nullable: true })
  transaction_id?: string;

  @Column({ type: 'varchar', nullable: true })
  wallet_address?: string;

  @Column({ type: 'varchar', nullable: true })
  wallet_network?: string;

  @Column({ type: 'varchar', nullable: true })
  description?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;

  @Column({ type: 'uuid', nullable: true })
  updated_by?: string;

  @Column({ type: 'jsonb', default: {} })
  metadata?: any;
}

@EventSubscriber()
export class MerchantInvoiceEntitySubscriber
  implements EntitySubscriberInterface<MerchantInvoiceEntity>
{
  async beforeInsert(event: InsertEvent<MerchantInvoiceEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }
  async beforeUpdate(event: UpdateEvent<MerchantInvoiceEntity>) {
    event.entity.updated_at = Date.now();
  }
}
