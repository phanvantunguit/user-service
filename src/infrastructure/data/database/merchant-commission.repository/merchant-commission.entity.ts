import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import {
  Entity,
  Column,
  PrimaryColumn,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm';

export const MERCHANT_COMMISSION_TABLE = 'merchant_commissions';
@Entity(MERCHANT_COMMISSION_TABLE)
@Unique('merchant_id_asset_id_un', ['merchant_id', 'asset_id'])
export class MerchantCommissionEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'uuid' })
  merchant_id?: string;

  @Column({ type: 'uuid' })
  asset_id?: string;

  @Column({ type: 'varchar' })
  category?: string;

  @Column({ type: 'varchar', default: MERCHANT_COMMISION_STATUS.ACTIVE })
  status?: string;

  @Column({ type: 'float4', default: 0 })
  commission?: number;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;

  @Column({ type: 'uuid', nullable: true })
  updated_by?: string;
}

@EventSubscriber()
export class MerchantCommissionEntitySubscriber
  implements EntitySubscriberInterface<MerchantCommissionEntity>
{
  async beforeInsert(event: InsertEvent<MerchantCommissionEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }

  async beforeUpdate(event: UpdateEvent<MerchantCommissionEntity>) {
    if (!event.entity.deleted_at) {
      event.entity.updated_at = Date.now();
    }
  }
}
