import { ConnectionName } from 'src/const/database';
import { BOT_STATUS } from 'src/const/permission';
import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { BOT_TRADING_TABLE } from '../bot-trading.repository/bot-trading.entity';
import {
  MerchantCommissionEntity,
  MERCHANT_COMMISSION_TABLE,
} from './merchant-commission.entity';

export class MerchantCommissionRepository extends BaseRepository<MerchantCommissionEntity> {
  repo(): Repository<MerchantCommissionEntity> {
    return getRepository(MerchantCommissionEntity, ConnectionName.user_role);
  }
  async getPaging(param: {
    page?: number;
    size?: number;
    from?: number;
    to?: number;
    sort_by?: string;
    order_by?: string;
    status?: MERCHANT_COMMISION_STATUS;
    category?: string;
    merchant_id: string;
  }) {
    let { page, size, sort_by } = param;
    const { status, from, to, merchant_id, order_by, category } = param;
    page = Number(page || 1);
    size = Number(size || 50);
    const whereArray = [];
    if (sort_by && sort_by === 'created_at') {
      sort_by = 'mc.created_at';
    }
    if (from) {
      whereArray.push(`mc.created_at >= ${from}`);
    }
    if (to) {
      whereArray.push(`mc.created_at <= ${to}`);
    }
    if (status) {
      whereArray.push(`mc.status = '${status}'`);
    }
    if (merchant_id) {
      whereArray.push(`mc.merchant_id = '${merchant_id}'`);
    }
    if (category) {
      whereArray.push(`mc.category = '${category}'`);
    }
    
    whereArray.push(`bt.status = '${BOT_STATUS.OPEN}'`);

    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryTransaction = `
      SELECT mc.*, bt.name, bt.pnl, bt.max_drawdown, bt.price, bt.image_url, bt.order
      FROM 
      ${MERCHANT_COMMISSION_TABLE} mc 
      LEFT JOIN ${BOT_TRADING_TABLE} bt on bt.id = mc.asset_id
      ${where}  
      ORDER BY ${sort_by ? sort_by : 'mc.created_at'} ${
      order_by ? order_by : 'DESC'
    } 
      OFFSET ${(page - 1) * size}
      LIMIT ${size}
    `;
    const queryCount = `
    SELECT COUNT(*)
    FROM 
    ${MERCHANT_COMMISSION_TABLE} mc 
    LEFT JOIN ${BOT_TRADING_TABLE} bt on bt.id = mc.asset_id
    ${where}  
    `;
    const [rows, total] = await Promise.all([
      this.repo().query(queryTransaction),
      this.repo().query(queryCount),
    ]);
    return {
      rows,
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    };
  }
}
