import { ConnectionName } from 'src/const/database';
import { EventDomain } from 'src/domain/event/event.domain';
import { EVENT_STATUS } from 'src/domain/event/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { EventEntity, EVENTS_TABLE } from './event.entity';

export class EventRepository extends BaseRepository<EventEntity> {
  repo(): Repository<EventEntity> {
    return getRepository(EventEntity, ConnectionName.xtrading_user_service);
  }
  async getPaging(param: {
    page?: number;
    size?: number;
    keyword?: string;
    status?: EVENT_STATUS;
    deleted?: number;
  }) {
    let { page, size, deleted } = param;
    const { keyword, status } = param;
    page = Number(page || 1);
    size = Number(size || 50);
    deleted = Number(deleted || 0);
    const whereArray = [];
    if (status) {
      whereArray.push(`status = '${status}'`);
    }
    if (deleted == 0) {
      whereArray.push(`deleted_at is null`);
    }
    if (keyword) {
      whereArray.push(`(name LIKE '%${keyword}%'
        OR code LIKE '%${keyword}%')`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryEvent = `
        SELECT  *
        FROM ${EVENTS_TABLE}  
        ${where}  
        ORDER BY created_at DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `;
    const queryCount = `
    SELECT COUNT(*)
    FROM ${EVENTS_TABLE}  
    ${where}
    `;
    const [rows, total] = await Promise.all([
      this.repo().query(queryEvent),
      this.repo().query(queryCount),
    ]);
    return {
      rows: rows as EventDomain[],
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    };
  }
  async getEventToRemind(param: {
    remind?: '0' | '1';
    status?: EVENT_STATUS;
    from?: number;
    to?: number;
  }): Promise<EventEntity[]> {
    const whereArray = [];
    if (param.to) {
      whereArray.push(`email_remind_at <= ${param.to}`);
    }
    if (param.remind) {
      whereArray.push(
        `(remind = ${param.remind === '1' ? true : false} OR remind is null)`,
      );
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUserEvent = `
    SELECT *
    FROM ${EVENTS_TABLE}
    ${where}
    `;
    return this.repo().query(queryUserEvent);
  }
}
