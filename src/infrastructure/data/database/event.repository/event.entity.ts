import { EVENT_STATUS } from 'src/domain/event/types';
import {
  Entity,
  Column,
  PrimaryColumn,
  Index,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm';

export const EVENTS_TABLE = 'events';
@Entity(EVENTS_TABLE)
@Unique('code_deleted_at_un', ['code', 'deleted_at'])
export class EventEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'varchar' })
  @Index()
  name?: string;

  @Column({ type: 'varchar', nullable: true })
  @Index()
  code?: string;

  @Column({ type: 'varchar' })
  status?: EVENT_STATUS;

  @Column({ type: 'int4', nullable: true })
  attendees_number?: number;

  @Column({ type: 'bigint', nullable: true })
  email_remind_at?: number;

  @Column({ type: 'varchar', nullable: true })
  email_remind_template_id?: string;

  @Column({ type: 'boolean', nullable: true })
  email_confirm?: boolean;

  @Column({ type: 'varchar', nullable: true })
  email_confirm_template_id?: string;

  @Column({ type: 'bigint', nullable: true })
  start_at?: number;

  @Column({ type: 'bigint', nullable: true })
  finish_at?: number;

  @Column({ type: 'boolean', nullable: true, default: false })
  remind?: boolean;

  @Column({ type: 'boolean', nullable: true, default: false })
  create_qrcode?: boolean;

  @Column({ type: 'uuid', nullable: true })
  created_by?: string;

  @Column({ type: 'uuid', nullable: true })
  updated_by?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;

  @Column({ type: 'bigint', nullable: true })
  deleted_at?: number;
}

@EventSubscriber()
export class EventEntitySubscriber
  implements EntitySubscriberInterface<EventEntity>
{
  async beforeInsert(event: InsertEvent<EventEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
  }

  async beforeUpdate(event: UpdateEvent<EventEntity>) {
    if (!event.entity.deleted_at) {
      event.entity.updated_at = Date.now();
    }
  }
}
