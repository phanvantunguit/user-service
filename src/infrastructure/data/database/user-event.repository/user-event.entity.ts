import { EVENT_CONFIRM_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';
import {
  Entity,
  Column,
  PrimaryColumn,
  Index,
  Generated,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  Unique,
} from 'typeorm';

export const USERS_EVENTS_TABLE = 'users_events';
@Entity(USERS_EVENTS_TABLE)
@Unique('event_id_invite_code_un', ['event_id', 'invite_code'])
@Unique('event_id_email_un', ['event_id', 'email'])
export class UserEventEntity {
  @PrimaryColumn()
  @Generated('uuid')
  id?: string;

  @Column({ type: 'uuid', nullable: true })
  created_by?: string;

  @Column({ type: 'uuid', nullable: true })
  updated_by?: string;

  @Column({ type: 'bigint', default: Date.now() })
  created_at?: number;

  @Column({ type: 'bigint', default: Date.now() })
  updated_at?: number;

  @Column({ type: 'varchar', nullable: true })
  email?: string;

  @Column({ type: 'varchar', nullable: true })
  phone?: string;

  @Column({ type: 'varchar', nullable: true })
  telegram?: string;

  @Column({ type: 'varchar' })
  fullname?: string;

  @Column({ type: 'varchar', default: EVENT_CONFIRM_STATUS.WAITING })
  confirm_status?: EVENT_CONFIRM_STATUS;

  @Column({ type: 'boolean', default: false })
  attend?: boolean;

  @Column({ type: 'varchar' })
  @Index()
  event_id?: string;

  @Column({ type: 'varchar' })
  invite_code?: string;

  @Column({ type: 'boolean', nullable: true, default: false })
  remind?: boolean;

  @Column({ type: 'varchar', nullable: true, default: PAYMENT_STATUS.WAITING })
  payment_status?: PAYMENT_STATUS;
}

@EventSubscriber()
export class UserEventEntitySubscriber
  implements EntitySubscriberInterface<UserEventEntity>
{
  async beforeInsert(event: InsertEvent<UserEventEntity>) {
    event.entity.created_at = Date.now();
    event.entity.updated_at = Date.now();
    //   if (event.entity.password) {
    //     event.entity.password = await hashPassword(event.entity.password)
    //   }
  }

  async beforeUpdate(event: UpdateEvent<UserEventEntity>) {
    event.entity.updated_at = Date.now();
    //   if (event.entity.password) {
    //     event.entity.password = await hashPassword(event.entity.password)
    //   }
  }
}
