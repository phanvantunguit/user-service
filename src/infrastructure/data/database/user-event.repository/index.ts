import { ConnectionName } from 'src/const/database';
import {
  EVENT_CONFIRM_STATUS,
  INVITE_CODE_TYPE,
  PAYMENT_STATUS,
} from 'src/domain/event/types';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { UserEventEntity, USERS_EVENTS_TABLE } from './user-event.entity';

export class UserEventRepository extends BaseRepository<UserEventEntity> {
  repo(): Repository<UserEventEntity> {
    return getRepository(UserEventEntity, ConnectionName.xtrading_user_service);
  }
  async getPaging(param: {
    page?: number;
    size?: number;
    keyword?: string;
    type?: INVITE_CODE_TYPE;
    event_id?: string;
    confirm_status?: EVENT_CONFIRM_STATUS;
    payment_status?: PAYMENT_STATUS;
    attend?: number;
  }) {
    let { page, size, keyword } = param;
    const { type, event_id, confirm_status, attend, payment_status } = param;
    page = Number(page || 1);
    size = Number(size || 50);
    const whereArray = [];
    if (confirm_status) {
      whereArray.push(`confirm_status = '${confirm_status}'`);
    }
    if (payment_status) {
      whereArray.push(`payment_status = '${payment_status}'`);
    }
    if (attend) {
      whereArray.push(`attend = ${attend == 1 ? true : false}`);
    }
    if (type) {
      whereArray.push(`invite_code LIKE '%${type}%'`);
    }
    if (event_id) {
      whereArray.push(`event_id = '${event_id}'`);
    }
    if (keyword) {
      keyword = keyword.toLowerCase();
      whereArray.push(`(lower(email) LIKE '%${keyword}%'
        OR lower(fullname) LIKE '%${keyword}%'
        OR phone LIKE '%${keyword}%'
        OR lower(telegram) LIKE '%${keyword}%'
        OR invite_code LIKE '%${keyword.toUpperCase()}%')`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUserEvent = `
        SELECT  *
        FROM ${USERS_EVENTS_TABLE}  
        ${where}  
        ORDER BY created_at DESC
        OFFSET ${(page - 1) * size}
        LIMIT ${size}
    `;
    const queryCount = `
    SELECT COUNT(*)
    FROM ${USERS_EVENTS_TABLE}  
    ${where}
    `;
    const [rows, total] = await Promise.all([
      this.repo().query(queryUserEvent),
      this.repo().query(queryCount),
    ]);
    return {
      rows: rows as UserEventDomain[],
      page,
      size,
      count: rows.length as number,
      total: Number(total[0].count),
    };
  }
  async getUserByDate(param: {
    from?: number;
    to?: number;
    event_id: string;
  }): Promise<UserEventEntity[]> {
    const whereArray = [];
    if (param.event_id) {
      whereArray.push(`event_id = '${param.event_id}'`);
    }
    if (param.to) {
      whereArray.push(`created_at <= ${param.to}`);
    }
    if (param.from) {
      whereArray.push(`created_at >= ${param.from}`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUserEvent = `
    SELECT *
    FROM ${USERS_EVENTS_TABLE}
    ${where}
    `;
    return this.repo().query(queryUserEvent);
  }
  async getEmails(param: {
    event_id?: string;
    confirm_status?: EVENT_CONFIRM_STATUS;
    payment_status?: PAYMENT_STATUS;
    attend?: boolean;
  }): Promise<
    {
      email: string;
      fullname: string;
    }[]
  > {
    const whereArray = [];
    if (param.event_id) {
      whereArray.push(`event_id = '${param.event_id}'`);
    }
    if (param.confirm_status) {
      whereArray.push(`confirm_status = '${param.confirm_status}'`);
    }
    if (param.payment_status) {
      whereArray.push(`payment_status = '${param.payment_status}'`);
    }
    if (typeof param.attend === 'boolean') {
      whereArray.push(`attend = ${param.attend}`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const queryUserEvent = `
    SELECT email, fullname 
    FROM ${USERS_EVENTS_TABLE}
    ${where}
    group by email, fullname 
    `;
    return this.repo().query(queryUserEvent);
  }
}
