import { ConnectionName } from 'src/const/database';
import { BOT_STATUS } from 'src/domain/bot/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { BotEntity, BOT_TABLE } from './bot.entity';

export class BotRepository extends BaseRepository<BotEntity> {
  repo(): Repository<BotEntity> {
    return getRepository(BotEntity, ConnectionName.user_role);
  }
  list(): Promise<{ id: string; name: string }[]> {
    const queryBot = `
    SELECT *
    FROM ${BOT_TABLE}
    WHERE status = '${BOT_STATUS.OPEN}'
    ORDER BY name ASC
    `;
    return this.repo().query(queryBot);
  }
}
