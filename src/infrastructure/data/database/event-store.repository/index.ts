import { ConnectionName } from 'src/const/database';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import { EventStoreEntity } from './event-store.entity';

export class EventStoreRepository extends BaseRepository<EventStoreEntity> {
  repo(): Repository<EventStoreEntity> {
    return getRepository(EventStoreEntity, ConnectionName.user_role);
  }
}
