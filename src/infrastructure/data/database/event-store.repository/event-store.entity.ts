import {
    Entity,
    Column,
    PrimaryColumn,
    EventSubscriber,
    EntitySubscriberInterface,
    InsertEvent,
    Generated,
    Unique,
  } from 'typeorm'
  
  export const EVENT_STORE_TABLE = 'event_stores'
  @Entity(EVENT_STORE_TABLE, { synchronize: false })
  @Unique('event_id_event_name_state_es_un', ['event_id', 'event_name', 'state'])
  export class EventStoreEntity {
    @PrimaryColumn()
    @Generated('uuid')
    id?: string

    @Column({ type: 'varchar' })
    event_id?: string
  
    @Column({ type: 'varchar' })
    event_name?: string

    @Column({ type: 'uuid', nullable: true })
    user_id?: string

    @Column({ type: 'varchar' })
    state?: string

    @Column({ type: 'jsonb', default: {} })
    metadata?: any
    
    @Column({ type: 'varchar', nullable: true })
    description?: string
    
    @Column({ type: 'bigint', default: Date.now() })
    created_at?: number
  }
  
  @EventSubscriber()
  export class EventStoreEntitySubscriber
    implements EntitySubscriberInterface<EventStoreEntity>
  {
    async beforeInsert(event: InsertEvent<EventStoreEntity>) {
      event.entity.created_at = Date.now()
    }
  }
  