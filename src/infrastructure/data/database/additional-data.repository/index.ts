import { ConnectionName } from 'src/const/database';
import {
  ADDITIONAL_DATA_TYPE,
  MERCHANT_ADDITIONAL_DATA_STATUS,
} from 'src/domain/additional-data/types';
import { Repository, getRepository } from 'typeorm';
import { BaseRepository } from '../base.repository';
import {
  AdditionalDataEntity,
  ADDITIONAL_DATA_TABLE,
} from './additional-data.entity';

export class AdditionalDataRepository extends BaseRepository<AdditionalDataEntity> {
  repo(): Repository<AdditionalDataEntity> {
    return getRepository(AdditionalDataEntity, ConnectionName.user_role);
  }
  list(param: {
    keyword?: string;
    type?: ADDITIONAL_DATA_TYPE;
  }) {
    const { keyword, type } = param;
    const whereArray = [];
    if (keyword) {
      whereArray.push(`(name ILIKE '%${keyword}%')`);
    }
    if (type) {
      whereArray.push(`type = '${type}'`);
    }
    const where =
      whereArray.length > 0 ? `WHERE ${whereArray.join(' AND ')}` : '';
    const query = `
    SELECT *
    FROM ${ADDITIONAL_DATA_TABLE}
    ${where} 
    order by created_at DESC
  `;
    return this.repo().query(query);
  }
}
