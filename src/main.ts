import { NestFactory } from '@nestjs/core';
import { TradingAppModule } from './trading-app.module';
import { APP_CONFIG } from './config';
import { ENVIRONMENT } from './config/types';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { ExceptionsUtil } from './utils/exceptions';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { BackgroudAppModule } from './background-app.module';
import { EventJob } from './backgroud-jobs/event';
import { MerchantJob } from './backgroud-jobs/merchant';

async function bootstrap() {
  const tradingAppModule = await NestFactory.create<NestExpressApplication>(
    TradingAppModule,
  );
  tradingAppModule.enableCors();
  tradingAppModule.useStaticAssets(join(__dirname, '..', 'public'));
  tradingAppModule.useGlobalPipes(
    new ValidationPipe({ whitelist: false, forbidNonWhitelisted: true }),
  );
  tradingAppModule.useGlobalFilters(new ExceptionsUtil());
  if (APP_CONFIG.ENVIRONMENT !== ENVIRONMENT.PRODUCTION) {
    const merchantSwaggerConfig = new DocumentBuilder()
      .setTitle('8X Trading Document')
      .setDescription('This is user roles API document')
      .setVersion('1.0')
      .addBearerAuth()
      .addServer(APP_CONFIG.BASE_URL)
      .build();
    const userRolesAppDocument = SwaggerModule.createDocument(
      tradingAppModule,
      merchantSwaggerConfig,
    );
    SwaggerModule.setup(
      'api/v1/swagger',
      tradingAppModule,
      userRolesAppDocument,
    );
  }
  await tradingAppModule.listen(APP_CONFIG.PORT || 3000);

  const backgroudApp = await NestFactory.createApplicationContext(
    BackgroudAppModule,
  );
  const eventJob = backgroudApp.get(EventJob);
  eventJob.run();
  // const merchantJob = backgroudApp.get(MerchantJob);
  // merchantJob.run();
}
bootstrap();
