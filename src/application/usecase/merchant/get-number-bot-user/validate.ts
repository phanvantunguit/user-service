import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsObject, IsString, IsUUID } from 'class-validator';

export class GetNumberOfBotForUser {
  @ApiProperty({
    required: true,
    description: 'id',
  })
  @IsString()
  id?: string;

  @ApiProperty({
    required: true,
    description: 'Userid',
  })
  @IsString()
  user_id?: string;

  @ApiProperty({
    required: true,
    description: 'Owner created',
  })
  @IsString()
  owner_created?: string;

  @ApiProperty({
    required: true,
    description: 'name',
  })
  @IsString()
  name?: string;

  @ApiProperty({
    required: true,
    description: 'value number of bot',
  })
  @IsNumber()
  value?: string;

  @ApiProperty({
    required: true,
    description: 'description',
  })
  @IsString()
  description?: string;

  @ApiProperty({
    required: true,
    description: 'created at',
  })
  @IsString()
  created_at?: number;

  @ApiProperty({
    required: true,
    description: 'created at',
  })
  @IsString()
  updated_at?: number;
}

export class GetNumberOfBotForUserOutput {
  @ApiProperty({
    required: true,
    description: 'Get number of bot',
  })
  @IsObject()
  payload: GetNumberOfBotForUser;
}
