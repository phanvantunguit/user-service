import { Inject } from '@nestjs/common';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { GetNumberOfBotForUserOutput } from './validate';
export class GetNumberOfBotUserHandler {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
  ) {}
  async execute(
    user_id: string,
    owner_created: string,
  ): Promise<GetNumberOfBotForUserOutput> {
    const dataRes = await this.settingRepository.findOneAppSetting({
      user_id,
      owner_created,
    });
    return {
      payload: dataRes,
    };
  }
}
