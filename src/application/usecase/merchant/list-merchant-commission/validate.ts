import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsIn,
  IsNumber,
  IsNumberString,
  IsObject,
  IsOptional,
} from 'class-validator';
import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import {
  ORDER_CATEGORY,
  TRANSACTION_STATUS,
} from 'src/domain/transaction/types';
import { AdminUpdateCommissionValidate } from '../validate';

export class ListMerchantCommissionValidate extends AdminUpdateCommissionValidate {
  @ApiProperty({
    required: true,
    description: `${Object.keys(ORDER_CATEGORY).join(',')}`,
    example: ORDER_CATEGORY.TBOT,
  })
  @IsIn(Object.values(ORDER_CATEGORY))
  category: ORDER_CATEGORY;
}

export class ListMerchantCommissionInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number;

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;

  @ApiProperty({
    required: false,
    description: 'filed name: created_at, commission, price',
    example: 'commission',
  })
  @IsOptional()
  @IsIn(['created_at', 'commission', 'price'])
  sort_by: string;

  @ApiProperty({
    required: false,
    description: 'desc or asc',
    example: 'desc',
  })
  @IsOptional()
  @IsIn(['desc', 'asc'])
  order_by: string;

  @ApiProperty({
    required: false,
    description: `${MERCHANT_COMMISION_STATUS.ACTIVE} or ${MERCHANT_COMMISION_STATUS.INACTIVE}`,
    example: MERCHANT_COMMISION_STATUS.ACTIVE,
  })
  @IsOptional()
  @IsIn(Object.values(MERCHANT_COMMISION_STATUS))
  status?: MERCHANT_COMMISION_STATUS;

  @ApiProperty({
    required: false,
    description: `${Object.keys(ORDER_CATEGORY).join(',')}`,
    example: ORDER_CATEGORY.TBOT,
  })
  @IsOptional()
  @IsIn(Object.values(ORDER_CATEGORY))
  category: ORDER_CATEGORY;
}

export class PagingListMerchantCommissionOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number;

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number;

  @ApiProperty({
    required: false,
    isArray: true,
    type: ListMerchantCommissionValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ListMerchantCommissionValidate)
  rows: ListMerchantCommissionValidate[];
}

export class ListMerchantCommissioOutput {
  @ApiProperty({
    required: true,
    description: 'Result transaction paging',
  })
  @IsObject()
  payload: PagingListMerchantCommissionOutput;
}
