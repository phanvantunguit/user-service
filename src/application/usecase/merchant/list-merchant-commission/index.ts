import { Inject } from '@nestjs/common';
import { MerchantCommissionRepository } from 'src/infrastructure/data/database/merchant-commission.repository';
import {
  ListMerchantCommissionInput,
  ListMerchantCommissioOutput,
} from './validate';
export class ListMerchantCommissionHandler {
  constructor(
    @Inject(MerchantCommissionRepository)
    private merchantCommissionRepository: MerchantCommissionRepository,
  ) {}
  async execute(
    param: ListMerchantCommissionInput,
    id: string,
  ): Promise<ListMerchantCommissioOutput> {
    const dataRes = await this.merchantCommissionRepository.getPaging({
      ...param,
      merchant_id: id,
    });
    return {
      payload: dataRes,
    };
  }
}
