import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';
import { MerchantValidate } from '../validate';

export class AdminCreateMerchantInput extends MerchantValidate {}

export class AdminCreateMerchantOutput {
  @ApiProperty({
    required: true,
    description: 'UUID of merchant',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsUUID()
  payload: string;
}
