import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import {
  MERCHANT_CONFIG_ONLY_SYSTEM,
  MERCHANT_TYPE,
} from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { generatePassword } from 'src/utils/hash.util';
import { CheckMerchantInfoHandler } from '../check-merchant-info';
import {
  AdminCreateMerchantInput,
  AdminCreateMerchantOutput,
} from './validate';

export class AdminCreateMerchantHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(CheckMerchantInfoHandler)
    private checkMerchantInfoHandler: CheckMerchantInfoHandler,
    @Inject(MailService) private mailService: MailService,
  ) {}
  async execute(
    param: AdminCreateMerchantInput,
    userId: string,
  ): Promise<AdminCreateMerchantOutput> {
    const checkInfo = await this.checkMerchantInfoHandler.execute(param);
    if (!checkInfo.payload) {
      badRequestError('Merchant', ERROR_CODE.INVALID);
    }
    if (param.domain) {
      const merchantExisted = await this.merchantRepository.findOne({
        domain: param.domain,
      });
      if (
        merchantExisted &&
        merchantExisted.config &&
        merchantExisted.config.domain_type === MERCHANT_TYPE.OTHERS
      ) {
        badRequestError('Domain', ERROR_CODE.EXISTED);
      }
    }
    if (param.config) {
      for (let key of MERCHANT_CONFIG_ONLY_SYSTEM) {
        delete param.config[key];
      }
    }

    const password = generatePassword();
    const data = new MerchantDomain({
      ...param,
      created_by: userId,
      updated_by: userId,
      password,
    });
    const result = await this.merchantRepository.save(data);
    await this.mailService.sendEmailCreateMerchant(param.email, password);
    return {
      payload: result.id,
    };
  }
}
