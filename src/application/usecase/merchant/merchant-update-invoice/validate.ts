import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsIn,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import {
  MERCHANT_INVOICE_STATUS,
  MERCHANT_WALLET_TYPE,
} from 'src/domain/merchant/types';

export class MerchantUpdateInvoiceInput {
  @ApiProperty({
    required: true,
    description: `${Object.keys(MERCHANT_INVOICE_STATUS).join(',')}`,
    example: MERCHANT_INVOICE_STATUS.COMPLETED,
  })
  @IsIn(Object.values(MERCHANT_INVOICE_STATUS))
  status: MERCHANT_INVOICE_STATUS;

  @ApiProperty({
    description: 'Transaction Id',
    example: '0daa9f2507c4e79e39391ea165bb76ed018c4cd69d7da129edf9e95f0dae99e2',
  })
  @IsOptional()
  transaction_id: string;

  @ApiProperty({
    required: false,
    description: 'Description',
    example: 'description',
  })
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty({
    required: true,
    example: MERCHANT_WALLET_TYPE.TRC20,
  })
  @IsIn(Object.values(MERCHANT_WALLET_TYPE))
  type: MERCHANT_WALLET_TYPE;
}

export class MerchantUpdateInvoiceOutput {
  @ApiProperty({
    required: true,
    description: 'true or false',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
