import { Inject } from '@nestjs/common';
import { MERCHANT_INVOICES_STATUS } from 'src/domain/invoices/types';
import {
  MERCHANT_STATUS,
  MERCHANT_WALLET_TYPE,
} from 'src/domain/merchant/types';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import {
  MerchantUpdateInvoiceInput,
  MerchantUpdateInvoiceOutput,
} from './validate';
import axios from 'axios';
import { APP_CONFIG } from 'src/config';
const TronWeb = require('tronweb');
var crypto = require('crypto');

var privateKey = crypto.randomBytes(32).toString('hex');
const HttpProvider = TronWeb.providers.HttpProvider;
const fullNode = new HttpProvider('https://api.trongrid.io');
const solidityNode = new HttpProvider('https://api.trongrid.io');
const eventServer = new HttpProvider('https://api.trongrid.io');
const tronWeb = new TronWeb(fullNode, solidityNode, eventServer, privateKey);

export class UpdateMerchantInvoiceHandler {
  constructor(
    @Inject(MerchantInvoiceRepository)
    private merchantInvoiceRepository: MerchantInvoiceRepository,
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    invoice_id: string,
    param: MerchantUpdateInvoiceInput,
  ): Promise<MerchantUpdateInvoiceOutput> {
    const invoice = await this.merchantInvoiceRepository.findById(invoice_id);

    if (!invoice) {
      badRequestError('Invoice', ERROR_CODE.NOT_FOUND);
    }

    const histories = invoice.metadata?.histories || [];
    delete invoice.metadata;
    if (invoice?.status == MERCHANT_INVOICES_STATUS.COMPLETED) {
      delete param.transaction_id;
      delete param.status;
      param['id'] = invoice_id;

      const dataUpdate = {
        ...invoice,
        description: param.description,
        created_at: Date.now(),
        updated_at: Date.now(),
      };
      histories.push(dataUpdate);
      param['metadata'] = {
        histories,
      };

      await this.merchantInvoiceRepository.save(param);
    } else {
      if (param.status != 'COMPLETED') {
        delete param.transaction_id;
        param['id'] = invoice_id;

        const dataUpdate = {
          ...invoice,
          description: param.description,
          status: param.status,
          created_at: Date.now(),
          updated_at: Date.now(),
        };
        histories.push(dataUpdate);
        param['metadata'] = {
          histories,
        };

        await this.merchantInvoiceRepository.save(param);
      } else {
        // Check transaction exist
        const TransactionExist =
          await this.merchantInvoiceRepository.getTransaction(
            param.transaction_id,
          );
        if (TransactionExist[0].count != '0') {
          badRequestError('Transaction was', ERROR_CODE.EXISTED);
        }
        if (!invoice) {
          badRequestError('Invoice', ERROR_CODE.NOT_FOUND);
        }
        const merchant = await this.merchantRepository.findById(
          invoice?.merchant_id,
        );
        if (merchant.config?.status == MERCHANT_STATUS.INACTIVE) {
          badRequestError('Wallet', ERROR_CODE.INVALID);
        }
        const hexToDecimal = (hex) => parseInt(hex, 16);
        if (param.type == MERCHANT_WALLET_TYPE.TRC20) {
          await tronWeb.trx
            .getConfirmedTransaction(param.transaction_id)
            .then((result) => {
              //Check wallet
              const walletAddress = tronWeb.address.fromHex(
                '41' +
                  result.raw_data.contract[0].parameter?.value?.data.slice(
                    32,
                    72,
                  ),
              );
              if (walletAddress != invoice?.wallet_address) {
                badRequestError('Wallet', ERROR_CODE.INVALID);
              }
              // Check Transaction new in month
              if (result.raw_data.timestamp < invoice.created_at) {
                badRequestError(
                  'Transaction created before invoice',
                  ERROR_CODE.BAD_REQUEST,
                );
              }
              // Check Transaction transfer amount
              if (!result.raw_data.contract[0].parameter?.value?.data) {
                badRequestError(
                  'Transaction transfer value',
                  ERROR_CODE.NOT_FOUND,
                );
              }
              const decimalAmount = hexToDecimal(
                result.raw_data.contract[0].parameter?.value?.data.slice(-64),
              );
              const amountCompele =
                Number(invoice.amount_commission_complete) * 1000000;

              if (decimalAmount < amountCompele) {
                badRequestError(
                  'The deposited amount is less than the commission amount',
                  ERROR_CODE.BAD_REQUEST,
                );
              }
              // if (decimalAmount > amountCompele) {
              //   badRequestError(
              //     'The deposited amount is greater than the commission amount.',
              //     ERROR_CODE.BAD_REQUEST,
              //   );
              // }
              if (result?.ret[0]?.contractRet != 'SUCCESS') {
                badRequestError('Confirmed Transaction', ERROR_CODE.INVALID);
              }
            })
            .catch((error) => {
              if (error.status) badRequestError('', error);
              badRequestError('Transaction', ERROR_CODE.NOT_FOUND);
            });
        } else if (param.type == MERCHANT_WALLET_TYPE.BSC20) {
          // Check invoices payment by BSC20
          let walletRp = await axios.get(
            `https://api.bscscan.com/api?module=proxy&action=eth_getTransactionByHash&txhash=${param.transaction_id}&apikey=${APP_CONFIG.APIKEY_BSC20}`,
          );
          let walletFrom = walletRp.data?.result?.from;
          let contractTo = walletRp.data?.result?.to;
          let blockBumber = hexToDecimal(walletRp.data?.result?.blockNumber);
          let checkInvoid = await axios.get(
            `https://api.bscscan.com/api?module=account&action=tokentx&contractaddress=${contractTo}&address=${walletFrom}&page=0&offset=5&startblock=${blockBumber}&endblock=${blockBumber}&sort=asc&apikey=${APP_CONFIG.APIKEY_BSC20}`,
          );
          // Check wallet
          const walletAddress = checkInvoid.data?.result[0].to;
          if (walletAddress != invoice?.wallet_address) {
            badRequestError('Wallet', ERROR_CODE.INVALID);
          }
          // Check Transaction new in month
          if (
            checkInvoid.data?.result[0].timeStamp * 1000 <
            invoice.created_at
          ) {
            badRequestError(
              'Transaction created before invoice',
              ERROR_CODE.BAD_REQUEST,
            );
          }
          // Check Transaction transfer amount
          if (!checkInvoid.data?.result[0]?.value) {
            badRequestError('Transaction transfer value', ERROR_CODE.NOT_FOUND);
          }
          const amount = checkInvoid.data?.result[0]?.value;
          const amountCompele =
            Number(invoice.amount_commission_complete) * 1000000000000000000;

          if (amount < amountCompele) {
            badRequestError(
              'The deposited amount is less than the commission amount',
              ERROR_CODE.BAD_REQUEST,
            );
          }
        }
        invoice.status = param.status;
        invoice.transaction_id = param.transaction_id;
        invoice.description = param.description;
        invoice.wallet_address = merchant.config?.wallet?.wallet_address;

        const dataUpdate = {
          ...invoice,
          created_at: Date.now(),
          updated_at: Date.now(),
        };
        histories.push(dataUpdate);
        param['metadata'] = {
          histories,
        };

        await this.merchantInvoiceRepository.save(invoice);
      }
    }
    return {
      payload: true,
    };
  }
}
