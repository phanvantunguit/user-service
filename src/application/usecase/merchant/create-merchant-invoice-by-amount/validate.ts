import { ApiProperty } from "@nestjs/swagger";
import { IsNumber } from "class-validator";

export class CreateMerchantInvoiceByAmountInput {
    @ApiProperty({
      required: true,
      description: 'amount',
    })
    @IsNumber()
    amount: number;
  }