import { Inject } from '@nestjs/common';
import { toDate } from 'date-fns-tz';
import { startOfMonth, endOfDay } from 'date-fns';
import { MERCHANT_INVOICE_STATUS } from 'src/domain/merchant/types';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { APP_CONFIG } from 'src/config';
import { CreateMerchantInvoiceByAmountInput } from './validate';
import { BaseCreateOutput } from '../../validate';

export class CreateMerchantInvoiceByAmountHandler {
  constructor(
    @Inject(MerchantInvoiceRepository)
    private merchantInvoiceRepository: MerchantInvoiceRepository,
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: CreateMerchantInvoiceByAmountInput,
    merchant_id: string,
    merchant_code?: string,
    user_id?: string,
  ): Promise<BaseCreateOutput> {
    const { amount } = param;
    if (amount < APP_CONFIG.MIN_INVOICE_AMOUNT) {
      const error = {
        error_code: ERROR_CODE.INVALID.error_code,
        message: `minimum is ${APP_CONFIG.MIN_INVOICE_AMOUNT} USD`,
      };
      badRequestError('Amount', error);
    }
    const merchant = await this.merchantRepository.findById(merchant_id);
    if (!merchant_code) {
      if (!merchant) {
        badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
      }
      merchant_code = merchant.code;
    }
    const invoiceRepository = await this.merchantInvoiceRepository.findOne([
      {
        merchant_id,
        status: MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
      },
      {
        merchant_id,
        status: MERCHANT_INVOICE_STATUS.WALLET_INVALID,
      },
    ]);
    if (invoiceRepository) {
      badRequestError('Invoice', ERROR_CODE.EXISTED);
    }

    const [totalCommission, totalAmountInvoice] = await Promise.all([
      this.transactionRepository.totalCommission({ merchant_code }),
      this.merchantInvoiceRepository.totalCommission({ merchant_id }),
    ]);
    const amountAvailable =
      Number(totalCommission[0].total) + Number(totalAmountInvoice[0].total);
    if (Number(amountAvailable.toFixed(2)) < +Number(amount)) {
      badRequestError('Amount', ERROR_CODE.INVALID);
    }
    const invoice = {
      merchant_id: merchant_id,
      amount_commission_complete: Number(amount),
      status: MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
      updated_by: user_id,
      wallet_address: merchant.config?.wallet?.wallet_address,
      wallet_network: merchant.config?.wallet?.network,
      created_at: Date.now()
    };
    const invoiceCreate = {
      ...invoice,
      metadata: {
        histories: [invoice],
      },
    };
    const saved = await this.merchantInvoiceRepository.save(invoiceCreate);
    return {
      payload: saved.id,
    };
  }
}
