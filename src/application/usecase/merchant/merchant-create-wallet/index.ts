import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import { MERCHANT_STATUS } from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { comparePassword } from 'src/utils/hash.util';
import { MerchantCreateInput, MerchantCreateOutput } from './validate';
import axios from 'axios';
import { APP_CONFIG } from 'src/config';
const tronWeb = require('tronweb');

export class CreateWalletHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(MailService)
    private mailService: MailService,
  ) {}
  async execute(
    param: MerchantCreateInput,
    id: string,
  ): Promise<MerchantCreateOutput> {
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('id', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(param.password, merchant.password);
    if (!checkPass) {
      badRequestError('Password', ERROR_CODE.INCORRECT);
    }
    if (param.otp != merchant.config?.wallet?.otp) {
      badRequestError('OTP', ERROR_CODE.INCORRECT);
    }
    const expiresTime = new Date().getTime();
    if (expiresTime > merchant.config?.wallet?.expires_at) {
      badRequestError('OTP', ERROR_CODE.EXPIRES);
    }
    if (param.type == 'BSC20') {
      const walletRp = await axios.get(
        `https://api.bscscan.com/api?module=account&action=balance&address=${param.wallet_address}&apikey=${APP_CONFIG.APIKEY_BSC20}`,
      );
      if (walletRp?.data?.message != 'OK') {
        badRequestError(
          'Type of wallet address must be',
          ERROR_CODE.NOT_TYPE_BSC20,
        );
      }
    } else {
      const checkWallet = await tronWeb.isAddress(param.wallet_address);
      if (!checkWallet) {
        badRequestError(
          'Type of wallet address must be',
          ERROR_CODE.NOT_TYPE_TRC20,
        );
      }
    }
    // const checkWallet = await tronWeb.isAddress(param.wallet_address);
    // if (!checkWallet) {
    //   badRequestError(
    //     'Type of wallet address must be',
    //     ERROR_CODE.NOT_TYPE_TRC20,
    //   );
    // }
    if (
      param.wallet_address != merchant.config?.wallet?.send_mail_wallet_address
    ) {
      badRequestError(
        'Wallet address does not match the wallet address in the email.',
        ERROR_CODE.BAD_REQUEST,
      );
    }
    merchant.config = {
      ...merchant.config,
      wallet: {
        wallet_address: param.wallet_address,
        status: MERCHANT_STATUS.ACTIVE,
        network: param.type,
        otp: null,
        expires_at: null,
      },
      wallet_active_history: merchant.config?.wallet_active_history ?? [],
    };
    // add wallet_active_history
    merchant.config.wallet_active_history.push({
      wallet_address: merchant.config.wallet.wallet_address,
      create_at: expiresTime,
    });
    delete merchant.password;

    await this.merchantRepository.save(merchant);
    return {
      payload: true,
    };
  }
}
