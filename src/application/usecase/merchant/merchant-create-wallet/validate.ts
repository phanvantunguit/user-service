import { IsString, IsBoolean, IsIn } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { MERCHANT_WALLET_TYPE } from 'src/domain/merchant/types';

export class MerchantCreateInput {
  @ApiProperty({
    required: true,
    description: 'Input password',
  })
  @IsString()
  password: string;

  @ApiProperty({
    required: true,
    description: 'Input wallet address',
  })
  @IsString()
  wallet_address: string;

  @ApiProperty({
    required: true,
    description: 'Input OTP',
  })
  @IsString()
  otp: string;

  @ApiProperty({
    required: true,
    example: MERCHANT_WALLET_TYPE.TRC20,
  })
  @IsIn(Object.values(MERCHANT_WALLET_TYPE))
  type: MERCHANT_WALLET_TYPE;
}

export class MerchantCreateOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
