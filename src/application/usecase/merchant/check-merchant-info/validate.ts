import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export class CheckMerchantInfoInput {
  @ApiProperty({
    required: false,
    description: 'id',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsOptional()
  @IsUUID()
  id?: string;

  @ApiProperty({
    required: false,
    description: 'name',
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    required: false,
    description: 'code',
  })
  @IsOptional()
  @IsString()
  code?: string;

  @ApiProperty({
    required: false,
    description: 'email',
  })
  @IsOptional()
  @IsEmail()
  email?: string;
}
export class CheckMerchantInfoOutput {
  @ApiProperty({
    required: true,
    description: 'Status of validate: true is valid, false is invalid',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
