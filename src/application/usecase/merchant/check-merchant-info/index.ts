import { Inject } from '@nestjs/common';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { CheckMerchantInfoInput, CheckMerchantInfoOutput } from './validate';

export class CheckMerchantInfoHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: CheckMerchantInfoInput,
  ): Promise<CheckMerchantInfoOutput> {
    const query = [];
    if (param.code) {
      query.push({ code: param.code });
    }
    if (param.name) {
      query.push({ name: param.name });
    }
    if (param.email) {
      query.push({ email: param.email });
    }
    let result = true;
    if (query.length === 0) {
      result = false;
    } else {
      const merchant = await this.merchantRepository.findOne(query);
      if (merchant) {
        if (merchant.id !== param.id) {
          result = false;
        }
      }
    }
    return {
      payload: result,
    };
  }
}
