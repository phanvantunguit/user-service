import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString } from 'class-validator';

export class MerchantLoginInput {
  @ApiProperty({
    required: true,
    description: 'Email to login',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Phone number',
    example: '123456Aa',
  })
  @IsString()
  password: string;
}

export class MerchantLoginOutput {
  @ApiProperty({
    required: true,
    description: 'Token',
    example: 'jwt',
  })
  @IsString()
  payload: string;
}
