import { Inject } from '@nestjs/common';
import { APP_CONFIG } from 'src/config';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { comparePassword, generateTokenSecret } from 'src/utils/hash.util';
import { MerchantLoginInput, MerchantLoginOutput } from './validate';

export class MerchantLoginHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(param: MerchantLoginInput, merchantCode?: string): Promise<MerchantLoginOutput> {
    const merchant = await this.merchantRepository.findOne({
      email: param.email,
    });
    if (!merchant) {
      badRequestError('Email', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(param.password, merchant.password);
    if (!checkPass) {
      badRequestError('Password', ERROR_CODE.INCORRECT);
    }
    let merchantAccess
    if(merchantCode && merchant.supper_merchant) {
      merchantAccess = await this.merchantRepository.findOne({
        code: merchantCode,
      });
    }
    if(!merchantAccess) {
      merchantAccess = merchant
    }

    const token = generateTokenSecret(
      {
        user_id: merchantAccess.id,
        email: merchantAccess.email,
        iss: 'cextrading',
        merchant: true,
        merchant_code: merchantAccess.code,
        super_merchant: merchant.supper_merchant,
      },
      APP_CONFIG.SECRET_KEY,
      Number(APP_CONFIG.TIME_EXPIRED_LOGIN),
    );
    return {
      payload: token,
    };
  }
}
