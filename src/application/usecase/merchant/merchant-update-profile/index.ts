import { Inject } from '@nestjs/common';
import {
  MERCHANT_CONFIG_ONLY_ADMIN,
  MERCHANT_CONFIG_ONLY_SYSTEM,
} from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import {
  MerchantUpdateProfileInput,
  MerchantUpdateProfileOutput,
} from './validate';

export class MerchantUpdateProfileHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: MerchantUpdateProfileInput,
    id: string,
  ): Promise<MerchantUpdateProfileOutput> {
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }
    if (param['password']) {
      delete param['password'];
    }
    const data = {
      id,
    };
    if (param.config) {
      for (let key of MERCHANT_CONFIG_ONLY_ADMIN) {
        delete param.config[key];
      }
      for (let key of MERCHANT_CONFIG_ONLY_SYSTEM) {
        delete param.config[key];
      }

      data['config'] = {
        ...merchant.config,
        ...param.config,
      };
    }
    await this.merchantRepository.save(data);
    return {
      payload: true,
    };
  }
}
