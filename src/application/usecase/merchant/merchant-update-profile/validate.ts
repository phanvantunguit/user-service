import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsObject, IsOptional } from 'class-validator';
import { MerchantModifyConfigValidate } from '../validate';

export class MerchantUpdateProfileInput {
  @ApiProperty({
    required: false,
    description: 'config merchant',
  })
  @IsObject()
  @IsOptional()
  config: MerchantModifyConfigValidate;
}

export class MerchantUpdateProfileOutput {
  @ApiProperty({
    required: true,
    description: 'true or false',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
