import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEmail,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import { MERCHANT_STATUS } from 'src/domain/merchant/types';
import { AdminModifyMerchantConfigValidate } from '../validate';

export class AdminUpdateMerchantInput {
  @ApiProperty({
    required: false,
    description: 'name',
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    required: false,
    description: 'code',
  })
  @IsOptional()
  @IsString()
  code?: string;

  @ApiProperty({
    required: false,
    description: 'email',
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    required: false,
    description: `${Object.keys(MERCHANT_STATUS).join(',')}`,
    example: MERCHANT_STATUS.ACTIVE,
  })
  @IsOptional()
  @IsString()
  status: MERCHANT_STATUS;

  @ApiProperty({
    required: false,
    description: 'code',
  })
  @IsString()
  @IsOptional()
  description: string;

  @ApiProperty({
    required: false,
    description: 'config merchant',
  })
  @IsObject()
  @IsOptional()
  config: AdminModifyMerchantConfigValidate;

  @ApiProperty({
    required: false,
    description: 'domain',
  })
  @IsString()
  @IsOptional()
  domain: string;
}

export class AdminUpdateMerchantOutput {
  @ApiProperty({
    required: true,
    description: 'true or false',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
