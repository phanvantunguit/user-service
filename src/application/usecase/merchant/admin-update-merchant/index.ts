import { Inject } from '@nestjs/common';
import { MERCHANT_CONFIG_ONLY_SYSTEM } from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { CheckMerchantInfoHandler } from '../check-merchant-info';
import {
  AdminUpdateMerchantInput,
  AdminUpdateMerchantOutput,
} from './validate';

export class AdminUpdateMerchantHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(CheckMerchantInfoHandler)
    private checkMerchantInfoHandler: CheckMerchantInfoHandler,
  ) {}
  async execute(
    param: AdminUpdateMerchantInput,
    id: string,
    userId: string,
  ): Promise<AdminUpdateMerchantOutput> {
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }
    if (param['password']) {
      delete param['password'];
    }

    if (param.code || param.name || param.email) {
      const checkInfo = await this.checkMerchantInfoHandler.execute({
        ...param,
        id,
      });
      if (!checkInfo.payload) {
        badRequestError('Merchant', ERROR_CODE.INVALID);
      }
    }
    const data = {
      id,
      updated_by: userId,
      ...param,
    };
    if (param.config) {
      for (let key of MERCHANT_CONFIG_ONLY_SYSTEM) {
        delete param.config[key];
      }
      data['config'] = {
        ...merchant.config,
        ...param.config,
      };
    }
    await this.merchantRepository.save(data);
    return {
      payload: true,
    };
  }
}
