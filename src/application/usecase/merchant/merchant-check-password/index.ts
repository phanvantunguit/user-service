import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { comparePassword } from 'src/utils/hash.util';
import {
  MerchantVerifyPasswordInput,
  MerchantVerifyPasswordOutput,
} from './validate';

export class MerchantVerifyPasswordHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: MerchantVerifyPasswordInput,
    id: string,
  ): Promise<MerchantVerifyPasswordOutput> {
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('id', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(param.password, merchant.password);
    if (!checkPass) {
      badRequestError('Password', ERROR_CODE.INCORRECT);
    }
    return {
      payload: true,
    };
  }
}
