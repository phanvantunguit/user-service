import { IsString, IsBoolean } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MerchantVerifyPasswordInput {
  @ApiProperty({
    required: true,
    description: 'Input password',
  })
  @IsString()
  password: string;
}

export class MerchantVerifyPasswordOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
