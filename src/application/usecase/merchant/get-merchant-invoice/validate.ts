import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsObject } from 'class-validator';
import { ListMerchantInvoiceValidate } from '../list-merchant-invoice/validate';

export class GetMerchantInvoiceIdOutput {
  @ApiProperty({
    required: true,
    description: 'Merchant invoice by id',
  })
  @IsObject()
  payload: ListMerchantInvoiceValidate;
}
