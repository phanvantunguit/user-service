import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';

export class GetMerchantInvoiceHandler {
  constructor(
    @Inject(MerchantInvoiceRepository)
    private merchantInvoiceRepository: MerchantInvoiceRepository,
  ) {}
  async execute(id: string): Promise<any> {
    const invoice = await this.merchantInvoiceRepository.getDetail(id);
    if (!invoice) {
      badRequestError('Invoice', ERROR_CODE.NOT_FOUND);
    }
    return {
      payload: invoice,
    };
  }
}
