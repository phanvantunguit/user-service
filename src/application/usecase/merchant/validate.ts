import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsString,
  IsEmail,
  IsOptional,
  IsArray,
  IsObject,
  IsNumber,
  IsUUID,
  IsIn,
} from 'class-validator';
import { MERCHANT_COMMISION_STATUS } from 'src/domain/commission/types';
import { MERCHANT_STATUS } from 'src/domain/merchant/types';

export class MerchantModifyConfigValidate {
  @ApiProperty({
    required: false,
    description: 'logo link',
  })
  @IsOptional()
  @IsString()
  logo_url: string;
}
export class AdminModifyMerchantConfigValidate extends MerchantModifyConfigValidate {
  @ApiProperty({
    required: false,
    description: 'permission',
    isArray: true,
  })
  @IsOptional()
  @IsArray()
  @Type(() => String)
  permission?: string[];

  @ApiProperty({
    required: false,
    description: 'commission for each transaction',
  })
  @IsOptional()
  @IsNumber()
  commission: number;
}
export class MerchantValidate {
  @ApiProperty({
    required: true,
    description: 'name',
  })
  @IsString()
  name: string;
  @ApiProperty({
    required: true,
    description: 'code',
  })
  @IsString()
  code: string;
  @ApiProperty({
    required: true,
    description: 'email',
  })
  @IsEmail()
  email: string;
  @ApiProperty({
    required: true,
    description: `${Object.keys(MERCHANT_STATUS).join(',')}`,
    example: MERCHANT_STATUS.ACTIVE,
  })
  @IsString()
  status: MERCHANT_STATUS;
  @ApiProperty({
    required: false,
    description: 'code',
  })
  @IsString()
  @IsOptional()
  description: string;

  @ApiProperty({
    required: false,
    description: 'domain',
  })
  @IsString()
  @IsOptional()
  domain: string;

  @ApiProperty({
    required: false,
    description: 'config merchant',
  })
  @IsObject()
  @IsOptional()
  config: AdminModifyMerchantConfigValidate;
}

export class AdminUpdateCommissionValidate {
  @ApiProperty({
    required: false,
    description: 'UUID of merchant commission',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsOptional()
  @IsUUID()
  id?: string;

  @ApiProperty({
    required: true,
    description: 'UUID of asset',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsUUID()
  asset_id: string;

  @ApiProperty({
    required: false,
    description: `${MERCHANT_COMMISION_STATUS.ACTIVE} or ${MERCHANT_COMMISION_STATUS.INACTIVE}`,
    example: MERCHANT_COMMISION_STATUS.ACTIVE,
  })
  @IsOptional()
  @IsIn(Object.values(MERCHANT_COMMISION_STATUS))
  status?: MERCHANT_COMMISION_STATUS;

  @ApiProperty({
    required: false,
    description: 'Percent commission: 0 to 1, example 0.05 is 5%',
    example: 0.1,
  })
  @IsOptional()
  @IsNumber()
  commission: number;
}

export class MerchantUpdateCommissionValidate {
  @ApiProperty({
    required: true,
    description: 'UUID of merchant commission',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsUUID()
  id?: string;

  @ApiProperty({
    required: true,
    description: 'UUID of asset',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsUUID()
  asset_id: string;

  @ApiProperty({
    required: false,
    description: `${MERCHANT_COMMISION_STATUS.ACTIVE} or ${MERCHANT_COMMISION_STATUS.INACTIVE}`,
    example: MERCHANT_COMMISION_STATUS.ACTIVE,
  })
  @IsOptional()
  @IsIn(Object.values(MERCHANT_COMMISION_STATUS))
  status?: MERCHANT_COMMISION_STATUS;
}
