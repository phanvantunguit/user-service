import { Inject } from '@nestjs/common';
import { MERCHANT_TYPE } from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { UpdateEmailSenderInput } from './validate';

export class UpdateEmailSenderHandler {
  constructor(
    @Inject(MailService) private mailService: MailService,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: UpdateEmailSenderInput,
    id: string,
    userId?: string,
  ): Promise<any> {
    const { email, name, country, city, address } = param;
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }

    if (merchant.config['domain_type'] === MERCHANT_TYPE.OTHERS) {
      const config = merchant.config;
      const dataSender = {
        nickname: name,
        from_email: email,
        from_name: name,
        reply_to: email,
        address,
        city,
        country,
      };
      let resSerder;
      if (
        config['email_sender'] &&
        config['email_sender']['from_email'] &&
        config['email_sender']['id']
      ) {
        if (config['email_sender']['from_email'] === email) {
          resSerder = await this.mailService.updateSender({
            ...dataSender,
            id: config['email_sender']['id'],
          });
        } else {
          await this.mailService.deleteSender({
            id: config['email_sender']['id'],
          });
          resSerder = await this.mailService.sendRequestVerifySender(
            dataSender,
          );
          config['verified_sender'] = false;
        }
      } else {
        resSerder = await this.mailService.sendRequestVerifySender(dataSender);
        config['verified_sender'] = false;
      }
      if (resSerder) {
        if (resSerder.id) {
          dataSender['id'] = resSerder.id;
        }
        config['email_sender'] = dataSender;
        const saveConfig = {
          id,
          config,
        };
        if (userId) {
          saveConfig['updated_by'] = userId;
        }
        await this.merchantRepository.save(saveConfig);
        return true;
      }
      badRequestError('Serder', ERROR_CODE.INVALID);
    }
    badRequestError('KOL', ERROR_CODE.BAD_REQUEST);
  }
}
