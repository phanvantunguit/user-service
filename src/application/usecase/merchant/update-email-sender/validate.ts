import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UpdateEmailSenderInput {
  @ApiProperty({
    required: true,
    description: 'Representative email',
  })
  @IsString()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Representative name',
  })
  @IsString()
  name: string;

  @ApiProperty({
    required: true,
    description: 'Country',
  })
  @IsString()
  country: string;

  @ApiProperty({
    required: true,
    description: 'City',
  })
  @IsString()
  city: string;

  @ApiProperty({
    required: true,
    description: 'Address',
  })
  @IsString()
  address: string;
}
