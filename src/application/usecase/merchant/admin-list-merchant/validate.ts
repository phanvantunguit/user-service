import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { MerchantValidate } from '../validate';

export class AdminListMerchantOutput {
  @ApiProperty({
    required: true,
    description: 'Numerical order failed',
    isArray: true,
    type: MerchantValidate,
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => MerchantValidate)
  payload: MerchantValidate[];
}
