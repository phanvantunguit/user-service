import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { AdminListMerchantOutput } from './validate';

export class AdminListMerchantHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(): Promise<AdminListMerchantOutput> {
    const merchants = (await this.merchantRepository.find(
      {},
    )) as MerchantDomain[];
    const result = merchants.map((e) => {
      delete e.password;
      return e;
    });
    return {
      payload: result,
    };
  }
}
