import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsIn,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { PACKAGE_TYPE } from 'src/domain/commission/types';

export class AddUserAssetInput {
  @ApiProperty({
    required: true,
    description: 'Asset id',
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e5',
  })
  @IsUUID()
  asset_id: string;

  @ApiProperty({
    required: true,
    description: 'quantity of item',
  })
  @IsNumber()
  quantity: number;

  @ApiProperty({
    required: true,
    description: 'type of item',
    example: PACKAGE_TYPE.MONTH,
  })
  @IsIn(Object.values(PACKAGE_TYPE))
  @IsString()
  type: PACKAGE_TYPE;
}

export class MerchantCreateAssetInput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: AddUserAssetInput,
  })
  @IsOptional()
  @IsArray()
  @Type(() => AddUserAssetInput)
  rows: AddUserAssetInput[];
}

export class MerchantCreateAssetOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
