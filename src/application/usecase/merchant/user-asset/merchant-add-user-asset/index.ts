import { HttpStatus, Inject } from '@nestjs/common';
import { TBOT_TYPE } from 'src/domain/bot/types';
import { PACKAGE_TYPE } from 'src/domain/commission/types';
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/domain/transaction/types';
import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import {
  MerchantCreateAssetInput,
  MerchantCreateAssetOutput,
} from './validate';

export class MerchantAddUserAssetHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
  ) {}
  async execute(
    params: MerchantCreateAssetInput,
    ownerCreated: string,
    user_id: string,
  ): Promise<MerchantCreateAssetOutput> {
    const { rows } = params;
    const botIds = [...new Set(rows.map((e) => e.asset_id))]; // get unique bot id
    const botIdList = await this.botTradingRepository.findByIds(botIds);
    if (botIdList.length !== botIds.length) {
      badRequestError('Asset', ERROR_CODE.BAD_REQUEST);
    }
    const dataAdds = [];
    const updateUserAssetLogs = [];
    for (let item of rows) {
      try {
        let expiresAt = null;
        if (item.quantity) {
          const expiredTime = new Date();
          if (item.type === PACKAGE_TYPE.DAY) {
            expiredTime.setDate(expiredTime.getDate() + Number(item.quantity));
          } else {
            expiredTime.setMonth(
              expiredTime.getMonth() + Number(item.quantity),
            );
          }
          expiresAt = expiredTime.valueOf();
        }
        const dataAdd = {
          user_id: user_id,
          expires_at: expiresAt,
          owner_created: ownerCreated,
          type: TBOT_TYPE.ASSIGN
        };
        const updateUserAssetLog = {
          user_id: user_id,
          category: ORDER_CATEGORY.TBOT,
          status: ITEM_STATUS.NOT_CONNECT,
          owner_created: ownerCreated,
          expires_at: expiresAt,
          quantity: item.quantity,
          package_type: item.type,
          type: TBOT_TYPE.ASSIGN
        };
        let asset;
        let user_asset;
        asset = botIdList.find((botId) => botId.id == item.asset_id);

        user_asset = await this.userRepository.findUserBotTrading({
          user_id: user_id,
          bot_id: item.asset_id,
        });
        dataAdd['bot_id'] = item.asset_id;
        if (asset) {
          if (user_asset[0]) {
            dataAdd['id'] = user_asset[0].id;
          }
          dataAdd['status'] = ITEM_STATUS.NOT_CONNECT;
          updateUserAssetLog['name'] = asset.name;
          //@ts-ignore
          dataAdds.push(dataAdd);
        }
        updateUserAssetLog['asset_id'] = asset.id;
        updateUserAssetLogs.push(updateUserAssetLog);
      } catch (error) {}
    }
    await this.userRepository.saveUserBotTrading(dataAdds);
    await this.userRepository.saveUserAssetLog(updateUserAssetLogs);
    return {
      payload: true,
    };
  }
}
