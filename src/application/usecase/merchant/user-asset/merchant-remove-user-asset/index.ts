import { HttpStatus, Inject } from '@nestjs/common';
import { PACKAGE_TYPE } from 'src/domain/commission/types';
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/domain/transaction/types';
import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import {
  MerchantRemoveAssetInput,
  MerchantRemoveAssetOutput,
} from './validate';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { comparePassword } from 'src/utils/hash.util';
import { CoinmapService } from 'src/infrastructure/services';

export class MerchantRemoveUserAssetHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository, // @Inject(UserBotTradingEntity) // private userBotTradingEntity: UserBotTradingEntity,
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(CoinmapService) private coinmapService: CoinmapService,
  ) {}
  async execute(
    params: MerchantRemoveAssetInput,
    ownerCreated: string,
  ): Promise<MerchantRemoveAssetOutput> {
    const { rows, password } = params;
    const merchant = await this.merchantRepository.findById(ownerCreated);
    if (!merchant) {
      badRequestError('id', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(password, merchant.password);
    if (!checkPass) {
      badRequestError('Password', ERROR_CODE.INCORRECT);
    }
    const assetIds = [];
    rows.map((item) => assetIds.push(item.user_asset_id));
    const assetIdList = await this.userRepository.findByAssetIds(assetIds);
    if (assetIdList.length !== rows.length) {
      badRequestError('Asset', ERROR_CODE.BAD_REQUEST);
    }

    const botIds = [];
    assetIdList.map((item) => botIds.push(item.bot_id));
    const botIdList = await this.botTradingRepository.findByIds(botIds);

    const updateUserAssetLogs = [];
    for (let item of assetIdList) {
      try {
        // call api remove account
        const deleted = await this.coinmapService.deleteUserBotTrading({
          id: item.id,
          merchant_code: merchant.code,
          password: merchant.password,
        });
        if (deleted.status) {
          const updateUserAssetLog = {
            user_id: item.user_id,
            category: ORDER_CATEGORY.TBOT,
            status: ITEM_STATUS.DELETED,
            owner_created: ownerCreated,
          };
          let asset;
          asset = botIdList.find((botId) => botId.id == item.bot_id);
          updateUserAssetLog['name'] = asset.name;
          updateUserAssetLog['asset_id'] = asset.id;
          updateUserAssetLogs.push(updateUserAssetLog);
        }
      } catch (error) {
        console.log('check error', error);
      }
    }
    await this.userRepository.saveUserAssetLog(updateUserAssetLogs);
    return {
      payload: true,
    };
  }
}
