import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsBoolean, IsOptional, IsString } from 'class-validator';

export class MerchantRemoveUserAssetInput {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  user_asset_id: string;
}

export class MerchantRemoveAssetInput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: MerchantRemoveUserAssetInput,
  })
  @IsOptional()
  @IsArray()
  @Type(() => MerchantRemoveUserAssetInput)
  rows: MerchantRemoveUserAssetInput[];

  @ApiProperty({
    required: true,
    example: 'Merchant password',
  })
  @IsString()
  password: string;
}

export class MerchantRemoveAssetOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
