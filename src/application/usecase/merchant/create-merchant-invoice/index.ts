import { Inject } from '@nestjs/common';
import { toDate } from 'date-fns-tz';
import { startOfMonth, endOfDay } from 'date-fns';
import { MERCHANT_INVOICE_STATUS } from 'src/domain/merchant/types';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';

export class CreateMerchantInvoiceHandler {
  constructor(
    @Inject(MerchantInvoiceRepository)
    private merchantInvoiceRepository: MerchantInvoiceRepository,
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    merchant_id: string,
    merchant_code?: string,
    user_id?: string,
  ): Promise<any> {
    console.log('merchant_id', merchant_id);
    return
    const lastInvoice = await this.merchantInvoiceRepository.findOne({
      merchant_id,
    });
    if (!merchant_code) {
      const merchant = await this.merchantRepository.findById(merchant_id);
      if (!merchant) {
        badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
      }
      merchant_code = merchant.code;
    }

    console.log('lastInvoice', lastInvoice);
    let lastDayInvoice = lastInvoice
      ? startOfMonth(Number(lastInvoice.created_at))
      : null;
    let from;
    if (lastDayInvoice) {
      from = toDate(lastDayInvoice, {
        timeZone: 'Asia/Ho_Chi_Minh',
      }).valueOf();
    }
    // const currentDay = endOfMonth(Date.now());
    // currentDay.setMonth(currentDay.getMonth() - 1);
    var currentDay = new Date();
    var lastDayOfMonth = new Date(
      currentDay.getFullYear(),
      currentDay.getMonth(),
      0,
    );
    const to = toDate(endOfDay(lastDayOfMonth), {
      timeZone: 'Asia/Ho_Chi_Minh',
    }).valueOf();

    console.log('CreateMerchantInvoiceHandler from:', from);
    console.log('CreateMerchantInvoiceHandler to:', to);

    const chart = await this.transactionRepository.chart({
      from,
      to,
      merchant_code,
      timezone: 'UTC-7',
      time_type: 'MONTH',
    });
    const dataMerchantInvoices = []
    for(let report of chart) {
      if(Number(report.commission_cash) > 0) {
        dataMerchantInvoices.push({
          merchant_id: merchant_id,
          start_at: report.start_at,
          finish_at: report.finish_at,
          count_pending:
            Number(report.count_total) -
            Number(report.count_complete) -
            Number(report.count_failed),
          count_complete: Number(report.count_complete),
          amount_pending:
            Number(report.amount_total) -
            Number(report.amount_complete) -
            Number(report.amount_failed),
          amount_complete: Number(report.amount_complete),
          amount_commission_pending:
            Number(report.commission_cash_total) -
            Number(report.commission_cash_failed) -
            Number(report.commission_cash),
          amount_commission_complete: Number(report.commission_cash),
          status: MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
          updated_by: user_id,
        })
      }
    }
    console.log('dataMerchantInvoices', dataMerchantInvoices);
    if (dataMerchantInvoices.length === 0) {
      return false;
    }
    const saved = await this.merchantInvoiceRepository.save(
      dataMerchantInvoices,
    );
    console.log('CreateMerchantInvoiceHandler saved:', saved);
    return saved;
  }
}
