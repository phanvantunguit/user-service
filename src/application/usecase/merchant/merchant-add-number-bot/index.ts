import { Inject } from '@nestjs/common';
import { TBOT_TYPE } from 'src/domain/bot/types';
import { ITEM_STATUS } from 'src/domain/transaction/types';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import {
  MerchantAddNumberBotDto,
  MerchantAddNumberBotOutput,
} from './validate';

export class MerchantAddBotForUserHandle {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
    @Inject(UserRepository)
    private userRepository: UserRepository,
  ) {}
  async execute(
    params: MerchantAddNumberBotDto,
    user_id: string,
    ownerId: string,
  ): Promise<MerchantAddNumberBotOutput> {
    //Find transaction user have bot
    const settingNumberOfBot = await this.settingRepository.findOneAppSetting({
      name: params.name,
      user_id: user_id,
    });
    const userBotConnected = await this.userRepository.findUserBotTradingSQL({user_id, type: TBOT_TYPE.SELECT})
    const settingNumberOfBotUsed = Number(userBotConnected.length) || 0
    if (settingNumberOfBot) {
      const valueAddBot = Number(params.value);
      // Check bot used < bot add
      if (settingNumberOfBotUsed > valueAddBot
      ) {
        badRequestError(
          'Value Bot no bigger than value Bot used',
          ERROR_CODE.BAD_REQUEST,
        );
      }
      await this.settingRepository.saveAppSetting({
        ...params,
        id: settingNumberOfBot.id,
        user_id: user_id,
        owner_created: ownerId,
      });
    } else {
      await this.settingRepository.saveAppSetting({
        ...params,
        user_id: user_id,
        owner_created: ownerId,
      });
    }
    return {
      payload: true,
    };
  }
}
