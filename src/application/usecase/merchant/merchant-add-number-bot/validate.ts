import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class MerchantAddNumberBotDto {
  @ApiProperty({
    required: true,
    description: 'Name of app setting',
    example: 'NUMBER_OF_TBOT',
  })
  @IsString()
  name: string;

  @ApiProperty({
    required: true,
    description: 'Number',
    example: '1',
  })
  @IsString()
  value: string;

  @ApiProperty({
    required: false,
    description: 'Description of app setting',
    example: '',
  })
  @IsOptional()
  @IsString()
  description: string;
}

export class MerchantAddNumberBotOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
