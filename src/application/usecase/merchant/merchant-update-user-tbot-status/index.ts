import { Inject } from '@nestjs/common';
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/domain/transaction/types';
import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { CoinmapService } from 'src/infrastructure/services/coinmap';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import {
  MerchantUpdateStatusInput,
  MerchantUpdateStatusInputOutput,
} from './validate';

export class MerchantUpdateUserBotTradingStatusHandler {
  constructor(
    @Inject(UserRepository) private userRepository: UserRepository,
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(CoinmapService) private coinmapService: CoinmapService,
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    params: MerchantUpdateStatusInput,
    id: string,
    merchantId: string,
  ): Promise<MerchantUpdateStatusInputOutput> {
    const userBot =  await this.userRepository.findByAssetIds([id])
    if(!userBot[0]) {
      badRequestError('Bot', ERROR_CODE.NOT_FOUND)
    }

    const bot = await this.botTradingRepository.findById(userBot[0].bot_id)
    const merchant = await this.merchantRepository.findById(merchantId)
    if(!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND)
    }

    if(params.status === ITEM_STATUS.INACTIVE) {
      const updateStatus = await this.coinmapService.updateUserBotTradingStatus({
        id: userBot[0].id,
        status: params.status,
        merchant_code: merchant.code,
        password: merchant.password
      })
      if(!updateStatus.status) {
        badRequestError('Bot', ERROR_CODE.UNPROCESSABLE)
      }
    } else {
      await this.userRepository.saveStatusBotTrading({
        id,
        status: params.status,
      });
    }
    const dataLog = {
      asset_id: userBot[0].bot_id,
      user_id: userBot[0].user_id,
      category: ORDER_CATEGORY.TBOT,
      status: params.status,
      owner_created: merchantId,
      name: bot.name
    };
    await this.userRepository.saveUserAssetLog([dataLog]);
    return {
      payload: true,
    };
  }
}
