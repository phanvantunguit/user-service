import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsObject, IsOptional } from 'class-validator';
import { ITEM_STATUS } from 'src/domain/transaction/types';

export class MerchantUpdateStatusInput {
  @ApiProperty({
    required: false,
    description: 'Status tbot',
    example: ITEM_STATUS.INACTIVE_BY_SYSTEM,
  })
  @IsOptional()
  status: ITEM_STATUS;
}

export class MerchantUpdateStatusInputOutput {
  @ApiProperty({
    required: true,
    description: 'true or false',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
