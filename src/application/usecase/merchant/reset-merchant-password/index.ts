import { Inject } from '@nestjs/common';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { generatePassword } from 'src/utils/hash.util';
import { AdminUpdateMerchantOutput } from '../admin-update-merchant/validate';

export class ResetMerchantPasswordHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(MailService) private mailService: MailService,
  ) {}
  async execute(
    id: string,
    user_id: string,
  ): Promise<AdminUpdateMerchantOutput> {
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }
    const password = await generatePassword();
    await this.merchantRepository.save({
      id: merchant.id,
      updated_by: user_id,
      password,
    });
    await this.mailService.sendEmailResetPassword(merchant.email, password);
    return {
      payload: true,
    };
  }
}
