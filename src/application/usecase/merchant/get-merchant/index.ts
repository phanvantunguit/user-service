import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { GetMerchantOutput } from './validate';

export class GetMerchantHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(id: string): Promise<GetMerchantOutput> {
    const merchant = (await this.merchantRepository.findById(
      id,
    )) as MerchantDomain;
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }
    delete merchant.password;
    return {
      payload: merchant,
    };
  }
}
