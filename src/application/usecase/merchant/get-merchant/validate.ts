import { ApiProperty } from '@nestjs/swagger';
import { IsObject } from 'class-validator';
import { MerchantValidate } from '../validate';

export class GetMerchantOutput {
  @ApiProperty({
    required: true,
    description: 'Merchant info',
  })
  @IsObject()
  payload: MerchantValidate;
}
