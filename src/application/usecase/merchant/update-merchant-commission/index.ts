import { Inject } from '@nestjs/common';
import { MerchantCommissionRepository } from 'src/infrastructure/data/database/merchant-commission.repository';
import { AdminUpdateMerchantOutput } from '../admin-update-merchant/validate';
import {
  MerchantUpdateCommissionInput,
  AdminUpdateCommissionInput,
} from './validate';

export class UpdateMerchantCommissionHandler {
  constructor(
    @Inject(MerchantCommissionRepository)
    private merchantCommissionRepository: MerchantCommissionRepository,
  ) {}
  async execute(
    param: AdminUpdateCommissionInput | MerchantUpdateCommissionInput,
    id: string,
    userId?: string,
  ): Promise<AdminUpdateMerchantOutput> {
    const dataUpdates = param.data.map((e) => {
      const commission = {
        ...e,
        category: param.category,
        merchant_id: id,
      };
      if (userId) {
        commission['updated_by'] = userId;
      } else {
        delete commission.commission;
      }
      return commission;
    });
    await this.merchantCommissionRepository.save(dataUpdates);
    return {
      payload: true,
    };
  }
}
