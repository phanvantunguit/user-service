import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsIn } from 'class-validator';
import { ORDER_CATEGORY } from 'src/domain/transaction/types';
import {
  AdminUpdateCommissionValidate,
  MerchantUpdateCommissionValidate,
} from '../validate';

export class AdminUpdateCommissionInput {
  @ApiProperty({
    required: true,
    description: `${Object.keys(ORDER_CATEGORY).join(',')}`,
    example: ORDER_CATEGORY.TBOT,
  })
  @IsIn(Object.values(ORDER_CATEGORY))
  category: ORDER_CATEGORY;

  @ApiProperty({
    required: true,
    isArray: true,
    type: AdminUpdateCommissionValidate,
  })
  @IsArray()
  @Type(() => AdminUpdateCommissionValidate)
  data: AdminUpdateCommissionValidate[];
}
export class MerchantUpdateCommissionInput {
  @ApiProperty({
    required: true,
    description: `${Object.keys(ORDER_CATEGORY).join(',')}`,
    example: ORDER_CATEGORY.TBOT,
  })
  @IsIn(Object.values(ORDER_CATEGORY))
  category: ORDER_CATEGORY;

  @ApiProperty({
    required: true,
    isArray: true,
    type: MerchantUpdateCommissionValidate,
  })
  @IsArray()
  @Type(() => MerchantUpdateCommissionValidate)
  data: MerchantUpdateCommissionValidate[];
}
