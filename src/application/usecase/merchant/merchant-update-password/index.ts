import { Inject } from '@nestjs/common';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { comparePassword } from 'src/utils/hash.util';
import {
  MerchantUpdatePasswordInput,
  MerchantUpdatePasswordOutput,
} from './validate';

export class MerchantUpdatePasswordHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: MerchantUpdatePasswordInput,
    userId: string,
  ): Promise<MerchantUpdatePasswordOutput> {
    const merchant = await this.merchantRepository.findById(userId);
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(
      param.current_password,
      merchant.password,
    );
    if (!checkPass) {
      badRequestError('Current Password', ERROR_CODE.INCORRECT);
    }
    await this.merchantRepository.save({
      id: userId,
      password: param.new_password,
    });
    return {
      payload: true,
    };
  }
}
