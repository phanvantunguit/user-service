import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsBoolean } from 'class-validator';

export class MerchantUpdatePasswordInput {
  @ApiProperty({
    required: true,
    description: 'Current password',
    example: '123456Aa',
  })
  @IsString()
  current_password: string;

  @ApiProperty({
    required: true,
    description: 'New password',
    example: '123456Ab',
  })
  @IsString()
  new_password: string;
}

export class MerchantUpdatePasswordOutput {
  @ApiProperty({
    required: true,
    description: 'true or false',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
