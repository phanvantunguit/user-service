import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class VerifyEmailSenderInput {
  @ApiProperty({
    required: true,
    description: 'Link verify',
  })
  @IsString()
  url_verify: string;
}
