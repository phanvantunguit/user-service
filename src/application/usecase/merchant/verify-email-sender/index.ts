import { Inject } from '@nestjs/common';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { VerifyEmailSenderInput } from './validate';

export class VerifyEmailSenderHandler {
  constructor(
    @Inject(MailService) private mailService: MailService,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: VerifyEmailSenderInput,
    id: string,
    userId?: string,
  ): Promise<any> {
    const { url_verify } = param;
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
    }
    const data = decodeURIComponent(url_verify);
    const query = data.split('?').find((e) => e.includes('token'));
    if (query) {
      console.log('query', query);
      const urlParams = new URLSearchParams(query);
      const token = urlParams.get('token'); // returns "John"
      console.log('token', token);
      const verified = await this.mailService.sendVerifySender({ token });
      if (verified) {
        const config = merchant.config;
        config['verified_sender'] = true;
        const saveConfig = {
          id,
          config,
        };
        if (userId) {
          saveConfig['updated_by'] = userId;
        }
        await this.merchantRepository.save(saveConfig);
        return {
          payload: true,
        };
      }
    }
    badRequestError('URL', ERROR_CODE.INVALID);
  }
}
