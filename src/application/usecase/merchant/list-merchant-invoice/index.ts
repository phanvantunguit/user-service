import { Inject } from '@nestjs/common';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import {
  ListMerchantInvoiceInput,
  ListMerchantInvoiceOutput,
} from './validate';
export class ListMerchantInvoiceHandler {
  constructor(
    @Inject(MerchantInvoiceRepository)
    private merchantInvoiceRepository: MerchantInvoiceRepository,
  ) {}
  async execute(
    param: ListMerchantInvoiceInput,
  ): Promise<ListMerchantInvoiceOutput> {
    const dataRes = await this.merchantInvoiceRepository.getPaging({
      ...param,
    });
    return {
      payload: dataRes,
    };
  }
}
