import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsIn,
  IsNumber,
  IsNumberString,
  IsObject,
  IsOptional,
  IsUUID,
} from 'class-validator';
import { MERCHANT_INVOICES_STATUS } from 'src/domain/invoices/types';

export class ListMerchantInvoiceInput {
  @ApiProperty({
    required: false,
    description: 'keyword',
    example: 'Merchant name or Merchant Email',
  })
  @IsOptional()
  keyword: string;

  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number;

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;

  @ApiProperty({
    required: false,
    description: 'filed name: created_at, start_at, finish_at',
    example: 'start_at',
  })
  @IsOptional()
  @IsIn(['created_at', 'start_at', 'finish_at'])
  sort_by: string;

  @ApiProperty({
    required: false,
    description: 'desc or asc',
    example: 'desc',
  })
  @IsOptional()
  @IsIn(['desc', 'asc'])
  order_by: string;

  @ApiProperty({
    required: false,
    description: `${MERCHANT_INVOICES_STATUS.COMPLETED} or ${MERCHANT_INVOICES_STATUS.WAITING_PAYMENT} or ${MERCHANT_INVOICES_STATUS.WALLET_INVALID}`,
    example: MERCHANT_INVOICES_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(MERCHANT_INVOICES_STATUS))
  status?: MERCHANT_INVOICES_STATUS;

  @ApiProperty({
    required: false,
    description: 'get by merchant',
  })
  @IsUUID()
  @IsOptional()
  merchant_id: string;
}

export class ListMerchantInvoiceValidate {
  @ApiProperty({
    required: false,
    description: 'id',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsOptional()
  id: string;

  @ApiProperty({
    required: false,
    description: 'merchant_id',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsOptional()
  merchant_id: string;

  @ApiProperty({
    required: false,
    description: 'created_at',
    example: '1678098869024',
  })
  @IsOptional()
  start_at: string;

  @ApiProperty({
    required: false,
    description: 'Finish at',
    example: '1678098869024',
  })
  @IsOptional()
  finish_at: string;
  //
  @ApiProperty({
    required: false,
    description: 'Count pending',
    example: 'Count pending',
  })
  @IsOptional()
  count_pending: string;

  @ApiProperty({
    required: false,
    description: 'Count complete',
    example: 'Complete',
  })
  @IsOptional()
  count_complete: string;

  @ApiProperty({
    required: false,
    description: 'Amount pending',
    example: 'Amount pending',
  })
  @IsOptional()
  amount_pending: string;

  @ApiProperty({
    required: false,
    description: 'Amount complete',
    example: 'Amount complete',
  })
  @IsOptional()
  amount_complete: string;

  @ApiProperty({
    required: false,
    description: 'Amount commission pending',
    example: 'Amount commission pending',
  })
  @IsOptional()
  amount_commission_pending: string;

  @ApiProperty({
    required: false,
    description: 'Amount commission complete',
    example: 'Amount commission complete',
  })
  @IsOptional()
  amount_commission_complete: string;

  @ApiProperty({
    required: false,
    description: `${MERCHANT_INVOICES_STATUS.COMPLETED} or ${MERCHANT_INVOICES_STATUS.WAITING_PAYMENT} or ${MERCHANT_INVOICES_STATUS.WALLET_INVALID}`,
    example: MERCHANT_INVOICES_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(MERCHANT_INVOICES_STATUS))
  status?: MERCHANT_INVOICES_STATUS;

  @ApiProperty({
    required: false,
    description: 'Transaction id',
    example: 'TRC20',
  })
  @IsOptional()
  transaction_id: string;

  @ApiProperty({
    required: false,
    description: 'Wallet address',
    example: 'TRC20',
  })
  @IsOptional()
  wallet_address: string;

  @ApiProperty({
    required: false,
    description: 'Description',
    example: 'Description',
  })
  @IsOptional()
  description: string;

  @ApiProperty({
    required: false,
    description: 'Created at',
    example: '1678098869024',
  })
  @IsOptional()
  created_at: string;

  @ApiProperty({
    required: false,
    description: 'Updated at',
    example: '1678098869024',
  })
  @IsOptional()
  updated_at: string;

  @ApiProperty({
    required: false,
    description: 'Updated by',
    example: 'Updated by',
  })
  @IsOptional()
  updated_by: string;
}

export class PagingListMerchantInvoiceOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number;

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number;

  @ApiProperty({
    required: false,
    isArray: true,
    type: ListMerchantInvoiceValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ListMerchantInvoiceValidate)
  rows: ListMerchantInvoiceValidate[];
}

export class ListMerchantInvoiceOutput {
  @ApiProperty({
    required: true,
    description: 'Result transaction paging',
  })
  @IsObject()
  payload: PagingListMerchantInvoiceOutput;
}
