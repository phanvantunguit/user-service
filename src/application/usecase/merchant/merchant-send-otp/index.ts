import { Inject } from '@nestjs/common';
import { MerchantDomain } from 'src/domain/merchant/merchant.domain';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { comparePassword } from 'src/utils/hash.util';
import { MerchantSendOtpInput, MerchantSendOtpOutput } from './validate';
import axios from 'axios';
import { env } from 'process';
import { APP_CONFIG } from 'src/config';
const tronWeb = require('tronweb');

export class SendOtpWalletHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
    @Inject(MailService)
    private mailService: MailService,
  ) {}
  async execute(
    param: MerchantSendOtpInput,
    id: string,
  ): Promise<MerchantSendOtpOutput> {
    const merchant = await this.merchantRepository.findById(id);
    if (!merchant) {
      badRequestError('id', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(param.password, merchant.password);
    if (!checkPass) {
      badRequestError('Password', ERROR_CODE.INCORRECT);
    }
    if (param.type == 'BSC20') {
      const walletRp = await axios.get(
        `https://api.bscscan.com/api?module=account&action=balance&address=${param.wallet_address}&apikey=${APP_CONFIG.APIKEY_BSC20}`,
      );
      if (walletRp?.data?.message != 'OK') {
        badRequestError(
          'Type of wallet address must be',
          ERROR_CODE.NOT_TYPE_BSC20,
        );
      }
    } else {
      const checkWallet = await tronWeb.isAddress(param.wallet_address);
      if (!checkWallet) {
        badRequestError(
          'Type of wallet address must be',
          ERROR_CODE.NOT_TYPE_TRC20,
        );
      }
    }
    const Otp = Math.floor(100000 + Math.random() * 900000); // Otp 6 number
    const timeNow = new Date().getTime();
    if (
      timeNow >= merchant.config?.wallet?.expires_at ||
      merchant.config?.wallet?.expires_at == undefined
    ) {
      await this.mailService.sendEmailOTP(
        merchant.email,
        Otp.toString(),
        param.wallet_address.toString(),
      ); // send Mail
      const expiresTime = new Date().getTime() + 60 * 1000 * 2; // 2 minutes
      merchant.config = {
        ...merchant.config,
        wallet: {
          wallet_address: merchant.config?.wallet?.wallet_address,
          status: merchant.config?.wallet?.status,
          network: param.type,
          otp: Otp,
          expires_at: expiresTime,
          send_mail_wallet_address: param.wallet_address,
        },
      };
      delete merchant.password;

      await this.merchantRepository.save(merchant);
    }
    return {
      payload: true,
    };
  }
}
