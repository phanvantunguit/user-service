import { Inject } from '@nestjs/common';
import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { BaseCreateOutput } from '../../validate';
import { ModifyAdditionalDataInput } from './validate';
import {
  ADDITIONAL_DATA_TYPE,
  ADDITIONAL_DEFAULT_NAME,
  STANDALONE_ADDITIONAL,
} from 'src/domain/additional-data/types';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { ERROR_CODE } from 'src/utils/exceptions/const';

export class ModifyAdditionalDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
  ) {}
  async execute(
    param: ModifyAdditionalDataInput,
    id?: string,
  ): Promise<BaseCreateOutput> {
    const { type, name, data } = param;
    const dataSave = {
      type,
      name,
      data,
    };
    if(id) {
      dataSave['id'] = id
    }
    // check standalone additional
    if (STANDALONE_ADDITIONAL.includes(type) && (!name || name === ADDITIONAL_DEFAULT_NAME)) {
      // check additional existed
      const additionalExisted = await this.additionalDataRepository.findOne({
        name: ADDITIONAL_DEFAULT_NAME,
        type: type,
      });
      if (additionalExisted) {
        if(!id) {
          badRequestError(type, ERROR_CODE.EXISTED);
        }
        if(additionalExisted.id !== id) {
          badRequestError('Id', ERROR_CODE.INVALID);
        }
        dataSave['id'] = additionalExisted.id

      }
      dataSave.name = ADDITIONAL_DEFAULT_NAME
    }
    if (type === ADDITIONAL_DATA_TYPE.TBOT_FEE) {
      data?.ranges.map((item, i, array) => {
        if (item.to != null && Number(item.from) > Number(item.to)) {
          badRequestError('Ranges', ERROR_CODE.RANGES_INVALID);
        }
        if (Number(i) > 0) {
          Number(array[i - 1].to) != Number(array[i].from) &&
            badRequestError('Ranges', ERROR_CODE.RANGES_FROM_TO);
        }
      });
    }
    const saved = await this.additionalDataRepository.save(dataSave);
    return {
      payload: saved.id,
    };
  }
}
