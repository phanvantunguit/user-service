import { Inject } from '@nestjs/common';
import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { ListMerchantAdditionalDataInput, ListMerchantAdditionalDataOutput } from './validate';

export class ListMerchantAdditionalDataHandler {
  constructor(
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
  ) {}
  async execute(param: ListMerchantAdditionalDataInput, merchantId?: string, id?: string): Promise<ListMerchantAdditionalDataOutput> {
    if(merchantId) {
      param['merchant_id'] = merchantId
    }
    const data = await this.merchantAdditionalDataRepository.list(param)
    return {
      payload: data
    };
  }
}
