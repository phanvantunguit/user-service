import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsArray, IsString, IsIn, IsObject } from 'class-validator';
import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';
import { MerchantAdditionalDataValidate } from '../validate';

export class ListMerchantAdditionalDataInput {
  @ApiProperty({
    required: false,
    description: `type: ${Object.values(ADDITIONAL_DATA_TYPE)}`,
  })
  @IsIn(Object.values(ADDITIONAL_DATA_TYPE))
  @IsOptional()
  type?: ADDITIONAL_DATA_TYPE;

  @ApiProperty({
    required: false,
    description: 'keyword',
  })
  @IsString()
  @IsOptional()
  keyword?: string;

  @ApiProperty({
    required: false,
    description: `status: ${Object.values(MERCHANT_ADDITIONAL_DATA_STATUS)}`,
  })
  @IsIn(Object.values(MERCHANT_ADDITIONAL_DATA_STATUS))
  @IsOptional()
  status?: MERCHANT_ADDITIONAL_DATA_STATUS;
}

export class ListMerchantAdditionalDataOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: MerchantAdditionalDataValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => MerchantAdditionalDataValidate)
  payload: MerchantAdditionalDataValidate[];
}
export class GetMerchantAdditionalDataOutput {
  @ApiProperty({
    required: true,
    type: MerchantAdditionalDataValidate,
  })
  @IsObject()
  payload: MerchantAdditionalDataValidate;
}

