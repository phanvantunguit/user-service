import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsIn, IsNumber, IsOptional, IsString } from 'class-validator';
import { MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';

export class ModifyMerchantAdditionalData {
  @ApiProperty({
    required: false,
    description: 'id',
  })
  @IsOptional()
  @IsString()
  id?: string;

  @ApiProperty({
    required: true,
    description: 'additional_data_id',
  })
  @IsString()
  additional_data_id?: string;
  
  @ApiProperty({
    required: false,
    description: `status: ${Object.values(MERCHANT_ADDITIONAL_DATA_STATUS)}`,
  })
  @IsOptional()
  @IsIn(Object.values(MERCHANT_ADDITIONAL_DATA_STATUS))
  status: MERCHANT_ADDITIONAL_DATA_STATUS

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsNumber()
  order?: number
}

export class ModifyMerchantAdditionalDataInput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: ModifyMerchantAdditionalData,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ModifyMerchantAdditionalData)
  data: ModifyMerchantAdditionalData[];
}
