import { Inject } from '@nestjs/common';
import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { BaseUpdateOutput } from '../../validate';
import { ModifyMerchantAdditionalDataInput } from './validate';

export class ModifyMerchantAdditionalDataHandler {
  constructor(
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
  ) {}
  async execute(
    param: ModifyMerchantAdditionalDataInput,
    merchantId: string,
    adminId?: string,
  ): Promise<BaseUpdateOutput> {
    if(adminId) {
      const data = param.data.map(d => ({
        ...d,
        merchant_id: merchantId,
        created_by: adminId,
        updated_by: adminId
      }))
      await this.merchantAdditionalDataRepository.save(data)
    } else {
      const data = param.data.map(d => ({
        ...d,
        merchant_id: merchantId
      }))
      await this.merchantAdditionalDataRepository.save(data)
    }
    return {
      payload: true
    };
  }
}
