import { ApiProperty } from '@nestjs/swagger';
import { IsObject } from 'class-validator';
import { AdditionalDataValidate } from '../validate';

export class GetAdditionalDataOutput {
  @ApiProperty({
    required: true,
    type: AdditionalDataValidate,
  })
  @IsObject()
  payload: AdditionalDataValidate;
}
