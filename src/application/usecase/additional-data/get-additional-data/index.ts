import { Inject } from '@nestjs/common';
import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { GetAdditionalDataOutput } from './validate';

export class GetAdditionalDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
  ) {}
  async execute(id: string): Promise<GetAdditionalDataOutput> {
    const data = await this.additionalDataRepository.findById(id)
    return {
      payload: data
    };
  }
}
