import { Inject } from '@nestjs/common';
import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { BaseUpdateOutput } from '../../validate';

export class DeleteAdditionalDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
  ) {}
  async execute(id: string): Promise<BaseUpdateOutput> {
    const merchantAdditional = await this.merchantAdditionalDataRepository.findOne({additional_data_id: id})
    if(merchantAdditional) {
        badRequestError('Data', ERROR_CODE.UNPROCESSABLE)
    }
    await this.additionalDataRepository.deleteById(id)
    return {
      payload: true
    };
  }
}
