import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsOptional,
  IsIn,
  IsUUID,
  ValidateIf,
} from 'class-validator';
import {
  ADDITIONAL_DATA_TYPE,
  MERCHANT_ADDITIONAL_DATA_STATUS,
  STANDALONE_ADDITIONAL,
} from 'src/domain/additional-data/types';

export class AdditionalDataValidate {
  @ApiProperty({
    required: false,
    description: `type: ${Object.values(ADDITIONAL_DATA_TYPE)}`,
  })
  @IsIn(Object.values(ADDITIONAL_DATA_TYPE))
  @IsOptional()
  type?: ADDITIONAL_DATA_TYPE;

  @ApiProperty({
    required: false,
    description: `Required if type is not includes in ${Object.values(STANDALONE_ADDITIONAL)}`,
  })
  @IsString()
  @IsOptional()
  @ValidateIf((o) => !STANDALONE_ADDITIONAL.includes(o.type))
  name?: string;

  @ApiProperty({
    required: false,
    description: 'data',
  })
  @IsOptional()
  data?: any;
}
export class MerchantAdditionalDataValidate extends AdditionalDataValidate {
  @ApiProperty({
    required: true,
    description: `status: ${Object.values(MERCHANT_ADDITIONAL_DATA_STATUS)}`,
  })
  @IsIn(Object.values(MERCHANT_ADDITIONAL_DATA_STATUS))
  status?: MERCHANT_ADDITIONAL_DATA_STATUS;

  @ApiProperty({
    required: true,
    description: 'merchant_id',
  })
  @IsString()
  merchant_id?: string;

  @ApiProperty({
    required: true,
    description: 'additional_data_id',
  })
  @IsString()
  additional_data_id?: string;
}
