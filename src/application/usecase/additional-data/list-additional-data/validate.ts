import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsArray, IsString, IsIn } from 'class-validator';
import { ADDITIONAL_DATA_TYPE, MERCHANT_ADDITIONAL_DATA_STATUS } from 'src/domain/additional-data/types';
import { AdditionalDataValidate } from '../validate';

export class ListAdditionalDataInput {
  @ApiProperty({
    required: false,
    description: `type: ${Object.values(ADDITIONAL_DATA_TYPE)}`,
  })
  @IsIn(Object.values(ADDITIONAL_DATA_TYPE))
  @IsOptional()
  type?: ADDITIONAL_DATA_TYPE;

  @ApiProperty({
    required: false,
    description: 'keyword',
  })
  @IsString()
  @IsOptional()
  keyword?: string;
}

export class ListAdditionalDataOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: AdditionalDataValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => AdditionalDataValidate)
  payload: AdditionalDataValidate[];
}
