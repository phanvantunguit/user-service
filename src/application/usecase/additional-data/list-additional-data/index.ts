import { Inject } from '@nestjs/common';
import {
  STANDALONE_ADDITIONAL,
  ADDITIONAL_DEFAULT_NAME,
} from 'src/domain/additional-data/types';
import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { ListAdditionalDataInput, ListAdditionalDataOutput } from './validate';

export class ListAdditionalDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
  ) {}
  async execute(
    param: ListAdditionalDataInput,
  ): Promise<ListAdditionalDataOutput> {
    const { type } = param;
    if (STANDALONE_ADDITIONAL.includes(type)) {
      param.keyword = ADDITIONAL_DEFAULT_NAME;
    }

    const data = await this.additionalDataRepository.list(param);
    return {
      payload: data,
    };
  }
}
