import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsOptional } from 'class-validator';
import { AdditionalDataValidate } from '../validate';

export class ModifyMerchantStandaloneAdditionalDataInput extends AdditionalDataValidate {
  @ApiProperty({
    required: false,
    description: 'id',
  })
  @IsUUID()
  @IsOptional()
  id?: string;

  @ApiProperty({
    required: false,
    description: 'additional_data_id',
  })
  @IsUUID()
  @IsOptional()
  additional_data_id?: string;
}
