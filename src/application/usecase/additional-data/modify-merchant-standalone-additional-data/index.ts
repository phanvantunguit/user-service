import { Inject } from '@nestjs/common';
import {
  ADDITIONAL_DATA_TYPE,
  MERCHANT_ADDITIONAL_DATA_STATUS,
  STANDALONE_ADDITIONAL,
} from 'src/domain/additional-data/types';
import { AdditionalDataRepository } from 'src/infrastructure/data/database/additional-data.repository';
import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { BaseUpdateOutput } from '../../validate';
import { ModifyMerchantStandaloneAdditionalDataInput } from './validate';

export class ModifyMerchantStandaloneAdditionalDataHandler {
  constructor(
    @Inject(AdditionalDataRepository)
    private additionalDataRepository: AdditionalDataRepository,
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
  ) {}
  async execute(
    param: ModifyMerchantStandaloneAdditionalDataInput,
    merchantId: string,
    adminId?: string,
  ): Promise<BaseUpdateOutput> {
    const { type, data, name } = param;
    let additionalName = name;
    if (STANDALONE_ADDITIONAL.includes(type) && !name) {
      additionalName = merchantId;
    }
    const additionalDataSave = {
      type,
      name: additionalName,
      data,
    };
    // check additional exited
    const additionalExisted = await this.additionalDataRepository.findOne({
      name: merchantId,
      type,
    });
    if (additionalExisted) {
      additionalDataSave['id'] = additionalExisted.id;
    }
    const additional = await this.additionalDataRepository.save(
      additionalDataSave,
    );
    const standAloneAdditionalDataSave = {
      status: MERCHANT_ADDITIONAL_DATA_STATUS.ON,
      merchant_id: merchantId,
    };

    // check standaloneAdditionalExisted
    const standaloneAdditionalExisted =
      await this.merchantAdditionalDataRepository.findOne({
        additional_data_id: additional.id,
      });

    if (standaloneAdditionalExisted) {
      standAloneAdditionalDataSave['id'] = standaloneAdditionalExisted.id;
    }

    if (param.id) {
      standAloneAdditionalDataSave['id'] = param.id;
    }

    standAloneAdditionalDataSave['additional_data_id'] = additional.id;

    if (adminId) {
      standAloneAdditionalDataSave['created_by'] = adminId;
      standAloneAdditionalDataSave['updated_by'] = adminId;
    }
    await this.merchantAdditionalDataRepository.save(
      standAloneAdditionalDataSave,
    );
    return {
      payload: true,
    };
  }
}
