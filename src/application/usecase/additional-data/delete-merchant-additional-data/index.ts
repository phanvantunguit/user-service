import { Inject } from '@nestjs/common';
import { MerchantAdditionalDataRepository } from 'src/infrastructure/data/database/merchant-additional-data.repository';
import { BaseUpdateOutput } from '../../validate';

export class DeleteMerchantAdditionalDataHandler {
  constructor(
    @Inject(MerchantAdditionalDataRepository)
    private merchantAdditionalDataRepository: MerchantAdditionalDataRepository,
  ) {}
  async execute(
    id: string,
  ): Promise<BaseUpdateOutput> {
    await this.merchantAdditionalDataRepository.deleteById(id)
    return {
      payload: true
    };
  }
}
