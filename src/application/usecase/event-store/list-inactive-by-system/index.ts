import { Inject } from '@nestjs/common';
import { EVENT_STORE_NAME, EVENT_STORE_STATE } from 'src/domain/event-store/types';
import { EventStoreRepository } from 'src/infrastructure/data/database/event-store.repository';
import {
  ListEventInactiveBySystemInput,
  ListEventInactiveBySystemOutput,
} from './validate';

export class ListInactiveBySystemHandler {
  constructor(
    @Inject(EventStoreRepository)
    private eventStoreRepository: EventStoreRepository,
  ) {}
  async execute(
    param: ListEventInactiveBySystemInput,
  ): Promise<ListEventInactiveBySystemOutput> {
    const { user_id, bot_id } = param;
    const events = await this.eventStoreRepository.find({
      user_id,
      event_name: EVENT_STORE_NAME.TBOT_INACTIVE_BY_SYSTEM,
      state: EVENT_STORE_STATE.OPEN,
    });
    let dataResult = []
    if(bot_id) {
        events.forEach(e => {
            if(e.metadata?.bot_id === bot_id) {
                dataResult.push(e.metadata)
            }
        })
    } else {
        dataResult = events.map(e => e.metadata) 
    }
    return {
      payload: dataResult
    };
  }
}
