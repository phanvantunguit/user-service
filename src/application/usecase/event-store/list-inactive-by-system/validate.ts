import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsOptional, IsString, IsUUID, ValidateNested } from 'class-validator';

export class ListEventInactiveBySystemInput {
  @ApiProperty({
    required: false,
    description: 'user_id',
  })
  @IsUUID()
  @IsOptional()
  user_id?: string;

  @ApiProperty({
    required: false,
    description: 'state',
  })
  @IsString()
  @IsOptional()
  state?: string;

  @ApiProperty({
    required: false,
    description: 'bot_id',
  })
  @IsUUID()
  @IsOptional()
  bot_id: string
}

export class EventInactiveBySystemValidate {
  @ApiProperty({
    required: true,
    description: 'bot_id',
  })
  @IsUUID()
  bot_id: string

  @ApiProperty({
    required: true,
    description: 'strategy_id',
  })
  @IsString()
  strategy_id: string

  @ApiProperty({
    required: true,
    description: 'subscriber_id',
  })
  @IsString()
  subscriber_id: string

  @ApiProperty({
    required: true,
    description: 'trade_id',
  })
  @IsString()
  trade_id: string

  @ApiProperty({
    required: true,
    description: 'symbol',
  })
  @IsString()
  symbol: string

  @ApiProperty({
    required: true,
    description: 'time',
  })
  @IsString()
  time: number
}

export class ListEventInactiveBySystemOutput {
  @ApiProperty({
    required: true,
    description: 'Numerical order failed',
    isArray: true,
    type: EventInactiveBySystemValidate,
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => EventInactiveBySystemValidate)
  payload: EventInactiveBySystemValidate[];
}