import {
  IsBoolean,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EventDataValidate, UserDataValidate } from '../validate';

export class RemindDynamicInput {
  @ApiProperty({
    required: true,
    description: 'user',
  })
  @IsObject()
  user: UserDataValidate;

  @ApiProperty({
    required: true,
    description: 'user',
  })
  @IsObject()
  event: EventDataValidate;

  @ApiProperty({
    required: true,
    description: 'template id',
    example: 'abc',
  })
  @IsString()
  template_id: string;

  @ApiProperty({
    required: false,
    description: 'remind at',
    example: Date.now(),
  })
  @IsOptional()
  @IsNumber()
  remind_at?: number;
}

export class RemindDynamicOutput {
  @ApiProperty({
    required: true,
    description: 'Status of update',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
