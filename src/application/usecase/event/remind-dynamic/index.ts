import { Inject, Injectable } from '@nestjs/common';
import { RemindDynamicInput, RemindDynamicOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { formatInTimeZone } from 'date-fns-tz';
import { UserRemindRepository } from 'src/infrastructure/data/database/user-remind.repository';
import { DAY } from 'src/utils/time.util';

@Injectable()
export class RemindDynamicHandler {
  constructor(
    @Inject(MailService) private mailService: MailService,
    @Inject(UserRemindRepository)
    private userRemindRepository: UserRemindRepository,
  ) {}
  async execute(param: RemindDynamicInput): Promise<RemindDynamicOutput> {
    const { user, event, template_id, remind_at } = param;
    const remindAt = remind_at || Number(user.created_at) + DAY;
    const userRemind = await this.userRemindRepository.findOne({
      event_id: event.id,
      template_id,
      remind_at: remindAt,
      user_id: user.id,
    });
    if (userRemind && userRemind.remind) {
      return {
        payload: true,
      };
    }
    let userRemindSaved;
    if (userRemind) {
      userRemindSaved = await this.userRemindRepository.save({
        id: userRemind.id,
        event_id: event.id,
        user_id: user.id,
        template_id,
        remind: true,
        remind_at: remindAt,
      });
    } else {
      userRemindSaved = await this.userRemindRepository.save({
        event_id: event.id,
        user_id: user.id,
        template_id,
        remind: true,
        remind_at: remindAt,
      });
    }

    const count_user = Number(user.invite_code.replace(/\D/g, ''));
    const sendEmail = await this.mailService.sendEmailDynamicTemplate(
      user.email,
      {
        email: user.email,
        name: user.fullname,
        phone: user.phone,

        event_name: event.name,
        event_code: event.code,
        invite_code: user.invite_code,
        attendees_number: event.attendees_number,
        start_at: `${formatInTimeZone(
          Number(event.start_at),
          'Asia/Ho_Chi_Minh',
          'HH:mm dd/MM/yyyy',
        )} (GMT+7)`,
        finish_at: `${formatInTimeZone(
          Number(event.finish_at),
          'Asia/Ho_Chi_Minh',
          'HH:mm dd/MM/yyyy',
        )} (GMT+7)`,
        count_user,
        countdown: '2 ngày',
      },
      template_id,
    );
    if (!sendEmail) {
      await this.userRemindRepository.save({
        id: userRemindSaved.id,
        event_id: event.id,
        user_id: user.id,
        template_id,
        remind: false,
        remind_at,
      });
      return {
        payload: false,
      };
    }
    return {
      payload: true,
    };
  }
}
