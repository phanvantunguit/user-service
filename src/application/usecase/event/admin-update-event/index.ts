import { Inject } from '@nestjs/common';
import { EventDomain } from 'src/domain/event/event.domain';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { AdminUpdateEventInput, AdminUpdateEventOutput } from './validate';

export class AdminUpdateEventHandler {
  constructor(
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(
    id: string,
    param: AdminUpdateEventInput,
    userId: string,
  ): Promise<AdminUpdateEventOutput> {
    const data = new EventDomain();
    data.id = id;
    data.name = param.name;
    data.code = param.code;
    data.attendees_number = param.attendees_number;
    data.status = param.status;
    data.email_remind_at = param.email_remind_at;
    data.email_remind_template_id = param.email_remind_template_id;
    data.email_confirm = param.email_confirm;
    data.email_confirm_template_id = param.email_confirm_template_id;
    data.start_at = param.start_at;
    data.finish_at = param.finish_at;
    data.updated_by = userId;
    data.create_qrcode = param.create_qrcode;
    if (data.code) {
      const event = await this.eventRepository.findOne({ code: data.code });
      if (event && event.id !== data.id) {
        badRequestError('Event Code', ERROR_CODE.EXISTED);
      }
    }
    await this.eventRepository.save(data);
    return {
      payload: true,
    };
  }
}
