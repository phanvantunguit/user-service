import { Inject } from '@nestjs/common';
import { EVENT_CONFIRM_STATUS } from 'src/domain/event/types';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { UserConfirmEventInput, UserConfirmEventOutput } from './validate';

export class UserConfirmEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
  ) {}
  async execute(param: UserConfirmEventInput): Promise<UserConfirmEventOutput> {
    const userRegistered = await this.userEventRepository.findById(param.token);
    if (!userRegistered) {
      badRequestError('Registration', ERROR_CODE.NOT_FOUND);
    }
    await this.userEventRepository.save({
      id: param.token,
      confirm_status: param.status.toUpperCase() as EVENT_CONFIRM_STATUS,
    });
    return {
      payload: true,
    };
  }
}
