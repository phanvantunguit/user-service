import { IsString, IsBoolean } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EVENT_CONFIRM_STATUS } from 'src/domain/event/types';

export class UserConfirmEventInput {
  @ApiProperty({
    required: true,
    description: 'Id of registration',
    example: '2d676c14-f781-4668-aec0-692ffe4a0ae1',
  })
  @IsString()
  token: string;

  @ApiProperty({
    required: true,
    description: `Confirm status ${EVENT_CONFIRM_STATUS.APPROVED} or ${EVENT_CONFIRM_STATUS.REJECTED}`,
    example: EVENT_CONFIRM_STATUS.APPROVED,
  })
  @IsString()
  status: EVENT_CONFIRM_STATUS;
}

export class UserConfirmEventOutput {
  @ApiProperty({
    required: true,
    description: 'Result of confirm',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
