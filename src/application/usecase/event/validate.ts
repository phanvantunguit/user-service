import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsOptional,
  IsIn,
  IsNumber,
  IsBoolean,
  IsEmail,
} from 'class-validator';
import {
  EVENT_CONFIRM_STATUS,
  EVENT_STATUS,
  PAYMENT_STATUS,
} from 'src/domain/event/types';

export class EventDataValidate {
  @ApiProperty({
    required: true,
    description: 'UUID of event',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsString()
  id: string;

  @ApiProperty({
    required: true,
    description: 'Name of event',
    example: 'Coinmap vs Axi',
  })
  @IsString()
  name: string;

  @ApiProperty({
    required: false,
    description: 'Code of event',
    example: 'coinmapaxi',
  })
  @IsOptional()
  @IsString()
  code?: string;

  @ApiProperty({
    required: false,
    description: `Status of event ${Object.values(EVENT_STATUS).join(' or ')}`,
    example: EVENT_STATUS.OPEN_REGISTRATION,
  })
  @IsOptional()
  @IsIn(Object.values(EVENT_STATUS))
  status?: EVENT_STATUS;

  @ApiProperty({
    required: false,
    description: 'Attendees number',
    example: 1000,
  })
  @IsOptional()
  @IsNumber()
  attendees_number?: number;

  @ApiProperty({
    required: false,
    description: 'Email remind at',
    example: Date.now(),
  })
  @IsOptional()
  @IsNumber()
  email_remind_at?: number;

  @ApiProperty({
    required: false,
    description: 'Email remind template id',
    example: Date.now(),
  })
  @IsOptional()
  @IsString()
  email_remind_template_id?: string;

  @ApiProperty({
    required: false,
    description: 'Email confirm',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  email_confirm?: boolean;

  @ApiProperty({
    required: false,
    description: 'Email confirm template id',
    example: Date.now(),
  })
  @IsOptional()
  @IsString()
  email_confirm_template_id?: string;

  @ApiProperty({
    required: true,
    description: 'Time start event',
    example: 1664519091465,
  })
  @IsOptional()
  @IsString()
  start_at?: number;

  @ApiProperty({
    required: true,
    description: 'Time finish event',
    example: 1664519091465,
  })
  @IsOptional()
  @IsString()
  finish_at?: number;

  @ApiProperty({
    required: true,
    description: 'Time created',
    example: 1664519091465,
  })
  @IsString()
  created_at: number;

  @ApiProperty({
    required: true,
    description: 'Last updated',
    example: 1664519091465,
  })
  @IsString()
  updated_at: number;

  @ApiProperty({
    required: false,
    description: 'Deleted',
    example: 1664519091465,
  })
  @IsOptional()
  @IsString()
  deleted_at?: number;

  @ApiProperty({
    required: false,
    description: 'Determined create qrcode',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  create_qrcode?: boolean;
}

export class UserDataValidate {
  @ApiProperty({
    required: true,
    description: 'UUID of user',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsString()
  id: string;

  @ApiProperty({
    required: true,
    description: 'Status of user confirm attend to event',
    example: EVENT_CONFIRM_STATUS.APPROVED,
  })
  @IsString()
  confirm_status: EVENT_CONFIRM_STATUS;

  @ApiProperty({
    required: true,
    description: 'Status attend of user',
    example: true,
  })
  @IsBoolean()
  attend: boolean;

  @ApiProperty({
    required: false,
    description: 'Remind user attend',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  remind: boolean;

  @ApiProperty({
    required: true,
    description: 'Fullname of user register',
    example: 'Nguyen Van A',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: true,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone?: string;

  @ApiProperty({
    required: true,
    description: 'Code to attend to event',
    example: 'Guest0001',
  })
  @IsString()
  invite_code: string;

  @ApiProperty({
    required: false,
    description: 'Status payment of event',
    example: PAYMENT_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(PAYMENT_STATUS))
  payment_status: PAYMENT_STATUS;

  @ApiProperty({
    required: false,
    description: 'Telegram',
  })
  @IsOptional()
  @IsString()
  telegram?: string;

  @ApiProperty({
    required: false,
    description: 'Updated By',
  })
  @IsOptional()
  @IsString()
  updated_by?: string;

  @ApiProperty({
    required: false,
    description: 'Created By',
  })
  @IsOptional()
  @IsString()
  created_by?: string;

  @ApiProperty({
    required: true,
    description: 'Time user register',
    example: 1664519091465,
  })
  @IsString()
  created_at: number;

  @ApiProperty({
    required: true,
    description: 'Last time user update',
    example: 1664519091465,
  })
  @IsString()
  updated_at: number;
}
