import { Inject, Injectable } from '@nestjs/common';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { notFoundError } from 'src/utils/exceptions/throw.exception';
import {
  RemindAllAttendEventInput,
  RemindAllAttendEventOutput,
} from './validate';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { RemindAttendEventHandler } from '../remind-attend';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { EventDomain } from 'src/domain/event/event.domain';
import { EventDataValidate } from '../validate';

@Injectable()
export class RemindAllAttendEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
    @Inject(RemindAttendEventHandler)
    private remindAttendEventHandler: RemindAttendEventHandler,
  ) {}
  async execute(
    param: RemindAllAttendEventInput,
  ): Promise<RemindAllAttendEventOutput> {
    if (param.event_id) {
      const event = (await this.eventRepository.findById(
        param.event_id,
      )) as EventDomain;
      if (!event) {
        notFoundError('Event');
      }
      this.asyncExecute(event);
    } else if (param.invite_id) {
      const user = (await this.userEventRepository.findById(
        param.invite_id,
      )) as UserEventDomain;
      if (!user) {
        notFoundError('Event');
      }
      const event = (await this.eventRepository.findById(
        param.event_id,
      )) as EventDomain;
      await this.remindAttendEventHandler.execute({
        user,
        event,
      });
    }
    return {
      payload: true,
    };
  }

  async asyncExecute(
    event: EventDataValidate,
  ): Promise<RemindAllAttendEventOutput> {
    const users = (await this.userEventRepository.find({
      event_id: event.id,
    })) as UserEventDomain[];

    for (const user of users) {
      try {
        await this.remindAttendEventHandler.execute({
          event,
          user,
        });
      } catch (error) {}
    }
    return {
      payload: true,
    };
  }
}
