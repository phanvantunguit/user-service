import { IsString, IsBoolean, IsOptional, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RemindAllAttendEventInput {
  @ApiProperty({
    required: false,
    description: 'Ticket id uuid',
  })
  @ValidateIf((o) => !o.event_id)
  @IsOptional()
  @IsString()
  invite_id?: string;

  @ApiProperty({
    required: false,
    description: 'Event id uuid',
  })
  @ValidateIf((o) => !o.invite_id)
  @IsOptional()
  @IsString()
  event_id?: string;
}

export class RemindAllAttendEventOutput {
  @ApiProperty({
    required: true,
    description: 'Status of update',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
