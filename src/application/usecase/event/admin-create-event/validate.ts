import {
  IsString,
  IsOptional,
  IsIn,
  IsNumber,
  IsBoolean,
  IsNotEmpty,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EVENT_STATUS } from 'src/domain/event/types';

export class AdminCreateEventInput {
  @ApiProperty({
    required: true,
    description: 'Name of event',
    example: 'Coinmap vs Axi',
  })
  @IsString()
  @IsNotEmpty()
  name: string;
  @ApiProperty({
    required: false,
    description: 'Code of event',
    example: 'coinmapaxi',
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  code?: string;

  @ApiProperty({
    required: false,
    description: 'Attendees number',
    example: 1000,
  })
  @IsOptional()
  @IsNumber()
  attendees_number?: number;

  @ApiProperty({
    required: false,
    description: 'Email remind at',
    example: Date.now(),
  })
  @IsOptional()
  @IsNumber()
  email_remind_at?: number;

  @ApiProperty({
    required: false,
    description: 'Email remind template id',
    example: Date.now(),
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  email_remind_template_id?: string;

  @ApiProperty({
    required: false,
    description: 'Email confirm',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  email_confirm?: boolean;

  @ApiProperty({
    required: false,
    description: 'Email confirm template id',
    example: Date.now(),
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  email_confirm_template_id?: string;

  @ApiProperty({
    required: false,
    description: `Status of event ${Object.values(EVENT_STATUS).join(' or ')}`,
    example: EVENT_STATUS.OPEN_REGISTRATION,
  })
  @IsOptional()
  @IsIn(Object.values(EVENT_STATUS))
  status?: EVENT_STATUS;

  @ApiProperty({
    required: false,
    description: 'Time start event',
    example: 1664519091465,
  })
  @IsOptional()
  @IsNumber()
  start_at?: number;

  @ApiProperty({
    required: false,
    description: 'Time finish event',
    example: 1664519091465,
  })
  @IsOptional()
  @IsNumber()
  finish_at?: number;

  @ApiProperty({
    required: false,
    description: 'Determined create qrcode',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  create_qrcode?: boolean;
}

export class AdminCreateEventOutput {
  @ApiProperty({
    required: true,
    description: 'UUID of event',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsString()
  payload: string;
}
