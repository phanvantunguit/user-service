import { IsString, IsBoolean, IsOptional, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RemindAllDynamicInput {
  @ApiProperty({
    required: false,
    description: 'Ticket id uuid',
  })
  @ValidateIf((o) => !o.event_id)
  @IsOptional()
  @IsString()
  invite_id?: string;

  @ApiProperty({
    required: false,
    description: 'Event id uuid',
  })
  @ValidateIf((o) => !o.invite_id)
  @IsOptional()
  @IsString()
  event_id?: string;

  @ApiProperty({
    required: true,
    description: 'template id',
    example: 'abc',
  })
  @IsString()
  template_id: string;
}

export class RemindAllDynamicOutput {
  @ApiProperty({
    required: true,
    description: 'Status of update',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
