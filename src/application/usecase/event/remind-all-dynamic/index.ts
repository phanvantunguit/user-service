import { Inject, Injectable } from '@nestjs/common';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { notFoundError } from 'src/utils/exceptions/throw.exception';
import { RemindAllDynamicInput, RemindAllDynamicOutput } from './validate';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { EventDomain } from 'src/domain/event/event.domain';
import { EventDataValidate } from '../validate';
import { RemindDynamicHandler } from '../remind-dynamic';

@Injectable()
export class RemindAllDynamicHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
    @Inject(RemindDynamicHandler)
    private remindDynamicHandler: RemindDynamicHandler,
  ) {}
  async execute(param: RemindAllDynamicInput): Promise<RemindAllDynamicOutput> {
    if (param.event_id) {
      const event = (await this.eventRepository.findById(
        param.event_id,
      )) as EventDomain;
      if (!event) {
        notFoundError('Event');
      }
      this.asyncExecute(event, param.template_id);
    } else if (param.invite_id) {
      const user = (await this.userEventRepository.findById(
        param.invite_id,
      )) as UserEventDomain;
      if (!user) {
        notFoundError('Event');
      }
      const event = (await this.eventRepository.findById(
        param.event_id,
      )) as EventDomain;
      await this.remindDynamicHandler.execute({
        user,
        event,
        template_id: param.template_id,
      });
    }
    return {
      payload: true,
    };
  }

  async asyncExecute(
    event: EventDataValidate,
    template_id: string,
  ): Promise<RemindAllDynamicOutput> {
    const users = (await this.userEventRepository.find({
      event_id: event.id,
    })) as UserEventDomain[];

    for (const user of users) {
      try {
        await this.remindDynamicHandler.execute({
          event,
          user,
          template_id,
        });
      } catch (error) {}
    }
    return {
      payload: true,
    };
  }
}
