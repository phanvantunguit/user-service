import { Inject } from '@nestjs/common';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminListEventInput, AdminListEventOutput } from './validate';

export class AdminListEventHandler {
  constructor(
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(param: AdminListEventInput): Promise<AdminListEventOutput> {
    const result = await this.eventRepository.getPaging(param);
    return {
      payload: result,
    };
  }
}
