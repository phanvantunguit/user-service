import {
  IsString,
  IsOptional,
  IsIn,
  IsNumberString,
  ValidateNested,
  IsNumber,
  IsObject,
  IsArray,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EVENT_STATUS } from 'src/domain/event/types';
import { Type } from 'class-transformer';
import { EventDataValidate } from '../validate';

export class AdminListEventInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number;

  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: phone number, name, email',
  })
  @IsOptional()
  @IsString()
  keyword: string;

  @ApiProperty({
    required: false,
    description: `Status of event ${Object.values(EVENT_STATUS).join(' or ')}`,
  })
  @IsOptional()
  @IsIn(Object.values(EVENT_STATUS))
  status: EVENT_STATUS;

  @ApiProperty({
    required: false,
    description: 'Includes event was deleted',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  @IsIn(['1', '0'])
  deleted: number;
}

export class PagingEventDataOutput {
  @ApiProperty({
    required: true,
    description: 'List of event',
    isArray: true,
    type: EventDataValidate,
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => EventDataValidate)
  rows: EventDataValidate[];

  @ApiProperty({
    required: true,
    description: 'Current page',
    example: 1,
  })
  @IsNumber()
  page: number;

  @ApiProperty({
    required: true,
    description: 'Maximun number of list',
    example: 50,
  })
  @IsNumber()
  size: number;

  @ApiProperty({
    required: true,
    description: 'Number of current list',
    example: 10,
  })
  @IsNumber()
  count: number;

  @ApiProperty({
    required: true,
    description: 'Number of query',
    example: 100,
  })
  @IsNumber()
  total: number;
}
export class AdminListEventOutput {
  @ApiProperty({
    required: true,
    description: 'Result event paging',
  })
  @IsObject()
  payload: PagingEventDataOutput;
}
