import { IsString, IsOptional, IsIn, IsBoolean } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EVENT_CONFIRM_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';

export class AdminUpdateStatusInviteEventInput {
  @ApiProperty({
    required: true,
    description: 'Ticket id uuid',
  })
  @IsString()
  id: string;

  @ApiProperty({
    required: false,
    description: 'Status confirm of ticket',
    example: EVENT_CONFIRM_STATUS.APPROVED,
  })
  @IsOptional()
  @IsIn(Object.values(EVENT_CONFIRM_STATUS))
  confirm_status: EVENT_CONFIRM_STATUS;

  @ApiProperty({
    required: false,
    description: 'Status payment of event',
    example: PAYMENT_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(PAYMENT_STATUS))
  payment_status: PAYMENT_STATUS;

  @ApiProperty({
    required: true,
    description: 'Checkin or not',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  attend: boolean;
}

export class AdminUpdateStatusInviteEventOutput {
  @ApiProperty({
    required: true,
    description: 'Status of update',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
