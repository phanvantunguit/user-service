import { Inject, Injectable } from '@nestjs/common';
import { formatInTimeZone } from 'date-fns-tz';
import { APP_CONFIG } from 'src/config';
import { PAYMENT_STATUS } from 'src/domain/event/types';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { MailService } from 'src/infrastructure/services';
import { notFoundError } from 'src/utils/exceptions/throw.exception';
import {
  AdminUpdateStatusInviteEventInput,
  AdminUpdateStatusInviteEventOutput,
} from './validate';

@Injectable()
export class AdminUpdateStatusInviteEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
    @Inject(MailService) private mailService: MailService,
  ) {}
  async execute(
    param: AdminUpdateStatusInviteEventInput,
    admin_id: string,
  ): Promise<AdminUpdateStatusInviteEventOutput> {
    const registered = await this.userEventRepository.findById(param.id);
    if (!registered) {
      notFoundError('Ticket');
    }

    await this.userEventRepository.save({
      ...param,
      updated_by: admin_id,
    });
    if (
      param.payment_status !== registered.payment_status &&
      param.payment_status === PAYMENT_STATUS.COMPLETED
    ) {
      const event = await this.eventRepository.findById(registered.event_id);
      const template_id = APP_CONFIG.PAYMENT_EVENT_TEMPLATE_ID;
      const count_user = Number(registered.invite_code.replace(/\D/g, ''));
      await this.mailService.sendEmailDynamicTemplate(
        registered.email,
        {
          email: registered.email,
          name: registered.fullname,
          phone: registered.phone,

          event_name: event.name,
          event_code: event.code,
          invite_code: registered.invite_code,
          attendees_number: event.attendees_number,
          start_at: `${formatInTimeZone(
            Number(event.start_at),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy',
          )} (GMT+7)`,
          finish_at: `${formatInTimeZone(
            Number(event.finish_at),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy',
          )} (GMT+7)`,
          count_user,
        },
        template_id,
      );
    }
    return {
      payload: true,
    };
  }
}
