import { IsString, IsBoolean, IsEmail, IsObject } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserCheckinEventInput {
  @ApiProperty({
    required: true,
    description: 'Id of registration',
    example: '2d676c14-f781-4668-aec0-692ffe4a0ae1',
  })
  @IsString()
  token: string;
}
export class UserCheckinEventDataOutput {
  @ApiProperty({
    required: true,
    description: 'Fullname of user register',
    example: 'Nguyen Van A',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: true,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsString()
  phone: string;

  @ApiProperty({
    required: true,
    description: 'Code to attend to event',
    example: 'Guest0001',
  })
  @IsString()
  invite_code: string;

  @ApiProperty({
    required: true,
    description: 'Checkin success or not',
    example: true,
  })
  @IsBoolean()
  checkin: boolean;
}
export class UserCheckinEventOutput {
  @ApiProperty({
    required: true,
    description: 'User information',
    example: { fullname: 'fullname', email: 'email', phone: 'phone' },
  })
  @IsObject()
  payload: UserCheckinEventDataOutput;
}
