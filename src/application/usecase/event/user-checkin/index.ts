import { Inject } from '@nestjs/common';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { HOUR } from 'src/utils/time.util';
import { UserCheckinEventInput, UserCheckinEventOutput } from './validate';

export class UserCheckinEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(param: UserCheckinEventInput): Promise<UserCheckinEventOutput> {
    const userRegistered = await this.userEventRepository.findById(param.token);
    if (!userRegistered) {
      badRequestError('Registration', ERROR_CODE.NOT_FOUND);
    }
    const event = await this.eventRepository.findById(userRegistered.event_id);
    if (!event) {
      badRequestError('Event', ERROR_CODE.NOT_FOUND);
    }
    if (event.start_at) {
      const timeChange = event.start_at - Date.now();
      const allowCheckin = 3 * HOUR;
      if (timeChange > allowCheckin) {
        return {
          payload: {
            fullname: userRegistered.fullname,
            email: userRegistered.email,
            phone: userRegistered.phone,
            invite_code: userRegistered.invite_code,
            checkin: false,
          },
        };
      }
    }
    await this.userEventRepository.save({
      id: param.token,
      attend: true,
    });
    return {
      payload: {
        fullname: userRegistered.fullname,
        email: userRegistered.email,
        phone: userRegistered.phone,
        invite_code: userRegistered.invite_code,
        checkin: true,
      },
    };
  }
}
