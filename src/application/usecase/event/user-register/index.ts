import { Inject, Injectable } from '@nestjs/common';
import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE } from 'src/domain/event/types';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import {
  badRequestError,
  notFoundError,
  serverError,
} from 'src/utils/exceptions/throw.exception';
import { UserRegisterEventInput, UserRegisterEventOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { StorageService } from 'src/infrastructure/services/storage';
import { DBContext } from 'src/infrastructure/data/database/db-context';
import { generateQRCode } from 'src/utils/qrcode.util';
import { formatInTimeZone } from 'date-fns-tz';
import { APP_CONFIG } from 'src/config';

@Injectable()
export class UserRegisterEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
    @Inject(MailService) private mailService: MailService,
    @Inject(DBContext) private dBContext: DBContext,
    @Inject(StorageService) private storageService: StorageService,
  ) {}
  async execute(
    param: UserRegisterEventInput,
  ): Promise<UserRegisterEventOutput> {
    const event = param.event_code
      ? await this.eventRepository.findOne({ code: param.event_code })
      : await this.eventRepository.findOne({});
    if (!event) {
      notFoundError('Event');
    }
    const [email, telegram, phone] = await Promise.all([
      this.userEventRepository.findOne({
        event_id: event.id,
        email: param.email,
      }),
      this.userEventRepository.findOne({
        event_id: event.id,
        telegram: param.telegram,
      }),
      this.userEventRepository.findOne({
        event_id: event.id,
        phone: param.phone,
      }),
    ]);
    const validate = [];
    if (email) {
      validate.push('Email');
    }
    if (telegram) {
      validate.push('Telegram');
    }
    if (phone) {
      validate.push('Phone');
    }
    if (validate.length > 0) {
      badRequestError(validate.join(', '), ERROR_CODE.EXISTED);
    }

    const count = await this.userEventRepository.count({ event_id: event.id });

    const userEvent = new UserEventDomain();
    userEvent.fullname = param.fullname;
    userEvent.phone = param.phone;
    userEvent.email = param.email;
    userEvent.event_id = event.id;
    userEvent.confirm_status = EVENT_CONFIRM_STATUS.WAITING;
    const lengthCount = event.attendees_number
      ? event.attendees_number.toString().length
      : 3;
    userEvent.setInviteCode(INVITE_CODE_TYPE.GUEST, count + 1, lengthCount);
    userEvent.telegram = param.telegram;

    const userEventSaved = await this.userEventRepository.save(userEvent);
    let qrcode_url = '';
    let template_id = APP_CONFIG.INVITE_EVENT_SIMPLE_TEMPLATE_ID;
    if (event.create_qrcode) {
      template_id = APP_CONFIG.INVITE_EVENT_QRCODE_TEMPLATE_ID;
      try {
        const attendUrl = param.callback_attend_url.includes('?')
          ? param.callback_attend_url + `&token=${userEventSaved.id}`
          : param.callback_attend_url + `?token=${userEventSaved.id}`;
        // qrcode_url = `https://api.qrserver.com/v1/create-qr-code/?data=${attendUrl}&amp;size=100x100`;
        const qrcode = await generateQRCode(attendUrl);
        qrcode_url = await this.storageService.uploadBuffer(
          qrcode,
          `${userEventSaved.id}.png`,
        );
      } catch (error) {
        console.log('error', error);
        await this.userEventRepository.deleteById(userEventSaved.id);
        serverError('QRCode', ERROR_CODE.UNPROCESSABLE);
      }
    }
    if (event.email_confirm) {
      template_id = event.email_confirm_template_id || template_id;
      const sendEmail = await this.mailService.sendEmailDynamicTemplate(
        userEvent.email,
        {
          email: userEvent.email,
          name: userEvent.fullname,
          phone: userEvent.phone,

          event_name: event.name,
          event_code: event.code,
          invite_code: userEvent.invite_code,
          attendees_number: event.attendees_number,
          start_at: `${formatInTimeZone(
            Number(event.start_at),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy',
          )} (GMT+7)`,
          finish_at: `${formatInTimeZone(
            Number(event.finish_at),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy',
          )} (GMT+7)`,
          qrcode_url: qrcode_url,
          callback_confirm_url: param.callback_confirm_url,
        },
        template_id,
      );
      if (!sendEmail) {
        await this.userEventRepository.deleteById(userEventSaved.id);
        serverError('Email', ERROR_CODE.UNPROCESSABLE);
      }
    }
    return {
      payload: userEventSaved.invite_code,
    };
  }
}
