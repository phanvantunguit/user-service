import { IsString, IsEmail, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserRegisterEventInput {
  @ApiProperty({
    required: true,
    description: 'Fullname of user register',
    example: 'Nguyen Van A',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: true,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsString()
  phone: string;

  @ApiProperty({
    required: false,
    description: 'Telegram',
  })
  @IsOptional()
  @IsString()
  telegram: string;

  @ApiProperty({
    required: true,
    description: 'Url webstite result confirm',
    example: 'http://localhost:3000',
  })
  @IsOptional()
  @IsString()
  callback_confirm_url: string;

  @ApiProperty({
    required: true,
    description: 'Url webstite result scan qrcode',
    example: 'http://localhost:3000',
  })
  @IsOptional()
  @IsString()
  callback_attend_url: string;

  @ApiProperty({
    required: false,
    description: 'Unique code of event',
    example: 'coinmapaxi',
  })
  @IsOptional()
  @IsString()
  event_code: string;
}

export class UserRegisterEventOutput {
  @ApiProperty({
    required: true,
    description: 'Code to attend to event',
    example: 'Guest0001',
  })
  @IsString()
  payload: string;
}
