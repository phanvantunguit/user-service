import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEmail,
  IsOptional,
  IsBoolean,
  IsObject,
} from 'class-validator';

export class UserCheckParamInput {
  @ApiProperty({
    required: false,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone: string;

  @ApiProperty({
    required: false,
    description: 'Telegram',
  })
  @IsOptional()
  @IsString()
  telegram: string;

  @ApiProperty({
    required: false,
    description: 'Unique code of event',
    example: 'coinmapaxi',
  })
  @IsOptional()
  @IsString()
  event_code: string;
}

export class UserCheckParamDataOutput {
  @ApiProperty({
    required: false,
    description: 'Email to confirm register',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  email?: boolean;

  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  phone?: boolean;

  @ApiProperty({
    required: false,
    description: 'Telegram',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  telegram?: boolean;
}
export class UserCheckParamOutput {
  @ApiProperty({
    required: true,
    description: 'Status of information',
  })
  @IsObject()
  payload: UserCheckParamDataOutput;
}
