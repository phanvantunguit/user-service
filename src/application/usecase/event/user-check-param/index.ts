import { Inject } from '@nestjs/common';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { notFoundError } from 'src/utils/exceptions/throw.exception';
import {
  UserCheckParamDataOutput,
  UserCheckParamInput,
  UserCheckParamOutput,
} from './validate';

export class UserCheckParamHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(param: UserCheckParamInput): Promise<UserCheckParamOutput> {
    const event = param.event_code
      ? await this.eventRepository.findOne({ code: param.event_code })
      : await this.eventRepository.findOne({});
    if (!event) {
      notFoundError('Event');
    }
    const result: UserCheckParamDataOutput = {};
    if (param.email) {
      const email = await this.userEventRepository.findOne({
        event_id: event.id,
        email: param.email,
      });
      result.email = email ? false : true;
    }
    if (param.telegram) {
      const telegram = await this.userEventRepository.findOne({
        event_id: event.id,
        telegram: param.telegram,
      });
      result.telegram = telegram ? false : true;
    }
    if (param.phone) {
      const phone = await this.userEventRepository.findOne({
        event_id: event.id,
        phone: param.phone,
      });
      result.phone = phone ? false : true;
    }
    return {
      payload: result,
    };
  }
}
