import { Inject } from '@nestjs/common';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { AdminDeleteEventInput, AdminDeleteEventOutput } from './validate';

export class AdminDeleteEventHandler {
  constructor(
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(
    param: AdminDeleteEventInput,
    userId?: string,
  ): Promise<AdminDeleteEventOutput> {
    await this.eventRepository.softDeleteById(param.id, userId);
    return {
      payload: true,
    };
  }
}
