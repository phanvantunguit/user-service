import { IsBoolean, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AdminDeleteEventInput {
  @ApiProperty({
    required: true,
    description: 'id',
  })
  @IsString()
  id: string;
}
export class AdminDeleteEventOutput {
  @ApiProperty({
    required: true,
    description: 'boolean',
  })
  @IsBoolean()
  payload: boolean;
}
