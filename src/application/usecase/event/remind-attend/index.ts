import { Inject, Injectable } from '@nestjs/common';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { RemindAttendEventInput, RemindAttendEventOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { APP_CONFIG } from 'src/config';
import { formatInTimeZone } from 'date-fns-tz';

@Injectable()
export class RemindAttendEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(MailService) private mailService: MailService,
  ) {}
  async execute(
    param: RemindAttendEventInput,
  ): Promise<RemindAttendEventOutput> {
    const { user, event } = param;
    if (!user.remind) {
      await this.userEventRepository.save({
        id: user.id,
        remind: true,
      });
      const sendEmail = await this.mailService.sendEmailDynamicTemplate(
        user.email,
        {
          email: user.email,
          name: user.fullname,
          phone: user.phone,

          event_name: event.name,
          event_code: event.code,
          invite_code: user.invite_code,
          attendees_number: event.attendees_number,
          start_at: `${formatInTimeZone(
            Number(event.start_at),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy',
          )} (GMT+7)`,
          finish_at: `${formatInTimeZone(
            Number(event.finish_at),
            'Asia/Ho_Chi_Minh',
            'HH:mm dd/MM/yyyy',
          )} (GMT+7)`,
          countdown: '7 ngày',
        },
        event.email_remind_template_id || APP_CONFIG.REMIND_EVENT_TEMPLATE_ID,
      );
      if (!sendEmail) {
        await this.userEventRepository.save({
          id: user.id,
          remind: false,
        });
        return {
          payload: false,
        };
      }
    }
    return {
      payload: true,
    };
  }
}
