import { IsBoolean, IsObject } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EventDataValidate, UserDataValidate } from '../validate';

export class RemindAttendEventInput {
  @ApiProperty({
    required: true,
    description: 'user',
  })
  @IsObject()
  user: UserDataValidate;
  @ApiProperty({
    required: true,
    description: 'user',
  })
  @IsObject()
  event: EventDataValidate;
}

export class RemindAttendEventOutput {
  @ApiProperty({
    required: true,
    description: 'Status of update',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
