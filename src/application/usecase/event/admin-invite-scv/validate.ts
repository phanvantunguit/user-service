import {
  IsString,
  IsEmail,
  IsOptional,
  IsUUID,
  IsNumber,
  IsArray,
  ValidateNested,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  EVENT_CONFIRM_STATUS,
  INVITE_CODE_TYPE,
  PAYMENT_STATUS,
} from 'src/domain/event/types';
import { Type } from 'class-transformer';

export class AdminInviteEventCSVInput {
  @ApiProperty({
    required: true,
    description: 'Url webstite result confirm',
    example: 'http://localhost:3000',
  })
  @IsString()
  callback_confirm_url: string;

  @ApiProperty({
    required: true,
    description: 'Url webstite result scan qrcode',
    example: 'http://localhost:3000',
  })
  @IsString()
  callback_attend_url: string;

  @ApiProperty({
    required: true,
    description: 'Event id uuid',
  })
  @IsUUID()
  event_id: string;
}
export class AdminInviteEventCSVFileDataInput {
  @ApiProperty({
    required: true,
    description: 'Numerical order',
    example: 1,
  })
  @IsNumber()
  index: number;

  @ApiProperty({
    required: false,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Fullname',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: false,
    description: 'Telegram',
  })
  @IsOptional()
  @IsString()
  telegram: string;

  @ApiProperty({
    required: true,
    description: 'Phone number',
  })
  @IsString()
  phone: string;

  @ApiProperty({
    required: true,
    description: 'type of ticket',
  })
  @IsString()
  type: INVITE_CODE_TYPE;

  @ApiProperty({
    required: true,
    description: 'Confirm status of user',
  })
  @IsString()
  status: EVENT_CONFIRM_STATUS;

  @ApiProperty({
    required: true,
    description: 'Payment status of user',
  })
  @IsString()
  payment_status: PAYMENT_STATUS;
}
export class AdminInviteEventCSVDataOutput {
  @ApiProperty({
    required: true,
    description: 'Numerical order',
    example: 1,
  })
  @IsNumber()
  index: number;

  @ApiProperty({
    required: true,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Error message',
    example: 'Email duplicate',
  })
  @IsString()
  message: string;
}
export class AdminInviteEventCSVOutput {
  @ApiProperty({
    required: true,
    description: 'Numerical order failed',
    isArray: true,
    type: AdminInviteEventCSVDataOutput,
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => AdminInviteEventCSVDataOutput)
  payload: AdminInviteEventCSVDataOutput[];
}
