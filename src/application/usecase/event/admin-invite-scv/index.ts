import { Inject, Injectable } from '@nestjs/common';
import { EVENT_CONFIRM_STATUS, INVITE_CODE_TYPE } from 'src/domain/event/types';
import * as XLSX from 'xlsx';
import {
  AdminInviteEventCSVDataOutput,
  AdminInviteEventCSVFileDataInput,
  AdminInviteEventCSVInput,
  AdminInviteEventCSVOutput,
} from './validate';
import { AdminInviteEventHandler } from '../admin-invite';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { notFoundError } from 'src/utils/exceptions/throw.exception';

@Injectable()
export class AdminInviteEventCSVHandler {
  constructor(
    @Inject(AdminInviteEventHandler)
    private adminInviteEventHandler: AdminInviteEventHandler,
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(
    file: any,
    param: AdminInviteEventCSVInput,
    admin_id: string,
  ): Promise<AdminInviteEventCSVOutput> {
    const workbook = XLSX.read(file.buffer, { type: 'buffer' });
    const rowObjects = XLSX.utils.sheet_to_json(
      workbook.Sheets[workbook.SheetNames[0]],
    ) as AdminInviteEventCSVFileDataInput[];

    const event = await this.eventRepository.findById(param.event_id);
    if (!event) {
      notFoundError('Event');
    }

    const result = (await new Promise((resolve) => {
      const emails = [];
      let count = 0;
      const dataOutputs = [];
      rowObjects.map(async (row: any) => {
        if (row.email) {
          const registered = await this.userEventRepository.findOne({
            event_id: event.id,
            email: row.email,
          });
          if (registered) {
            dataOutputs.push({
              index: row.index,
              email: row.email,
              message: 'Email Existed',
            });
          } else if (emails.includes(row.email)) {
            dataOutputs.push({
              index: row.index,
              email: row.email,
              message: 'Email duplicate',
            });
          } else {
            emails.push(row.email);
          }
        }
        count += 1;
        if (count === rowObjects.length) {
          resolve(dataOutputs);
        }
      });
    })) as AdminInviteEventCSVDataOutput[];
    this.asyncExecute(
      rowObjects,
      param.callback_confirm_url,
      param.callback_attend_url,
      param.event_id,
      admin_id,
    );
    return {
      payload: result,
    };
  }
  private async asyncExecute(
    rowObjects: AdminInviteEventCSVFileDataInput[],
    callback_confirm_url: string,
    callback_attend_url: string,
    event_id: string,
    admin_id: string,
  ) {
    let count = 0;
    while (count < rowObjects.length) {
      try {
        if (rowObjects[count]) {
          await this.adminInviteEventHandler.execute(
            {
              fullname: rowObjects[count].fullname,
              email: rowObjects[count].email,
              phone: rowObjects[count].phone,
              callback_confirm_url: callback_confirm_url,
              callback_attend_url: callback_attend_url,
              type: rowObjects[count].type || INVITE_CODE_TYPE.VIP,
              event_id: event_id,
              confirm_status: rowObjects[count].email
                ? EVENT_CONFIRM_STATUS.WAITING
                : rowObjects[count].status &&
                  Object.values(EVENT_CONFIRM_STATUS).includes(
                    rowObjects[count].status,
                  )
                ? rowObjects[count].status
                : EVENT_CONFIRM_STATUS.APPROVED,
              telegram: rowObjects[count].telegram,
              payment_status: rowObjects[count].payment_status,
            },
            admin_id,
          );
        }
      } catch (error) {}
      count += 1;
    }
  }
}
