import { IsString, IsObject } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EventDataValidate } from '../validate';

export class UserGetEventInput {
  @ApiProperty({
    required: true,
    description: 'Code of event',
  })
  @IsString()
  code: string;
}
export class UserGetEventOutput {
  @ApiProperty({
    required: true,
    description: 'Result event',
  })
  @IsObject()
  payload: EventDataValidate;
}
