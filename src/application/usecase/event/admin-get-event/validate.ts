import { IsString, IsObject } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EventDataValidate } from '../validate';

export class AdminGetEventInput {
  @ApiProperty({
    required: true,
    description: 'UUID of event',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsString()
  id: string;
}
export class AdminGetEventOutput {
  @ApiProperty({
    required: true,
    description: 'Result event',
  })
  @IsObject()
  payload: EventDataValidate;
}
