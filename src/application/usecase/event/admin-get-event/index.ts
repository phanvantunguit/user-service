import { Inject } from '@nestjs/common';
import { EventDomain } from 'src/domain/event/event.domain';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { notFoundError } from 'src/utils/exceptions/throw.exception';
import { AdminGetEventInput, AdminGetEventOutput } from './validate';

export class AdminGetEventHandler {
  constructor(
    @Inject(EventRepository)
    private eventRepository: EventRepository,
  ) {}
  async execute(param: AdminGetEventInput): Promise<AdminGetEventOutput> {
    const event = await this.eventRepository.findById(param.id);
    if (!event) {
      notFoundError('Event');
    }
    const result = new EventDomain();
    result.name = event.name;
    result.code = event.code;
    result.attendees_number = event.attendees_number;
    result.status = event.status;
    result.email_remind_at = event.email_remind_at;
    result.email_remind_template_id = event.email_remind_template_id;
    result.email_confirm = event.email_confirm;
    result.email_confirm_template_id = event.email_confirm_template_id;
    result.start_at = event.start_at;
    result.remind = event.remind;
    result.finish_at = event.finish_at;
    result.updated_by = event.updated_by;
    result.created_by = event.created_by;
    result.updated_at = event.updated_at;
    result.created_at = event.created_at;
    result.deleted_at = event.deleted_at;
    return {
      payload: result,
    };
  }
}
