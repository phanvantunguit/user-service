import { Inject, Injectable } from '@nestjs/common';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { EventRepository } from 'src/infrastructure/data/database/event.repository';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import {
  badRequestError,
  notFoundError,
  serverError,
} from 'src/utils/exceptions/throw.exception';
import { AdminInviteEventInput, AdminInviteEventOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { StorageService } from 'src/infrastructure/services/storage';
import { generateQRCode } from 'src/utils/qrcode.util';
import { formatInTimeZone } from 'date-fns-tz';
import { APP_CONFIG } from 'src/config';
import { PAYMENT_STATUS } from 'src/domain/event/types';

@Injectable()
export class AdminInviteEventHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(EventRepository)
    private eventRepository: EventRepository,
    @Inject(MailService) private mailService: MailService,
    @Inject(StorageService) private storageService: StorageService,
  ) {}
  async execute(
    param: AdminInviteEventInput,
    admin_id: string,
  ): Promise<AdminInviteEventOutput> {
    const event = await this.eventRepository.findById(param.event_id);
    if (!event) {
      notFoundError('Event');
    }
    if (param.email) {
      const registered = await this.userEventRepository.findOne({
        event_id: event.id,
        email: param.email,
      });
      if (registered) {
        badRequestError('Email', ERROR_CODE.EXISTED);
      }
    }
    const count = await this.userEventRepository.count({ event_id: event.id });

    const userEvent = new UserEventDomain();
    userEvent.fullname = param.fullname;
    userEvent.phone = param.phone;
    userEvent.email = param.email;
    userEvent.event_id = event.id;
    userEvent.confirm_status = param.confirm_status;
    userEvent.created_by = admin_id || null;
    userEvent.updated_by = admin_id || null;
    const lengthCount = event.attendees_number
      ? event.attendees_number.toString().length
      : 3;
    userEvent.setInviteCode(param.type, count + 1, lengthCount);
    userEvent.telegram = param.telegram;
    userEvent.payment_status = param.payment_status;
    const userEventSaved = await this.userEventRepository.save(userEvent);
    const attendUrl = param.callback_attend_url.includes('?')
      ? param.callback_attend_url + `&token=${userEventSaved.id}`
      : param.callback_attend_url + `?token=${userEventSaved.id}`;

    if (param.email) {
      let qrcode_url = '';
      let template_id = APP_CONFIG.INVITE_EVENT_SIMPLE_TEMPLATE_ID;
      if (event.create_qrcode) {
        template_id = APP_CONFIG.INVITE_EVENT_QRCODE_TEMPLATE_ID;
        try {
          // qrcode_url = `https://api.qrserver.com/v1/create-qr-code/?data=${attendUrl}&amp;size=100x100`;
          const qrcode = await generateQRCode(attendUrl);
          qrcode_url = await this.storageService.uploadBuffer(
            qrcode,
            `${userEventSaved.id}.png`,
          );
        } catch (error) {
          await this.userEventRepository.deleteById(userEventSaved.id);
          serverError('QRCode', ERROR_CODE.UNPROCESSABLE);
        }
      }

      if (event.email_confirm) {
        template_id = event.email_confirm_template_id || template_id;
        const sendEmail = await this.mailService.sendEmailDynamicTemplate(
          userEvent.email,
          {
            email: userEvent.email,
            name: userEvent.fullname,
            phone: userEvent.phone,

            event_name: event.name,
            event_code: event.code,
            invite_code: userEvent.invite_code,
            attendees_number: event.attendees_number,
            start_at: `${formatInTimeZone(
              Number(event.start_at),
              'Asia/Ho_Chi_Minh',
              'HH:mm dd/MM/yyyy',
            )} (GMT+7)`,
            finish_at: `${formatInTimeZone(
              Number(event.finish_at),
              'Asia/Ho_Chi_Minh',
              'HH:mm dd/MM/yyyy',
            )} (GMT+7)`,
            qrcode_url: qrcode_url,
            callback_confirm_url: param.callback_confirm_url,
          },
          template_id,
        );
        if (!sendEmail) {
          await this.userEventRepository.deleteById(userEventSaved.id);
          serverError('Email', ERROR_CODE.UNPROCESSABLE);
        }
      }
      if (param.payment_status === PAYMENT_STATUS.COMPLETED) {
        const template_id = APP_CONFIG.PAYMENT_EVENT_TEMPLATE_ID;
        await this.mailService.sendEmailDynamicTemplate(
          userEvent.email,
          {
            email: userEvent.email,
            name: userEvent.fullname,
            phone: userEvent.phone,

            event_name: event.name,
            event_code: event.code,
            invite_code: userEvent.invite_code,
            attendees_number: event.attendees_number,
            start_at: `${formatInTimeZone(
              Number(event.start_at),
              'Asia/Ho_Chi_Minh',
              'HH:mm dd/MM/yyyy',
            )} (GMT+7)`,
            finish_at: `${formatInTimeZone(
              Number(event.finish_at),
              'Asia/Ho_Chi_Minh',
              'HH:mm dd/MM/yyyy',
            )} (GMT+7)`,
          },
          template_id,
        );
      }
    }
    return {
      payload: attendUrl,
    };
  }
}
