import { IsString, IsEmail, IsOptional, IsIn } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  EVENT_CONFIRM_STATUS,
  INVITE_CODE_TYPE,
  PAYMENT_STATUS,
} from 'src/domain/event/types';

export class AdminInviteEventInput {
  @ApiProperty({
    required: true,
    description: 'Fullname of user register',
    example: 'Nguyen Van A',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: false,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone?: string;

  @ApiProperty({
    required: false,
    description: 'Telegram',
  })
  @IsOptional()
  @IsString()
  telegram?: string;

  @ApiProperty({
    required: true,
    description: 'Url webstite result confirm',
    example: 'http://localhost:3000',
  })
  @IsString()
  callback_confirm_url: string;

  @ApiProperty({
    required: true,
    description: 'Url webstite result scan qrcode',
    example: 'http://localhost:3000',
  })
  @IsString()
  callback_attend_url: string;

  @ApiProperty({
    required: true,
    description: 'Type of ticket',
    example: INVITE_CODE_TYPE.VIP,
  })
  @IsIn(Object.values(INVITE_CODE_TYPE))
  type: INVITE_CODE_TYPE;

  @ApiProperty({
    required: true,
    description: 'Event id uuid',
  })
  @IsString()
  event_id: string;

  @ApiProperty({
    required: true,
    description: 'Status confirm of ticket',
    example: EVENT_CONFIRM_STATUS.APPROVED,
  })
  @IsIn(Object.values(EVENT_CONFIRM_STATUS))
  confirm_status: EVENT_CONFIRM_STATUS;

  @ApiProperty({
    required: false,
    description: 'Status payment of event',
    example: PAYMENT_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(PAYMENT_STATUS))
  payment_status: PAYMENT_STATUS;
}

export class AdminInviteEventOutput {
  @ApiProperty({
    required: true,
    description: 'Data to generate qrcode',
    example: 'https://event-dev.coinmap.tech?token=abc',
  })
  @IsString()
  payload: string;
}
