import {
  IsString,
  IsOptional,
  IsIn,
  IsNumberString,
  ValidateNested,
  IsNumber,
  IsObject,
  IsArray,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  EVENT_CONFIRM_STATUS,
  INVITE_CODE_TYPE,
  PAYMENT_STATUS,
} from 'src/domain/event/types';
import { Type } from 'class-transformer';
import { UserDataValidate } from '../validate';

export class AdminListUserInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number;

  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: phone number, name, email',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string;

  @ApiProperty({
    required: false,
    description: 'Event id uuid',
  })
  @IsOptional()
  @IsString()
  event_id: string;

  @ApiProperty({
    required: false,
    description: 'Type of ticket',
    example: INVITE_CODE_TYPE.GUEST,
  })
  @IsOptional()
  @IsIn(Object.values(INVITE_CODE_TYPE))
  type: INVITE_CODE_TYPE;

  @ApiProperty({
    required: false,
    description: 'Status confirm of ticket',
    example: EVENT_CONFIRM_STATUS.APPROVED,
  })
  @IsOptional()
  @IsIn(Object.values(EVENT_CONFIRM_STATUS))
  confirm_status: EVENT_CONFIRM_STATUS;

  @ApiProperty({
    required: false,
    description: 'Status payment of event',
    example: PAYMENT_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(PAYMENT_STATUS))
  payment_status: PAYMENT_STATUS;

  @ApiProperty({
    required: false,
    description: 'Status checkin of user',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  @IsIn(['1', '0'])
  attend: number;
}
export class PagingUserDataOutput {
  @ApiProperty({
    required: true,
    description: 'List of user',
    isArray: true,
    type: UserDataValidate,
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UserDataValidate)
  rows: UserDataValidate[];

  @ApiProperty({
    required: true,
    description: 'Current page',
    example: 1,
  })
  @IsNumber()
  page: number;

  @ApiProperty({
    required: true,
    description: 'Maximun number of list',
    example: 50,
  })
  @IsNumber()
  size: number;

  @ApiProperty({
    required: true,
    description: 'Number of current list',
    example: 10,
  })
  @IsNumber()
  count: number;

  @ApiProperty({
    required: true,
    description: 'Number of query',
    example: 100,
  })
  @IsNumber()
  total: number;
}
export class AdminListUserOutput {
  @ApiProperty({
    required: true,
    description: 'Result user paging',
  })
  @IsObject()
  payload: PagingUserDataOutput;
}
