import { Inject } from '@nestjs/common';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { AdminListUserInput, AdminListUserOutput } from './validate';

export class AdminListUserHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
  ) {}
  async execute(param: AdminListUserInput): Promise<AdminListUserOutput> {
    const admin = await this.userEventRepository.getPaging(param);
    return {
      payload: admin,
    };
  }
}
