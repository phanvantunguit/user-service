import { Inject } from '@nestjs/common';
import { UserEventDomain } from 'src/domain/event/user-event.domain';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { AdminGetUserInput, AdminGetUserOutput } from './validate';

export class AdminGetUserHandler {
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
  ) {}
  async execute(param: AdminGetUserInput): Promise<AdminGetUserOutput> {
    const user = await this.userEventRepository.findById(param.id);
    const result = new UserEventDomain();
    result.id = user.id;
    result.fullname = user.fullname;
    result.email = user.email;
    result.phone = user.phone;
    result.event_id = user.event_id;
    result.attend = user.attend;
    result.invite_code = user.invite_code;
    result.confirm_status = user.confirm_status;
    result.payment_status = user.payment_status;
    result.telegram = user.telegram;
    result.created_by = user.created_by;
    result.updated_by = user.updated_by;
    result.created_at = user.created_at;
    result.updated_at = user.updated_at;
    return {
      payload: result,
    };
  }
}
