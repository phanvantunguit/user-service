import { IsString, IsObject } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserDataValidate } from '../validate';

export class AdminGetUserInput {
  @ApiProperty({
    required: true,
    description: 'UUID of user',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsString()
  id: string;
}
export class AdminGetUserOutput {
  @ApiProperty({
    required: true,
    description: 'Result user',
  })
  @IsObject()
  payload: UserDataValidate;
}
