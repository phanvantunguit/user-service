import { ApiProperty } from '@nestjs/swagger';
import { AppSettingValidate } from '../validate';
import { Type } from 'class-transformer';

export class GetAppSettingOutput {
  @ApiProperty({
    required: false,
    type: AppSettingValidate,
  })
  @Type(() => AppSettingValidate)
  payload: AppSettingValidate;
}
