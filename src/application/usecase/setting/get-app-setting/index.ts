import { Inject } from '@nestjs/common';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { GetAppSettingOutput } from './validate';

export class GetAppSettingHandler {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
  ) {}
  async execute(id: string): Promise<GetAppSettingOutput> {
    const result = await this.settingRepository.findById(id);
    return {
      payload: result,
    };
  }
}

