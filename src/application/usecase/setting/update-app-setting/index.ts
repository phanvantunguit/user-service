import { Inject } from '@nestjs/common';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { BaseUpdateOutput } from '../../validate';
import { UpdateAppSettingInput } from './validate';

export class UpdateAppSettingHandler {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
  ) {}
  async execute(
    param: UpdateAppSettingInput,
  ): Promise<BaseUpdateOutput> {
    await this.settingRepository.save(param)
    return {
      payload: true,
    };
  }
}
