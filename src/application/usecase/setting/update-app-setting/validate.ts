import {
    IsArray,
    IsOptional,
  } from 'class-validator';
  import { ApiProperty } from '@nestjs/swagger';
  import { AppSettingValidate } from '../validate';
  import { Type } from 'class-transformer';
  
  export class UpdateAppSettingInput extends AppSettingValidate {}
