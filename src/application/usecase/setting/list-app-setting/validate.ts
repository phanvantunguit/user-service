import {
  IsArray,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { AppSettingValidate } from '../validate';
import { Type } from 'class-transformer';

export class ListAppSettingInput {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  user_id?: string;
}

export class ListAppSettingOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: AppSettingValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => AppSettingValidate)
  payload: AppSettingValidate[];
}
