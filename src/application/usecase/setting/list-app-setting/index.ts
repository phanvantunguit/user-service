import { Inject } from '@nestjs/common';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { ListAppSettingInput, ListAppSettingOutput } from './validate';

export class ListAppSettingHandler {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
  ) {}
  async execute(
    param: ListAppSettingInput,
  ): Promise<ListAppSettingOutput> {
    const result = await this.settingRepository.find(param)
    return {
      payload: result,
    };
  }
}
