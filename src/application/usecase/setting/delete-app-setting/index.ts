import { Inject } from '@nestjs/common';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { BaseUpdateOutput } from '../../validate';

export class DeleteAppSettingHandler {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
  ) {}
  async execute(id: string): Promise<BaseUpdateOutput> {
    await this.settingRepository.deleteById(id);
    return {
      payload: true,
    };
  }
}
