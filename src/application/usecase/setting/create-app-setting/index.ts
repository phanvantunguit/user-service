import { Inject } from '@nestjs/common';
import { SettingRepository } from 'src/infrastructure/data/database/setting.repository';
import { BaseCreateOutput } from '../../validate';
import { CreateAppSettingInput } from './validate';

export class CreateAppSettingHandler {
  constructor(
    @Inject(SettingRepository)
    private settingRepository: SettingRepository,
  ) {}
  async execute(
    param: CreateAppSettingInput,
  ): Promise<BaseCreateOutput> {
    const result = await this.settingRepository.save(param)
    return {
      payload: result.id,
    };
  }
}
