import { IsBoolean, IsEmail, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AppSettingValidate {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  id?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  value?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  user_id?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  owner_created?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  created_at?: number;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  updated_at?: number;
}
