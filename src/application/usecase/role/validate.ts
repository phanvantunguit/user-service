import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class MerchantRoleValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string;

  @ApiProperty({
    required: true,
    example: 'Role 1',
  })
  @IsString()
  name: string;
}
