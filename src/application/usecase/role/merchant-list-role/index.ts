import { Inject } from '@nestjs/common';
import { RoleRepository } from 'src/infrastructure/data/database/role.repository';
import { MerchantListRoleOuput } from './validate';

export class MerchantListRoleHandler {
  constructor(
    @Inject(RoleRepository)
    private roleRepository: RoleRepository,
  ) {}
  async execute(): Promise<MerchantListRoleOuput> {
    const roles = await this.roleRepository.list();
    return {
      payload: roles,
    };
  }
}
