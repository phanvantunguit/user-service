import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsArray } from 'class-validator';
import { MerchantRoleValidate } from '../validate';

export class MerchantListRoleOuput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: MerchantRoleValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => MerchantRoleValidate)
  payload: MerchantRoleValidate[];
}
