import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEmail, IsOptional } from 'class-validator';
export class UserDataPromotionValidate {
  @ApiProperty({
    required: true,
    description: 'email',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: false,
    description: 'fullname',
  })
  @IsOptional()
  @IsString()
  fullname?: string;
}
