import { Inject, Injectable } from '@nestjs/common';
import { PromotionDynamicInput, PromotionDynamicOutput } from './validate';
import { MailService } from 'src/infrastructure/services/mail';
import { UserPromotionRepository } from 'src/infrastructure/data/database/user-promotion.repository';
import { UserPromotionDomain } from 'src/domain/promotion/user-promotion.domain';

@Injectable()
export class SendPromotionDynamicHandler {
  constructor(
    @Inject(MailService) private mailService: MailService,
    @Inject(UserPromotionRepository)
    private userPromotionRepository: UserPromotionRepository,
  ) {}
  async execute(param: PromotionDynamicInput): Promise<PromotionDynamicOutput> {
    const { fullname, email, template_id, from_email, from_name } = param;
    const userPromotion = await this.userPromotionRepository.findOne({
      template_id,
      email,
    });
    const dataUser = new UserPromotionDomain({
      template_id,
      email,
      send: true,
    });
    // if (userPromotion) {
    //   if (userPromotion.send) {
    //     return {
    //       payload: true,
    //     };
    //   }
    //   dataUser.id;
    // }

    // const userPromotionSaved = await this.userPromotionRepository.save(
    //   dataUser,
    // );
    const sendEmail = await this.mailService.sendEmailDynamicTemplate(
      email,
      {
        email: email,
        name: fullname,
      },
      template_id,
      null,
      from_email,
      from_name,
    );
    if (!sendEmail) {
      // userPromotionSaved.send = false;
      // await this.userPromotionRepository.save(userPromotionSaved);
      // return {
      //   payload: false,
      // };
    }
    return {
      payload: true,
    };
  }
}
