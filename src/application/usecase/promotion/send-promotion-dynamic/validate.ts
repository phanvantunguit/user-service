import { IsBoolean, IsEmail, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PromotionDynamicInput {
  @ApiProperty({
    required: false,
    description: 'Email sender',
  })
  @IsOptional()
  @IsEmail()
  from_email?: string;

  @ApiProperty({
    required: false,
    description: 'Sender name',
  })
  @IsOptional()
  @IsString()
  from_name?: string;

  @ApiProperty({
    required: true,
    description: 'fullname',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: true,
    description: 'email',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'template id',
    example: 'abc',
  })
  @IsString()
  template_id: string;
}

export class PromotionDynamicOutput {
  @ApiProperty({
    required: true,
    description: 'Status of update',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
