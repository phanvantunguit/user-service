import { Inject, Injectable } from '@nestjs/common';
import { APP_CONFIG } from 'src/config';
import { UserEventRepository } from 'src/infrastructure/data/database/user-event.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { SendPromotionDynamicHandler } from '../send-promotion-dynamic';
import { UserDataPromotionValidate } from '../validate';
import { AdminSendPromotionInput, AdminSendPromotionOutput } from './validate';

@Injectable()
export class AdminSendPromotionHandler {
  proccessing: boolean;
  constructor(
    @Inject(UserEventRepository)
    private userEventRepository: UserEventRepository,
    @Inject(SendPromotionDynamicHandler)
    private sendPromotionDynamicHandler: SendPromotionDynamicHandler,
  ) {}

  async execute(
    param: AdminSendPromotionInput,
  ): Promise<AdminSendPromotionOutput> {
    if (this.proccessing) {
      badRequestError('Promotion Email', ERROR_CODE.PROCESSING);
    }
    const template_id = param.template_id || APP_CONFIG.PROMOTION_TEMPLATE_ID;
    this.proccessing = true;
    let users: UserDataPromotionValidate[];
    if (param.users) {
      users = param.users;
    } else {
      users = await this.userEventRepository.getEmails(param);
    }
    this.asyncExecute(users, template_id, param.from_email, param.from_name);
    return {
      payload: users.length,
    };
  }

  async asyncExecute(
    users: UserDataPromotionValidate[],
    template_id: string,
    from_email?: string,
    from_name?: string,
  ) {
    for (const user of users) {
      try {
        await this.sendPromotionDynamicHandler.execute({
          email: user.email,
          fullname: user.fullname,
          template_id,
          from_email,
          from_name,
        });
      } catch (error) {}
    }
    this.proccessing = false;
    return {
      payload: true,
    };
  }
}
