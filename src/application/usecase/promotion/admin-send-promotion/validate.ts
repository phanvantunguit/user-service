import {
  IsOptional,
  IsIn,
  IsArray,
  IsBoolean,
  ValidateNested,
  IsUUID,
  IsNumber,
  IsString,
  IsEmail,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { EVENT_CONFIRM_STATUS, PAYMENT_STATUS } from 'src/domain/event/types';
import { UserDataPromotionValidate } from '../validate';
import { Type } from 'class-transformer';

export class AdminSendPromotionInput {
  @ApiProperty({
    required: false,
    description: 'Email sender',
  })
  @IsOptional()
  @IsEmail()
  from_email?: string;

  @ApiProperty({
    required: false,
    description: 'Sender name',
  })
  @IsOptional()
  @IsString()
  from_name?: string;

  @ApiProperty({
    required: false,
    description: 'Template id',
  })
  @IsOptional()
  @IsString()
  template_id: string;

  @ApiProperty({
    required: false,
    description: 'Event id uuid',
  })
  @IsOptional()
  @IsUUID()
  event_id: string;

  @ApiProperty({
    required: false,
    description: 'Status confirm of ticket',
    example: EVENT_CONFIRM_STATUS.APPROVED,
  })
  @IsOptional()
  @IsIn(Object.values(EVENT_CONFIRM_STATUS))
  confirm_status: EVENT_CONFIRM_STATUS;

  @ApiProperty({
    required: false,
    description: 'Status payment of event',
    example: PAYMENT_STATUS.COMPLETED,
  })
  @IsOptional()
  @IsIn(Object.values(PAYMENT_STATUS))
  payment_status: PAYMENT_STATUS;

  @ApiProperty({
    required: false,
    description: 'attend',
  })
  @IsOptional()
  @IsBoolean()
  attend: boolean;

  @ApiProperty({
    required: false,
    isArray: true,
    type: UserDataPromotionValidate,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UserDataPromotionValidate)
  users: UserDataPromotionValidate[];
}

export class AdminSendPromotionOutput {
  @ApiProperty({
    required: true,
    description: 'number of email',
    example: 1000,
  })
  @IsNumber()
  payload: number;
}
