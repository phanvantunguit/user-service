import { v4 } from 'uuid';

import { Inject } from '@nestjs/common';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { RawVerifyToken } from './verify-token.types';
export class VerifyTokenService {
  constructor(@Inject(UserRepository) private userRepo: UserRepository) {}

  createTokenUUID(
    params: RawVerifyToken,
    metadata?: any,
  ): Promise<RawVerifyToken> {
    const { user_id, expires_at, type } = params;
    const verifyToken = {
      user_id,
      expires_at,
      type,
      token: v4(),
      metadata: metadata,
    };
    return this.userRepo.saveVerifyToken(verifyToken);
  }
}
