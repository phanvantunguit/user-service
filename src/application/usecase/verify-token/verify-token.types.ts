import { VERIFY_TOKEN_TYPE } from 'src/const/user';

export interface VerifyTokenDomain {
  // write
  createTokenUUID(
    params: CreateTokenUUID,
    metdata: any,
  ): Promise<RawVerifyToken>;
}
export type CreateTokenUUID = {
  user_id: string;
  type: VERIFY_TOKEN_TYPE;
  expires_at: number;
};
export type QueryToken = {
  user_id?: string;
  token?: string;
  type?: VERIFY_TOKEN_TYPE;
};
export type DeleteToken = {
  user_id?: string;
  token?: string;
  type?: VERIFY_TOKEN_TYPE;
};
export type RawVerifyToken = {
  id?: string;
  user_id?: string;
  token?: string;
  type?: VERIFY_TOKEN_TYPE;
  expires_at?: number;
  metadata?: any;
};
