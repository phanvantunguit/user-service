import { Inject } from '@nestjs/common';
import { ITEM_STATUS, ORDER_CATEGORY } from 'src/domain/transaction/types';
import { BotTradingRepository } from 'src/infrastructure/data/database/bot-trading.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MerchantListBotTradingOuput } from './validate';

export class MerchantListBotTradingHandler {
  constructor(
    @Inject(BotTradingRepository)
    private botTradingRepository: BotTradingRepository,
    @Inject(UserRepository) private userRepository: UserRepository,
  ) {}
  async execute(user_id?: string, merchant_id?: string): Promise<MerchantListBotTradingOuput> {
    let bots = [];
    if (user_id) {
      const userBots = await this.userRepository.findUserBotTrading({
        user_id,
      });
      const userAssetLog = await this.userRepository.findAssetLogLatest({
        user_id,
        category: ORDER_CATEGORY.TBOT,
        status: [ITEM_STATUS.NOT_CONNECT],
      });
      const botIds = userBots.map((e) => e.bot_id);
      if (botIds.length > 0) {
        const tbots = await this.botTradingRepository.findByIds(botIds);
        bots = tbots.map((e) => {
          const userBotExit = userBots.find((ub) => ub.bot_id === e.id);
          const userAssetLogExit = userAssetLog.find(
            (ual) => ual.asset_id === e.id,
          );
          return {
            ...e,
            user_bot_id: userBotExit?.id,
            expires_at: userBotExit?.expires_at,
            status: userBotExit?.status,
            quantity: userAssetLogExit?.quantity,
            package_type: userAssetLogExit?.package_type,
            tbot_type: userBotExit.type,
            balance: userBotExit.balance,
            price: userAssetLogExit.price,
            created_at: userAssetLogExit.created_at
          };
        });
      }
    } else {
      bots = await this.botTradingRepository.list({merchant_id});
    }
    return {
      payload: bots,
    };
  }
}
