import { Inject } from '@nestjs/common';
import { BotRepository } from 'src/infrastructure/data/database/bot.repository';
import { MerchantListBotOuput } from './validate';

export class MerchantListBotHandler {
  constructor(
    @Inject(BotRepository)
    private botRepository: BotRepository,
  ) {}
  async execute(): Promise<MerchantListBotOuput> {
    const bots = await this.botRepository.list();
    return {
      payload: bots,
    };
  }
}
