import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsArray } from 'class-validator';
import { MerchantBotValidate } from '../validate';

export class MerchantListBotOuput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: MerchantBotValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => MerchantBotValidate)
  payload: MerchantBotValidate[];
}
