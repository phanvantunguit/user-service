import { Inject } from '@nestjs/common';
import { APP_CONFIG } from 'src/config';
import { DEFAULT_ROLE } from 'src/const/permission';
import { VERIFY_TOKEN_TYPE } from 'src/const/user';
import { MERCHANT_TYPE } from 'src/domain/merchant/types';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { generatePassword } from 'src/utils/hash.util';
import { SECOND } from 'src/utils/time.util';
import { VerifyTokenService } from '../../verify-token';
import { PutUserProfileDto } from './validate';

export class MerchantUserCreateServiceHandler {
  constructor(
    @Inject(MailService) private mailService: MailService,
    @Inject(VerifyTokenService) private verifyTokenService: VerifyTokenService,
    @Inject(UserRepository) private userRepo: UserRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    params: PutUserProfileDto,
    owner_created: string,
  ): Promise<any> {
    const email = params.email.toLocaleLowerCase();
    const merchant = await this.merchantRepository.findOne({
      id: owner_created,
    });
    // if merchant MERCHANT_TYPE.OTHERS check email in this merchant
    if (merchant.config['domain_type'] === MERCHANT_TYPE.OTHERS) {
      const findUser = await this.userRepo.findOneUser({
        email,
        merchant_code: merchant.code,
      });
      if (findUser) {
        badRequestError('Email address was', ERROR_CODE.EXISTED);
      }
    } else {
      // if merchant MERCHANT_TYPE.COINAMP
      // get all merchant coinmap
      // check email in all of them
      const merchants = await this.merchantRepository.findMerchant({
        domain_type: MERCHANT_TYPE.COINMAP,
      });
      if (merchants.length > 0) {
        const findUser = await this.userRepo.findUser({
          email,
          merchant_code: merchants.map((m) => m.code),
        });
        if (findUser.length > 0) {
          badRequestError('Email address was', ERROR_CODE.EXISTED);
        }
      }
    }

    const userCreated = await this.userRepo.saveUser({
      created_at: Date.now(),
      ...params,
      email,
      merchant_code: merchant.code,
    });
    const verifyToken = await this.verifyTokenService.createTokenUUID({
      user_id: userCreated.id,
      expires_at:
        Date.now() + Number(APP_CONFIG.TIME_EXPIRED_VERIFY_EMAIL) * SECOND,
      type: VERIFY_TOKEN_TYPE.VERIFY_EMAIL,
    });

    if (verifyToken) {
      await this.userRepo.saveUserRole([
        {
          user_id: userCreated.id,
          role_id: DEFAULT_ROLE.id,
          description: 'Role default',
          owner_created: 'system',
        },
      ]);
      let from_email;
      let from_name;
      let logo_url;
      let header_url;
      let main_content;
      let footer_content;
      let twitter_url;
      let youtube_url;
      let facebook_url;
      let telegram_url;
      let company_name;
      if (merchant && merchant.config) {
        if (
          merchant.config['verified_sender'] &&
          merchant.config['email_sender']
        ) {
          from_email = merchant.config['email_sender']['from_email'];
          from_name = merchant.config['email_sender']['from_name'];
        }
        if (merchant.config['social_media']) {
          twitter_url = merchant.config['social_media']['twitter_url'];
          youtube_url = merchant.config['social_media']['youtube_url'];
          facebook_url = merchant.config['social_media']['facebook_url'];
          telegram_url = merchant.config['social_media']['telegram_url'];
        }
        logo_url = merchant.config['email_logo_url'];
        header_url = merchant.config['email_banner_url'];
        main_content = `We're eager to have you here, and we'd adore saying thank you on behalf of our whole team for choosing us. We believe our Trading applications will help you trade safety and in comfort.`;
        footer_content = 'Contact us:';
        if (merchant.config.domain_type === MERCHANT_TYPE.OTHERS) {
          company_name = merchant.name;
          from_email = from_email || APP_CONFIG.SENDGRID_SENDER_ALGO_EMAIL
          from_name = from_name || APP_CONFIG.SENDGRID_SENDER_ALGO_NAME
          logo_url = logo_url || APP_CONFIG.ALGO_LOGO_EMAIL
          header_url = header_url || APP_CONFIG.ALGO_BANNER_EMAIL
        }
      }

      const resultSendMail = await this.mailService.sendEmailConfirmAccount(
        params.email,
        verifyToken.token,
        params.password,
        from_email,
        from_name,
        logo_url,
        header_url,
        main_content,
        twitter_url,
        youtube_url,
        facebook_url,
        telegram_url,
        company_name,
        `${userCreated.first_name || ''} ${userCreated.last_name || ''}`,
      );
      if (resultSendMail) {
        await this.userRepo.saveUser({
          id: userCreated.id,
          note_updated: 'Send mail verify success',
        });
      } else {
        await this.userRepo.saveUser({
          id: userCreated.id,
          note_updated: 'Send mail verify failed',
        });
      }
      delete userCreated.password;
      return userCreated;
    }
    badRequestError('', ERROR_CODE.SEND_EMAIL_FAILED);
  }
}
