import { Inject } from '@nestjs/common';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';

import { ListUserInput, ListUserOutput } from './validate';
export class ListUserHandler {
  constructor(
    @Inject(UserRepository)
    private userRepository: UserRepository,
  ) {}
  async execute(
    param: ListUserInput,
    merchant_code: string,
  ): Promise<ListUserOutput> {
    const { from, to, tbots, page, size, keyword, email_confirmed, tbot_status } =
      param;
    const dataRes = await this.userRepository.getPaging({
      page,
      size,
      keyword,
      from,
      to,
      merchant_code,
      tbots: tbots ? tbots.split(',') : [],
      email_confirmed,
      tbot_status: tbot_status ? tbot_status.split(',') : [],
    });
    return {
      payload: dataRes,
    };
  }
}
