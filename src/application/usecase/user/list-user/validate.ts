import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsOptional,
  IsNumberString,
  IsString,
  IsNumber,
  IsArray,
  IsObject,
  IsBooleanString,
} from 'class-validator';
import { ITEM_STATUS } from 'src/domain/transaction/types';
import { ListUserValidate } from '../validate';

export class ListUserInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number;

  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: order id, name, email',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string;

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;

  @ApiProperty({
    required: false,
    description: `List bot filter: ${Object.values(ITEM_STATUS)}`,
  })
  @IsOptional()
  @IsString()
  tbot_status?: string;

  @ApiProperty({
    required: false,
    description: 'List bot id',
  })
  @IsOptional()
  @IsString()
  tbots?: string;

  @ApiProperty({
    required: false,
    description: 'type',
  })
  @IsOptional()
  @IsString()
  asset_type?: string;

  @ApiProperty({
    required: false,
    description: 'merchant_code',
  })
  @IsOptional()
  @IsString()
  merchant_code?: string;

  @ApiProperty({
    required: false,
    description: 'Email confirmed: true false',
    example: 'true',
  })
  @IsOptional()
  @IsBooleanString()
  email_confirmed?: boolean;
}

export class PagingUserOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number;

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number;

  @ApiProperty({
    required: false,
    isArray: true,
    type: ListUserValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ListUserValidate)
  rows: ListUserValidate[];
}

export class ListUserOutput {
  @ApiProperty({
    required: true,
    description: 'Result transaction paging',
  })
  @IsObject()
  payload: PagingUserOutput;
}
