import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional,
  IsNumberString,
  IsNumber,
  IsObject,
} from 'class-validator';

export class ReportUserInput {
  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;
}

export class ReportUserValidate {
  @ApiProperty({
    required: false,
    description: 'User confirmed email',
  })
  @IsNumber()
  count_confirmed: number;

  @ApiProperty({
    required: false,
    description: 'Waiting user confirm email',
  })
  @IsNumber()
  count_not_confirmed: number;

  @ApiProperty({
    required: false,
    description: 'Number View',
  })
  @IsNumber()
  pageView: number;
}

export class ReportUserOutput {
  @ApiProperty({
    required: true,
    description: 'Report user',
  })
  @IsObject()
  payload: ReportUserValidate;
}
