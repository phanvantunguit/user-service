import { Inject } from '@nestjs/common';
import * as moment from 'moment-timezone';
import { APP_CONFIG } from 'src/config';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
const { google } = require('googleapis');

import { ReportUserOutput, ReportUserInput } from './validate';

const scopes = 'https://www.googleapis.com/auth/analytics.readonly';
const jwt = new google.auth.JWT(
  APP_CONFIG.CLIENT_EMAIL,
  null,
  APP_CONFIG.PRIVATE_KEY.replace(/\\n/g, '\n'),
  scopes,
);
async function getViews(view_id: any, from: any, to: any) {
  try {
    const timeFrom = moment(Number(from))
      .tz('Asia/Ho_Chi_Minh')
      .format('YYYY-MM-DD');
    const timeTo = moment(Number(to))
      .tz('Asia/Ho_Chi_Minh')
      .format('YYYY-MM-DD');
    const today = 'today';

    await jwt.authorize();

    if (from != undefined && to != undefined) {
      const response = await google.analytics('v3').data.ga.get({
        auth: jwt,
        ids: 'ga:' + view_id,
        'start-date': timeFrom,
        'end-date': timeTo,
        metrics: 'ga:pageviews',
      });
      return Object.values(response.data?.totalsForAllResults)[0];
    } else if (from != undefined && to == undefined) {
      const response = await google.analytics('v3').data.ga.get({
        auth: jwt,
        ids: 'ga:' + view_id,
        'start-date': timeFrom,
        'end-date': today,
        metrics: 'ga:pageviews',
      });
      return Object.values(response.data?.totalsForAllResults)[0];
    } else if (from == undefined && to != undefined) {
      const response = await google.analytics('v3').data.ga.get({
        auth: jwt,
        ids: 'ga:' + view_id,
        'start-date': new Date(1672531200000).toISOString().substring(0, 10),
        'end-date': timeTo,
        metrics: 'ga:pageviews',
      });
      return Object.values(response.data?.totalsForAllResults)[0];
    } else {
      const response = await google.analytics('v3').data.ga.get({
        auth: jwt,
        ids: 'ga:' + view_id,
        'start-date': new Date(1672531200000).toISOString().substring(0, 10),
        'end-date': today,
        metrics: 'ga:pageviews',
      });
      return Object.values(response.data?.totalsForAllResults)[0];
    }
  } catch (err) {
    console.log(err);
  }
}
export class ReportUserHandler {
  constructor(
    @Inject(UserRepository)
    private userRepository: UserRepository,
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: ReportUserInput,
    merchant_code: string,
  ): Promise<ReportUserOutput> {
    const { from, to } = param;

    const merchantConfig = await this.merchantRepository.queryMerchant(
      merchant_code,
    );
    const view_id = merchantConfig[0]?.config?.view_id;
    const getViewReport = await getViews(view_id, from, to);

    const reports = await this.userRepository.report({
      from,
      to,
      merchant_code,
    });
    const dataRes = reports[0];
    return {
      payload: {
        count_confirmed: Number(dataRes.count_confirmed),
        count_not_confirmed: Number(dataRes.count_not_confirmed),
        pageView: Number(getViewReport),
      },
    };
  }
}
