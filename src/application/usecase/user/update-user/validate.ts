import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEmail,
  IsIn,
  IsOptional,
  IsString,
  Matches,
  ValidateIf,
} from 'class-validator';
import { GENDER, PasswordRegex } from 'src/const/user';

export class UpdateUserProfileDto {
  @ApiProperty({
    required: false,
    description: 'First name',
    example: 'John',
  })
  @IsOptional()
  @IsString()
  first_name: string;

  @ApiProperty({
    required: false,
    description: 'Last name',
    example: 'Nguyen',
  })
  @IsOptional()
  @IsString()
  last_name: string;

  @ApiProperty({
    required: false,
    description:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
    example: '123456Aa',
  })
  @IsOptional()
  @Matches(new RegExp(PasswordRegex), {
    message:
      'Password must be at least 8 characters with upper case letter, lower case letter, and number',
  })
  password: string;

  @ApiProperty({
    required: false,
    description: 'gender: male, female or other',
    example: 'male',
  })
  @IsOptional()
  @IsString()
  @IsIn(Object.values(GENDER))
  gender: string;
}

export class MerchantUpdateUserOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
