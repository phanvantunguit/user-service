import { Inject } from '@nestjs/common';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { UpdateUserProfileDto } from './validate';

export class MerchantUserUpdateServiceHandler {
  constructor(@Inject(UserRepository) private userRepo: UserRepository) {}
  async execute(params: UpdateUserProfileDto, user_id: string): Promise<any> {
    const findUser = await this.userRepo.findById(user_id);
    if (!findUser) {
      badRequestError('User', ERROR_CODE.NOT_FOUND);
    }
    // if (findUser.is_admin !== isAdmin) {
    //   throwError({
    //     status: HttpStatus.FORBIDDEN,
    //     ...ERROR_CODE.NO_PERMISSION,
    //   })
    // }
    if (params.password) {
      // const passIsCorrect = await hashUtil.comparePassword(
      //   params.old_password,
      //   findUser.password
      // )
      // if (!passIsCorrect) {
      //   throwError({
      //     status: HttpStatus.BAD_REQUEST,
      //     ...ERROR_CODE.PASSWORD_INCORRECT,
      //   })
      // }
    }
    if (findUser?.username) {
      const check = await this.checkUsername(findUser.username, findUser.id);
      if (!check) {
        badRequestError('Username was', ERROR_CODE.EXISTED);
      }
    }
    params['id'] = user_id;
    const userUpdated = await this.userRepo.saveUser(params);
    delete userUpdated.password;
    return userUpdated;
  }
  // Check name
  async checkUsername(username: string, user_id?: string): Promise<boolean> {
    const user = await this.userRepo.findOneUser({ username });
    if (!user || user.id === user_id) {
      return true;
    }
    return false;
  }
}
