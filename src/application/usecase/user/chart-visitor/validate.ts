import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsNumber, IsString, IsArray } from 'class-validator';

export class ReportChartVisitor {
  @ApiProperty({
    required: true,
    description: 'Time report visitor',
  })
  @IsString()
  time: any;

  @ApiProperty({
    required: true,
    description: 'Count visitor by 30days ago',
  })
  @IsNumber()
  visitor: number;
}

export class ReportChartVisitorOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: ReportChartVisitor,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ReportChartVisitor)
  payload: ReportChartVisitor[];
}
