import { Inject } from '@nestjs/common';
import moment from 'moment';
import { APP_CONFIG } from 'src/config';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { ReportChartVisitorOutput } from './validate';
const { google } = require('googleapis');

const scopes = 'https://www.googleapis.com/auth/analytics.readonly';
const jwt = new google.auth.JWT(
  APP_CONFIG.CLIENT_EMAIL,
  null,
  APP_CONFIG.PRIVATE_KEY.replace(/\\n/g, '\n'),
  scopes,
);

async function getChart30Days(view_id: any) {
  try {
    await jwt.authorize();
    const response = await google.analytics('v3').data.ga.get({
      auth: jwt,
      ids: 'ga:' + view_id,
      'start-date': '30daysAgo',
      'end-date': 'today',
      dimensions: 'ga:date',
      metrics: 'ga:pageviews',
    });
    return await response.data?.rows;
  } catch (err) {
    console.log(err);
  }
}

export class ReportChartVisitorHandler {
  constructor(
    @Inject(MerchantRepository)
    private merchantRepository: MerchantRepository,
  ) {}

  async execute(merchant_code: string): Promise<ReportChartVisitorOutput> {
    const merchantConfig = await this.merchantRepository.queryMerchant(
      merchant_code,
    );
    const view_id = merchantConfig[0]?.config?.view_id;
    const getViewReport = await getChart30Days(view_id);
    const retunrViewReport = getViewReport?.map((item) => {
      const [timeRaw, visitor] = item;
      const year = timeRaw.substring(0, 4);
      const month = timeRaw.substring(4, 6);
      const day = timeRaw.substring(6, 8);
      const time = year + '-' + month + '-' + day;
      const yourObject = { time, visitor };
      return yourObject;
    });
    return {
      payload: retunrViewReport,
    };
  }
}
