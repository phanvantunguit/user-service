import { Inject } from '@nestjs/common';
import { UserRepository } from 'src/infrastructure/data/database/user.repository';
import { accessDeniedError } from 'src/utils/exceptions/throw.exception';
import { GetUserOutput } from './validate';

export class GetUserHandler {
  constructor(
    @Inject(UserRepository)
    private userRepository: UserRepository,
  ) {}
  async execute(
    id: string,
    merchant_code?: string,
  ): Promise<GetUserOutput> {
    const dataRes = await this.userRepository.findById(id)
    if(merchant_code && merchant_code !== dataRes.merchant_code) {
        accessDeniedError()
    }
    return {
      payload: dataRes,
    };
  }
}
