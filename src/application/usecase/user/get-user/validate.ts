import { ApiProperty } from '@nestjs/swagger';
import {
  IsObject,
} from 'class-validator';
import { UserValidate } from '../validate';

export class GetUserOutput {
  @ApiProperty({
    required: true,
    description: 'Get user by id',
  })
  @IsObject()
  payload: UserValidate;
}
