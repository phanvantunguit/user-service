import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsString,
  IsOptional,
  IsEmail,
  IsBoolean,
  IsNumber,
  IsArray,
} from 'class-validator';
export class UserValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string;

  @ApiProperty({
    required: true,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: false,
    description: 'username',
    example: 'nguyenvana',
  })
  @IsOptional()
  @IsString()
  username: string;

  @ApiProperty({
    required: false,
    description: 'phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone: string;

  @ApiProperty({
    required: false,
    description: 'first name',
    example: 'John',
  })
  @IsOptional()
  @IsString()
  first_name: string;

  @ApiProperty({
    required: false,
    description: 'last name',
    example: 'Dang',
  })
  @IsOptional()
  @IsString()
  last_name: string;

  @ApiProperty({
    required: false,
    description: 'address',
  })
  @IsOptional()
  @IsString()
  address: string;

  @ApiProperty({
    required: false,
    description: 'affiliate code',
  })
  @IsOptional()
  @IsString()
  affiliate_code: string;

  @ApiProperty({
    required: false,
    description: 'link affiliate',
  })
  @IsOptional()
  @IsString()
  link_affiliate: string;

  @ApiProperty({
    required: false,
    description: 'affiliate code of another user',
  })
  @IsOptional()
  @IsString()
  referral_code: string;

  @ApiProperty({
    required: false,
    description: 'profile picture link',
  })
  @IsOptional()
  @IsString()
  profile_pic: string;

  @ApiProperty({
    required: false,
    description: 'active status',
  })
  @IsOptional()
  @IsBoolean()
  active: boolean;

  @ApiProperty({
    required: false,
    description: 'email confirmed status',
  })
  @IsOptional()
  @IsBoolean()
  email_confirmed: boolean;

  @ApiProperty({
    required: false,
    description: 'note updated',
  })
  @IsOptional()
  @IsString()
  note_updated: string;

  @ApiProperty({
    required: false,
    description: 'country',
  })
  @IsOptional()
  @IsString()
  country: string;

  @ApiProperty({
    required: false,
    description: 'year of birth',
  })
  @IsOptional()
  @IsString()
  year_of_birth: string;

  @ApiProperty({
    required: false,
    description: 'gender',
  })
  @IsOptional()
  @IsString()
  gender: string;

  @ApiProperty({
    required: false,
    description: 'phone code',
  })
  @IsOptional()
  @IsString()
  phone_code: string;

  @ApiProperty({
    required: true,
    description: 'merchant code',
  })
  @IsString()
  merchant_code: string;

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  created_at?: number;

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsNumber()
  updated_at?: number;
}
export class ListUserValidate extends UserValidate {
  @ApiProperty({
    required: false,
    isArray: true,
    type: String,
  })
  @IsOptional()
  @IsArray()
  @Type(() => String)
  tbots: string[];
}
