import { Inject } from '@nestjs/common';
import { AdminRepository } from 'src/infrastructure/data/database/admin.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { AdminVerifyEmailInput, AdminVerifyEmailOutput } from './validate';

export class AdminVerifyEmailHandler {
  constructor(
    @Inject(AdminRepository)
    private adminRepository: AdminRepository,
  ) {}
  async execute(param: AdminVerifyEmailInput): Promise<AdminVerifyEmailOutput> {
    const admin = await this.adminRepository.findById(param.token);
    if (!admin) {
      badRequestError('Account', ERROR_CODE.NOT_FOUND);
    }
    await this.adminRepository.save({ id: param.token, email_confirmed: true });
    return {
      payload: true,
    };
  }
}
