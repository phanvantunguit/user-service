import { IsString, IsBoolean } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class AdminVerifyEmailInput {
  @ApiProperty({
    required: true,
    description: 'Token to confirm email',
  })
  @IsString()
  token: string;
}

export class AdminVerifyEmailOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
