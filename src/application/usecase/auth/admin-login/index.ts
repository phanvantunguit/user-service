import { Inject } from '@nestjs/common';
import { APP_CONFIG } from 'src/config';
import { AdminRepository } from 'src/infrastructure/data/database/admin.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { comparePassword, generateTokenSecret } from 'src/utils/hash.util';
import { AdminLoginInput, AdminLoginOutput } from './validate';

export class AdminLoginHandler {
  constructor(
    @Inject(AdminRepository)
    private adminRepository: AdminRepository,
  ) {}
  async execute(param: AdminLoginInput): Promise<AdminLoginOutput> {
    const admin = await this.adminRepository.findOne({ email: param.email });
    if (!admin) {
      badRequestError('Email', ERROR_CODE.NOT_FOUND);
    }
    const checkPass = await comparePassword(param.password, admin.password);
    if (!checkPass) {
      badRequestError('Password', ERROR_CODE.INCORRECT);
    }
    const token = generateTokenSecret(
      {
        user_id: admin.id,
        email: admin.email,
        iss: 'cextrading',
        admin: true,
        super_admin: admin.super_admin,
      },
      APP_CONFIG.SECRET_KEY,
      Number(APP_CONFIG.TIME_EXPIRED_LOGIN),
    );
    return {
      payload: token,
    };
  }
}
