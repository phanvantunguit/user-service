import { IsString, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AdminLoginInput {
  @ApiProperty({
    required: true,
    description: 'Email to login',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: true,
    description: 'Phone number',
    example: '123456Aa',
  })
  @IsString()
  password: string;
}

export class AdminLoginOutput {
  @ApiProperty({
    required: true,
    description: 'Token',
    example: 'jwt',
  })
  @IsString()
  payload: string;
}
