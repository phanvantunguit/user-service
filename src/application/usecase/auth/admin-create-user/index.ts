import { Inject } from '@nestjs/common';
import { AdminRepository } from 'src/infrastructure/data/database/admin.repository';
import { DBContext } from 'src/infrastructure/data/database/db-context';
import { MailService } from 'src/infrastructure/services';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { generatePassword } from 'src/utils/hash.util';
import { AdminCreateUserInput, AdminCreateUserOutput } from './validate';

export class AdminCreateUserHandler {
  constructor(
    @Inject(AdminRepository)
    private adminRepository: AdminRepository,
    @Inject(DBContext) private dBContext: DBContext,
    @Inject(MailService) private mailService: MailService,
  ) {}
  async execute(
    param: AdminCreateUserInput,
    admin_id: string,
  ): Promise<AdminCreateUserOutput> {
    await this.dBContext.runInTransaction(async (queryRunner) => {
      const email = param.email.toLocaleLowerCase();
      const findAdmin = await this.adminRepository.findOne({ email });
      if (findAdmin) {
        badRequestError('Email', ERROR_CODE.EXISTED);
      }
      const password = generatePassword();
      const admin = await this.adminRepository.save(
        {
          ...param,
          email,
          password,
          created_by: admin_id || null,
          updated_by: admin_id || null,
        },
        queryRunner,
      );
      await this.mailService.sendEmailCreateAccount(email, admin.id, password);
      return admin;
    });
    return {
      payload: true,
    };
  }
}
