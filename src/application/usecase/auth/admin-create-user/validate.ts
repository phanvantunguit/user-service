import {
  IsString,
  IsOptional,
  IsIn,
  IsEmail,
  IsBoolean,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ADMIN_STATUS } from 'src/domain/admin/types';

export class AdminCreateUserInput {
  @ApiProperty({
    required: true,
    description: 'Fullname of user register',
    example: 'Nguyen Van A',
  })
  @IsString()
  fullname: string;

  @ApiProperty({
    required: true,
    description: 'Email to confirm register',
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    required: false,
    description: 'Phone number',
    example: '0987654321',
  })
  @IsOptional()
  @IsString()
  phone: string;

  @ApiProperty({
    required: false,
    description: 'Set super admin',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  super_admin: boolean;

  @ApiProperty({
    required: false,
    description: `Status of account ${Object.values(ADMIN_STATUS).join(
      ' or ',
    )}`,
    example: ADMIN_STATUS.ACTIVATE,
  })
  @IsOptional()
  @IsIn(Object.values(ADMIN_STATUS))
  status: ADMIN_STATUS;
}

export class AdminCreateUserOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}
