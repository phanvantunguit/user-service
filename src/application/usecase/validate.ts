import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsString } from 'class-validator';

export class BaseUpdateOutput {
  @ApiProperty({
    required: true,
    description: 'Status',
    example: true,
  })
  @IsBoolean()
  payload: boolean;
}

export class BaseCreateOutput {
  @ApiProperty({
    required: true,
    description: 'UUID of event',
    example: '7e865a11-d9ee-4e59-8331-1ee197c42c6e',
  })
  @IsString()
  payload: string;
}
