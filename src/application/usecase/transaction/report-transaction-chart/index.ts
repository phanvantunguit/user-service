import { Inject } from '@nestjs/common';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';

import {
  ReportTransactionChartInput,
  ReportTransactionChartOutput,
} from './validate';
export class ReportTransactionChartHandler {
  constructor(
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
  ) {}
  async execute(
    param: ReportTransactionChartInput,
    merchant_code: string,
  ): Promise<ReportTransactionChartOutput> {
    const { from, to, time_type, timezone } = param;
    const timezoneString =
      Number(timezone) > 0
        ? `UTC-${Number(timezone)}`
        : `UTC+${Number(timezone) * -1}`;
    const reports = await this.transactionRepository.chart({
      from,
      to,
      merchant_code,
      time_type,
      timezone: timezoneString,
    });
    return {
      payload: reports,
    };
  }
}
