import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsOptional,
  IsNumberString,
  IsNumber,
  IsObject,
  IsString,
  IsIn,
  IsArray,
} from 'class-validator';

export class ReportTransactionChartInput {
  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;

  @ApiProperty({
    required: true,
    description: 'timezone: +7, -7',
    example: '+7',
  })
  @IsString()
  timezone: string;

  @ApiProperty({
    required: true,
    description: 'time_type: DAY or MONTH',
    example: 'DAY',
  })
  @IsString()
  @IsIn(['DAY', 'MONTH'])
  time_type: 'DAY' | 'MONTH';
}

export class ReportTransactionChartValidate {
  @ApiProperty({
    required: true,
    description: 'Time group',
  })
  @IsString()
  time: string;

  @ApiProperty({
    required: true,
    description: 'Count transaction complete',
  })
  @IsNumber()
  count_complete: number;

  @ApiProperty({
    required: true,
    description: 'Amount transaction complete',
  })
  @IsNumber()
  amount_complete: number;

  @ApiProperty({
    required: false,
    description: 'Total commission cash',
  })
  @IsNumber()
  commission_cash: number;
}

export class ReportTransactionChartOutput {
  @ApiProperty({
    required: false,
    isArray: true,
    type: ReportTransactionChartValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ReportTransactionChartValidate)
  payload: ReportTransactionChartValidate[];
}
