import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsOptional,
  IsNumberString,
  IsString,
  IsNumber,
  IsArray,
  IsObject,
} from 'class-validator';
import { TRANSACTION_STATUS } from 'src/domain/transaction/types';
import { ListTransactionValidate, TransactionValidate } from '../validate';

export class ListTransactionInput {
  @ApiProperty({
    required: false,
    description: 'Page',
    example: 1,
  })
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty({
    required: false,
    description: 'Size',
    example: 50,
  })
  @IsOptional()
  @IsNumberString()
  size: number;

  @ApiProperty({
    required: false,
    description: 'The keyword to find user ex: phone number, name, email',
    example: 'john',
  })
  @IsOptional()
  @IsString()
  keyword: string;

  @ApiProperty({
    required: false,
    description: 'Category',
    example: 'BOT',
  })
  @IsOptional()
  @IsString()
  category?: string;

  @ApiProperty({
    required: false,
    description: 'Item name',
    example: 'Bot abc',
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    required: false,
    description: 'Status of transaction',
    example: 'COMPLETE',
  })
  @IsOptional()
  @IsString()
  status: TRANSACTION_STATUS;

  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;
}

export class PagingTransactionOutput {
  @ApiProperty({
    required: true,
    description: 'Page',
    example: 1,
  })
  @IsNumber()
  page: number;

  @ApiProperty({
    required: true,
    description: 'Size',
    example: 50,
  })
  @IsNumber()
  size: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  count: number;

  @ApiProperty({
    required: true,
    description: 'count item of page',
    example: 30,
  })
  @IsNumber()
  total: number;

  @ApiProperty({
    required: false,
    isArray: true,
    type: ListTransactionValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ListTransactionValidate)
  rows: ListTransactionValidate[];
}

export class ListTransactionOutput {
  @ApiProperty({
    required: true,
    description: 'Result transaction paging',
  })
  @IsObject()
  payload: PagingTransactionOutput;
}
