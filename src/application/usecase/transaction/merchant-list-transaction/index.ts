import { Inject } from '@nestjs/common';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';

import { ListTransactionInput, ListTransactionOutput } from './validate';
export class MerchantListTransactionHandler {
  constructor(
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
  ) {}
  async execute(
    param: ListTransactionInput,
    merchant_code: string,
  ): Promise<ListTransactionOutput> {
    const dataRes = await this.transactionRepository.getPaging({
      ...param,
      merchant_code,
    });
    return {
      payload: dataRes,
    };
  }
}
