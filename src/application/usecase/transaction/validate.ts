import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsString,
  IsIn,
  IsOptional,
  IsObject,
  IsNumber,
  IsArray,
} from 'class-validator';
import {
  PAYMENT_METHOD,
  TRANSACTION_STATUS,
} from 'src/domain/transaction/types';

export class TransactionValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string;
  @ApiProperty({
    required: false,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e1',
  })
  @IsOptional()
  @IsString()
  user_id?: string;

  @ApiProperty({
    required: false,
    example: 'Nguyen Van A',
  })
  @IsOptional()
  @IsString()
  fullname?: string;

  @ApiProperty({
    required: false,
    example: 'nguyenvana@gmail.com',
  })
  @IsOptional()
  @IsString()
  email?: string;

  @ApiProperty({
    required: true,
    example: 'BOT1802220001',
  })
  @IsString()
  order_id?: string;

  @ApiProperty({
    required: true,
    example: '2',
  })
  @IsString()
  amount?: string;

  @ApiProperty({
    required: true,
    example: 0,
    description: 'total commission cash',
  })
  @IsNumber()
  commission_cash?: number;

  @ApiProperty({
    required: true,
    description: 'Currency for user payment',
    example: 'LTCT',
  })
  @IsString()
  currency?: string;

  @ApiProperty({
    required: true,
    description: 'Payment method',
    example: PAYMENT_METHOD.COIN_PAYMENT,
  })
  @IsString()
  @IsIn(Object.values(PAYMENT_METHOD))
  payment_method?: PAYMENT_METHOD;

  @ApiProperty({
    required: true,
    description: Object.values(TRANSACTION_STATUS).join(','),
    example: TRANSACTION_STATUS.CREATED,
  })
  @IsString()
  status?: TRANSACTION_STATUS;

  @ApiProperty({
    required: true,
    description: 'Payment id of coinpayment',
    example: 'OAINONOFASF1203412',
  })
  @IsString()
  payment_id?: string;

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number;

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  updated_at?: number;
}
export class ItemValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id: string;
  @ApiProperty({
    required: true,
    example: 'Bot signal',
  })
  @IsString()
  name: string;

  @ApiProperty({
    required: true,
    example: 'MONTH',
  })
  @IsString()
  type: string;

  @ApiProperty({
    required: true,
    example: '0.001',
  })
  @IsString()
  price: string;

  @ApiProperty({
    required: true,
    example: 'BOT',
  })
  @IsString()
  category: string;

  @ApiProperty({
    required: true,
    example: 1,
  })
  @IsNumber()
  quantity: number;
  @ApiProperty({
    required: true,
    example: 0,
  })
  @IsNumber()
  discount_rate: number;
  @ApiProperty({
    required: true,
    example: 0,
  })
  @IsNumber()
  discount_amount: number;

  @ApiProperty({
    required: true,
    example: 0,
    description: 'commission rate',
  })
  @IsNumber()
  commission_rate: number;

  @ApiProperty({
    required: true,
    example: 0,
    description: 'commission cash',
  })
  @IsNumber()
  commission_cash: number;
}
export class ListTransactionValidate extends TransactionValidate {
  @ApiProperty({
    required: false,
    isArray: true,
    type: ItemValidate,
  })
  @IsOptional()
  @IsArray()
  @Type(() => ItemValidate)
  items?: ItemValidate[];
}

export class TransactionMetadataValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string;

  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  transaction_id?: string;

  @ApiProperty({
    required: true,
    example: 'wallet_address',
  })
  @IsString()
  attribute?: string;

  @ApiProperty({
    required: true,
    example: 'moBpkWoiMmJJKhBjDXeXPWPaTJFRCdmM1g',
  })
  @IsString()
  value?: string;

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number;
}

export class TransactionLogValidate {
  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  id?: string;

  @ApiProperty({
    required: true,
    example: '1c9e1891-b5b0-4949-bcfe-a2e25c1e53e2',
  })
  @IsString()
  transaction_id?: string;

  @ApiProperty({
    required: true,
    example: 'PAYMENT_COMPLETE',
  })
  @IsString()
  transaction_event?: string;

  @ApiProperty({
    required: true,
    example: 'PROCESSING',
  })
  @IsString()
  transaction_status?: string;

  @ApiProperty({
    required: true,
    example: {},
  })
  @IsObject()
  metadata?: any;

  @ApiProperty({
    required: true,
    example: Date.now(),
  })
  @IsString()
  created_at?: number;
}

export class TransactionDetailValidate {
  transaction: TransactionValidate;
  metadatas: TransactionMetadataValidate[];
  logs: TransactionLogValidate[];
}
