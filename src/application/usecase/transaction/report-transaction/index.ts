import { Inject } from '@nestjs/common';
import { MERCHANT_INVOICE_STATUS } from 'src/domain/merchant/types';
import { MerchantInvoiceRepository } from 'src/infrastructure/data/database/merchant-invoice.repository';
import { MerchantRepository } from 'src/infrastructure/data/database/merchant.repository';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';

import { ReportTransactionInput, ReportTransactionOutput } from './validate';
export class ReportTransactionHandler {
  constructor(
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
    @Inject(MerchantInvoiceRepository)
    private merchantInvoiceRepository: MerchantInvoiceRepository,
    @Inject(MerchantRepository) private merchantRepository: MerchantRepository,
  ) {}
  async execute(
    param: ReportTransactionInput,
    merchant_id: string,
    merchant_code?: string,
  ): Promise<ReportTransactionOutput> {
    const { from, to } = param;
    if (!merchant_code) {
      const merchant = await this.merchantRepository.findById(merchant_id);
      if (!merchant) {
        badRequestError('Merchant', ERROR_CODE.NOT_FOUND);
      }
      merchant_code = merchant.code;
    }
    const [amountInvoiceComplete, amountInvoicePending] = await Promise.all([
      this.merchantInvoiceRepository.totalCommission({
        merchant_id,
        status: [MERCHANT_INVOICE_STATUS.COMPLETED],
      }),
      this.merchantInvoiceRepository.totalCommission({
        merchant_id,
        status: [
          MERCHANT_INVOICE_STATUS.WAITING_PAYMENT,
          MERCHANT_INVOICE_STATUS.WALLET_INVALID,
        ],
      }),
    ]);
    const reports = await this.transactionRepository.report({
      from,
      to,
      merchant_code,
    });
    const dataRes = reports[0];
    return {
      payload: {
        count_pending:
          Number(dataRes.count_total) -
          Number(dataRes.count_complete) -
          Number(dataRes.count_failed),
        count_complete: Number(dataRes.count_complete),
        count_failed: Number(dataRes.count_failed),
        amount_pending:
          Number(dataRes.amount_total) -
          Number(dataRes.amount_complete) -
          Number(dataRes.amount_failed),
        amount_complete: Number(dataRes.amount_complete),
        amount_failed: Number(dataRes.amount_failed),
        commission_cash: Number(dataRes.commission_cash),
        payout_complete: Number(amountInvoiceComplete[0].total),
        payout_pending: Number(amountInvoicePending[0].total),
      },
    };
  }
}
