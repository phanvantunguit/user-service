import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional,
  IsNumberString,
  IsNumber,
  IsObject,
} from 'class-validator';

export class ReportTransactionInput {
  @ApiProperty({
    required: false,
    description: 'From',
  })
  @IsOptional()
  @IsNumberString()
  from: number;

  @ApiProperty({
    required: false,
    description: 'To',
  })
  @IsOptional()
  @IsNumberString()
  to: number;
}

export class ReportTransactionValidate {
  @ApiProperty({
    required: false,
    description: 'Count transaction pending',
  })
  @IsNumber()
  count_pending: number;

  @ApiProperty({
    required: false,
    description: 'Count transaction complete',
  })
  @IsNumber()
  count_complete: number;

  @ApiProperty({
    required: false,
    description: 'Count transaction failed',
  })
  @IsNumber()
  count_failed: number;

  @ApiProperty({
    required: false,
    description: 'Amount transaction pending',
  })
  @IsNumber()
  amount_pending: number;

  @ApiProperty({
    required: false,
    description: 'Amount transaction complete',
  })
  @IsNumber()
  amount_complete: number;

  @ApiProperty({
    required: false,
    description: 'Amount transaction failed',
  })
  @IsNumber()
  amount_failed: number;

  @ApiProperty({
    required: false,
    description: 'Total commission cash',
  })
  @IsNumber()
  commission_cash: number;

  @ApiProperty({
    required: false,
    description: 'Payout complete',
  })
  @IsNumber()
  payout_complete: number;

  @ApiProperty({
    required: false,
    description: 'Payout pending',
  })
  @IsNumber()
  payout_pending: number;
}

export class ReportTransactionOutput {
  @ApiProperty({
    required: true,
    description: 'Report user',
  })
  @IsObject()
  payload: ReportTransactionValidate;
}
