import { Inject } from '@nestjs/common';
import { TransactionRepository } from 'src/infrastructure/data/database/transaction.repository';
import { ERROR_CODE } from 'src/utils/exceptions/const';
import { badRequestError } from 'src/utils/exceptions/throw.exception';
import { GetTransactionOutput } from './validate';

export class MerchantGetTransactionHandler {
  constructor(
    @Inject(TransactionRepository)
    private transactionRepository: TransactionRepository,
  ) {}
  async execute(params: {
    id: string;
    merchant_code: string;
  }): Promise<GetTransactionOutput> {
    const dataRes = await this.transactionRepository.getDetail(params);
    if (!dataRes.transaction) {
      badRequestError('Transaction', ERROR_CODE.NOT_FOUND);
    }
    return {
      payload: dataRes,
    };
  }
}
