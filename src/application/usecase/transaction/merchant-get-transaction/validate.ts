import { ApiProperty } from '@nestjs/swagger';
import { IsObject } from 'class-validator';
import { TransactionDetailValidate } from '../validate';

export class GetTransactionOutput {
  @ApiProperty({
    required: false,
    type: TransactionDetailValidate,
  })
  @IsObject()
  payload: TransactionDetailValidate;
}
