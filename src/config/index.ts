import * as dotenv from 'dotenv';
import { ENVIRONMENT, Iconfig } from './types';
dotenv.config();

const envConfig: Iconfig = process.env as any;
let DASHBOARD_ADMIN_BASE_URL = 'https://admin.coinmap.tech';
if (envConfig.ENVIRONMENT.toUpperCase() === ENVIRONMENT.DEVELOPMENT) {
  DASHBOARD_ADMIN_BASE_URL = 'https://admin-dev.coinmap.tech';
}
let gwEnv = '';
switch (process.env.ENVIRONMENT) {
  case ENVIRONMENT.DEVELOPMENT:
    gwEnv = '-dev';
    break;
  case ENVIRONMENT.STAGING:
    gwEnv = '-staging';
    break;
}
export const APP_CONFIG = {
  SECRET_KEY: 'KeyY29pbm1hcA==',
  TIME_EXPIRED_LOGIN: 86400,
  DASHBOARD_ADMIN_BASE_URL,
  INVITE_EVENT_SIMPLE_TEMPLATE_ID: 'd-ce3bfb8eb8cf477183de4325fc3ceaf4',
  INVITE_EVENT_QRCODE_TEMPLATE_ID: 'd-cf49af60f69442eaaf525a8684867020',
  REMIND_EVENT_TEMPLATE_ID: 'd-d71c5c72467c4e58865f26e9c04fd356',
  PAYMENT_EVENT_TEMPLATE_ID: 'd-c402ab2acca34be2a9316f0d85eddc98',
  REMIND_PAYMENT_TEMPLATE_ID: 'd-285c33bf3ebf4fcfb30554e4b5d5ee61',
  PROMOTION_TEMPLATE_ID: 'd-18cc885db2cf4f4a908ec39d848272e7',
  TIME_EXPIRED_VERIFY_EMAIL: 86400,
  USER_ROLE_BASE_URL: `https://gw${gwEnv}.cextrading.io/cm-user-roles`,
  APIKEY_BSC20: '387M4HK4S26S7T9XR4QBWRE5BYAYH1J1EW',

  // PAYMENT_POSTGRES_HOST: 'localhost',
  // PAYMENT_POSTGRES_PORT: 55000,
  // PAYMENT_POSTGRES_USER: 'postgres',
  // PAYMENT_POSTGRES_PASS: 'postgrespw',
  // PAYMENT_POSTGRES_DB: 'simple',

  // UR_POSTGRES_HOST: 'localhost',
  // UR_POSTGRES_PORT: 55000,
  // UR_POSTGRES_USER: 'postgres',
  // UR_POSTGRES_PASS: 'postgrespw',
  // UR_POSTGRES_DB: 'postgres',

  PAYMENT_POSTGRES_HOST: 'postgresql-ha-pgpool.db',
  PAYMENT_POSTGRES_PORT: 5432,
  PAYMENT_POSTGRES_USER: 'cm_postgresql_dev',
  PAYMENT_POSTGRES_PASS: 'D3GaosGQPoZp1lk1EaS0OwQTj',
  PAYMENT_POSTGRES_DB: 'cm_payment_service',

  UR_POSTGRES_HOST: 'postgresql-ha-pgpool.db',
  UR_POSTGRES_PORT: 5432,
  UR_POSTGRES_USER: 'cm_postgresql_dev',
  UR_POSTGRES_PASS: 'D3GaosGQPoZp1lk1EaS0OwQTj',
  UR_POSTGRES_DB: 'cm_user_roles',

  CLIENT_EMAIL:
    'google-analytics@spatial-genius-378516.iam.gserviceaccount.com',
  PRIVATE_KEY:
    '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCrz9xIS1cxA2ns\n7Bco/lqQrao+pN08Uz41rrX0H9oPEo6azsEmSGNF3zJmcrHE+xE+k72QuxRRioE8\n72Y2f7JG6a8eU389IU2sLTMQLrFDMp2RiVuC94WhwShq/fdiTcGVOYf34UK9a6Nn\nDgEf9Qj9Yhp0R7fTiNWpOf7wQ9Ry0whi/svZSN/seVTmE7xiqlQlAM8d3RLk64Y6\nQYJCRWc/5LCG3uI337diWLt8mW3ajooFXhFFFMdpxE9aDsYxYwJgmc754T6eOex1\nCFFbhoy2vfYcpTtZgCWTY4QNC5SUV22ox7i7CLRXCE1xlJ1tg6ZZ/ql0sF0nD4B3\nrM1buNbhAgMBAAECggEADd90CjtZIDpGFo0Opq75CZcYOMQvn/QZwRRpGTvsK8zk\nCmB47JoqN+VYiLionX7nSs37n8DXWGkuW8j50Bs5/itWykRUMQ+sGaOxrJQUmJgw\n7GQyqS3K0r+Jv6/fhobAQ4gYcD22WZykHkldcmLi9bCy8aIr1+8Dz40BBo33jMjX\nElO47W3LcBrDLVZavNsRswocexc9y6nUm0d7QvvYRRDr5h50mfa4L3m7TKs4iLdR\nr9kqQCDLiYC9l8FIlYLTewHilh2q6VtjyUf6/IR6bK9fnK3wKU/c5P7nMP0DuUfp\nIIWIZE2fJ8jGf5UYH10ffO86m1azZaKtZysnVO00UQKBgQDXaYXX+L99Y5vZfBwo\nUmU5ZT2VhdOGJ9sPDKYAmlaOoYvW3KW9/zPVgBlK8s8kDUDq8eV8BTRYT2/AFlNR\nax9gBIOOPiFL28WBA5h5B3l/FPfpdJdlDGDn2nFBvB/hfcC7M7TwjYS8ghWz2/HD\nxHbBsZNk4eLhEA+UMf6xRwS2uQKBgQDML0QKNTv/+6qpCngQhyRc0KGW+e4NhfHL\nHo7RTHJStvxqTHaR4mALPKHsGTD4nbHvqhON93dHNqp5D0XueivVQrUeeF9NKcxN\nUe4xfNKUgwDfswubfJdKjJs4LJqJXFeRvtpaqOafj2J1YY25QwLQT//Lwp7pB/Z/\n5xNmQFaNaQKBgHeOk8C4yN85J8jfv2kJwjDG5hVU/3+YI1cI+CGhh6UGOGz6besz\n93rp7B/S//DMgllkBLB+2vygqi98tKWdxP4vwyxxg11tDwN6EWgrI93Kr4YdNeHZ\nqvdEIFQRq8glWZENM8HHagKOWKlIy7iSFC+Q9XOqg5fSqDLiRK/z8MPRAoGACE5b\n7DZiLHWmaZ39aF63AfeSuzuLjY1HWsXxdV4wwGClQAXARQr6cTqUM7CKp3JyzJcU\nmI/akFEO+kG+zIa3xcMi7wxihBcRMowEroqvXk8999umuzqERQoXPl/CIZhaD5m9\n9DYuu4rvrGq9gy3QrXyzoSxXno8uXeGejmhLnRECgYEAwdPgWHI+vbKsm7zk2CTO\n/GsuKb+GeMKlVrD88SukZuHNR28AOi6wrytisWN40CBNPyse2StaICrzh12i2NCQ\nF3yMs7edKENayoykFCR8jF3HkXTpSInVVkrs/KdwHTYasq2fTyqGYH6JDBVebii7\nQuD5JQUYvGlmJjz7HdBu2w0=\n-----END PRIVATE KEY-----\n',

  MIN_INVOICE_AMOUNT: 2,

  SENDGRID_SENDER_ALGO_EMAIL: 'hello@algotradingmarket.com',
  SENDGRID_SENDER_ALGO_NAME: 'ATM',
  ALGO_LOGO_EMAIL:
    'https://static-dev.cextrading.io/images/cm-user-roles/1683703900434.png',
  ALGO_BANNER_EMAIL:
    'https://static-dev.cextrading.io/images/cm-user-roles/1683703868125.jpg',
  ...envConfig,
};
