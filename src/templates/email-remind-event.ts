export const emailRemindEvent = `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>
  </title>
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if mso]>
    <noscript>
    <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    </noscript>
    <![endif]-->
  <!--[if lte mso 11]>
    <style type="text/css">
      .mj-outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
  <!--[if !mso]><!-->
  <link href="https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,500;0,700;1,400&display=swap" rel="stylesheet" type="text/css">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,500;0,700;1,400&display=swap);

  </style>
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }

      .mj-column-per-40 {
        width: 40% !important;
        max-width: 40%;
      }

      .mj-column-per-60 {
        width: 60% !important;
        max-width: 60%;
      }

      .mj-column-per-30 {
        width: 30% !important;
        max-width: 30%;
      }

      .mj-column-per-70 {
        width: 70% !important;
        max-width: 70%;
      }
    }

  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }

    .moz-text-html .mj-column-per-40 {
      width: 40% !important;
      max-width: 40%;
    }

    .moz-text-html .mj-column-per-60 {
      width: 60% !important;
      max-width: 60%;
    }

    .moz-text-html .mj-column-per-30 {
      width: 30% !important;
      max-width: 30%;
    }

    .moz-text-html .mj-column-per-70 {
      width: 70% !important;
      max-width: 70%;
    }

  </style>
  <style type="text/css">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }

  </style>
  <style type="text/css">
  </style>
</head>

<body style="word-spacing:normal;background-color:#E5E5E5;">
  <div style="background-color:#E5E5E5;">
    <!-- header.mjml -->
    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#1D2842" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#1D2842;background-color:#1D2842;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#1D2842;background-color:#1D2842;width:100%;">
        <tbody>
          <tr>
            <td style="width:600px;">
              <img height="auto" src="https://static-dev.cextrading.io/1665384138043.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="600" />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#FFFFFF" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#FFFFFF;background-color:#FFFFFF;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#FFFFFF;background-color:#FFFFFF;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:0 40px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><![endif]-->
              <!-- content.mjml -->
              <!--[if mso | IE]><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div style="margin:0px auto;max-width:520px;margin-top: 30px">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                  <tbody>
                    <tr>
                      <td align="left" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;;">
                        <div style="font-family:Archivo;font-size:18px;font-weight:700;line-height:1;text-align:left;color:#000000;">Thư nhắc tham gia sự kiện vào ngày mai</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;">
                        <div style="font-family:Archivo;font-size:13px;font-weight:400;line-height:16px;text-align:left;color:#000000;">Chỉ còn vài tiếng nữa thôi là sự kiện THE NEW TASTE OF TRADING sẽ chính thức được diễn ra tại Gem Center! Để quý khách mời có thể đến sảnh sự kiện một cách nhanh chóng và thuận lợi nhất, chúng tôi sẽ có một số hướng dẫn như sau:</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="font-size:0px;padding:0 0 20px 20px;padding-bottom:20px;word-break:break-word;">
                        <div style="font-family:Archivo;font-size:13px;font-weight:400;line-height:16px;text-align:left;color:#000000;">- Bạn có thể gửi xe ngay tại GEM Center hoặc bất kỳ hầm xe nào ở gần đó. Nhưng bạn hãy cố gắng tìm chỗ gần nơi tổ chức nhất để tiện đi lại nhé!</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="font-size:0px;padding:0 0 20px 20px;padding-bottom:20px;word-break:break-word;">
                        <div style="font-family:Archivo;font-size:13px;font-weight:400;line-height:16px;text-align:left;color:#000000;">- Bạn chỉ cần đi vào phía trong sảnh chính (Main Lobby) sẽ có 2 khu thang máy nằm ở 2 bên hông sảnh chính, bạn bấm thang máy lên tầng 5 - CASTOR sẽ đến nơi diễn ra sự kiện.</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                        <div style="font-family:Archivo;font-size:13px;font-weight:400;line-height:16px;text-align:left;color:#000000;">Và cũng đừng quên chuẩn bị sẵn trong tay điện thoại có kèm QR CODE mà chúng tôi đã gửi cho bạn để vào tham dự chương trình nhé!</div>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" style="font-size:0px;padding:0;word-break:break-word;padding-top:20px;padding-bottom:20px;">
                        <div style="font-family:Archivo;font-size:16px;font-weight:700;line-height:1;text-align:left;color:#000000;">Thông tin chương trình</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" bgcolor="#2E3A52" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                <div style="background:#2E3A52;background-color:#2E3A52;margin:0px auto;border-radius:4px 4px 0px 0px;max-width:520px;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#2E3A52;background-color:#2E3A52;width:100%;border-radius:4px 4px 0px 0px;">
                    <tbody>
                      <tr>
                        <td style="border-bottom:2px solid #FFFFFF;direction:ltr;font-size:0px;padding:0;text-align:center;">
                          <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:208px;" ><![endif]-->
                          <div class="mj-column-per-40 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                              <tbody>
                                <tr>
                                  <td align="right" style="font-size:0px;padding:12px 20px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:13px;font-weight:700;line-height:1;text-align:right;color:#FFFFFF;">Thời gian</div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:312px;" ><![endif]-->
                          <div class="mj-column-per-60 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color:#F9F9F9;border:0px;vertical-align:top;" width="100%">
                              <tbody>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:12px 0px 12px 20px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:13px;line-height:1;text-align:left;color:#000000;">13h30 - Ngày 15, Tháng 10, Năm 2022</div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" bgcolor="#0C122A" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                <div style="background:#0C122A;background-color:#0C122A;margin:0px auto;border-radius:0px 0px 4px 4px;max-width:520px;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#0C122A;background-color:#0C122A;width:100%;border-radius:0px 0px 4px 4px;">
                    <tbody>
                      <tr>
                        <td style="border:0;direction:ltr;font-size:0px;padding:0;text-align:center;">
                          <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:208px;" ><![endif]-->
                          <div class="mj-column-per-40 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                              <tbody>
                                <tr>
                                  <td align="right" style="font-size:0px;padding:12px 20px;word-break:break-word;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="100%" style="vertical-align:top;height:100%;"><![endif]-->
                                    <div style="font-family:Archivo;font-size:13px;font-weight:700;line-height:1;text-align:right;color:#FFFFFF;height:100%;">Địa điểm</div>
                                    <!--[if mso | IE]></td></tr></table><![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:312px;" ><![endif]-->
                          <div class="mj-column-per-60 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color:#F9F9F9;vertical-align:top;" width="100%">
                              <tbody>
                                <tr>
                                  <td align="left" style="font-size:0px;padding:12px 0px 12px 20px;word-break:break-word;">
                                    <div style="font-family:Archivo;font-size:13px;line-height:120%;text-align:left;color:#000000;">Tầng 5 - CASTOR, GEM Center, số 8 Nguyễn Bỉnh Khiêm, Đa Kao, Quận 1, TP.HCM</div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div style="margin:0px auto;max-width:520px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                  <tbody>
                    <tr>
                      <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                        <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:520px;" ><![endif]-->
                        <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                            <tbody>
                              <tr>
                                <td align="right" style="font-size:0px;padding:0;padding-bottom:12px;word-break:break-word;">
                                  <div style="font-family:Archivo;font-size:13px;font-style:italic;line-height:1;text-align:right;color:#000000;">Hẹn gặp lại bạn tại <span style="font-weight: 700;">THE NEW TASTE OF TRADING!</span></div>
                                </td>
                              </tr>
                              <tr>
                                <td align="right" style="font-size:0px;padding:0;word-break:break-word;">
                                  <div style="font-family:Archivo;font-size:13px;font-style:italic;line-height:1;text-align:right;color:#000000;">Regards,<br /> Coinmap Team.</div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <!--[if mso | IE]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <!--[if mso | IE]></td></tr></table></td></tr><![endif]-->
              <!-- footer.mjml -->
              <!--[if mso | IE]><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:520px;" width="520" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
              <div style="margin:0px auto;max-width:520px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                  <tbody>
                    <tr>
                      <td style="border-top:1px solid #DDDDDD;direction:ltr;font-size:0px;padding:24px 0;text-align:left;">
                        <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:520px;" ><![endif]-->
                        <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                          <!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" role="presentation" ><tr><td style="vertical-align:top;width:156px;" ><![endif]-->
                          <div class="mj-column-per-30 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:30%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="border-right:1px solid #DDDDDD;vertical-align:top;padding:0;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                              <tbody>
                                                <tr>
                                                  <td style="width:64px;">
                                                    <img height="auto" src="https://static.cextrading.io/images/1/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="64" />
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td><td style="vertical-align:top;width:364px;" ><![endif]-->
                          <div class="mj-column-per-70 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:70%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                              <tbody>
                                <tr>
                                  <td style="vertical-align:top;padding-left:16px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                      <tbody>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0;padding-bottom:5px;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:11px;line-height:11px;text-align:left;color:#979797;">Coinmap is a "Trading Ecosystem", founded by a group of Vietnamese passionate traders. Our mission is to shorten the space between individual traders and professional trading organizations with our top-of-the-book analysis instruments.</div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                                            <div style="font-family:Archivo;font-size:13px;line-height:1;text-align:left;color:#000000;"><a style="text-decoration: none; margin-right:8px;" href="https://www.facebook.com/CoinmapTrading" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/facebook.png"></img>
                                              </a>
                                              <a href="https://twitter.com/CoinmapTrading" style="text-decoration: none; margin-right:8px;" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/twitter.png"></img>
                                              </a>
                                              <a href="https://t.me/trading8x" style="text-decoration: none; margin-right:8px;" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/telegram.png"></img>
                                              </a>
                                              <a href="https://www.youtube.com/c/8xTRADING" style="text-decoration: none; margin-right:8px;" target="_blank">
                                                <img padding="0" src="https://static.cextrading.io/images/1/youtube.png"></img>
                                              </a>
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </div>
                        <!--[if mso | IE]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:0;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  <tbody>
                    <tr>
                      <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
                        <div style="font-family:Archivo;font-size:11px;line-height:1;text-align:center;color:#979797;">Please do not reply to this email.<br /> This is an automatic email sent because you submitted a form that uses Coinmap Platform.</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><![endif]-->
  </div>
</body>

</html>
`;
