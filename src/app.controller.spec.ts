import { Test, TestingModule } from '@nestjs/testing';
import { UserV1EventController } from './adapter/controllers';
import { UserRegisterEventHandler } from './application';

describe('UserV1EventController', () => {
  let userV1EventController: UserV1EventController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [UserV1EventController],
      providers: [UserRegisterEventHandler],
    }).compile();

    userV1EventController = app.get<UserV1EventController>(
      UserV1EventController,
    );
  });

  describe('root', () => {
    it('should return "Guest0001"', () => {
      const user = {
        fullname: 'Nguyen Van A',
        email: 'trongnguyen@coinmap.tech',
        phone: '0347941348',
      };
      expect(userV1EventController.register(user)).toBe('Guest0001');
    });
  });
});
