export const WITHOUT_AUTHORIZATION = 'WITHOUT_AUTHORIZATION';

export const PasswordRegex = `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*(),.~/?=|;:'"{}<>]{8,}$`;
