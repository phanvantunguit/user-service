import { TRANSACTION_EVENT, TRANSACTION_STATUS } from './types';

export class TransactionLogDomain {
  constructor(param: {
    transaction_id: string;
    transaction_event: TRANSACTION_EVENT;
    transaction_status: TRANSACTION_STATUS;
    metadata: any;
  }) {
    this.transaction_id = param.transaction_id;
    this.transaction_event = param.transaction_event;
    this.transaction_status = param.transaction_status;
    this.metadata = param.metadata;
  }
  id?: string;
  transaction_id?: string;
  transaction_event?: TRANSACTION_EVENT;
  transaction_status?: TRANSACTION_STATUS;
  metadata?: any;
  created_at?: number;
}
