import { BaseDomain } from '../base/base.domain';
import { PAYMENT_METHOD, TRANSACTION_STATUS } from './types';

export class TransactionDomain extends BaseDomain {
  constructor(param: {
    id?: string;
    order_id: string;
    order_type?: string;
    amount: string;
    currency: string;
    payment_method: PAYMENT_METHOD;
    integrate_service: string;
    status: TRANSACTION_STATUS;
    ipn_url?: string;
    user_id?: string;
    email?: string;
    fullname?: string;
    merchant_code?: string;
  }) {
    super();
    this.id = param.id;
    this.order_id = param.order_id;
    this.order_type = param.order_type;
    this.amount = param.amount;
    this.currency = param.currency;
    this.payment_method = param.payment_method;
    this.integrate_service = param.integrate_service;
    this.status = param.status;
    this.ipn_url = param.ipn_url;
    this.user_id = param.user_id;
    this.email = param.email;
    this.fullname = param.fullname;
    this.merchant_code = param.merchant_code;
  }
  id: string;
  order_id: string;
  order_type?: string;
  amount: string;
  currency: string;
  payment_method: PAYMENT_METHOD;
  integrate_service: string;
  ipn_url?: string;
  payment_id?: string;
  status: TRANSACTION_STATUS;
  user_id?: string;
  email?: string;
  fullname?: string;
  merchant_code?: string;
}
