import { TRANSACTION_METADATA } from './types';

export class TransactionMetadataDomain {
  constructor(param: {
    transaction_id: string;
    attribute: TRANSACTION_METADATA;
    value: string;
  }) {
    this.transaction_id = param.transaction_id;
    this.attribute = param.attribute;
    this.value = param.value;
  }
  id?: string;
  transaction_id?: string;
  attribute: TRANSACTION_METADATA;
  value?: string;
  created_at?: number;
}
