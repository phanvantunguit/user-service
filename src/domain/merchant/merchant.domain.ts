import { BaseDomain } from '../base/base.domain';
import { MERCHANT_STATUS } from './types';

export class MerchantDomain extends BaseDomain {
  constructor(param: {
    id?: string;
    name: string;
    code: string;
    status: MERCHANT_STATUS;
    description?: string;
    email: string;
    password?: string;
    domain?: string;
    created_by: string;
    updated_by: string;
    created_at?: number;
    updated_at?: number;
    config?: any;
  }) {
    super();
    this.id = param.id;
    this.name = param.name;
    this.code = param.code;
    this.status = param.status;
    this.description = param.description;
    this.email = param.email;
    this.password = param.password;
    this.created_by = param.created_by;
    this.updated_by = param.updated_by;
    this.created_at = param.created_at;
    this.created_at = param.created_at;
    this.config = param.config;
    this.domain = param.domain;
  }
  id: string;
  name: string;
  code: string;
  status: MERCHANT_STATUS;
  description: string;
  email: string;
  password: string;
  config: any;
  domain: string;
  created_by: string;
  updated_by: string;
  created_at: number;
  updated_at: number;
}
