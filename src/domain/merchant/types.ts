export enum MERCHANT_STATUS {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}
export enum MERCHANT_TYPE {
  COINMAP = 'COINMAP',
  OTHERS = 'OTHERS',
}
export const MERCHANT_CONFIG_ONLY_ADMIN = ['permission', 'commission'];
export const MERCHANT_CONFIG_ONLY_SYSTEM = ['verified_sender', 'email_sender'];

export enum MERCHANT_INVOICE_STATUS {
  COMPLETED = 'COMPLETED',
  WAITING_PAYMENT = 'WAITING_PAYMENT',
  WALLET_INVALID = 'WALLET_INVALID',
}

export enum MERCHANT_WALLET_TYPE {
  TRC20 = 'TRC20',
  BSC20 = 'BSC20',
}
