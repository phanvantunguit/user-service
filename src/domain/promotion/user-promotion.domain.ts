export class UserPromotionDomain {
  constructor(param: { template_id: string; email: string; send: boolean }) {
    this.template_id = param.template_id;
    this.email = param.email;
    this.send = param.send;
  }

  id?: string;
  template_id: string;
  email: string;
  send: boolean;
  created_at: number;
  updated_at: number;
}
