export const ERROR_CODE = {
  SUCCESS: {
    error_code: 'SUCCESS',
    message: 'Success',
  },
  UNKNOWN: {
    error_code: 'UNKNOWN',
    message: 'unknown',
  },
  BAD_REQUEST: {
    error_code: 'BAD_REQUEST',
    message: 'Bad Request',
  },
  NOT_FOUND: {
    error_code: 'NOT_FOUND',
    message: 'Not Found',
  },
  EXISTED: {
    error_code: 'EXISTED',
    message: 'Existed',
  },
  INVALID: {
    error_code: 'INVALID',
    message: 'Invalid',
  },
  INCORRECT: {
    error_code: 'INCORRECT',
    message: 'Incorrect',
  },
  UNAUTHORIZED: {
    error_code: 'UNAUTHORIZED',
    message: 'Unauthorized',
  },
  ACCESS_DENIED: {
    error_code: 'ACCESS_DENIED',
    message: 'Access denied',
  },
  NOT_VERIFIED: {
    error_code: 'NOT_VERIFIED',
    message: 'Not verified',
  },
  EXPIRED: {
    error_code: 'EXPIRED',
    message: 'Expired',
  },
  UNPROCESSABLE: {
    error_code: 'UNPROCESSABLE',
    message: 'Unprocessable',
  },
  PROCESSING: {
    error_code: 'PROCESSING',
    message: 'Processing',
  },
  EXPIRES: {
    error_code: 'EXPIRES',
    message: 'expires',
  },
  NOT_TYPE_TRC20: {
    error_code: 'Tether USDT(TRC20) network',
    message: 'Tether USDT(TRC20) network',
  },
  SEND_EMAIL_FAILED: {
    error_code: 'SEND_EMAIL_FAILED',
    message: 'Send Email is failed',
  },
  SESSION_INVALID: {
    error_code: 'SESSION_INVALID',
    message: 'Session is invalid',
  },
  NOT_TYPE_BSC20: {
    error_code: 'Tether USDT(BSC20) network',
    message: 'Tether USDT(BSC20) network',
  },
  RANGES_INVALID: {
    error_code: 'RANGES_INVALID',
    message: 'From must be smaller than to.',
  },
  RANGES_FROM_TO: {
    error_code: 'RANGES_FROM_TO',
    message:
      'The "from" value of the next element must be equal to the "to" value of the previous element.',
  },
};

export enum BAD_REQUEST_CODE {
  BAD_REQUEST = 'BAD_REQUEST',
  EXISTED = 'EXISTED',
  INVALID = 'INVALID',
  INCORRECT = 'INCORRECT',
  EXPIRED = 'EXPIRED',
}
export enum UNAUTHORIZED_CODE {
  UNAUTHORIZED = 'UNAUTHORIZED',
  EXPIRED = 'EXPIRED',
}
