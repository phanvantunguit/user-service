import { HttpStatus } from '@nestjs/common';
import { ERROR_CODE } from './const';
import {
  IBadRequestError,
  IThrowExceptionOutput,
  IUnauthoriedError,
} from './type';

export function notFoundError(data: string): IThrowExceptionOutput {
  throw {
    status: HttpStatus.NOT_FOUND,
    error_code: ERROR_CODE.NOT_FOUND.error_code,
    message: `${data} ${ERROR_CODE.NOT_FOUND.message}`,
  };
}
export function badRequestError(
  data: string,
  error: IBadRequestError,
): IThrowExceptionOutput {
  throw {
    status: HttpStatus.BAD_REQUEST,
    error_code: error.error_code,
    message: `${data} ${error.message}`,
  };
}
export function unauthorizedError(
  data: string,
  error: IUnauthoriedError,
): IThrowExceptionOutput {
  throw {
    status: HttpStatus.UNAUTHORIZED,
    error_code: error.error_code,
    message: `${data} ${error.message}`,
  };
}
export function accessDeniedError(): IThrowExceptionOutput {
  throw {
    status: HttpStatus.FORBIDDEN,
    ...ERROR_CODE.ACCESS_DENIED,
  };
}
export function serverError(
  data: string,
  error: IBadRequestError,
): IThrowExceptionOutput {
  throw {
    status: HttpStatus.SERVICE_UNAVAILABLE,
    error_code: error.error_code,
    message: `${data} ${error.message}`,
  };
}
