import * as qrcode from 'qrcode';
export async function generateQRCode(data: string): Promise<any> {
  return qrcode.toDataURL(data);
}
