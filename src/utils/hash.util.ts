import * as bcrypt from 'bcrypt';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import { PasswordRegex } from 'src/domain/auth/types';

export function hashPassword(password: string): Promise<string> {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, 10, function (err, hash) {
      if (err) return reject(err);
      return resolve(hash);
    });
  });
}
export function comparePassword(
  password: string,
  hash: string,
): Promise<boolean> {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, function (err, res) {
      if (err) {
        return reject(err);
      } else {
        return resolve(res);
      }
    });
  });
}
export function generateToken(
  data: any,
  private_key: string,
  expires_in: number,
): string {
  return jwt.sign(data, private_key, {
    expiresIn: expires_in,
    algorithm: 'RS256',
  });
}
export function verifyToken(token: string, public_key: string): Promise<any> {
  return new Promise((resolve, reject) => {
    jwt.verify(token, public_key, function (err, decoded) {
      if (err) return reject(err);
      return resolve(decoded);
    });
  });
}
export function generateTokenSecret(
  data: any,
  secret_key: string,
  expires_in: number,
): string {
  return jwt.sign(data, secret_key, {
    expiresIn: expires_in,
  });
}
export function verifyTokenSecret(
  token: string,
  secret_key: string,
): Promise<any> {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret_key, function (err, decoded) {
      if (err) return reject(err);
      return resolve(decoded);
    });
  });
}
export function decodeBase64(encrypt_data: string): string {
  return Buffer.from(encrypt_data, 'base64').toString('utf8');
}
export function encodeBase64(decrypt_data: string): string {
  return Buffer.from(decrypt_data).toString('base64');
}
export function createChecksum(data: string, secret_key: string): string {
  const hash = crypto
    .createHash('sha256')
    .update(data + secret_key)
    .digest('hex');
  return hash;
}
export function validateChecksum(
  data: string,
  checksum: string,
  secret_key: string,
): boolean {
  const hash = crypto
    .createHash('sha256')
    .update(data + secret_key)
    .digest('hex');
  return hash === checksum;
}
function create3DESKey(key) {
  let hashKey = crypto.createHash('md5').update(key).digest();
  hashKey = Buffer.concat([hashKey, hashKey.slice(0, 8)]);
  return hashKey;
}
export function tripleDESEncrypt(
  data: string,
  secret_key: string,
  output_type: 'base64' | 'hex',
) {
  try {
    const threeDESKey = create3DESKey(secret_key);
    const cipher = crypto.createCipheriv('des-ede3', threeDESKey, '');
    const encrypted = cipher.update(data, 'utf8', output_type);
    return encrypted + cipher.final(output_type);
  } catch (error) {
    return false;
  }
}

export function tripleDESDecrypt(
  data: string,
  secret_key: string,
  input_type: 'base64' | 'hex',
) {
  try {
    const threeDESKey = create3DESKey(secret_key);
    const decipher = crypto.createDecipheriv('des-ede3', threeDESKey, '');
    const decrypted = decipher.update(data, input_type);
    return decrypted.toString() + decipher.final().toString();
  } catch (error) {
    return false;
  }
}

export function createChecksumSHA512HMAC(
  data: string,
  key: string,
  output: 'hex' | 'base64',
) {
  const hmac = crypto.createHmac('sha512', key);
  const signed = hmac.update(data).digest(output);
  return signed;
}

export function generatePassword() {
  const password =
    Math.random().toString(36).substring(2, 7) +
    Math.random().toString(36).substring(2, 7).toUpperCase();
  const test = new RegExp(PasswordRegex).test(password);
  if (test) {
    return password;
  }
  return generatePassword();
}
