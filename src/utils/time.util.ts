export const SECOND = 1000;
export const MINUTE = 60 * SECOND;
export const HOUR = 60 * MINUTE;
export const DAY = 24 * HOUR;

export function setBeginDate(time = Date.now()): number {
  const timeDefault = new Date(Number(time));
  timeDefault.setHours(0);
  timeDefault.setMinutes(0);
  timeDefault.setSeconds(0);
  timeDefault.setMilliseconds(0);
  return timeDefault.valueOf();
}
export function setEndDate(time = Date.now()): number {
  const timeDefault = new Date(Number(time));
  timeDefault.setHours(24);
  timeDefault.setMinutes(0);
  timeDefault.setSeconds(0);
  timeDefault.setMilliseconds(0);
  return timeDefault.valueOf();
}
