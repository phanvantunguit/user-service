import { PasswordRegex } from 'src/const/user';

export function cleanObject(obj: object) {
  const newObj = {};
  for (const key in obj) {
    if (obj[key]) {
      newObj[key] = obj[key];
    }
  }
  return newObj;
}
export function generatePassword() {
  const password =
    Math.random().toString(36).substring(2, 7) +
    Math.random().toString(36).substring(2, 7).toUpperCase();
  const test = new RegExp(PasswordRegex).test(password);
  if (test) {
    return password;
  }
  return generatePassword();
}
export function formatRequestDataToString(params: object): string {
  return Object.keys(params)
    .map((key) => `${key}=${params[key]}`)
    .join('&');
}
export function formatRequestDataToStringV2(params: object): string {
  try {
    return Object.keys(params)
      .map((key) => {
        if (typeof params[key] === 'object') {
          return `${key}=${JSON.stringify(params[key])}`;
        } else {
          return `${key}=${params[key]}`;
        }
      })
      .join('&');
  } catch (error) {
    console.log('error', error);
  }
}
export function formatQueryArray(params: object) {
  return Object.keys(params).map((key) => {
    if (typeof params[key] === 'string') {
      return `${key}='${params[key]}'`;
    } else {
      return `${key}=${params[key]}`;
    }
  });
}
