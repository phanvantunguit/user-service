import { hashPassword } from 'src/utils/hash.util';
import { MigrationInterface, QueryRunner } from 'typeorm';
export class InsertAdmin1664523922143 implements MigrationInterface {
  name: 'InsertAdmin1664523922143';

  async up(queryRunner: QueryRunner): Promise<any> {
    const password = await hashPassword('123456Aa');
    const queryString = `
    INSERT INTO admins
    (email, phone, fullname, super_admin, email_confirmed, "password", created_at, updated_at, deleted_at)
    VALUES
    ('nguyentoan@coinmap.tech', null, 'Nguyen Toan', true, true, '${password}', 1664519091464, 1664519091464, null),
    ('trongnguyen@coinmap.tech', null, 'Nguyen Trong', true, true, '${password}', 1664519091465, 1664519091465, null);`;
    await queryRunner.query(queryString);
  }
  down(queryRunner: QueryRunner): Promise<any> {
    return;
  }
}
