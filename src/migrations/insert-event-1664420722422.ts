import { EVENT_STATUS } from 'src/domain/event/types';
import { MigrationInterface, QueryRunner } from 'typeorm';
export class InsertEvent1664420722422 implements MigrationInterface {
  name: 'InsertEvent1664420722422';
  async up(queryRunner: QueryRunner): Promise<any> {
    const queryString = `
        INSERT INTO events ("name", code, status, start_at, finish_at, created_at, updated_at, deleted_at, attendees_number)
            VALUES ('Coinmap vs Axi', 'coinmapaxi', '${
              EVENT_STATUS.OPEN_REGISTRATION
            }', 1665766800000, null, ${Date.now()},${Date.now()}, null, 1000)
        `;
    await queryRunner.query(queryString);
  }
  down(queryRunner: QueryRunner): Promise<any> {
    return;
  }
}
