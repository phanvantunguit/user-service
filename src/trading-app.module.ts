import { Module } from '@nestjs/common';
import * as controllers from './adapter/controllers';
import * as usercases from './application/usecase';
import { DatabaseModule } from './infrastructure/data/database';
import * as services from './infrastructure/services';
@Module({
  imports: [DatabaseModule],
  controllers: [...Object.values(controllers)],
  providers: [...Object.values(usercases), ...Object.values(services)],
})
export class TradingAppModule {}
