import { Module } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';
import {
  CreateMerchantInvoiceHandler,
  RemindAllAttendEventHandler,
  RemindAttendEventHandler,
  RemindDynamicHandler,
} from './application';
import { BackgroudJob } from './backgroud-jobs';
import { EventJob } from './backgroud-jobs/event';
import { MerchantJob } from './backgroud-jobs/merchant';
import { DatabaseModule } from './infrastructure/data/database';
import { MailService, SendGridTransport } from './infrastructure/services';

@Module({
  imports: [DatabaseModule],
  providers: [
    BackgroudJob,
    EventJob,
    RemindAllAttendEventHandler,
    RemindAttendEventHandler,
    MailService,
    SendGridTransport,
    SchedulerRegistry,
    RemindDynamicHandler,
    MerchantJob,
    CreateMerchantInvoiceHandler,
  ],
})
export class BackgroudAppModule {}
